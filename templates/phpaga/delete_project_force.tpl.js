{literal}
<script type="text/javascript">
if (window.confirm('{/literal}{t}This project has dependencies (i.e. operations, project memberships, expenses) or is the parent of one or more other projects. If you delete this project, all depending records or project relationships will be deleted as well. Related projects and their records will not be deleted. Are you sure you want to delete this project and all of its dependencies?{/t}{literal}')) {
        location.href = 'project.php?id={/literal}{$prj_id}&action={$smarty.const.ACTION_PRJ_REMOVE_FORCE}{literal}';
}
</script>
{/literal}
