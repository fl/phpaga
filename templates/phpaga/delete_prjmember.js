{literal}
<script type="text/javascript">

function del_prjmember(prj_id, pe_id, pe_name)
{
    if (window.confirm(pe_name+': {/literal}{t}Are you sure you want to delete this member from the project?{/t}{literal}')) {
        location.href = 'project.php?id={/literal}'+prj_id+'&pe_id='+pe_id+'&action={$smarty.const.ACTION_PRJ_MEMBER_REMOVE}{literal}';
    }
}

</script>
{/literal}
