{literal}
<script type="text/javascript">
$(document).ready(function() {
        $("#dlg-del-confirm").hide();

        $(".filedel").each(function() {
                var el_id = $(this).attr('id');
                var file_id = $(this).attr('id').replace(/^[^_]+__/, '');
                $('#'+el_id+' .del_item').live('click', function() { 
                    $("#dlg-del-confirm").dialog({
			resizable: false,
			modal: true,
			buttons: {
                                OK: function() {
                                    var tmpdlg = $(this);
                                    $.ajax({
                                        url: 'service.php',
                                        type: 'POST',
                                        data: 'saction=39&id='+file_id,
                                        dataType: 'json',
                                        cache: false,
                                        success: function(result) {
                                            if (result.status != 0) {
                                                alert(result.message);
                                            } else {
                                                tmpdlg.dialog("close");
                                                $("#"+el_id).fadeOut();
                                            }
                                        }
                                    });

				},
				Cancel: function() { $(this).dialog("close"); }
			}
                    });
                });
        });
    });

</script>
{/literal}

<div id="dlg-del-confirm" title="{t}Remove file?{/t}">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
    {t}This file will be permanently removed. Are you sure?{/t}</p>
</div>
