Translations
============

While phpaga comes with support for various languages, not all of the language
catalogs are fully translated. If you would like to help with a specific
language, or add support for a new language, head over to `phpaga's project
page at Transifex <http://www.transifex.net/projects/p/phpaga/>`_, or more
specifically, to the `resource page
<http://www.transifex.net/projects/p/phpaga/resource/messagespo/>`_. 

Send a notification via Transifex or drop me a line at florian at phpaga dot
net if you would like to take care of a translation.

