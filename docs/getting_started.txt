
Getting Started Guide
=====================

*Sean Hull*
*$Date: 2005-07-10 15:57:50 +0200 (Sun, 10 Jul 2005) $*

(**Note**: *This guide has been written by Sean Hull in 2005. Some things have
changed since then, but the core information still applies. Florian Lanthaler*)

Introduction
------------

phpaga is one of those Open Source projects that really fits a niche.
Are you an independent contractor, sole proprietor, freelancer, or
perhaps you run a small business? Then surely you need a system to
handle your billing. phpaga not only fits the need, it will do much
more and surprise you with information about your business that you
may not have paid attention to.

Presumably if you're already here reading these docs you're already
*SOLD* on the `LAMP or
LAPP framework <http://en.wikipedia.org/wiki/LAMP_(software_bundle)>`_. If not
I'll say a word or two about that. If you're using a desktop application to do
your finances you have to worry about licensing and upgrades, bugs & fixes.
What's more you're confined to the desktop, can't share the information,
accross the city when you're at various client sites, or across the world if
you're sharing the information, exchanging ideas with colleagues or have
business partners in other locales. A web-based application moves the data out
of your office and into the datacenter, where it can easily be part of your
regular backup routine or hosted by a hosting provider who upgrades the OS,
monitors security, and provides backups and so on.

Features
--------

A picture or in this case a website is worth a thousand words. So before I dig
into the features of phpaga, I'd like you to take a minute and check out `the
live demo <http://live.phpaga.net>`_ (**Note**: *the demo is no longer online*) with
user:demo pass:demo. It already has lots of companies, projects, invoices,
quotations, and resources so you can see all the graphs and reports in action.

phpaga is a billing application that can keep track of your personal or small
business finances, and is built on the `LAMP or LAPP framework
<http://en.wikipedia.org/wiki/LAMP_(software_bundle)>`_. What else can we say.
Well it is international, handling billing for Australia, Canada, Germany,
Italy, and the USA.  And that's just with the builtin plugins. If you don't
find what you need, you can write your own plugin quite easily. It can handle
invoices based on hourly work, or named items with a cost. It can handle
projects with multiple resources each with different hourly rates. And if you
use those folks on another project, they can have different rates on those
projects too. It can produce spectacular PDF quotations and invoices for your
clients at week-end, month-end, or however you like. Furthermore it has
extensive reports such as paid and unpaid invoices (turnover), summary reports
for people, projects, and categories. The main page or dashboard also includes
a nice 52 week graph of weekly manhours work. All this information and
presentation helps you begin to get a bigger picture of your business, and
finances to help you run your business better.

* Major Features

  - custom user logins
  - persons (project resources) without logins
  - default and per-project customizable hourly billing rates
  - company (client) can have more than one project
  - projects + subprojects

* Reports

  - weekly manhours reports
  - paid/unpaid invoices
  - project timeline
  - summary by persons
  - summary by project
  - summary by task
  - summary by customer

* Internationalization

  - international language support
  - international currency support
  - billing plugins for Australia, Canada, Germany, Italy & USA

* Invoices

  - hour based line items at billable rate
  - task based line items at fixed cost
  - printable/emailable PDF for client
  - enable/disable letterhead, tasklist, timesheet

* Quotations

  - customizable line items

* Miscellaneous

  - customizable company, person, project, and project status categories
  - customizable color scheme
  - default hourly billing rates
  - customizable hourly billing rates by project

Quickstart
----------

A quick start is what everyone wants don't they. I know when I install
a new piece of open source software, I quickly jump to this section,
skim through the steps, and wing it. If I can't get it to work I go
back to that section, and only if I absolutely have to do I touch the
real docs. So, here goes.

The first thing you probably want to do when you test out this system
is to generate a PDF invoice, right? Ok, here's what you do. After
installation, you'll have an "admin" account, so login with that.

* Create a client - the recipient of your invoice

  - click on the "Companies" link (MAIN section)
  - click the "add company" link
  - fill in all the information and click "submit"
  - follow these steps a-c to create your parent company as that is
    disassociated from the user you login as

* Create a project

  - click on the "Projects" link (MAIN section)
  - click "Add Project" link in the "Selected projects" section
  - fill in the information, be sure to select the "Customer"
    who is the recipient, and "Billing Company" who is the owner
    of the project

* Create a person

  - click on the "Persons" link (MAIN section)
  - click on the "Add Person" link in the "Persons" section
  - fill in the info and select a company
  - default hourly rate can be changed for each project this
    person works on
  - Don't jump to the project resource section yet, this person won't
    show up.  First you have to create a user.

* Create a user

  - click on the "Users" link (ADMINISTRATION section)
  - click "Add user" under the Users section
  - enter the login, and select the "Person" you created in step 3

* Add project members and tasks

  - click "Projects" link (MAIN section)
  - under "Selected projects" click your project name
  - click the "Add Project Member" link
  - select a Person from the popup and their default job category and
    default hourly wage will be filled in automatically!  Of course if
    they are different for this project, you may change them.
  - click "submit" when you're done
  - when the popup window for "Add Project Member" closes click "Add Task"
    on the "Project: your-project-name" page.
  - select the "Project member"
  - enter the start/end date or use the popup calendar to select them.
    The first + last day of the month make a lot of sense here.
  - Fill in the duration as total hours worked.
  - enter a description of what that resource did this month

* Review project page

  - click the project name under "Project information" section
  - you'll see some changes there, and the graphs and details will
    include that new task you've added

* Create your invoice

  - click on the "Invoices" link (FINANCE section)
  - click "Add invoice with tasks" under "Invoices" section
  - if these are your first invoices and tasks it won't be obvious,
    but at this point it's worth noting that only *unassigned* tasks
    which don't appear on another invoice will show up.
  - click the "Create Invoice" link
  - select or deselect tasks as appropriate, or add custom line items
  - click recalculate if you've removed items
  - when you're done, click "Create line items for associated tasks"
    button
  - finally if all is ok click "Create Invoice" button
  - note a *Notice* box may be displayed at the top if you don't select
    a recipient company
  - enter a custom invoice number if necessary
  - the resulting window will be your invoice in HTML format
  - click "Export PDF" + select options
  - select save-as or print from your Acrobat client

Understanding the workflow
--------------------------

Users vs Persons
~~~~~~~~~~~~~~~~

The separation of users and persons revolves around the idea of
those who can login to phpaga (users) and those who are just in the
contact database (persons). At this point in time a user can exist,
that is a login to phpaga, with associated permissions to use and
administrate the system, *without* being part of any project or
company. What this means is that you can use phpaga as your contact
database, but all of those Persons won't show up in a resource list
when you are creating or modifying a project. Only if they have a
User defined will they show up in such a popup list.

Permissions for a user, and how they can use the phpaga system must be
modified *after* that user is created. Click the "Users" link in
ADMINISTRATION section and you'll see a "Users" table. (You can also
search of course) If you click the "Login" column, you'll get user
details and all of the permissions checkboxes. If you click the "Name"
column, you'll get the user's profile (same as you do when you are
logged in as yourself, and click the "Profile" link under your named
section. All of the various permissions are available here, allowing
the administrator to control what other phpaga users can do.

Companies
~~~~~~~~~

As explained in the quick start section above, a company is any
financial entity that needs to be represented in phpaga, whether it is
the *billing* company, that is your company that is using phpaga, or
your clients - recipient companies.

Projects
~~~~~~~~

Projects are associated with companies so you must have the *OWNER*
company created before you create a project. That company can have 0
or more projects associated with it. However, and here's where it gets
tricky, but very FLEXIBLE in ways you may not need now, but could need
later. The billing company must also exist when you create a project.

For example, suppose your 5-man consulting enterprise wants to use
phpaga to manage it's business invoicing. You have three projects, but
you decide to break the company into Windows-arm, and Linux-arm. Two
of your new projects are on Linux, and one is on Windows, for GE
Capital. You create project A and select the customer as "GE Capital"
and the Billing company as "Linux-arm". For project B it's the same
thing. However for project C you select the Billing company as
"Windows-arm".

For each project you can select a parent project. This allows phpaga to keep
track of and visualize relationships between projects. If you go to
`live.phpaga.net <http://live.phpaga.net/>`_ (**Note**: *the demo is no longer
online*) and log in (with "demo" and "demo") and then go to the details page
for the project "phpaga parent", you can see a relationship graph under the
section "relations". This graph is created on the fly with the Graphviz
package. Try clicking on one of the boxes representing a related project and
you this project's page will load. The currently selected project will always
show in green, whereas the other projects are shown in grey.

Creating Quotations
~~~~~~~~~~~~~~~~~~~

Tasks + Hours
~~~~~~~~~~~~~

As you can see in the section above, you can enter tasks at any time
while using phpaga. At the time you record a task, you can set a
duration, or a start and stop time, plus a description. At that time
you specify the project to which the task belongs, but you *DON'T*
specify the invoice. Again, seemingly complex but you will find that
it is really FLEXIBILITY in disguise. You may have worked some hours
in May but you want, or the client asked you to bill them on your June
invoice. That's no problem.

When you add tasks they go into a pool of unassigned hours. Under the
FINANCE section you can click "Unbilled hours" at any time to see the
list. Of course when you go to create an invoice, and assign those
tasks, they will no longer be included in this list.

Creating Invoices
~~~~~~~~~~~~~~~~~

This should be your favorite part of using phpaga, and your favorite
part of running your business! When you go to create an invoice you
can do it with description/price line items or with tasks. Your
choice. You may have to bill your client for equipment only one month,
for that you would simple "Add invoice" not "Add invoice with Tasks".

Enter Invoice Paid Information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When you get a check from a client, you want to let phpaga know about
it. Click on "Invoices" under the FINANCE section, and click the
invoice from the list, or do a search. The HTML detail page for the
invoice will display, with a "Set Payment Date". Click the calendar
button and select the date it was paid. This will update various
graphs, and reports in phpaga, letting you know where your finances
are.

Understanding the reports
-------------------------

Main report - weekly manhours
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By clicking "Main" under the MAIN section you can see two manhours
breakdowns, one for the year, and one by day for the current month.

Finance report
~~~~~~~~~~~~~~

This report is a summary of billed and collected money for your
business. When a check comes in from one of the invoices you've sent
to a client, you record the date it was paid (see 7. Enter Invoice
Paid Information above).

Summary reports
~~~~~~~~~~~~~~~

Under the ADMINISTRATION section you see a link for "Summary report".
There are actually *FOUR* great reports hidden under this little link.
One provides a projects breakdown, by hours per project. The next one
provides breakdowns by persons or resources who worked on all
projects, and how much they contributed to the total. Next is a
category breakdown pie chart, which as described in the user profile
section below, if you have people who work as managers, programming
and development, consulting, sales, marketing, human relations, and so
on, this will break up each of their tasks so you can see where the
bulk of billable time went. The last one is the turnover graph.

Project timeline
~~~~~~~~~~~~~~~~

(**deprecated** - *this feature has been removed*)

There is another report hidden away in this application which is quite
useful, and you'll be glad when you find it. Click on "Projects", and
under "Selected projects" click the "Timeline" link next to "Add
project". Under the project column you'll see each project you're
working on, and based on the estimated start and end times, the
timeline will display bars so you can see which projects overlap, and
thus when thinks might be more rocky, or conversely when you have time
for some new work.

User profile report
~~~~~~~~~~~~~~~~~~~

In this report you'll see three sections. Manhours is the same detail
as seen in the main dashboard report, but just for you. It is a
breakdown of the number of hours worked per week on a yearly scale.
Very useful for viewing your business at a glance. The Task graph is
the next section you will see and it is broken up nicely into type of
work you do and how much time you spend. If you wear a lot of
different hats, for instance business manager, DBA, Unix
Administrator, marketer, sales-drone, and so on, you can see the
breakdown easily.

Settings and customizing
------------------------

Except for the user settings below, all of these settings are under
the link "Sitewide settings" in the ADMINISTRATION section.

Graphs
~~~~~~

JpGraph is the PHP module you installed to provide graphs for phpaga.
You can set the width and height for the various graphs, change the
work-week to 35 hours for France, and 70 hours for New York!! Also
you can changes settings for the `Gantt Chart <http://en.wikipedia.org/wiki/Gantt_chart>`_
showing financial summary of paid and unpaid invoices.

(**Note**: *Charts are now generated via a JavaScript library, JpGraph is no
longer required.*)

Invoice Letterhead Formatting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Letterhead section includes all the fields you will need to
control how the PDF will format. It may make sense to put the street
address in the "Location" field, and the "New York, NY 10003" entry in
the "Street address" field just because of the order they show up on
the PDF. Experiment until you like the output. In the US if you're a
C-Corp you'll fill out Company tax number, but leave personal tax
number blank, and vice-versa if you're a sole proprietorship. Other
countries regulations are different of course, so take that into
consideration. The footer line can be left blank, or filled in as
appropriate for your needs, as can the logo file.

PDF Settings
~~~~~~~~~~~~

In this section there are some general PDF settings for invoices,
quotations, and the "Print" button you'll see in various places
throughout phpaga. They are fairly self-explanatory, and direct how
the PDF will be generated so that Acrobat can do it's magic.

Misc. Settings
~~~~~~~~~~~~~~

Categories, Currencies + Countries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just declared your own soverign state, you can add it into phpaga!
Want to add another currency besides the included USD and Euro
abbreviations, go for it. The categories edit is probably the one
you'll use most often. Types of companies, types of tasks in a
project, types of jobs or positions held within a company, categories
for resources, and even project statuses can all be configured.

Themes
~~~~~~

In the layout section phpaga will list the CSS stylesheets that it
finds in your install directory
:file:`htdocs/styles/*.css` so if you want a new one,
copy one of those files, rename it, and edit the settings inside
there. See `Wikipedia <http://en.wikipedia.org/wiki/Cascading_style_sheets>`_
for details on CSS.

User settings
~~~~~~~~~~~~~

Under your name section, the "Settings" link jumps to a page allowing
you to change your user login information, and your permissions.

Other Layout settings
~~~~~~~~~~~~~~~~~~~~~

Do you like to use '.' to separate numbers and decimals, or do you
prefer ','? You can configure that here. Like to have your
month/day/year date layout, no problem. Set the sitewide date format
as well. All sorts of other settings can be configured here, such as
how many items to display for tasks, projects, invoices, and so on.

Miscellaneous items
-------------------

Forgotten Password
~~~~~~~~~~~~~~~~~~

If you forget your password, you have to get your SQL thinking cap on
and change it. If you don't recall the name of your database, do: ::

    $ md5sum.textutils --string oldpassword
    d5b5fffc89f961903fb3c9a173f1b667  "oldpassword"
    $ mysqlshow

Then login as follows: ::

    $ mysql phpaga
    mysql> select usr_id, usr_login, usr_passwd from users;
    +--------+-----------+----------------------------------+
    | usr_id | usr_login | usr_passwd                       |
    +--------+-----------+----------------------------------+
    |      2 | sean9     | 4f2a1493c661c0f2d2ee9a37040b8082 |
    |      3 | neal9     | 3e7023ed317ed603851f22d510924ca1 |
    |      4 | akahn     | b27fad92c6ddeddf0bfd6eb9871a8c79 |
    +--------+-----------+----------------------------------+
    3 rows in set (0.00 sec)
    mysql> update users
    set usr_passwd = 'd5b5fffc89f961903fb3c9a173f1b667'
    where usr_id = 4;
    Query OK, 1 row affected (0.00 sec)
    Rows matched: 1  Changed: 1  Warnings: 0

Note that if you don't have :command:`md5sum.textutils` installed you can also
use this bit of php code to get the hash string: ::

    <?php print md5("oldpassword")."\n"; ?>

Using billing plugins
~~~~~~~~~~~~~~~~~~~~~

There is a howto on creating billing plugins in the :file:`docs/` directory.
Read that for details. You'll basically put a new php file into
:file:`plugins/billing/` and then select it in your "Sitewide Settings" so it
will be the default. You can copy one of the existing ones in that directory,
and edit it as appropriate.

Customizing templates
~~~~~~~~~~~~~~~~~~~~~

Templates are found in the :file:`templates/` directory.
You select one in "Sitewide Settings".

The customized template sets are an option, if someone has particular
layout needs.

The theme (.css) should be enough to suit most people's needs. In case you need
a rather exotic layout, say you don't need certain information or you want the
layout to be readable on a handheld device over a slow network link, you can
create your own template set. Simply copy the whole :file:`templates/phpaga/`
directory to a new name, say :file:`templates/mytheme/`, and edit the files at
will. You can, for example, leave out all the graphs and optimize the layout
for 200x160 pixel screens.

In general, it is not recommended that users create their own template set
unless they really want to keep up with development. Whenever we introduce a
new variable (or remove an existing one) in one of the template files, this
change would need to be applied to the customized template set too. If you do
venture into this area, be sure to contact us here at phpaga, and contribute
those changes for everyone to benefit from. That's what open source is all
about after all.

Customizing themes
~~~~~~~~~~~~~~~~~~

Themes are found in :file:`htdocs/styles/` so copy one of
those css files to rename the file, and then edit the contents for
your needs. `Wikipedia
CSS Info <http://en.wikipedia.org/wiki/Cascading_style_sheets>`_
