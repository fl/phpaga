<?php
/**
 * phpaga
 *
 * This file contains the various rights/permissions/actions a user can
 * have/perform.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2004 Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define("PHPAGA_PERM_VIEW_FINANCE", 1);
$phpaga_perm[PHPAGA_PERM_VIEW_FINANCE] = _("View financial information");

define("PHPAGA_PERM_VIEW_INVOICES", 2);
$phpaga_perm[PHPAGA_PERM_VIEW_INVOICES] = _("View invoices");

define("PHPAGA_PERM_VIEW_QUOTATIONS", 3);
$phpaga_perm[PHPAGA_PERM_VIEW_QUOTATIONS] = _("View quotations");

define("PHPAGA_PERM_MANAGE_INVOICES", 4);
$phpaga_perm[PHPAGA_PERM_MANAGE_INVOICES] = _("Manage invoices");

define("PHPAGA_PERM_MANAGE_QUOTATIONS", 5);
$phpaga_perm[PHPAGA_PERM_MANAGE_QUOTATIONS] = _("Manage quotations");

define("PHPAGA_PERM_VIEW_OTHERPROJECTS", 6);
$phpaga_perm[PHPAGA_PERM_VIEW_OTHERPROJECTS] = _("View projects without being a project member");

define("PHPAGA_PERM_MANAGE_PROJECTS", 7);
$phpaga_perm[PHPAGA_PERM_MANAGE_PROJECTS] = _("Manage projects");

define("PHPAGA_PERM_MANAGE_OTHERPROJECTS", 8);
$phpaga_perm[PHPAGA_PERM_MANAGE_OTHERPROJECTS] = _("Manage projects created by other persons");

/* disabled - everybody should be able to create companies and edit
 * his own records
define("PHPAGA_PERM_MANAGE_COMPANIES", 9);
$phpaga_perm[PHPAGA_PERM_MANAGE_COMPANIES] = _("Manage companies");
*/

define("PHPAGA_PERM_MANAGE_OTHERCOMPANIES", 10);
$phpaga_perm[PHPAGA_PERM_MANAGE_OTHERCOMPANIES] = _("Manage companies created by other persons");

/* disabled - everybody should be able to create persons and edit his
 * own records
define("PHPAGA_PERM_MANAGE_PERSONS", 11);
$phpaga_perm[PHPAGA_PERM_MANAGE_PERSONS] = _("Manage persons");
*/

define("PHPAGA_PERM_MANAGE_OTHERPERSONS", 12);
$phpaga_perm[PHPAGA_PERM_MANAGE_OTHERPERSONS] = _("Manage persons created by other persons");

define("PHPAGA_PERM_VIEW_OTHERMEMTASKS", 13);
$phpaga_perm[PHPAGA_PERM_VIEW_OTHERMEMTASKS] = _("View tasks filed by other project members");

define("PHPAGA_PERM_VIEW_ALLTASKS", 14);
$phpaga_perm[PHPAGA_PERM_VIEW_ALLTASKS] = _("View all tasks");

define("PHPAGA_PERM_MANAGE_ALLTASKS", 15);
$phpaga_perm[PHPAGA_PERM_MANAGE_ALLTASKS] = _("View and manage all tasks");

define("PHPAGA_PERM_MANAGE_SYSSETTINGS", 16);
$phpaga_perm[PHPAGA_PERM_MANAGE_SYSSETTINGS] = _("Manage system-wide settings, core data, and users");

?>