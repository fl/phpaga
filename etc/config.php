<?php
/**
 * phpaga
 *
 * This is phpaga's internal configuration file. There should be no
 * need to modify anything in here. All local configuration settings
 * are done in config.local.php
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, 2003 Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'config.local.php';

/* Let PHP handle the temporary directory if it is not specified
 * already */
if (!defined('PHPAGA_TMPDIR'))
    define('PHPAGA_TMPDIR', sys_get_temp_dir());

define('PHPAGA_ETCPATH', PHPAGA_BASEPATH.'etc/');
define('PHPAGA_EXTPATH', PHPAGA_BASEPATH.'ext/');
define('PHPAGA_LIBPATH', PHPAGA_BASEPATH.'lib/');
define('PHPAGA_LNGPATH', PHPAGA_BASEPATH.'locale/');
define('PHPAGA_FILEPATH', PHPAGA_BASEPATH.'files/');
define('PHPAGA_PLUGINPATH', PHPAGA_BASEPATH.'plugins/');
define('PHPAGA_BASETPLPATH', PHPAGA_BASEPATH.'templates/');

if (!defined('PHPAGA_ERR_VERBOSE'))
    define('PHPAGA_ERR_VERBOSE', false);

#if (!defined('PHPAGA_API_BASE_URI'))
#    define('PHPAGA_API_BASE_URI', '/api.php');

/* Compress output if the option is not disabled and the browser
 * supports gzip encoding */
if ((!defined('PHPAGA_COMPRESSOUT_DISABLED') || !PHPAGA_COMPRESSOUT_DISABLED)
    && isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start('gzencode');
    header('Content-Encoding: gzip');
}

/* Set error reporting */
if (defined('PHPAGA_ERR_VERBOSE') && (PHPAGA_ERR_VERBOSE == true))
    error_reporting(E_ALL);

/* DSN */
define('PHPAGA_DSN', sprintf("%s://%s:%s@%s/%s",
                             PHPAGA_DB_SYSTEM,
                             PHPAGA_DB_LOGIN, PHPAGA_DB_PASSWD,
                             PHPAGA_DB_HOST, PHPAGA_DB_NAME));

function phpaga_autoload($class) {
    $flourish_file = PHPAGA_EXTPATH.'flourish/'.$class.'.php';
    if (file_exists($flourish_file)) {
        return require $flourish_file;
    }
    $file = PHPAGA_BASEPATH.'/classes/'.$class.'.php';
    if (file_exists($file)) {
        return require $file;
    }
}

spl_autoload_register('phpaga_autoload');

fTimestamp::defineFormat('date', 'Y-m-d');
fCore::enableExceptionHandling('html');
fORMJSON::extend();
PhPagaOrmDb::init();

include_once PHPAGA_ETCPATH.'globals.php';
include_once PHPAGA_LIBPATH.'misc.php';
include_once PHPAGA_ETCPATH.'errors.php';

/* Remove slashes from request paramters if magic_quotes_gpc is On. */
if (get_magic_quotes_gpc() == 1) {
    $_REQUEST = phpaga_stripslashesArray($_REQUEST);
    $_GET = phpaga_stripslashesArray($_GET);
    $_POST = phpaga_stripslashesArray($_POST);
}

/* Use this array to read request parameters */
$REQUEST_DATA = array_merge($_GET, $_POST);

/* Set default timezone */
if(function_exists('date_default_timezone_set') and function_exists('date_default_timezone_get'))
    @date_default_timezone_set(@date_default_timezone_get());

/* During normal usage, take care of the session (and load user
 * settings from there); during installation just read the default
 * configuration settings */
if ((!defined('PHPAGA_INSTALL') || !PHPAGA_INSTALL) && (!defined('PHPAGA_FROMCRON') || !PHPAGA_FROMCRON))
    include_once PHPAGA_LIBPATH.'sessions.php';
else
    PConfig::initialize();

if (defined('PHPAGA_INSTALL') && PHPAGA_INSTALL) {
    if (!defined('PHPAGA_TEMPLATESET'))
        define('PHPAGA_TEMPLATESET', 'phpaga');

    if (!defined('PHPAGA_DEFAULT_SKIN'))
        define('PHPAGA_DEFAULT_SKIN', 'phpaga');
}

/* Uploads are enabled unless defined otherwise in config.local.php */
if (!defined('PHPAGA_UPLOADS_DISABLED'))
    define('PHPAGA_UPLOADS_DISABLED', false);

/* Include internal secondary libraries */
#include_once PHPAGA_LIBPATH.'billing_details.php';
include_once PHPAGA_LIBPATH.'bills.php';
include_once PHPAGA_LIBPATH.'finances.php';
include_once PHPAGA_LIBPATH.'materials.php';
include_once PHPAGA_LIBPATH.'operations.php';
include_once PHPAGA_LIBPATH.'products.php';
include_once PHPAGA_LIBPATH.'projects.php';
include_once PHPAGA_LIBPATH.'quotations.php';

include_once PHPAGA_ETCPATH.'permissions.php';

PhPagaLanguage::init();

define('PHPAGA_TPLPATH', PHPAGA_BASETPLPATH.PHPAGA_TEMPLATESET.'/');

if (!defined('PHPAGA_TCPDF_DIR'))
    define('PHPAGA_TCPDF_DIR', PHPAGA_EXTPATH.'tcpdf/');

/* File thumbnails */
if (!defined('PHPAGA_FILES_PREVIEW'))
    define('PHPAGA_FILES_PREVIEW', true);

if (!defined('PHPAGA_BIN_CONVERT'))
    define('PHPAGA_BIN_CONVERT', '/usr/bin/convert');

if (!defined('PHPAGA_BIN_DOT'))
    define('PHPAGA_BIN_DOT', 'dot');

define('PHPAGA_SELF', basename($_SERVER['PHP_SELF']));

if (defined('PHPAGA_VERSION_DB'))
    define('PHPAGA_DBUPGRADE_NECESSARY', ((PHPAGA_VERSION_DB < $phpaga_default_settings['VERSION_DB']) && (PHPAGA_SELF!='dbupgrade.php'))); 
else
    define('PHPAGA_DBUPGRADE_NECESSARY', false);

?>