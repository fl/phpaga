<?php
/**
 * phpaga
 *
 * Error
 *
 * This file contains error codes and messages.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define("PHPAGA_ERR_FILE_READ", 3);
define("PHPAGA_ERR_FILE_WRITE", 4);
define("PHPAGA_ERR_FILE_CREATE", 5);
define("PHPAGA_ERR_FILE_DELETE", 6);
define("PHPAGA_ERR_DB_EXEC_QUERY", 13);
define("PHPAGA_ERR_USER_INVALIDID", 14);
define("PHPAGA_ERR_USER_FIND", 15);
define("PHPAGA_ERR_AUTH_LOGINLENGTH", 31);
define("PHPAGA_ERR_AUTH_PASSWDLENGTH", 32);
define("PHPAGA_ERR_DB_DUPLICATE", 36);
define("PHPAGA_ERR_USER_STAT", 43);
define("PHPAGA_ERR_PROJECT_INVALIDID", 44);
define("PHPAGA_ERR_INSERT_DATA_INVALIDFORMAT", 45);
define("PHPAGA_ERR_OPCAT_INVALIDID", 46);
define("PHPAGA_ERR_PERSON_INVALIDID", 47);
define("PHPAGA_ERR_OPERATION_INVALIDID", 48);
define("PHPAGA_ERR_PRJSTAT_INVALIDID", 49);
define("PHPAGA_ERR_COMPANY_INVALIDID", 50);
define("PHPAGA_ERR_COUNTRY_INVALIDID", 51);
define("PHPAGA_ERR_CCAT_INVALIDID", 52);
define("PHPAGA_ERR_PECAT_INVALIDID", 53);
define("PHPAGA_ERR_JCAT_INVALIDID", 54);
define("PHPAGA_ERR_PRJCAT_INVALIDID", 55);
define("PHPAGA_ERR_AUTH_INVALIDUSER", 56);
define("PHPAGA_ERR_INVALID_FILENAME", 57);
define("PHPAGA_ERR_INVALID_PARAMTYPE", 58);
define("PHPAGA_ERR_CURRENCY_INVALIDID", 59);
define("PHPAGA_ERR_OPIDLIST_EMPTY", 60);
define("PHPAGA_ERR_INVOICETYPE_INVALIDID", 61);
define("PHPAGA_ERR_BILL_INVALID_NUMBER", 62);
define("PHPAGA_ERR_BILL_INVALID_DATE", 63);
define("PHPAGA_ERR_BILL_INVALID_DESC", 64);
define("PHPAGA_ERR_BILL_INVALID_CPNID", 65);
define("PHPAGA_ERR_BILL_INVALID_BMTID", 66);
define("PHPAGA_ERR_BILL_INVALID_CURRID", 67);
define("PHPAGA_ERR_BILL_INVALID_STARTSUM", 68);
define("PHPAGA_ERR_INVALID_INVOICEID", 69);
define("PHPAGA_ERR_INVALID_DATEFORMAT", 70);
define("PHPAGA_ERR_NOUPDATE", 71);
define("PHPAGA_ERR_INVALIDSEARCHPARAM", 72);
define("PHPAGA_ERR_OPCAT_INVALIDTITLE", 73);
define("PHPAGA_ERR_DEL_DEPENDENCIES", 74);
define("PHPAGA_ERR_PRJCAT_INVALIDTITLE", 75);
define("PHPAGA_ERR_PECAT_INVALIDTITLE", 76);
define("PHPAGA_ERR_JOBCAT_INVALIDTITLE", 77);
define("PHPAGA_ERR_CCAT_INVALIDTITLE", 78);
define("PHPAGA_ERR_PRJSTAT_INVALIDTITLE", 79);
define("PHPAGA_ERR_CURR_INVALIDNAME", 80);
define("PHPAGA_ERR_CURR_INVALIDDECIMALS", 81);
define("PHPAGA_ERR_CTR_INVALIDNAME", 82);
define("PHPAGA_ERR_CTR_INVALIDCODE", 83);
define("PHPAGA_ERR_USR_NOTFOUND", 84);
define("PHPAGA_ERR_INVALID_EMAIL", 85);
define("PHPAGA_ERR_COMPANY_INVALIDNAME", 86);
define("PHPAGA_ERR_PERSON_INVALIDLASTNAME", 87);
define("PHPAGA_ERR_OP_DESC", 88);
define("PHPAGA_ERR_OP_STARTDATE", 89);
define("PHPAGA_ERR_OP_ENDDATE", 90);
define("PHPAGA_ERR_OP_STARTHOURS", 91);
define("PHPAGA_ERR_OP_ENDHOURS", 92);
define("PHPAGA_ERR_OP_DURATION", 93);
define("PHPAGA_ERR_PROJECT_INVALIDTITLE", 94);
define("PHPAGA_ERR_PRJ_STARTDATE", 95);
define("PHPAGA_ERR_PRJ_ESTIMATEDENDDATE", 96);
define("PHPAGA_ERR_QUOTATION_INVALID_NUMBER", 97);
define("PHPAGA_ERR_QUOTATION_INVALID_DATE", 98);
define("PHPAGA_ERR_QUOTATION_INVALID_DESC", 99);
define("PHPAGA_ERR_QUOTATION_INVALID_CPNID", 100);
define("PHPAGA_ERR_QUOTATION_INVALID_BMTID", 101);
define("PHPAGA_ERR_QUOTATION_INVALID_CURRID", 102);
define("PHPAGA_ERR_QUOTATION_INVALID_STARTSUM", 103);
define("PHPAGA_ERR_INVALID_QUOTATIONID", 104);
define("PHPAGA_ERR_LOGIN_PASSWDNOMATCH", 105);
define("PHPAGA_ERR_USR_NOPEASSOC", 106);
define("PHPAGA_ERR_USR_LOGINDUPLICATE", 107);
define("PHPAGA_ERR_INVALID_HOURLYRATE", 108);
define("PHPAGA_ERR_INVALID_MAILBACKEND", 109);
define("PHPAGA_ERR_EXP_DESC", 110);
define("PHPAGA_ERR_EXP_INVALIDSUM", 111);
define("PHPAGA_ERR_EXP_INVALIDEXCHANGERATE", 112);
define("PHPAGA_ERR_DEL_PRJBILLDEPENDENCIES", 113);
define("PHPAGA_ERR_BILL_INVALID_ID", 114);
define("PHPAGA_ERR_BILL_PAIDNODELETE", 115);
define("PHPAGA_ERR_DB_TRANSACTBEGIN", 116);
define("PHPAGA_ERR_DB_TRANSACTROLLBACK", 117);
define("PHPAGA_ERR_DB_TRANSACTCOMMIT", 118);
define("PHPAGA_ERR_BMT_DELETEUSED", 119);
define("PHPAGA_ERR_LOCALENOTAVAIL", 120);
define("PHPAGA_ERR_SEND_MAIL", 121);
define("PHPAGA_ERR_INVALID_MANHOURS", 122);
define("PHPAGA_ERR_INVALID_COST_MANHOURS", 123);
define("PHPAGA_ERR_INVALID_COST_MATERIAL", 124);
define("PHPAGA_ERR_INVALID_PARENTPRJ", 125);
define("PHPAGA_ERR_INVALID_BILLABLETYPE", 126);
define("PHPAGA_ERR_DB_QUERYNOTYETSUPPORTED", 127);
define("PHPAGA_ERR_CFG_INVALIDID", 128);
define("PHPAGA_ERR_DISABLED", 129);
define("PHPAGA_ERR_FILE_REMOVE", 130);
define("PHPAGA_ERR_PRCAT_INVALIDTITLE", 131);
define("PHPAGA_ERR_PRCAT_INVALIDID", 132);
define("PHPAGA_ERR_PRODUCT_INVALIDID", 133);
define("PHPAGA_ERR_PRODUCT_INVALIDCODE", 134);
define("PHPAGA_ERR_PRODUCT_INVALIDNAME", 135);
define("PHPAGA_ERR_PRODUCT_INVALIDPRICE", 136);
define("PHPAGA_ERR_PRODUCT_INVALIDCONSUMPTION", 137);
define("PHPAGA_ERR_PRODUCT_INVALIDSTOCK", 138);
define("PHPAGA_ERR_MATERIAL_INVALIDNAME", 139);
define("PHPAGA_ERR_MATERIAL_INVALIDPRICE", 140);
define("PHPAGA_ERR_INVALIDQTY", 141);
define("PHPAGA_ERR_MATERIAL_INVALIDID", 142);
define("PHPAGA_ERR_BMTPLUGIN_ALREADYINCLUDED", 143);
define("PHPAGA_ERR_BMTPLUGIN_DIFFINCLUDED", 144);
define("PHPAGA_ERR_INVALID_SCTTITLE", 145);
define("PHPAGA_ERR_INVALID_START", 146);
define("PHPAGA_ERR_INVALID_END", 147);
define("PHPAGA_ERR_INVALID_VERSION", 148);
define("PHPAGA_ERR_DBVERSION_INSTHIGH", 149);
define("PHPAGA_ERR_DBUPGRADE_STATEMENT", 150);
define("PHPAGA_ERR_DBINSTALL", 151);
define("PHPAGA_ERR_PRJ_DEADLINEDATE", 152);
define("PHPAGA_ERR_BILL_INVALID_DUEDATE", 153);
define("PHPAGA_ERR_DB_FETCHVALUE", 154);
define("PHPAGA_ERR_NOT_IMPLEMENTED", 155);
define("PHPAGA_ERR_BILL_INVALID_RBILLENDDATE", 156);
define("PHPAGA_ERR_DELPRJ_BILL", 157);
define("PHPAGA_ERR_PMT_INVALID_BILL_ID", 158);
define("PHPAGA_ERR_INVALID_PAYMENTID", 159);
define("PHPAGA_ERR_INVALID_PMTAMT", 160);
define("PHPAGA_ERR_BILL_NO_LINEITEMS", 161);

/* error messages */

phpaga_errdefine("PHPAGA_ERR_FILE_READ", _("Cannot read from file"));
phpaga_errdefine("PHPAGA_ERR_FILE_WRITE", _("Cannot write to file"));
phpaga_errdefine("PHPAGA_ERR_FILE_CREATE", _("Cannot create file"));
phpaga_errdefine("PHPAGA_ERR_FILE_DELETE", _("Cannot delete file"));
phpaga_errdefine("PHPAGA_ERR_DB_EXEC_QUERY", _("Unable to execute query"));
phpaga_errdefine("PHPAGA_ERR_USER_INVALIDID", _("Invalid user ID"));
phpaga_errdefine("PHPAGA_ERR_USER_FIND", _("Unable to find user"));
phpaga_errdefine("PHPAGA_ERR_AUTH_LOGINLENGTH", _("Invalid login length."));
phpaga_errdefine("PHPAGA_ERR_AUTH_PASSWDLENGTH", _("Invalid password length."));
phpaga_errdefine("PHPAGA_ERR_DB_DUPLICATE", _("Unable to insert the record because of a constraint violation (duplicate key)."));
phpaga_errdefine("PHPAGA_ERR_USER_STAT", _("Unable to create user statistics"));
phpaga_errdefine("PHPAGA_ERR_PROJECT_INVALIDID", _("Invalid project ID"));
phpaga_errdefine("PHPAGA_ERR_INSERT_DATA_INVALIDFORMAT", _("Some of the data you have inserted is in an invalid format."));
phpaga_errdefine("PHPAGA_ERR_OPCAT_INVALIDID", _("Invalid operation category ID"));
phpaga_errdefine("PHPAGA_ERR_PERSON_INVALIDID", _("Invalid person ID"));
phpaga_errdefine("PHPAGA_ERR_OPERATION_INVALIDID", _("Invalid operation ID"));
phpaga_errdefine("PHPAGA_ERR_PRJSTAT_INVALIDID", _("Invalid project status ID"));
phpaga_errdefine("PHPAGA_ERR_COMPANY_INVALIDID", _("Invalid company ID"));
phpaga_errdefine("PHPAGA_ERR_COUNTRY_INVALIDID", _("Invalid country ID"));
phpaga_errdefine("PHPAGA_ERR_CCAT_INVALIDID", _("Invalid company category ID"));
phpaga_errdefine("PHPAGA_ERR_PECAT_INVALIDID", _("Invalid person category ID"));
phpaga_errdefine("PHPAGA_ERR_JCAT_INVALIDID", _("Invalid job category ID"));
phpaga_errdefine("PHPAGA_ERR_PRJCAT_INVALIDID", _("Invalid project category ID"));
phpaga_errdefine("PHPAGA_ERR_AUTH_INVALIDUSER", _("User not found in database"));
phpaga_errdefine("PHPAGA_ERR_INVALID_FILENAME", _("Invalid file name"));
phpaga_errdefine("PHPAGA_ERR_INVALID_PARAMTYPE", _("Invalid parameter type"));
phpaga_errdefine("PHPAGA_ERR_CURRENCY_INVALIDID", _("Invalid currency ID"));
phpaga_errdefine("PHPAGA_ERR_OPIDLIST_EMPTY", _("No operation has been selected"));
phpaga_errdefine("PHPAGA_ERR_INVOICETYPE_INVALIDID", _("Invalid invoice type ID"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_NUMBER", _("Invalid or missing invoice number"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_DATE", _("Invalid or missing invoice date"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_DUEDATE", _("Invalid due date"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_DESC", _("Invalid or missing invoice description"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_CPNID", _("Invalid or missing invoice recipient (company)"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_BMTID", _("Invalid invoice type"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_CURRID", _("Invalid or missing currency"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_STARTSUM", _("Invalid or missing invoice amount"));
phpaga_errdefine("PHPAGA_ERR_INVALID_INVOICEID", _("Invalid or missing invoice ID"));
phpaga_errdefine("PHPAGA_ERR_PMT_INVALID_BILL_ID", _("Invalid bill ID specified for this payment"));
phpaga_errdefine("PHPAGA_ERR_INVALID_PAYMENTID", _("Invalid payment ID specified for this transaction"));
phpaga_errdefine("PHPAGA_ERR_INVALID_PMTAMT", _("Invalid payment amount specified for this transaction"));
phpaga_errdefine("PHPAGA_ERR_INVALID_DATEFORMAT", _("Invalid date format - dates are expected to be entered in the format YYYY-MM-DD (ISO 8601)"));
phpaga_errdefine("PHPAGA_ERR_NOUPDATE", _("The record could not be updated"));
phpaga_errdefine("PHPAGA_ERR_INVALIDSEARCHPARAM", _("Invalid search parameters."));
phpaga_errdefine("PHPAGA_ERR_OPCAT_INVALIDTITLE", _("Invalid task category title"));
phpaga_errdefine("PHPAGA_ERR_DEL_DEPENDENCIES", _("Unable to delete this record because of dependencies"));
phpaga_errdefine("PHPAGA_ERR_PRJCAT_INVALIDTITLE", _("Invalid project category title"));
phpaga_errdefine("PHPAGA_ERR_PECAT_INVALIDTITLE", _("Invalid person category title"));
phpaga_errdefine("PHPAGA_ERR_JOBCAT_INVALIDTITLE", _("Invalid job category title"));
phpaga_errdefine("PHPAGA_ERR_CCAT_INVALIDTITLE", _("Invalid company category title"));
phpaga_errdefine("PHPAGA_ERR_PRJSTAT_INVALIDTITLE", _("Invalid project status title"));
phpaga_errdefine("PHPAGA_ERR_CURR_INVALIDNAME", _("Invalid currency name"));
phpaga_errdefine("PHPAGA_ERR_CURR_INVALIDDECIMALS", _("Invalid decimals value for currency"));
phpaga_errdefine("PHPAGA_ERR_CTR_INVALIDNAME", _("Invalid country name"));
phpaga_errdefine("PHPAGA_ERR_CTR_INVALIDCODE", _("Invalid country code"));
phpaga_errdefine("PHPAGA_ERR_USR_NOTFOUND", _("Unable to find user"));
phpaga_errdefine("PHPAGA_ERR_INVALID_EMAIL", _("Invalid email address"));
phpaga_errdefine("PHPAGA_ERR_COMPANY_INVALIDNAME", _("Invalid company name"));
phpaga_errdefine("PHPAGA_ERR_PERSON_INVALIDLASTNAME", _("Invalid last name"));
phpaga_errdefine("PHPAGA_ERR_OP_DESC", _("Invalid task description"));
phpaga_errdefine("PHPAGA_ERR_OP_STARTDATE", _("Invalid start date"));
phpaga_errdefine("PHPAGA_ERR_OP_ENDDATE", _("Invalid end date"));
phpaga_errdefine("PHPAGA_ERR_OP_STARTHOURS", _("Invalid start time"));
phpaga_errdefine("PHPAGA_ERR_OP_ENDHOURS", _("Invalid end time"));
phpaga_errdefine("PHPAGA_ERR_OP_DURATION", _("Invalid task duration"));
phpaga_errdefine("PHPAGA_ERR_PROJECT_INVALIDTITLE", _("Invalid project title"));
phpaga_errdefine("PHPAGA_ERR_PRJ_STARTDATE", _("Invalid start date"));
phpaga_errdefine("PHPAGA_ERR_PRJ_ESTIMATEDENDDATE", _("Invalid estimated end date"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_NUMBER", _("Invalid or missing quotation number"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_DATE", _("Invalid or missing quotation date"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_DESC", _("Invalid or missing quotation description"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_CPNID", _("Invalid or missing quotation recipient (company)"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_BMTID", _("Invalid quotation type"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_CURRID", _("Invalid or missing currency"));
phpaga_errdefine("PHPAGA_ERR_QUOTATION_INVALID_STARTSUM", _("Invalid or missing quotation amount"));
phpaga_errdefine("PHPAGA_ERR_INVALID_QUOTATIONID", _("Invalid or missing quotation ID"));
phpaga_errdefine("PHPAGA_ERR_LOGIN_PASSWDNOMATCH", _("The new password and its confirmation do not match."));
phpaga_errdefine("PHPAGA_ERR_USR_NOPEASSOC", _("The user must be associated to a person."));
phpaga_errdefine("PHPAGA_ERR_USR_LOGINDUPLICATE", _("This login name is already used by another user."));
phpaga_errdefine("PHPAGA_ERR_INVALID_HOURLYRATE", _("Invalid hourly wage rate"));
phpaga_errdefine("PHPAGA_ERR_INVALID_MAILBACKEND", _("You have configured an invalid mail backend in your etc/config.local.php"));
phpaga_errdefine("PHPAGA_ERR_EXP_DESC", _("Invalid expense description"));
phpaga_errdefine("PHPAGA_ERR_EXP_INVALIDSUM", _("Invalid expense amount"));
phpaga_errdefine("PHPAGA_ERR_EXP_INVALIDEXCHANGERATE", _("Invalid exchange rate format"));
phpaga_errdefine("PHPAGA_ERR_DEL_PRJBILLDEPENDENCIES", _("This project cannot be deleted because some or all of its associated operations are already billed."));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_ID", _("Invalid bill id"));
phpaga_errdefine("PHPAGA_ERR_BILL_PAIDNODELETE", _("This invoice cannot be deleted because it has already been paid."));
phpaga_errdefine("PHPAGA_ERR_DB_TRANSACTBEGIN", _("Unable to begin transaction"));
phpaga_errdefine("PHPAGA_ERR_DB_TRANSACTROLLBACK", _("Unable to rollback transaction"));
phpaga_errdefine("PHPAGA_ERR_DB_TRANSACTCOMMIT", _("Unable to commit transaction"));
phpaga_errdefine("PHPAGA_ERR_BMT_DELETEUSED", _("This billing method plugin cannot be deleted because it has been used for one or more invoices."));
phpaga_errdefine("PHPAGA_ERR_LOCALENOTAVAIL", _("The locales for the language you selected are not available. You need to select another language or install the required locales."));
phpaga_errdefine("PHPAGA_ERR_SEND_MAIL", _("An error occurred while trying to send the email."));
phpaga_errdefine("PHPAGA_ERR_INVALID_MANHOURS", _("Invalid value for manhours"));
phpaga_errdefine("PHPAGA_ERR_INVALID_COST_MANHOURS", _("Invalid value for manhours cost"));
phpaga_errdefine("PHPAGA_ERR_INVALID_COST_MATERIAL", _("Invalid value for material cost"));
phpaga_errdefine("PHPAGA_ERR_INVALID_PARENTPRJ", _("Invalid parent project"));
phpaga_errdefine("PHPAGA_ERR_INVALID_BILLABLETYPE", _("Invalid billable status."));
phpaga_errdefine("PHPAGA_ERR_DB_QUERYNOTYETSUPPORTED", sprintf(_("This query is not yet supported for the underlying database system (%s)"), PHPAGA_DB_SYSTEM));
phpaga_errdefine("PHPAGA_ERR_CFG_INVALIDID", _("Invalid configuration setting ID"));
phpaga_errdefine("PHPAGA_ERR_DISABLED", _("This functionality is disabled."));
phpaga_errdefine("PHPAGA_ERR_FILE_REMOVE", _("Unable to remove the file"));
phpaga_errdefine("PHPAGA_ERR_PRCAT_INVALIDTITLE", _("Invalid product category title"));
phpaga_errdefine("PHPAGA_ERR_PRCAT_INVALIDID", _("Invalid product category"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDID", _("Invalid product"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDCODE", _("Invalid product code"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDNAME", _("Invalid product name"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDPRICE", _("Invalid product price"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDCONSUMPTION", _("Invalid product consumption"));
phpaga_errdefine("PHPAGA_ERR_PRODUCT_INVALIDSTOCK", _("Invalid product stock"));
phpaga_errdefine("PHPAGA_ERR_MATERIAL_INVALIDNAME", _("Invalid material name"));
phpaga_errdefine("PHPAGA_ERR_MATERIAL_INVALIDPRICE", _("Invalid material price"));
phpaga_errdefine("PHPAGA_ERR_INVALIDQTY", _("Invalid quantity"));
phpaga_errdefine("PHPAGA_ERR_MATERIAL_INVALIDID", _("Invalid material ID"));
phpaga_errdefine("PHPAGA_ERR_BMTPLUGIN_ALREADYINCLUDED", _("Unable to include billing plugin method. A billing plugin has already been included."));
phpaga_errdefine("PHPAGA_ERR_BMTPLUGIN_DIFFINCLUDED", _("Unable to include billing plugin method. A different billing plugin has already been included."));
phpaga_errdefine("PHPAGA_ERR_INVALID_SCTTITLE", _("Invalid scheduled task title"));
phpaga_errdefine("PHPAGA_ERR_INVALID_START", _("Invalid start date or time"));
phpaga_errdefine("PHPAGA_ERR_INVALID_END", _("Invalid end date or time"));
phpaga_errdefine("PHPAGA_ERR_INVALID_VERSION", _("Invalid version number"));
phpaga_errdefine("PHPAGA_ERR_DBVERSION_INSTHIGH", _("The version of the installed database is higher than the version provided by the upgrade file. The file is not going to be applied."));
phpaga_errdefine("PHPAGA_ERR_DBUPGRADE_STATEMENT", _("One or more upgrade statements contained an error. The upgrade was not successful."));
phpaga_errdefine("PHPAGA_ERR_DBINSTALL", _("One or more statements contained an error. The installation was not successful."));
phpaga_errdefine("PHPAGA_ERR_PRJ_DEADLINEDATE", _("Invalid deadline"));
phpaga_errdefine("PHPAGA_ERR_DB_FETCHVALUE", _("Unable to fetch the value."));
phpaga_errdefine("PHPAGA_ERR_NOT_IMPLEMENTED", _("Functionality not imlpemented!"));
phpaga_errdefine("PHPAGA_ERR_BILL_INVALID_RBILLENDDATE", _("Invalid end date for recurring invoice."));
phpaga_errdefine("PHPAGA_ERR_DELPRJ_BILL", _("This project cannot be removed because one or more bills have been issued that refer to its tasks."));
phpaga_errdefine("PHPAGA_ERR_BILL_NO_LINEITEMS", _("No line items"));

?>
