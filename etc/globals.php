<?php
/**
 * phpaga
 *
 * Global definitions.
 *
 * This file contains global definitions.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

$phpaga_error = array();

/* Default settings 
 *
 * Add any new settings to this array.
 */

$phpaga_default_settings = array(
    'VERSION_DB' => '18',
    'DATEFORMAT_LONG' => '%A, %d. %B %Y',
    'DATEFORMAT_SHORT' => '%d.%m.%Y (%a)',
    'DEFAULTBMT' => '3',
    'DEFAULTCOUNTRY' => '1',
    'DEFAULTCURRENCY' => '2',
    'DEFAULTLANGUAGE' => 'en',
    'DEFAULT_SKIN' => 'phpaga',
    'IMAGETYPE' => 'png',
    'LETTERHEAD_COMPANY_CITY' => 'Milano',
    'LETTERHEAD_COMPANY_EMAIL' => 'info@example.com',
    'LETTERHEAD_COMPANY_FULLLOCATION' => '99099 - Milano (MI), Italy',
    'LETTERHEAD_COMPANY_NAME' => 'phpaga Enterprises',
    'LETTERHEAD_COMPANY_PHONE' => 'Tel. +39 0000 00000000',
    'LETTERHEAD_COMPANY_STREET' => 'Viale Camilleri 2002',
    'LETTERHEAD_COMPANY_TAXNR' => 'P.IVA 00110022003',
    'LETTERHEAD_FOOTER' => 'KINKY BANK di Milano, ABI 0123 - CAB 98976 - c/c 12340987',
    'LETTERHEAD_LOGO_FILE' => '',
    'LETTERHEAD_OWNER_PERSONALTAXNR' => 'Cod. fisc. ABCZYX00A11B123Q',
    'LOCATION_FORMAT' => '%ZIP - %CITY (%REGION), %COUNTRY',
    'PDF_DOCFORMAT' => 'a4',
    'PDF_PRINT_LETTERHEAD_DEFAULT' => true,
    'PENAME_FORMAT' => '%LASTNAME, %FIRSTNAME',
    'SEPARATOR_DECIMALS' => ',',
    'SEPARATOR_THOUSANDS' => '.',
    'SHORTLOCATION_FORMAT' => '%CITY, %REGION',
    'SHOWLASTOPERATIONS' => '8',
    'SHOWOPDESCLENGHT' => '60',
    'TEMPLATESET' => 'phpaga',
    'DATEFORMAT_PRINT' => 'd.m.Y',
    'SHOWPRJTITLELENGHT' => '60',
    'SHOWOPCATTITLELENGHT' => '60',
    'MAIN_SHOW_PRJSTAT' => '1',
    'GRAPHVIZ_ENABLED' => 'false',
    'FIN_UNBILLED_GRPBY' => '',
    'MAIN_PRJTAB_SORT' => '',
    'FIN_FISCYEAR_MONTH' => 1,
    'FIN_FISCYEAR_DAY' => 1,
    'DATETIMEFORMAT_LONG' => '%A, %d. %B %Y %R',
    'DATETIMEFORMAT_SHORT' => '%d.%m.%Y (%a) %R',
    'SORT_PROJECTS' => 18,
    'SORT_OPERATIONS' => 2,
    'SORT_COMPANIES' => 51,
    'SORT_PERSONS' => 31,
    'RECORDS_PERPAGE' => 20,
    'SITENAME' => 'phpaga',
    'LETTERHEAD_ADDITIONAL_LINE' => '',
    'PHPAGA_LATE_FEE' => '1.5',
    'MONETARY_SYMBOL' => '',
    'SHOW_CURRENCY' => true,
    'PDF_INCOICE_TPL' => 'InvoicePDF',
    'PDF_QUOTATION_TPL' => 'QuotationPDF',
    'LANGUAGE' => 'en',
    'PDFTK_PATH' => '/usr/bin/pdftk',
    'GS_PATH' => '/usr/bin/gs',
    'PDFMERGE_BACKEND' => '',
    'PDF_PREPEND_INVOICE' => '',
    'PDF_APPEND_INVOICE' => ''


);

/* The following settings can be saved per user */

$phpaga_usersettings_keys = array( 
    'FIN_UNBILLED_GRPBY',
    'MAIN_PRJTAB_SORT',
    'MAIN_SHOW_PRJSTAT',
    'RECORDS_PERPAGE',
    'SORT_COMPANIES',
    'SORT_OPERATIONS',
    'SORT_PERSONS',
    'SORT_PROJECTS',
    'LANGUAGE'
);

/* sort order parameters */

define('ORDER_OP_TIMESTAMP_START_ASC',     1);
define('ORDER_OP_TIMESTAMP_START_DESC',    2);
define('ORDER_OP_PE_ASC',                  3);
define('ORDER_OP_PE_DESC',                 4);
define('ORDER_OP_PR_ASC',                  5);
define('ORDER_OP_PR_DESC',                 6);
define('ORDER_OP_OPCAT_ASC',               7);
define('ORDER_OP_OPCAT_DESC',              8);
define('ORDER_OP_DURATION_ASC',            9);
define('ORDER_OP_DURATION_DESC',          10);

/* Define some actions */

define('ACTION_NONE',                      0);
define('ACTION_OP_VIEW',                   1);
define('ACTION_OP_REMOVE',                 3);
define('ACTION_BILL_MAKEIT',               4);
define('ACTION_ADD',                       5);
define('ACTION_DELETE',                    6);
define('ACTION_DELETE_ASK',                7);
define('ACTION_UPDATE',                    8);
define('ACTION_QUOTATION_MAKEIT',          9);
define('ACTION_BILL_WITHOPERATIONS',      10);
define('ACTION_PE_VIEW',                  11);
define('ACTION_PE_REMOVE_ASK',            12);
define('ACTION_PE_REMOVE',                13);
define('ACTION_PRJ_MEMBER_ADD_ASK',       14);
define('ACTION_PRJ_MEMBER_ADD',           15);
define('ACTION_PRJ_MEMBER_REMOVE_ASK',    16);
define('ACTION_PRJ_MEMBER_REMOVE',        17);
define('ACTION_SAVECONFIG',               18);
define('ACTION_OKMSG',                    19);
define('ACTION_PRJ_REMOVE',               20);
define('ACTION_VIEW',                     21);
define('ACTION_REMOVE_ASK',               22);
define('ACTION_PRJ_REMOVE_FORCE',         23);
define('ACTION_CLONE',                    24);
define('ACTION_FROMQUOTATION',            25);
define('ACTION_REMOVE',                   26);
define('ACTION_CREATEDBSTRUCT',           27);
define('ACTION_UPDATE_OK',                28);
define('ACTION_BILL_ADDLINEITEMS',        29);
define('ACTION_BILL_RECALC',              30);
define('ACTION_BILL_MAKEOPITEMS',         31);
define('ACTION_BILL_MAKESINGLEOPITEM',    32);
define('ACTION_ENABLE',                   33);
define('ACTION_DISABLE',                  34);
define('ACTION_SERVICE_PAYTERM',          35);  // Do not modify this value, it is hardcoded in htdocs/js/billing.js
define('ACTION_SERVICE_MEMBERPARM',       36);
define('ACTION_SERVICE_OPDETAILS',        37);  // Do not modify this value, it is hardcoded in htdocs/js/tasks.js
define('ACTION_FILE_REMOVE',              39);  // Do not modify this value, it is hardcoded in some templates
define('ACTION_SERVICE_PRODINFO',         40);  // Do not modify this value, it is hardcoded in htdocs/js/billing.js, htdocs/js/material.js, templates/phpaga/edit_material.tpl.html
define('ACTION_MATERIAL_REMOVE',          41);
define('ACTION_BILL_FROMPROJECT',         42);
define('ACTION_SERVICE_MATDETAILS',       43);
define('ACTION_EDIT',                     44);
define('ACTION_BILL_MAKEOPITEMSPERPRJ',   45);
define('ACTION_SERVICE_ADDTASK',          47);
define('ACTION_SERVICE_GETEMPLOYEES',     48);  // Do not modify this value, it is hardcoded in htdocs/js/billing.js
define('ACTION_SERVICE_GETCURRTIME',      49);  // Do not modify this value, it is hardcoded in htdocs/js/core.js
define('ACTION_GENERATE',                 50);
define('ACTION_SERVICE_GETADDTASK_HTML',  52);
define('ACTION_SERVICE_LOGIN',            53);
define('ACTION_SERVICE_DELETE_USER',      54);
define('ACTION_SERVICE_DELETE_OP',        55); // Do not modify this value, it is hardcoded in htdocs/js/tasks.js
define('ACTION_SERVICE_PRJHOURS',         56); // Do not modify this value, it is hardcoded in htdocs/js/tasks.js

/* other definitions */

define('PHPAGA_PROJECTSTATUS_ALL',         0);
define('PHPAGA_PROJECTSTATUS_OPEN',        1);
define('PHPAGA_PROJECTSTATUS_CLOSED',      2);
define('PHPAGA_PROJECTSTATUS_OVERDUE',     3);

/* operation billed/not billed */

define('PHPAGA_BILL_NOTBILLED',            2);
define('PHPAGA_BILL_BILLED',               1);

/* invoice paid/not paid */

define('PHPAGA_BILL_NOTPAID',              2);
define('PHPAGA_BILL_PAID',                 1);

/* invoice sent/not sent */

define('PHPAGA_BILL_NOTSENT',              2);
define('PHPAGA_BILL_SENT',                 1);

/* quotation sent/not sent */

define('PHPAGA_QUOTATION_NOTSENT',         2);
define('PHPAGA_QUOTATION_SENT',            1);

define('PHPAGA_NO',                        0);
define('PHPAGA_YES',                       1);

define('PHPAGA_SEPARATOR',                 1);

/* how to group the manhours / display type */
/* show all days of a month */
define('PHPAGA_MANHOURS_DISPLAY_MONTH',    2);

/* show all calendar weeks of a year */
define('PHPAGA_MANHOURS_DISPLAY_YEAR',     4);

define('PHPAGA_OPTION_ALL',                2);
define('PHPAGA_OPTION_SELECT',             3);
define('PHPAGA_OPTION_NONE',               0);
define('PHPAGA_OPTION_NEW',                1);

define('PHPAGA_OPCGRP_PROJECT',             1);
define('PHPAGA_OPCGRP_PERSON',              2);

define('PHPAGA_BILLABLETYPE_YES',           0);
define('PHPAGA_BILLABLETYPE_NO',           10);
define('PHPAGA_BILLABLETYPE_EXTRA',        20);

define('PHPAGA_BILL_TASKASSOC_LOCKED',     10);

$phpaga_projects_billabletype = array(PHPAGA_BILLABLETYPE_YES => _('Yes'),
                                      PHPAGA_BILLABLETYPE_NO => _('No'));

$phpaga_operations_billabletype = array(PHPAGA_BILLABLETYPE_YES => _('Yes'),
                                        PHPAGA_BILLABLETYPE_NO => _('No'),
                                        PHPAGA_BILLABLETYPE_EXTRA => _('Extra'));

define('PHPAGA_GROUPBY_PROJECT',            1);
define('PHPAGA_GROUPBY_CUSTOMER',           2);

$phpaga_groupby = array(PHPAGA_GROUPBY_PROJECT => _('Project'),
                        PHPAGA_GROUPBY_CUSTOMER => _('Customer'));

define('PHPAGA_TYPE_INVOICE', 1);
define('PHPAGA_TYPE_QUOTATION', 2);

define('PHPAGA_LOG_ERR', 3);
define('PHPAGA_LOG_INFO', 6);

define('PHPAGA_RELTYPE_PROJECT',   10);
define('PHPAGA_RELTYPE_PLANTASK',  20);
define('PHPAGA_RELTYPE_EXPENSE',   30);
define('PHPAGA_RELTYPE_TRACKTASK', 40);
define('PHPAGA_RELTYPE_PERSON',    50);
define('PHPAGA_RELTYPE_COMPANY',   60);

$phpaga_preview_types = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif', 'application/pdf', 'application/postscript');
$phpaga_preview_types_tojpg = array('application/pdf', 'application/postscript');
$phpaga_preview_types_firstpage = array('application/pdf', 'application/postscript');
$phpaga_mime_image_types = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');

define('PHPAGA_RBILL_TYPE_NONE', 0);
define('PHPAGA_RBILL_TYPE_WEEKLY', 10);
define('PHPAGA_RBILL_TYPE_MONTHLY', 20);
define('PHPAGA_RBILL_TYPE_ANNUAL', 30);

$phpaga_rbill_types = array(PHPAGA_RBILL_TYPE_NONE => _('Non-recurring'),
                            PHPAGA_RBILL_TYPE_WEEKLY => _('Weekly'),
                            PHPAGA_RBILL_TYPE_MONTHLY => _('Monthly'),
                            PHPAGA_RBILL_TYPE_ANNUAL => _('Annual'));

$phpaga_rbill_intervals = array(PHPAGA_RBILL_TYPE_WEEKLY => '1 week',
                                PHPAGA_RBILL_TYPE_MONTHLY => '1 month',
                                PHPAGA_RBILL_TYPE_ANNUAL => '1 year');

define('PHPAGA_RBILL_STATUS_ENABLED', 0);
define('PHPAGA_RBILL_STATUS_DISABLED', 10);

?>