-- phpaga
--
-- This file contains the necessary setup for the PostgreSQL DB used
-- by phpaga. Please consult INSTALL in the main directory for information
-- on how to use this file.
--
-- Copyright 2001, 2002, 2003 Florian Lanthaler <florian@phpaga.net>
--
-- @author Florian Lanthaler <florian@phpaga.net>
-- @version $Id$
--


CREATE SEQUENCE "billing_methods_bmt_id_seq";

CREATE TABLE "billing_methods" (
	"bmt_id" bigint DEFAULT nextval('billing_methods_bmt_id_seq'::text) NOT NULL,
	"bmt_description" character varying(250) NOT NULL,
	"bmt_filename" character varying(100) NOT NULL,
	"bmt_classname" character varying(100),
	"ctr_id" bigint NOT NULL,
        "bmt_status" smallint,
	Constraint "billing_methods_pkey" Primary Key ("bmt_id")
);

CREATE SEQUENCE "bills_bill_id_seq";

CREATE TABLE "bills" (
	"bill_id" bigint DEFAULT nextval('bills_bill_id_seq'::text) NOT NULL,
	"bill_number" character varying(10) NOT NULL,
	"bill_date" date NOT NULL,
	"bill_date_due" date,
	"cpn_id" bigint NOT NULL,
        "pe_id_recipient" bigint,
	"bill_sent" smallint NOT NULL,
	"bill_paydate" date,
        "bill_payterm" character varying(30),
	"bill_startsum" numeric(12,2) NOT NULL,
	"bill_endsum" numeric(12,2) NOT NULL,
	"bmt_id" bigint NOT NULL,
	"curr_id" smallint NOT NULL,
	"bill_footer" VARCHAR(255) DEFAULT NULL,
        "rbill_id" bigint,
	"bill_additional_line" VARCHAR(255) DEFAULT NULL,
	"bill_note" text DEFAULT ''::text,
	Constraint "bills_pkey" Primary Key ("bill_id")
);

CREATE SEQUENCE "companies_cpn_id_seq";

CREATE TABLE "companies" (
	"cpn_id" bigint DEFAULT nextval('companies_cpn_id_seq'::text) NOT NULL,
	"cpn_name" character varying(100) NOT NULL,
	"cpn_address" character varying(250),
	"cpn_address2" character varying(250),
	"cpn_city" character varying(100),
	"cpn_zip" character varying(30),
	"cpn_region" character varying(40),
	"ctr_id" integer NOT NULL,
	"ccat_id" smallint NOT NULL,
	"cpn_taxnr" character varying(30),
	"cpn_url" character varying(200),
	"cpn_phone" character varying(30),
	"cpn_fax" character varying(30),
	"cpn_email" character varying(100),
	"cpn_public" smallint,
	"pe_id" bigint NOT NULL,
	"cpn_logoname" character varying(250),
	"cpn_owner_taxnr" character varying(30),
        "cpn_defaultpayterm" character varying(30),
	Constraint "companies_pkey" Primary Key ("cpn_id")
);

CREATE SEQUENCE "companycat_ccat_id_seq";

CREATE TABLE "companycat" (
	"ccat_id" smallint DEFAULT nextval('companycat_ccat_id_seq'::text) NOT NULL,
	"ccat_title" character varying(30) NOT NULL,
	Constraint "companycat_pkey" Primary Key ("ccat_id")
);

CREATE SEQUENCE "countries_ctr_id_seq";

CREATE TABLE "countries" (
	"ctr_id" bigint DEFAULT nextval('countries_ctr_id_seq'::text) NOT NULL,
	"ctr_name" character varying(30) NOT NULL,
	"ctr_code" character varying(4) NOT NULL,
	Constraint "countries_pkey" Primary Key ("ctr_id")
);

CREATE SEQUENCE "currencies_curr_id_seq";

CREATE TABLE "currencies" (
	"curr_id" smallint DEFAULT nextval('currencies_curr_id_seq'::text) NOT NULL,
	"curr_name" character varying(15) NOT NULL,
	"curr_decimals" smallint DEFAULT 2,
	Constraint "currencies_pkey" Primary Key ("curr_id")
);

CREATE SEQUENCE "jobcat_jcat_id_seq";

CREATE TABLE "jobcat" (
	"jcat_id" smallint DEFAULT nextval('jobcat_jcat_id_seq'::text) NOT NULL,
	"jcat_title" character varying(30) NOT NULL,
	Constraint "jobcat_pkey" Primary Key ("jcat_id")
);

CREATE SEQUENCE "operationcat_opcat_id_seq";

CREATE TABLE "operationcat" (
	"opcat_id" bigint DEFAULT nextval('operationcat_opcat_id_seq'::text) NOT NULL,
	"opcat_title" character varying(30) NOT NULL,
        "opcat_color" character varying(20),
        "opcat_hourlyrate" numeric(12,2),
	Constraint "operationcat_pkey" Primary Key ("opcat_id")
);

CREATE SEQUENCE "operations_op_id_seq";

CREATE TABLE "operations" (
	"op_id" bigint DEFAULT nextval('operations_op_id_seq'::text) NOT NULL,
	"pe_id" bigint NOT NULL,
	"prj_id" bigint NOT NULL,
	"opcat_id" bigint NOT NULL,
        "op_start" timestamp without time zone,
        "op_end" timestamp without time zone,
	"op_desc" text NOT NULL,
	"op_duration" integer NOT NULL,
	"bill_id" bigint,
        "op_billabletype" int default 0,
        "op_amount" numeric(12,2),
	Constraint "operations_pkey" Primary Key ("op_id")
);

CREATE SEQUENCE "personcat_pecat_id_seq";

CREATE TABLE "personcat" (
	"pecat_id" smallint DEFAULT nextval('personcat_pecat_id_seq'::text) NOT NULL,
	"pecat_title" character varying(30) NOT NULL,
	Constraint "personcat_pkey" Primary Key ("pecat_id")
);

CREATE SEQUENCE "persons_pe_id_seq";

CREATE TABLE "persons" (
	"pe_id" bigint DEFAULT nextval('persons_pe_id_seq'::text) NOT NULL,
	"pe_lastname" character varying(100) NOT NULL,
	"pe_firstname" character varying(50),
	"pe_address" character varying(250),
	"pe_address2" character varying(250),
	"pe_city" character varying(100),
	"pe_zip" character varying(30),
	"pe_region" character varying(40),
	"ctr_id" integer NOT NULL,
	"pecat_id" bigint,
	"pe_taxnr" character varying(30),
	"pe_url" character varying(200),
	"pe_phonework" character varying(30),
	"pe_phonemobile" character varying(30),
	"pe_phonehome" character varying(30),
	"pe_fax" character varying(30),
	"pe_email" character varying(100),
	"cpn_id" bigint,
	"jcat_id" bigint,
        "pe_hourlyrate" numeric(12,2),
	"pe_note" text,
	"pe_public" smallint,
	"pe_pe_id" bigint,
	Constraint "persons_pkey" Primary Key ("pe_id")
);

CREATE SEQUENCE "projectcat_prjcat_id_seq";

CREATE TABLE "projectcat" (
	"prjcat_id" smallint DEFAULT nextval('projectcat_prjcat_id_seq'::text) NOT NULL,
	"prjcat_title" character varying(30) NOT NULL,
	Constraint "projectcat_pkey" Primary Key ("prjcat_id")
);

CREATE SEQUENCE "projects_prj_id_seq";

CREATE TABLE "projects" (
	"prj_id" bigint DEFAULT nextval('projects_prj_id_seq'::text) NOT NULL,
	"prj_title" character varying(200) NOT NULL,
	"prj_desc" text,
	"prj_startdate" date,
	"prj_estimatedend" date,
	"prj_deadline" date,
	"prj_enddate" date,
	"prj_status" smallint NOT NULL,
	"cpn_id" bigint NOT NULL,
	"cpn_id_issuebill" bigint DEFAULT NULL,
	"prjcat_id" smallint NOT NULL,
        "prj_planned_manhours" int,
        "prj_planned_cost_manhours" numeric(12,2),
        "prj_planned_cost_material" numeric(12,2),
	"pe_id" bigint NOT NULL,
	"prj_manager_pe_id" bigint,
        "prj_prj_id" bigint DEFAULT NULL,
        "prj_billabletype" int default 0,
        "prj_priority" smallint,
        "quot_id" bigint,
	Constraint "projects_pkey" Primary Key ("prj_id")
);

CREATE TABLE project_members (
    prj_id bigint NOT NULL,
    pe_id bigint NOT NULL,
    jcat_id bigint,
    prjmem_added timestamp without time zone DEFAULT now() NOT NULL,
    prjmem_pe_id bigint NOT NULL,
    prjmem_hourlyrate numeric(12,2),
    Constraint "prj_pe" Primary Key ("prj_id", "pe_id")
);


CREATE SEQUENCE "projectstatus_prjstat_id_seq";

CREATE TABLE "projectstatus" (
	"prjstat_id" smallint DEFAULT nextval('projectstatus_prjstat_id_seq'::text) NOT NULL,
	"prjstat_title" character varying(15) NOT NULL,
	Constraint "projectstatus_pkey" Primary Key ("prjstat_id")
);

CREATE SEQUENCE "quotations_quot_id_seq";

CREATE TABLE "quotations" (
	"quot_id" bigint DEFAULT nextval('quotations_quot_id_seq'::text) NOT NULL,
	"quot_number" character varying(10) NOT NULL,
	"quot_date" date NOT NULL,
        "quot_payterm" character varying(30),
	"cpn_id" bigint NOT NULL,
        "pe_id_recipient" bigint,
	"quot_sent" smallint NOT NULL,
	"quot_startsum" numeric(12,2) NOT NULL,
	"quot_endsum" numeric(12,2) NOT NULL,
	"bmt_id" bigint NOT NULL,
	"curr_id" smallint NOT NULL,
	"quot_footer" VARCHAR(255) DEFAULT NULL,
	"quot_additional_line" VARCHAR(255) DEFAULT NULL,
	Constraint "quotations_pkey" Primary Key ("quot_id")
);

CREATE SEQUENCE "users_usr_id_seq";

CREATE TABLE "users" (
	"usr_id" bigint DEFAULT nextval('users_usr_id_seq'::text) NOT NULL,
	"pe_id" bigint NOT NULL,
	"usr_login" character varying(15) NOT NULL,
	"usr_passwd" character varying(120) NOT NULL,
	"usr_pe_id" bigint NOT NULL,
	"usr_skin" character varying(20),
	"usr_perm" character varying(250),
	"usr_settings" text DEFAULT NULL,
	Constraint "users_pkey" Primary Key ("usr_id")
);

CREATE SEQUENCE "config_cfg_id_seq";

CREATE TABLE "config" (
	"cfg_id" int DEFAULT nextval('config_cfg_id_seq'::text) NOT NULL,
	"cfg_key" varchar(60) NOT NULL,
	"cfg_value" varchar(200) NOT NULL,
	Constraint "config_pkey" Primary Key ("cfg_id")
);

CREATE SEQUENCE "expenses_exp_id_seq";

CREATE TABLE "expenses" (
	"exp_id" bigint DEFAULT nextval('expenses_exp_id_seq'::text) NOT NULL,
	"exp_date" date NOT NULL,
	"pe_id" bigint NOT NULL,
	"prj_id" bigint NOT NULL,
	"exp_desc" text NOT NULL,
	"exp_sum" numeric(12,2) NOT NULL,
	"curr_id" smallint NOT NULL,
	"exp_sum_foreign" numeric(12,2) DEFAULT NULL,
	"curr_id_foreign" smallint DEFAULT NULL,
	"exp_exchange_rate" numeric(12,5) DEFAULT NULL,
	"bill_id" bigint DEFAULT NULL,
	Constraint "expenses_pkey" Primary Key ("exp_id")
);

CREATE SEQUENCE "lineitems_lit_id_seq";

CREATE TABLE "lineitems" (
	"lit_id" bigint DEFAULT nextval('lineitems_lit_id_seq'::text) NOT NULL,
	"bill_id" bigint,
	"quot_id" bigint,
        "op_id" bigint,
        "mat_id" bigint,
        "lit_prodcode" character varying(18),
	"lit_desc" text,
	"lit_qty" character varying(10),
        "lit_netprice" numeric(12,2),
	Constraint "lineitems_pkey" Primary Key ("lit_id")
);

CREATE SEQUENCE "log_id_seq";

CREATE TABLE log (
	id INT DEFAULT nextval('log_id_seq'::text) NOT NULL,
	logtime TIMESTAMP NOT NULL,
	ident CHARACTER VARYING(16) NOT NULL,
	priority INT NOT NULL,
	message text,
	PRIMARY KEY (id)
);

CREATE SEQUENCE "files_file_id_seq";

CREATE TABLE "files" (
	"file_id" bigint DEFAULT nextval('files_file_id_seq'::text) NOT NULL,
        "file_added" timestamp without time zone DEFAULT now() NOT NULL,
	"pe_id" bigint NOT NULL,
        "file_original_name" character varying(500) not null,
        "file_local_name" character varying(100) not null,
        "file_rel_type" int not null,
        "file_rel_id" bigint not null,
        "file_mimetype" character varying(30),
	Constraint "files_pkey" Primary Key ("file_id")
);

CREATE SEQUENCE "productcat_prcat_id_seq";

CREATE TABLE "productcat" (
       "prcat_id" bigint DEFAULT nextval('productcat_prcat_id_seq'::text) NOT NULL,
       "prcat_title" character varying(100) NOT NULL,
       Constraint "productcat_pkey" Primary Key ("prcat_id")
);

CREATE SEQUENCE "products_prod_id_seq";

CREATE TABLE "products" (
       "prod_id" bigint DEFAULT nextval('products_prod_id_seq'::text) NOT NULL,
       "prod_code" character varying(18) NOT NULL,
       "prod_name" character varying(100) NOT NULL,
       "prod_price" numeric(12,2),
       "prod_consumption" numeric(12,4),
       "prod_stock" int,
       "prcat_id" bigint,
       "prod_created" timestamp without time zone default now() NOT NULL,
       "prod_lastmod" timestamp without time zone NOT NULL,
       Constraint "products_pkey" Primary key ("prod_id")
);

CREATE SEQUENCE "material_mat_id_seq";

CREATE TABLE "material" (
	"mat_id" bigint DEFAULT nextval('material_mat_id_seq'::text) NOT NULL,
        "mat_prodcode" character varying(100),
        "prod_id" bigint,
        "mat_name" character varying(100),
	"mat_desc" text,
        "mat_qty" integer,
	"mat_price" numeric(12,2) NOT NULL,
	"curr_id" smallint NOT NULL,
	"mat_added" timestamp without time zone default now() NOT NULL,
	"prj_id" bigint NOT NULL,
	"pe_id" bigint NOT NULL,
	"bill_id" bigint,
	Constraint "material_pkey" Primary Key ("mat_id")
);

CREATE SEQUENCE "projects_hourlyrates_phr_id_seq";

CREATE TABLE "projects_hourlyrates" (
	"phr_id" bigint DEFAULT nextval('projects_hourlyrates_phr_id_seq'::text) NOT NULL,
        "phr_lastmod" timestamp without time zone DEFAULT now() NOT NULL,
	"pe_id_lastmod" bigint NOT NULL,
	"prj_id" bigint NOT NULL,
        "pe_id" bigint,
	"opcat_id" bigint NOT NULL,
        "phr_hourlyrate" numeric(12,2) NOT NULL,
	Constraint "projects_hourlyrates_pkey" Primary Key ("phr_id")
);

CREATE SEQUENCE "recurrbills_rbill_id_seq";

CREATE TABLE "recurrbills" (
	"rbill_id" bigint DEFAULT nextval('recurrbills_rbill_id_seq'::text) NOT NULL,
	"bill_id" bigint NOT NULL,
        "rbill_type" int NOT NULL,
        "rbill_start" date NOT NULL,
        "rbill_end" date,
        "rbill_status" smallint default 0 NOT NULL,
        "rbill_created" timestamp without time zone default now() NOT NULL,
	Constraint "recurrbills_pkey" Primary Key ("rbill_id")
);

CREATE SEQUENCE "latefees_fee_id_seq";

CREATE TABLE "latefees" (
        "fee_id" bigint DEFAULT nextval(('latefees_fee_id_seq'::text)::regclass) NOT NULL,
        "bill_id" bigint NOT NULL,
        "fee_amt" numeric(12,2) NOT NULL,
        "fee_date" date DEFAULT ('now'::text)::date NOT NULL,
	CONSTRAINT "latefees_pkey" PRIMARY KEY ("fee_id")
);

CREATE SEQUENCE "payments_pmt_id_seq";

CREATE TABLE "payments" (
        "pmt_id" bigint DEFAULT nextval(('payments_pmt_id_seq'::text)::regclass) NOT NULL,
        "bill_id" bigint NOT NULL,
        "pmt_amt" numeric(12,2) NOT NULL,
        "pmt_date" date NOT NULL,
        "pmt_note" text,
	CONSTRAINT payments_pkey PRIMARY KEY ("pmt_id")
);



CREATE INDEX cpn_name_companies_index ON companies USING btree (cpn_name);
CREATE INDEX ctr_id_companies_index ON companies USING btree (ctr_id);
CREATE INDEX ccat_id_companies_index ON companies USING btree (ccat_id);
CREATE INDEX cpn_taxnr_companies_index ON companies USING btree (cpn_taxnr);
CREATE INDEX cpn_city_companies_index ON companies USING btree (cpn_city);
CREATE INDEX cpn_zip_companies_index ON companies USING btree (cpn_zip);
CREATE INDEX cpn_region_companies_index ON companies USING btree (cpn_region);
CREATE INDEX cpn_public_companies_index ON companies USING btree (cpn_public);
CREATE INDEX pe_id_companies_index ON companies USING btree (pe_id);
CREATE UNIQUE INDEX prjstat_title_projectstatus_ind ON projectstatus USING btree (prjstat_title);
CREATE UNIQUE INDEX jcat_title_jobcat_index ON jobcat USING btree (jcat_title);
CREATE INDEX curr_decimals_currencies_index ON currencies USING btree (curr_decimals);
CREATE UNIQUE INDEX bmt_filename_billing_methods_in ON billing_methods USING btree (bmt_filename);
CREATE UNIQUE INDEX bmt_description_billing_methods ON billing_methods USING btree (bmt_description);
CREATE INDEX ctr_id_billing_methods_index ON billing_methods USING btree (ctr_id);
CREATE INDEX cpn_id_quotations_index ON quotations USING btree (cpn_id);
CREATE INDEX quot_sent_quotations_index ON quotations USING btree (quot_sent);
CREATE INDEX quot_date_quotations_index ON quotations USING btree (quot_date);
CREATE INDEX curr_id_quotations_index ON quotations USING btree (curr_id);
CREATE INDEX quot_number_quotations_index ON quotations USING btree (quot_number);
CREATE INDEX pe_id_operations_index ON operations USING btree (pe_id);
CREATE INDEX prj_id_operations_index ON operations USING btree (prj_id);
CREATE INDEX op_start_operations_i ON operations USING btree (op_start);
CREATE INDEX op_duration_operations_index ON operations USING btree (op_duration);
CREATE INDEX bill_id_operations_index ON operations USING btree (bill_id);
CREATE INDEX opcat_id_operations_index ON operations USING btree (opcat_id);
CREATE UNIQUE INDEX pecat_title_personcat_index ON personcat USING btree (pecat_title);
CREATE INDEX pe_lastname_persons_index ON persons USING btree (pe_lastname);
CREATE INDEX pe_firstname_persons_index ON persons USING btree (pe_firstname);
CREATE INDEX pe_city_persons_index ON persons USING btree (pe_city);
CREATE INDEX pe_zip_persons_index ON persons USING btree (pe_zip);
CREATE INDEX pe_region_persons_index ON persons USING btree (pe_region);
CREATE INDEX ctr_id_persons_index ON persons USING btree (ctr_id);
CREATE INDEX pecat_id_persons_index ON persons USING btree (pecat_id);
CREATE INDEX pe_taxnr_persons_index ON persons USING btree (pe_taxnr);
CREATE INDEX cpn_id_persons_index ON persons USING btree (cpn_id);
CREATE INDEX jcat_id_persons_index ON persons USING btree (jcat_id);
CREATE INDEX pe_public_persons_index ON persons USING btree (pe_public);
CREATE INDEX pe_pe_id_persons_index ON persons USING btree (pe_pe_id);
CREATE UNIQUE INDEX usr_login_users_index ON users USING btree (usr_login);
CREATE INDEX pe_id_users_index ON users USING btree (pe_id);
CREATE INDEX usr_pe_id_users_index ON users USING btree (usr_pe_id);
CREATE INDEX usr_skin_users_index ON users USING btree (usr_skin);
CREATE UNIQUE INDEX idx_ctr_name_countries_index ON countries USING btree (ctr_name);
CREATE UNIQUE INDEX ctr_code_countries_index ON countries USING btree (ctr_code);
CREATE UNIQUE INDEX opcat_title_operationcat_index ON operationcat USING btree (opcat_title);
CREATE INDEX bill_sent_bills_index ON bills USING btree (bill_sent);
CREATE INDEX bill_paydate_bills_index ON bills USING btree (bill_paydate);
CREATE INDEX bill_date_bills_index ON bills USING btree (bill_date);
CREATE INDEX bill_date_due_bills_index ON bills USING btree (bill_date_due);
CREATE INDEX curr_id_bills_index ON bills USING btree (curr_id);
CREATE INDEX bill_number_bills_index ON bills USING btree (bill_number);
CREATE UNIQUE INDEX ccat_title_companycat_index ON companycat USING btree (ccat_title);
CREATE UNIQUE INDEX prjcat_title_projectcat_index ON projectcat USING btree (prjcat_title);
CREATE INDEX prj_title_projects_index ON projects USING btree (prj_title);
CREATE INDEX prj_startdate_projects_index ON projects USING btree (prj_startdate);
CREATE INDEX prj_estimatedend_projects_index ON projects USING btree (prj_estimatedend);
CREATE INDEX prj_enddate_projects_index ON projects USING btree (prj_enddate);
CREATE INDEX prj_status_projects_index ON projects USING btree (prj_status);
CREATE INDEX cpn_id_projects_index ON projects USING btree (cpn_id);
CREATE INDEX quot_id_projects_index ON projects USING btree (quot_id);
CREATE INDEX prjcat_id_projects_index ON projects USING btree (prjcat_id);
CREATE INDEX pe_id_projects_index ON projects USING btree (pe_id);
CREATE INDEX cpn_id_bills_index ON bills USING btree (cpn_id);
CREATE INDEX cfg_id_config_index ON config USING btree (cfg_id);
CREATE UNIQUE INDEX cfg_key_config_index ON config USING btree (cfg_key);
CREATE INDEX pe_id_expenses_index ON expenses USING btree (pe_id);
CREATE INDEX prj_id_expenses_index ON expenses USING btree (prj_id);
CREATE INDEX curr_id_expenses_index ON expenses USING btree (curr_id);
CREATE INDEX curr_id_foreign_expenses_index ON expenses USING btree (curr_id_foreign);
CREATE INDEX bill_id_expenses_index ON expenses USING btree (bill_id);
CREATE INDEX cpn_id_issuebill_projects_index ON projects USING btree (cpn_id_issuebill);
CREATE INDEX lit_prodcode_lineitems_index ON lineitems USING btree (lit_prodcode);
CREATE INDEX bill_id_lineitems_index ON lineitems USING btree (bill_id);
CREATE INDEX quot_id_lineitems_index ON lineitems USING btree (quot_id);
CREATE INDEX op_id_lineitems_index ON lineitems USING btree (op_id);
CREATE INDEX mat_id_lineitems_index ON lineitems USING btree (mat_id);
CREATE INDEX prj_manager_pe_id_projects_index ON projects USING btree (prj_manager_pe_id);
CREATE INDEX prj_prj_id_projects_index ON projects USING btree (prj_prj_id);
CREATE INDEX pe_id_files_index ON files USING btree (pe_id);
CREATE UNIQUE INDEX prcat_title_productcat_ind ON productcat USING btree (prcat_title);
CREATE INDEX prod_code_products_index ON products USING btree (prod_code);
CREATE INDEX prcat_id_products_index ON products USING btree (prcat_id);
CREATE UNIQUE INDEX prod_code_products_ind ON products USING btree (prod_code);
CREATE INDEX bill_id_material_index ON material USING btree (bill_id);
CREATE INDEX pe_id_material_index ON material USING btree (pe_id);
CREATE INDEX prj_id_material_index ON material USING btree (prj_id);
CREATE INDEX prod_id_material_index ON material USING btree (prod_id);
CREATE UNIQUE INDEX projects_hourlyrates_prj_pe_opcat ON projects_hourlyrates USING btree (prj_id, pe_id, opcat_id);
CREATE INDEX bill_id_recurrbills_index ON recurrbills USING btree (bill_id);
CREATE INDEX rbill_type_recurrbills_index ON recurrbills USING btree (rbill_type);
CREATE INDEX rbill_start_recurrbills_index ON recurrbills USING btree (rbill_start);
CREATE INDEX rbill_end_recurrbills_index ON recurrbills USING btree (rbill_end);
CREATE INDEX rbill_status_recurrbills_index ON recurrbills USING btree (rbill_status);
CREATE INDEX rbill_id_bills_index ON bills USING btree (rbill_id);
CREATE INDEX billid_latefees_index ON latefees USING btree (bill_id);
CREATE INDEX feeid_latefees_index ON latefees USING btree (fee_id);
CREATE INDEX billid_payments_index ON payments USING btree (bill_id);
CREATE INDEX fki_bills_bill_id ON payments USING btree (bill_id);
CREATE INDEX pmtid_payments_index ON payments USING btree (pmt_id);


-- Functions

CREATE OR REPLACE FUNCTION MONTH(TIMESTAMP without TIME ZONE) RETURNS INTEGER AS 'SELECT EXTRACT(MONTH FROM $1)::INTEGER;' LANGUAGE SQL IMMUTABLE;
CREATE OR REPLACE FUNCTION MONTH(TIMESTAMP WITH TIME ZONE) RETURNS INTEGER AS 'SELECT EXTRACT(MONTH FROM $1)::INTEGER;' LANGUAGE SQL STABLE;
CREATE OR REPLACE FUNCTION MONTH(DATE) RETURNS INTEGER AS 'SELECT EXTRACT(MONTH FROM $1)::INTEGER;' LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION YEAR(TIMESTAMP without TIME ZONE) RETURNS INTEGER AS 'SELECT EXTRACT(YEAR FROM $1)::INTEGER;' LANGUAGE SQL IMMUTABLE;
CREATE OR REPLACE FUNCTION YEAR(TIMESTAMP WITH TIME ZONE) RETURNS INTEGER AS 'SELECT EXTRACT(YEAR FROM $1)::INTEGER;' LANGUAGE SQL STABLE;
CREATE OR REPLACE FUNCTION YEAR(DATE) RETURNS INTEGER AS 'SELECT EXTRACT(YEAR FROM $1)::INTEGER;' LANGUAGE SQL IMMUTABLE;


-- Views

CREATE VIEW v_summary_notbilledhours AS
SELECT p.cpn_id,
        cpn_name,
        p.prj_id,
        prj_title,
        SUM(op_duration) as prj_duration,
        SUM(case when (ph.phr_hourlyrate IS NOT NULL) then ph.phr_hourlyrate ELSE case when prjmem_hourlyrate IS NOT NULL THEN prjmem_hourlyrate ELSE phn.phr_hourlyrate END END * (cast(op_duration AS FLOAT)/ 60))  AS prj_amount,
        to_char(min(op_start), 'yyyy-mm-dd hh:mi') AS op_min_start,
        to_char(max(op_end), 'yyyy-mm-dd hh:mi') AS op_max_end,
        (select SUM(mat_price) from material where material.prj_id = p.prj_id and (material.bill_id IS NULL OR material.bill_id = 0)) as material_price
        FROM projects p
        LEFT JOIN operations o ON o.prj_id = p.prj_id
        LEFT JOIN project_members pm ON (pm.prj_id = o.prj_id AND pm.pe_id = o.pe_id)
        LEFT JOIN companies c ON c.cpn_id = p.cpn_id
        LEFT JOIN projects_hourlyrates ph ON (ph.opcat_id = o.opcat_id and ph.prj_id = o.prj_id AND ph.pe_id = o.pe_id)
        LEFT JOIN projects_hourlyrates phn ON (phn.opcat_id = o.opcat_id and phn.prj_id = o.prj_id AND phn.pe_id IS NULL)
        WHERE (prj_billabletype IS NULL OR prj_billabletype = 0)
        AND (o.bill_id IS NULL OR o.bill_id = 0)
        AND (op_billabletype IS NULL OR op_billabletype = 0)
        GROUP BY p.cpn_id, cpn_name, p.prj_id, prj_title
        ORDER BY cpn_name, prj_title;

CREATE VIEW v_summary_notbilledhours_bycustomer AS
SELECT p.cpn_id, cpn_name, sum(op_duration) AS prj_duration,
        SUM(case when (ph.phr_hourlyrate IS NOT NULL) then ph.phr_hourlyrate ELSE case when prjmem_hourlyrate IS NOT NULL THEN prjmem_hourlyrate ELSE phn.phr_hourlyrate END END * (cast(op_duration AS FLOAT)/ 60))  AS prj_amount,
        to_char(min(op_start), 'yyyy-mm-dd hh:mi'::text) AS op_min_start,
        to_char(max(op_end), 'yyyy-mm-dd hh:mi'::text) AS op_max_end,
        (select SUM(mat_price) from material left join projects p2 on p2.prj_id = material.prj_id where (p2.prj_billabletype IS NULL OR p2.prj_billabletype = 0) and p2.cpn_id = p.cpn_id and (material.bill_id IS NULL OR material.bill_id = 0)) as material_price
        FROM projects p
        LEFT JOIN operations o ON o.prj_id = p.prj_id
        LEFT JOIN project_members pm ON pm.prj_id = o.prj_id AND pm.pe_id = o.pe_id
        LEFT JOIN companies c ON c.cpn_id = p.cpn_id
        LEFT JOIN projects_hourlyrates ph ON (ph.opcat_id = o.opcat_id and ph.prj_id = o.prj_id AND ph.pe_id = o.pe_id)
        LEFT JOIN projects_hourlyrates phn ON (phn.opcat_id = o.opcat_id and phn.prj_id = o.prj_id AND phn.pe_id IS NULL)
        WHERE (prj_billabletype IS NULL OR prj_billabletype = 0)
        AND (o.bill_id IS NULL OR o.bill_id = 0)
        AND (op_billabletype IS NULL OR op_billabletype = 0)
        GROUP BY p.cpn_id, cpn_name
        ORDER BY cpn_name;

CREATE VIEW v_bills AS
SELECT DISTINCT
        bills.bill_id, bills.bill_number, bills.bill_date, bills.cpn_id, bills.bill_sent, bills.bill_paydate, bills.bill_startsum,
        bills.bill_endsum, bills.bmt_id, bills.curr_id, bills.bill_footer, bills.bill_payterm, bills.bill_date_due,
        bills.pe_id_recipient, bills.rbill_id, bills.bill_note, bills.bill_additional_line,
        ( SELECT coalesce(sum(payments.pmt_amt), 0) AS sum FROM payments WHERE payments.bill_id = bills.bill_id ) AS pamt,
        ( bills.bill_endsum - ( SELECT coalesce(sum(payments.pmt_amt), 0) AS pmts FROM payments WHERE payments.bill_id = bills.bill_id )) AS missing_pmt
        FROM bills
        LEFT JOIN currencies ON currencies.curr_id = bills.curr_id;

