-- PostgreSQL: Set sequence values
--
-- This file sets the correct sequence values to all sequences after a
-- migration from MySQL to PostgreSQL. As the sequences do not get
-- incremented during INSERTs which specify an ID, it is necessary to
-- manually set them to their highest values.
--
-- @author Florian Lanthaler <florian@phpaga.net>
-- @version $Id$
--
-- Copyright 2002 - 2007 Florian Lanthaler <florian@phpaga.net>

select setval('billing_methods_bmt_id_seq', (select max(bmt_id) from billing_methods));
select setval('bills_bill_id_seq', (select max(bill_id) from bills));
select setval('companies_cpn_id_seq', (select max(cpn_id) from companies));
select setval('companycat_ccat_id_seq', (select max(ccat_id) from companycat));
select setval('countries_ctr_id_seq', (select max(ctr_id) from countries));
select setval('config_cfg_id_seq', (select max(cfg_id) from config));
select setval('currencies_curr_id_seq', (select max(curr_id) from currencies));
select setval('expenses_exp_id_seq', (select max(exp_id) from expenses));
select setval('files_file_id_seq', (select max(file_id) from files));
select setval('jobcat_jcat_id_seq', (select max(jcat_id) from jobcat));
select setval('lineitems_lit_id_seq', (select max(lit_id) from lineitems));
select setval('log_id_seq', (select max(id) from log));
select setval('material_mat_id_seq', (select max(mat_id) from material));
select setval('operationcat_opcat_id_seq', (select max(opcat_id) from operationcat));
select setval('operations_op_id_seq', (select max(op_id) from operations));
select setval('personcat_pecat_id_seq', (select max(pecat_id) from personcat));
select setval('persons_pe_id_seq', (select max(pe_id) from persons));
select setval('productcat_prcat_id_seq', (select max(prcat_id) from productcat));
select setval('products_prod_id_seq', (select max(prod_id) from products));
select setval('projectcat_prjcat_id_seq', (select max(prjcat_id) from projectcat));
select setval('projects_prj_id_seq', (select max(prj_id) from projects));
select setval('projects_hourlyrates_phr_id_seq', (select max(phr_id) from projects_hourlyrates));
select setval('projectstatus_prjstat_id_seq', (select max(prjstat_id) from projectstatus));
select setval('quotations_quot_id_seq', (select max(quot_id) from quotations));
select setval('recurrbills_rbill_id_seq', (select max(rbill_id) from recurrbills));
select setval('users_usr_id_seq', (select max(usr_id) from users));
select setval('latefees_fee_id_seq', (select max(fee_id) from latefees));
select setval('payments_pmt_id_seq', (select max(pmt_id) from payments));

-- END
