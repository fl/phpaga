-- phpaga
--
-- This file contains the constraints for the PostgreSQL DB used by
-- phpaga. Please consult INSTALL in the main directory for information
-- on how to use this file.
--
-- Copyright 2001, 2002 Florian Lanthaler <florian@phpaga.net>
--
-- @author Florian Lanthaler <florian@phpaga.net>
-- @version $Id$
--

--
-- TOC Entry ID 195 (OID 17697)
--
-- Name: "RI_ConstraintTrigger_17696" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "operations"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'operations', 'persons', 'UNSPECIFIED', 'pe_id', 'pe_id');

--
-- TOC Entry ID 200 (OID 17699)
--
-- Name: "RI_ConstraintTrigger_17698" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "persons"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'operations', 'persons', 'UNSPECIFIED', 'pe_id', 'pe_id');

--
-- TOC Entry ID 201 (OID 17701)
--
-- Name: "RI_ConstraintTrigger_17700" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "persons"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'operations', 'persons', 'UNSPECIFIED', 'pe_id', 'pe_id');

--
-- TOC Entry ID 196 (OID 17703)
--
-- Name: "RI_ConstraintTrigger_17702" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "operations"  FROM "projects" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'operations', 'projects', 'UNSPECIFIED', 'prj_id', 'prj_id');

--
-- TOC Entry ID 210 (OID 17705)
--
-- Name: "RI_ConstraintTrigger_17704" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "projects"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'operations', 'projects', 'UNSPECIFIED', 'prj_id', 'prj_id');

--
-- TOC Entry ID 211 (OID 17707)
--
-- Name: "RI_ConstraintTrigger_17706" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "projects"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'operations', 'projects', 'UNSPECIFIED', 'prj_id', 'prj_id');

--
-- TOC Entry ID 197 (OID 17709)
--
-- Name: "RI_ConstraintTrigger_17708" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "operations"  FROM "operationcat" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'operations', 'operationcat', 'UNSPECIFIED', 'opcat_id', 'opcat_id');

--
-- TOC Entry ID 193 (OID 17711)
--
-- Name: "RI_ConstraintTrigger_17710" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "operationcat"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'operations', 'operationcat', 'UNSPECIFIED', 'opcat_id', 'opcat_id');

--
-- TOC Entry ID 194 (OID 17713)
--
-- Name: "RI_ConstraintTrigger_17712" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "operationcat"  FROM "operations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'operations', 'operationcat', 'UNSPECIFIED', 'opcat_id', 'opcat_id');

--
-- TOC Entry ID 167 (OID 17715)
--
-- Name: "RI_ConstraintTrigger_17714" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "companies"  FROM "countries" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'companies', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 179 (OID 17717)
--
-- Name: "RI_ConstraintTrigger_17716" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "countries"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'companies', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 180 (OID 17719)
--
-- Name: "RI_ConstraintTrigger_17718" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "countries"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'companies', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 168 (OID 17721)
--
-- Name: "RI_ConstraintTrigger_17720" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "companies"  FROM "companycat" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'companies', 'companycat', 'UNSPECIFIED', 'ccat_id', 'ccat_id');

--
-- TOC Entry ID 177 (OID 17723)
--
-- Name: "RI_ConstraintTrigger_17722" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "companycat"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'companies', 'companycat', 'UNSPECIFIED', 'ccat_id', 'ccat_id');

--
-- TOC Entry ID 178 (OID 17725)
--
-- Name: "RI_ConstraintTrigger_17724" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "companycat"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'companies', 'companycat', 'UNSPECIFIED', 'ccat_id', 'ccat_id');

--
-- TOC Entry ID 202 (OID 17727)
--
-- Name: "RI_ConstraintTrigger_17726" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "persons"  FROM "countries" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'persons', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 181 (OID 17729)
--
-- Name: "RI_ConstraintTrigger_17728" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "countries"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'persons', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 182 (OID 17731)
--
-- Name: "RI_ConstraintTrigger_17730" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "countries"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'persons', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 203 (OID 17733)
--
-- Name: "RI_ConstraintTrigger_17732" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "persons"  FROM "personcat" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'persons', 'personcat', 'UNSPECIFIED', 'pecat_id', 'pecat_id');

--
-- TOC Entry ID 198 (OID 17735)
--
-- Name: "RI_ConstraintTrigger_17734" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "personcat"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'persons', 'personcat', 'UNSPECIFIED', 'pecat_id', 'pecat_id');

--
-- TOC Entry ID 199 (OID 17737)
--
-- Name: "RI_ConstraintTrigger_17736" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "personcat"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'persons', 'personcat', 'UNSPECIFIED', 'pecat_id', 'pecat_id');

--
-- TOC Entry ID 204 (OID 17739)
--
-- Name: "RI_ConstraintTrigger_17738" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "persons"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'persons', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 169 (OID 17741)
--
-- Name: "RI_ConstraintTrigger_17740" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "companies"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'persons', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 170 (OID 17743)
--
-- Name: "RI_ConstraintTrigger_17742" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "companies"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'persons', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 205 (OID 17745)
--
-- Name: "RI_ConstraintTrigger_17744" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "persons"  FROM "jobcat" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'persons', 'jobcat', 'UNSPECIFIED', 'jcat_id', 'jcat_id');

--
-- TOC Entry ID 189 (OID 17747)
--
-- Name: "RI_ConstraintTrigger_17746" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "jobcat"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'persons', 'jobcat', 'UNSPECIFIED', 'jcat_id', 'jcat_id');

--
-- TOC Entry ID 190 (OID 17749)
--
-- Name: "RI_ConstraintTrigger_17748" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "jobcat"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'persons', 'jobcat', 'UNSPECIFIED', 'jcat_id', 'jcat_id');

--
-- TOC Entry ID 212 (OID 17751)
--
-- Name: "RI_ConstraintTrigger_17750" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "projects"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'projects', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 171 (OID 17753)
--
-- Name: "RI_ConstraintTrigger_17752" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "companies"  FROM "projects" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'projects', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 172 (OID 17755)
--
-- Name: "RI_ConstraintTrigger_17754" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "companies"  FROM "projects" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'projects', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 213 (OID 17757)
--
-- Name: "RI_ConstraintTrigger_17756" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "projects"  FROM "projectcat" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'projects', 'projectcat', 'UNSPECIFIED', 'prjcat_id', 'prjcat_id');

--
-- TOC Entry ID 208 (OID 17759)
--
-- Name: "RI_ConstraintTrigger_17758" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "projectcat"  FROM "projects" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'projects', 'projectcat', 'UNSPECIFIED', 'prjcat_id', 'prjcat_id');

--
-- TOC Entry ID 209 (OID 17761)
--
-- Name: "RI_ConstraintTrigger_17760" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "projectcat"  FROM "projects" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'projects', 'projectcat', 'UNSPECIFIED', 'prjcat_id', 'prjcat_id');

--
-- TOC Entry ID 214 (OID 17763)
--
-- Name: "RI_ConstraintTrigger_17762" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "quotations"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'quotations', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 173 (OID 17765)
--
-- Name: "RI_ConstraintTrigger_17764" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "companies"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'quotations', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 174 (OID 17767)
--
-- Name: "RI_ConstraintTrigger_17766" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "companies"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'quotations', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 215 (OID 17769)
--
-- Name: "RI_ConstraintTrigger_17768" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "quotations"  FROM "billing_methods" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'quotations', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 159 (OID 17771)
--
-- Name: "RI_ConstraintTrigger_17770" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "billing_methods"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'quotations', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 160 (OID 17773)
--
-- Name: "RI_ConstraintTrigger_17772" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "billing_methods"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'quotations', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 216 (OID 17775)
--
-- Name: "RI_ConstraintTrigger_17774" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "quotations"  FROM "currencies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'quotations', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 185 (OID 17777)
--
-- Name: "RI_ConstraintTrigger_17776" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "currencies"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'quotations', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 186 (OID 17779)
--
-- Name: "RI_ConstraintTrigger_17778" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "currencies"  FROM "quotations" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'quotations', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 218 (OID 17787)
--
-- Name: "RI_ConstraintTrigger_17786" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "users"  FROM "persons" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'users', 'persons', 'UNSPECIFIED', 'usr_pe_id', 'pe_id');

--
-- TOC Entry ID 206 (OID 17789)
--
-- Name: "RI_ConstraintTrigger_17788" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "persons"  FROM "users" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'users', 'persons', 'UNSPECIFIED', 'usr_pe_id', 'pe_id');

--
-- TOC Entry ID 207 (OID 17791)
--
-- Name: "RI_ConstraintTrigger_17790" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "persons"  FROM "users" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'users', 'persons', 'UNSPECIFIED', 'usr_pe_id', 'pe_id');

--
-- TOC Entry ID 161 (OID 17793)
--
-- Name: "RI_ConstraintTrigger_17792" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "billing_methods"  FROM "countries" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'billing_methods', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 183 (OID 17795)
--
-- Name: "RI_ConstraintTrigger_17794" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "countries"  FROM "billing_methods" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'billing_methods', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 184 (OID 17797)
--
-- Name: "RI_ConstraintTrigger_17796" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "countries"  FROM "billing_methods" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'billing_methods', 'countries', 'UNSPECIFIED', 'ctr_id', 'ctr_id');

--
-- TOC Entry ID 164 (OID 17799)
--
-- Name: "RI_ConstraintTrigger_17798" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "bills"  FROM "billing_methods" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'bills', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 162 (OID 17801)
--
-- Name: "RI_ConstraintTrigger_17800" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "billing_methods"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'bills', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 163 (OID 17803)
--
-- Name: "RI_ConstraintTrigger_17802" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "billing_methods"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'bills', 'billing_methods', 'UNSPECIFIED', 'bmt_id', 'bmt_id');

--
-- TOC Entry ID 165 (OID 17805)
--
-- Name: "RI_ConstraintTrigger_17804" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "bills"  FROM "currencies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'bills', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 187 (OID 17807)
--
-- Name: "RI_ConstraintTrigger_17806" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "currencies"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'bills', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 188 (OID 17809)
--
-- Name: "RI_ConstraintTrigger_17808" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "currencies"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'bills', 'currencies', 'UNSPECIFIED', 'curr_id', 'curr_id');

--
-- TOC Entry ID 166 (OID 17811)
--
-- Name: "RI_ConstraintTrigger_17810" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER INSERT OR UPDATE ON "bills"  FROM "companies" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins" ('<unnamed>', 'bills', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 175 (OID 17813)
--
-- Name: "RI_ConstraintTrigger_17812" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER DELETE ON "companies"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del" ('<unnamed>', 'bills', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

--
-- TOC Entry ID 176 (OID 17815)
--
-- Name: "RI_ConstraintTrigger_17814" Type: TRIGGER Owner: phpaga
--

CREATE CONSTRAINT TRIGGER "<unnamed>" AFTER UPDATE ON "companies"  FROM "bills" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd" ('<unnamed>', 'bills', 'companies', 'UNSPECIFIED', 'cpn_id', 'cpn_id');

