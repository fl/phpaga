/*
 * phpaga
 * upgrade4.pgsql
 * Florian Lanthaler
 * 2007-10-01
 *
 * Recurring invoices.
 *
 */

BEGIN TRANSACTION;

CREATE SEQUENCE "recurrbills_rbill_id_seq";

CREATE TABLE "recurrbills" (
	"rbill_id" bigint DEFAULT nextval('recurrbills_rbill_id_seq'::text) NOT NULL,
	"bill_id" bigint NOT NULL,
        "rbill_type" int NOT NULL,
        "rbill_start" date NOT NULL,
        "rbill_end" date,
        "rbill_status" smallint default 0 NOT NULL,
        "rbill_created" timestamp without time zone default now() NOT NULL,
	Constraint "recurrbills_pkey" Primary Key ("rbill_id")
);

CREATE INDEX bill_id_recurrbills_index ON recurrbills USING btree (bill_id);
CREATE INDEX rbill_type_recurrbills_index ON recurrbills USING btree (rbill_type);
CREATE INDEX rbill_start_recurrbills_index ON recurrbills USING btree (rbill_start);
CREATE INDEX rbill_end_recurrbills_index ON recurrbills USING btree (rbill_end);
CREATE INDEX rbill_status_recurrbills_index ON recurrbills USING btree (rbill_status);

ALTER TABLE recurrbills ADD CONSTRAINT fk_recurrbills_bill_id FOREIGN KEY (bill_id) REFERENCES bills(bill_id) MATCH FULL;

ALTER TABLE bills ADD COLUMN "rbill_id" bigint;
CREATE INDEX rbill_id_bills_index ON bills USING btree (rbill_id);
ALTER TABLE bills ADD CONSTRAINT fk_bills_rbill_id FOREIGN KEY (rbill_id) REFERENCES recurrbills(rbill_id) MATCH FULL;

COMMIT;
