/*
 * phpaga
 * upgrade13.pgsql
 * Florian Lanthaler
 * 2010-12-09
 *
 * Add a settings field to the users table
 *
 */

alter table users add column "usr_settings" text DEFAULT NULL;
