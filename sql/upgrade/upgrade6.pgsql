/*
 * phpaga
 * upgrade6.pgsql
 * Florian Lanthaler
 * 2008-03-10
 *
 * Pdf templates path
 *
 */

BEGIN TRANSACTION;

INSERT INTO config (cfg_key, cfg_value) VALUES ('PDF_TEMPLATE', 'default');

COMMIT;
