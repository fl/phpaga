/*
 * phpaga
 * upgrade1.pgsql
 * Florian Lanthaler
 * 2007-04-12
 *
 * Alter views related to unbilled hours - also show unbilled materials amount.
 *
 */


DROP VIEW IF EXISTS v_summary_notbilledhours;
CREATE VIEW v_summary_notbilledhours AS
 SELECT p.cpn_id,
        cpn_name,
        p.prj_id,
        prj_title,
        SUM(op_duration) as prj_duration,
        SUM(case when (ph.phr_hourlyrate IS NOT NULL) then ph.phr_hourlyrate ELSE case when prjmem_hourlyrate IS NOT NULL THEN prjmem_hourlyrate ELSE phn.phr_hourlyrate END END * (cast(op_duration AS FLOAT)/ 60))  AS prj_amount,
        to_char(cast(min(op_timestamp_start) AS abstime), 'yyyy-mm-dd hh:mi') AS op_min_start,
        to_char(cast(max(op_timestamp_end) AS abstime), 'yyyy-mm-dd hh:mi') AS op_max_end,
        (select SUM(mat_price) from material where material.prj_id = p.prj_id and (material.bill_id IS NULL OR material.bill_id = 0)) as material_price
        FROM projects p
        LEFT JOIN operations o ON o.prj_id = p.prj_id
        LEFT JOIN project_members pm ON (pm.prj_id = o.prj_id AND pm.pe_id = o.pe_id)
        LEFT JOIN companies c ON c.cpn_id = p.cpn_id
        LEFT JOIN projects_hourlyrates ph ON (ph.opcat_id = o.opcat_id and ph.prj_id = o.prj_id AND ph.pe_id = o.pe_id)
        LEFT JOIN projects_hourlyrates phn ON (phn.opcat_id = o.opcat_id and phn.prj_id = o.prj_id AND phn.pe_id IS NULL)
        WHERE (prj_billabletype IS NULL OR prj_billabletype = 0)
        AND (o.bill_id IS NULL OR o.bill_id = 0)
        AND (op_billabletype IS NULL OR op_billabletype = 0)
        GROUP BY p.cpn_id, cpn_name, p.prj_id, prj_title
        ORDER BY cpn_name, prj_title;


DROP VIEW IF EXISTS v_summary_notbilledhours_bycustomer;
CREATE VIEW v_summary_notbilledhours_bycustomer AS
SELECT p.cpn_id, cpn_name, sum(op_duration) AS prj_duration,
        SUM(case when (ph.phr_hourlyrate IS NOT NULL) then ph.phr_hourlyrate ELSE case when prjmem_hourlyrate IS NOT NULL THEN prjmem_hourlyrate ELSE phn.phr_hourlyrate END END * (cast(op_duration AS FLOAT)/ 60))  AS prj_amount,
        to_char(min(op_timestamp_start)::abstime::timestamp with time zone, 'yyyy-mm-dd hh:mi'::text) AS op_min_start,
        to_char(max(op_timestamp_end)::abstime::timestamp with time zone, 'yyyy-mm-dd hh:mi'::text) AS op_max_end, 
        (select SUM(mat_price) from material left join projects p2 on p2.prj_id = material.prj_id where (p2.prj_billabletype IS NULL OR p2.prj_billabletype = 0) and p2.cpn_id = p.cpn_id and (material.bill_id IS NULL OR material.bill_id = 0)) as material_price
        FROM projects p
        LEFT JOIN operations o ON o.prj_id = p.prj_id
        LEFT JOIN project_members pm ON pm.prj_id = o.prj_id AND pm.pe_id = o.pe_id
        LEFT JOIN companies c ON c.cpn_id = p.cpn_id
        LEFT JOIN projects_hourlyrates ph ON (ph.opcat_id = o.opcat_id and ph.prj_id = o.prj_id AND ph.pe_id = o.pe_id)
        LEFT JOIN projects_hourlyrates phn ON (phn.opcat_id = o.opcat_id and phn.prj_id = o.prj_id AND phn.pe_id IS NULL)
        WHERE (prj_billabletype IS NULL OR prj_billabletype = 0)
        AND (o.bill_id IS NULL OR o.bill_id = 0)
        AND (op_billabletype IS NULL OR op_billabletype = 0) 
        GROUP BY p.cpn_id, cpn_name
        ORDER BY cpn_name;
