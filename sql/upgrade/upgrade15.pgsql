/*
 * phpaga
 * upgrade15.pgsql
 * Florian Lanthaler
 * 2010-12-21
 *
 * The language settings field from the user table is now handled by
 * usr_settings.
 *
 */

alter table users alter column lng_id drop not null;
