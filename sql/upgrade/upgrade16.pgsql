/*
 * phpaga
 * upgrade16.pgsql
 * Florian Lanthaler
 * 2011-10-28
 *
 * Add missing foreign key.
 *
 */

ALTER TABLE projects ADD CONSTRAINT fk_projects_pe_id FOREIGN KEY (pe_id) REFERENCES persons(pe_id) MATCH FULL;

