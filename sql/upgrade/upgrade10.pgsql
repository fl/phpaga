/*
 * phpaga
 * upgrade10.pgsql
 * Florian Lanthaler
 * 2010-04-12
 *
 * Modify existing view
 *
 */

CREATE OR REPLACE VIEW v_bills AS
SELECT DISTINCT
        bills.bill_id, bills.bill_number, bills.bill_date, bills.cpn_id, bills.bill_sent, bills.bill_paydate, bills.bill_startsum,
        bills.bill_endsum, bills.bmt_id, bills.curr_id, bills.bill_footer, bills.bill_payterm, bills.bill_date_due,
        bills.pe_id_recipient, bills.rbill_id, bills.bill_note, bills.bill_additional_line,
        ( SELECT coalesce(sum(payments.pmt_amt), 0) AS sum FROM payments WHERE payments.bill_id = bills.bill_id ) AS pamt,
        ( bills.bill_endsum - ( SELECT coalesce(sum(payments.pmt_amt), 0) AS pmts FROM payments WHERE payments.bill_id = bills.bill_id )) AS missing_pmt
        FROM bills
        LEFT JOIN currencies ON currencies.curr_id = bills.curr_id;

