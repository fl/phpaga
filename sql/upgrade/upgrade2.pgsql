/*
 * phpaga
 * upgrade2.pgsql
 * Florian Lanthaler
 * 2007-05-30
 *
 * Add "date due" to the invoice.
 *
 */

ALTER TABLE bills ADD COLUMN "bill_date_due" date;
CREATE INDEX bill_date_due_bills_index ON bills USING btree (bill_date_due);
