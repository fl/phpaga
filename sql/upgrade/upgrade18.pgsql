/*
 * phpaga
 * upgrade18.pgsql
 * Florian Lanthaler
 * 2011-11-10
 *
 * Increased password field size for fCryptography compatibility.
 * Log ident is not a fixed length field.
 */

alter table users alter column usr_passwd type character varying(120);
alter table log alter column ident type character varying(16);
alter table log alter column id set default nextval('log_id_seq');
alter table persons alter column pe_pe_id drop not null;

