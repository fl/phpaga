/*
 * phpaga
 * upgrade3.pgsql
 * Florian Lanthaler
 * 2007-07-05
 *
 * Add an optional person to invoices and quotations.
 *
 */

ALTER TABLE bills ADD COLUMN "pe_id_recipient" bigint;
CREATE INDEX bill_pe_id_recipient_bills_index ON bills USING btree (pe_id_recipient);

ALTER TABLE quotations ADD COLUMN "pe_id_recipient" bigint;
CREATE INDEX quot_pe_id_recipient_quotations_index ON bills USING btree (pe_id_recipient);
