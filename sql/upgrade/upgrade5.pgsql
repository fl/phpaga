/*
 * phpaga
 * upgrade5.pgsql
 * Christian Graffer
 * 2008-02-09
 *
 * Add an additional line to invoices and quotations
 *
 */

BEGIN TRANSACTION;

ALTER TABLE bills ADD COLUMN "bill_additional_line" VARCHAR(255);

ALTER TABLE quotations ADD COLUMN "quot_additional_line" VARCHAR(255);

INSERT INTO config (cfg_key, cfg_value) VALUES ('LETTERHEAD_ADDITIONAL_LINE', '');

COMMIT;
