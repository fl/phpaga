/*
 * phpaga
 * upgrade8.pgsql
 * Florian Lanthaler
 * 2008-04-02
 *
 * Sidebar setting
 *
 */

BEGIN TRANSACTION;

INSERT INTO config (cfg_key, cfg_value) VALUES ('SIDEBAR', 'true');

COMMIT;
