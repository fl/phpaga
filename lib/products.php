<?php
/**
 * phpaga
 *
 * Product management functionality.
 *
 * This file contains the necessary routines to manage products.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2005, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * Gets a product's information
 *
 * @param int    $prodID     Product ID (or product code, see $bycode)
 * @param bool   $bycode     If set to true then $prodID contains the product
 *                           code, otherwise the product ID
 *
 * @return array $peInfo     Array containing the results. A PhPagaError object on failure.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @since phpaga 0.3
 */

function phpaga_products_getInfo($prodID, $bycode = false)
{
    $prodInfo = array();

    if (!isset($prodID) || (!strlen($prodID)))
        return PhPagaError::raiseErrorByCode(PHPAGA_ERR_PRODUCT_INVALIDID);

    if ($bycode)
        $idfield = "products.prod_code";
    else
        $idfield = "products.prod_id";

    if (PHPAGA_DB_SYSTEM == "pgsql")
        $_times = "to_char(prod_created, 'yyyy-mm-dd hh24:mi:ss'::text) as prod_created, ".
            "to_char(prod_lastmod, 'yyyy-mm-dd hh24:mi:ss'::text) as prod_lastmod ";
    else
        $_times = "prod_created, prod_lastmod";

    $query = 'SELECT products.prod_id, prod_code, prod_name, prod_price, prod_consumption, prod_stock,
        products.prcat_id, prcat_title, '.$_times.'
        FROM products
        LEFT JOIN productcat ON productcat.prcat_id = products.prcat_id
        WHERE '.$idfield.' = ?';

    $d = new PhPagaDbData;

    $prodInfo = $d->queryRow($query, $prodID);

    if (PhPagaError::isError($prodInfo) || !count($prodInfo))
        return $prodInfo;

    phpaga_sanitizeArray($prodInfo);

    return $prodInfo;
}


/**
 * Gets all products matching the search parmeters
 * (from $offset to $limit, if set)
 *
 * @param array  $rows      Array containing the results
 * @param int    $count     Number of records matching the query
 * @param array  $prodInfo  Product information
 * @param array  $prodIds   IDs of the resulting products
 * @param int    $order     Specifies the order by field
 *
 * @param int    $offset    Offset (optional)
 * @param int    $limit     Limit (optional)
 *
 * @return array $rows      Array containing the results. A PhPagaError object on failure.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @since phpaga 0.3
 */

function phpaga_products_search(&$count,
                                &$prodIds,
                                $prodInfo,
                                $order = 0,
                                $offset = null,
                                $limit = null)
{

    $count = 0;
    $rows = array();
    $prodIds = array();
    $where = array();
    $qparam = array();

    $countquery = 'SELECT COUNT(products.prod_id) AS count_prod FROM products
                   LEFT JOIN productcat ON productcat.prcat_id = products.prcat_id ';

    if (PHPAGA_DB_SYSTEM == 'pgsql')
        $_times = "to_char(prod_created, 'yyyy-mm-dd hh24:mi:ss'::text) as prod_created,
            to_char(prod_lastmod, 'yyyy-mm-dd hh24:mi:ss'::text) as prod_lastmod ";
    else
        $_times = 'prod_created, prod_lastmod';

    $query = 'SELECT products.prod_id, prod_code, prod_name, prod_price, prod_consumption, prod_stock,
        products.prcat_id, prcat_title, '.$_times.'
        FROM products
        LEFT JOIN productcat ON productcat.prcat_id = products.prcat_id ';

    if (isset($prodInfo['prod_name']) && strlen($prodInfo['prod_name'])) {
        $where[] = 'products.prod_name like ?';
        $qparam[] = '%'.$prodInfo['prod_name'].'%';
    }

    if (isset($prodInfo['prod_code']) && strlen($prodInfo['prod_code'])) {
        $where[] = 'products.prod_code = ?';
        $qparam[] = $prodInfo['prod_code'];
    }

    if (isset($prodInfo['prcat_id']) && is_numeric($prodInfo['prcat_id'])) {
        $where[] = 'products.prcat_id = ?';
        $qparam[] = $prodInfo['prcat_id'];
    }

    if (count($where)) {
        $h = sprintf('WHERE %s ', implode(' AND ', $where));
        $countquery .= $h;
        $query .= $h;
    }

    $query .= ' ORDER BY prod_name ';

    $d = new PhPagaDbData;

    $count_pr = $d->queryOneVal($countquery, $qparam);

    if (!PhPagaError::isError($count_pr))
        $count = $count_pr;

    $products = $d->queryAll($query, $qparam, $offset, $limit);

    if (PhPagaError::isError($products))
        return $products;

    foreach($products as $qrow) {

        phpaga_sanitizearray($qrow);

        $rows[] = $qrow;
        $prodIds[] = $qrow['prod_id'];
    }

    phpaga_sanitizearray($rows);
    return $rows;
}

?>
