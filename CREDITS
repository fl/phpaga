Credit where credit is due
##########################

The following items come with phpaga's sources
==============================================

Icons
-----

| phpaga uses icons from the "Silk" icon set by Mark James.
| Creative Commons Attribution 2.5 License
| http://www.famfamfam.com/lab/icons/silk/

| phpaga also uses icons from the "Flags" icon set by Mark James.
| The flag icons are "Available for free use for any purpose".
| http://www.famfamfam.com/lab/icons/flags/


Color picker
------------

| The "Really Simple Color Picker" is written by Lakshan Perera.
| MIT license
| http://www.web2media.net/laktek/2008/10/27/really-simple-color-picker-in-jquery/

JavaScript dates
----------------

| JavaScript date handling is done with datejs by Coolite Inc.
| MIT License
| http://www.datejs.com/

jQuery and jQuery UI
--------------------

| phpaga comes with the jQuery JavaScript Library
| Dual licensed under the MIT or GPL Version 2 licenses.
| http://jquery.com/

| Also included is jQuery UI
| Dual licensed under the MIT and GPL licenses.
| http://jqueryui.com/

Tooltips
--------

| Tooltips are handled by the jQuery plugin tinyTips.
| http://www.digitalinferno.net/blog/jquery-plugin-tinytips-1-1/

Charts
------

JavaScript charts are generate via jqPlot, "A Versatile and Expandable jQuery
Plotting Plugin". 

| Available under both the MIT and GPL version 2.0 licenses.
| http://www.jqplot.com/

phpaga's source code repository contains a stripped-down version of the jqPlot
release (no examples, only minified .js files).

Sparklines
---------- 

| Sparklines are generated with jQuery Sparklines.
| http://omnipotent.net/jquery.sparkline/
| New BSD License

phpaga's source code repository contains the minified release of jQuery
Sparklines.

ColorBox
--------

| ColorBox, a light-weight, customizable lightbox plugin for jQuery 1.3 and 1.4
| http://colorpowered.com/colorbox/
| MIT License

phpaga's source code repository contains a stripped-down version of the
ColorBox release (one theme, minified .js file only).

smarty-gettext
--------------

| smarty-gettext is a Smarty plugin that provides gettext support
| http://smarty.incutio.com/?page=SmartyGettext
| LGPL

jqGrid
------

jqGrid is an Ajax-enabled JavaScript control that provides solutions for
representing and manipulating tabular data on the web.

| Dual licensed under the MIT and GPL licenses
| http://www.trirand.com/blog/
| http://www.trirand.com/jqgridwiki/

tonic
-----

Tonic is an open source less is more, RESTful Web application development PHP
library.

| MIT License
| http://peej.github.com/tonic/
| https://github.com/peej/tonic

Bundled libraries
================= 

If this file was part of a release package, the following libraries were
bundled:

* Flourish (MIT license) http://flourishlib.com/
* Smarty (LGPL) http://smarty.php.net/
* TCPDF (LGPL) http://www.tcpdf.org/


