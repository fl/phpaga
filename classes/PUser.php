<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PUser extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('usr_login' => 'asc'), $limit=null, $page=null) {
        $conditions['usr_id>'] = 0;
        $set = fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
        $set->precreatePPersons('pe_id');
        $set->precreatePPersons('usr_pe_id');
        return $set;
    }

    public static function findAsArray($conditions=array(), $orderBy=array('usr_login' => 'asc'), $limit=null, $page=null) {

        $set = self::find($conditions, $orderBy, $limit, $page);
        $rows = array();
        foreach ($set as $r)
            $rows[] = $r->asArray();
        return $rows;
    }

    public function asArray() {
        $uvalues = $this->values;
        $uvalues['pe_name'] = $this->createPPerson('pe_id')->getFullName();
        unset($uvalues['usr_passwd']);
        if (strlen($uvalues['usr_settings']))
            $uvalues['usr_settings'] = unserialize($uvalues['usr_settings']);
        $permissions = array();
        $h_perm = explode('|', trim($uvalues['usr_perm']));
        foreach ($h_perm as $h_p)
            $permissions[$h_p] = true;
        $uvalues['usr_perm'] = $permissions;
        return $uvalues;
    }

    public function getAuthUserInfo() {
        $userInfo = array();
        $userInfo['usr_id'] = $this->getUsrId();
        $userInfo['usr_login'] = $this->getUsrLogin();
        $userInfo['pe_id'] = $this->getPeId();
        $userInfo['pe_name'] = $this->createPPerson('pe_id')->getFullName();
        $userInfo['skin'] = $this->getUsrSkin();
        $p = $this->getUsrPerm();
        $permissions = array();
        $h_perm = explode('|', trim($p));
        foreach ($h_perm as $h_p)
            $permissions[$h_p] = true;
        $userInfo['perm'] = $permissions;
        return $userInfo;
    }

    public static function authenticate($login, $passwd) {
        $userID = null;
        if (!strlen($login))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_USER_INVALIDID);

        try {
            $user = new PUser(array('usr_login' => $login));
            $currPasswd = $user->getUsrPasswd();
            if (preg_match('/^fCryptography::password_hash#/', $currPasswd))
                $matchStatus = fCryptography::checkPasswordHash($passwd, $currPasswd);
            else
                // Support for legacy password hashing
                $matchStatus = $currPasswd==md5($passwd);

            if ($matchStatus) {
                PLog::add(sprintf("LOGIN: %s", $login));
                return $user->getUsrId();
            }
        } catch (Exception $e) {
            PLog::add($e->getMessage());
        }

        PLog::toDb(sprintf("LOGIN FAILURE: %s", $login));
        return PhPagaError::raiseErrorByCode(PHPAGA_ERR_USER_INVALIDID);
    }

    public static function count() {
        try {
            return self::find()->count();
        } catch (Exception $e) {
            PLog::add($e->getMessage());
            return 0;
        }
    }

    public function setPassword($s) {
        $this->setUsrPasswd(fCryptography::hashPassword($s));
    }

    public function hasPermission($perm) {
        $p = $this->getUsrPerm();
        if (!is_numeric($perm) || !strlen($p))
            return false;

        $permissions = array();
        $h_perm = explode('|', trim($p));
        foreach ($h_perm as $h_p)
            $permissions[$h_p] = true;

        return isset($permissions[$perm]);
    }

    public static function hasPerm($perm, $uid=null) {

        if (is_null($uid)) {
            return(is_numeric($perm) && isset($_SESSION['auth_user'])
                && isset($_SESSION['auth_user']['perm'])
                && isset($_SESSION['auth_user']['perm'][$perm]));
        } else {
            try {
                $user = new PUser($uid);
                return $user->hasPermission($perm);
            } catch (Exception $e) {
                return false;
            }
        }
    }

    public static function protectPage($perm) {

        if (is_array($perm)) {

            foreach($perm as $p)
                if (self::hasPerm($p))
                    return;

        } else if (self::hasPerm($perm))
            return;

        phpaga_header();
        phpaga_message(_('No access'), _("You don't have the necessary rights to access this page."));
        phpaga_footer();
        die();
    }

}

?>