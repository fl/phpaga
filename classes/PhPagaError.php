<?php
/**
 * phpaga
 *
 * Error handling
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2007, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


class PhPagaError {

    private $medium = 'html';
    private $code = 0;
    private $message = null;
    private $debugMessage = null;
    private $backtrace = null;


    public function __construct($message, $code = null, $debugmsg = null) {

        $this->message = $message;
        $this->code = $code;
        $this->debugMessage = $debugmsg;

        $errtxt = null;

        if (PHPAGA_ERR_VERBOSE && !defined('PHPAGA_DBUPGRADE') && function_exists('debug_backtrace'))
            $this->backtrace = debug_backtrace();

        if (phpaga_strlen($this->code))
            $errtxt .= "Code: ".$this->code."\n";

        if (phpaga_strlen($this->message))
            $errtxt .= "Message: ".$this->message."\n";

        if (phpaga_strlen($this->debugMessage))
            $errtxt .= "Debug information:\n".$this->debugMessage."\n";

        if (!is_null($this->backtrace))
            $errtxt .= "\nBacktrace:\n".phpaga_format_backtrace($this->backtrace);

        if (!defined('PHPAGA_INSTALL'))
            PLog::toDb($errtxt, PHPAGA_LOG_ERR);
    }

    public static function isError($err) {
        return $err instanceof PhPagaError;
    }

    public function raiseError($message, $code = null, $debugmsg = null) {
        $e = new PhPagaError($message, $code, $debugmsg);
        return $e;
    }

    public function raiseErrorByCode($code, $debugmsg = null) {
        global $phpaga_error;

        $h = array();
        $t = null;
        $msg = null;

        if (!is_array($code)) {
            $t = $code;
            $code = array($code);
        }

        foreach ($code as $c)
            if (isset($phpaga_error[$c]))
                $h[] = $phpaga_error[$c];

        $msg = implode("\n", $h);

        $e = new PhPagaError($msg, $t, $debugmsg);
        return $e;
    }

    public function getCode() {
        return $this->code;
    }

    public function getMessage() {
        return $this->message;
    }

    public function printMessage() {

        if ($this->medium == 'html') {

            $tpl = new PSmarty;
            $tpl->assign("type_err", true);
            $tpl->assign("TITLE", _("The following error occured"));
            $tpl->assign("MESSAGE", $this->message);
            $tpl->assign("debugmessage", $this->debugMessage);
            $tpl->assign("backtrace", $this->backtrace);
            $tpl->display("message.tpl.html");

        } else
            print $this->message;

    }

    public function getFormattedMessage() {

        $tpl = new PSmarty;
        $tpl->assign("type_err", true);
        $tpl->assign("TITLE", _("The following error occured"));
        $tpl->assign("MESSAGE", $this->message);
        $tpl->assign("debugmessage", $this->debugMessage);
        $tpl->assign("backtrace", $this->backtrace);
        return $tpl->fetch("message.tpl.html");
    }

  }

?>
