<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /company
 * @uri /company/{id}
 */
class CompanyResource extends PhPagaApi {

    protected $idFieldOp = 'cpn_id=';

    protected $map = array(
    'country.name' => 'countries{ctr_id}.ctr_name',
    'category.id' => 'companycat.ccat_id',
    'category.title' => 'companycat.ccat_id');

    protected $basicSearchOptions = array(
        array('key' => 'cpn_id', 'operator' => '='),
        array('key' => 'cpn_name', 'operator' => '~')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PCompany::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {
        //$category = $row->createPCompanyCategory();
        //$country= $row->createPCountry();
        
        $cat = $row->createPCompanyCategory('ccat_id');
        $category = new stdClass;
        $category->id = $row->getCcatId();
        $category->title = $cat->getCcatTitle();

        $ctr = $row->createPCountry('ctr_id');
        $country = new stdClass;
        $country->id = $row->getCtrId();
        $country->name = $ctr->getCtrName();
        
        $cell = new stdClass();
        $cell->cpn_id = $row->getCpnId();
        $cell->cpn_name = $row->getCpnName();
        $cell->cpn_address = $row->getCpnAddress();
        $cell->cpn_address2 = $row->getCpnAddress2();
        $cell->cpn_email = $row->getCpnEmail();
        $cell->cpn_phone = $row->getCpnPhone();
        $cell->cpn_city = $row->getCpnCity();
        $cell->full_location = $row->getFullLocation();
        $cell->category = $category;
        $cell->country = $country;
        
        return $cell;
    }
    
    protected function delete($request, $id) {

        $response = new Response($request);
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;

        try {
            $company = new PCompany($id);
            $pe_id = $_SESSION['auth_user']['pe_id'];
            if (!$company->hasOwner($pe_id) && !PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERCOMPANIES)) {
                $response->code = Response::FORBIDDEN;
                return $response;
            }
            $company->delete();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Deleted';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

}

?>