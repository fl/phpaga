<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /expense
 * @uri /expense/{id}
 */
class ExpenseResource extends PhPagaApi {

    protected $idFieldOp = 'exp_id=';
    protected $map = array('prj_title' => 'projects.prj_title',
        'pe_person' => 'persons.pe_lastname');

    protected $basicSearchOptions = array(
        array('key' => 'bill_id', 'operator' => '='),
        array('key' => 'prj_id', 'operator' => '='),
        array('key' => 'pe_id', 'operator' => '=')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PExpense::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {

        $person = $row->createPPerson();
        $project = $row->createPProject();

        $cell = new stdClass();
        $cell->exp_id = (int)$row->getExpId(); 
        $cell->bill_id = $row->getBillId(); 
        $cell->pe_id = $row->getPeId(); 
        $cell->prj_id = $row->getPrjId(); 
        $cell->exp_sum = $row->getExpSum(); 
        $cell->exp_date = (string)$row->getExpDate();
        $cell->exp_desc = $row->getExpDesc();
        $cell->pe_person = $person->getFullName();
        $cell->prj_title = $project->getPrjTitle(); 

        return $cell;
    }

    protected function get($request, $id) {

        /* To be able to view an expense other than their own the user must 
         * meet one of these conditions:
         * - is the project manager (if a project is specified)
         * - can manage invoices
         * - can view invoices
         */

        $prj_id = phpaga_fetch_GET('prj_id');
        if (is_numeric($id)) {
            try {
                $exp = new PExpense($id);
                $prj_id = $exp->getPrjId();
                unset($exp);
            } catch (Exception $e) {
                $prj_id = null;
            }
        }
        $current_pe_id = $_SESSION['auth_user']['pe_id'];
        $can_manage = phpaga_project_canmanage($prj_id);

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES)
            && !PUser::hasPerm(PHPAGA_PERM_VIEW_INVOICES)
            && !$can_manage
        ) {
            $_GET['pe_id'] = $current_pe_id;
        }

        return parent::get($request, $id);
    }

    protected function delete($request, $id) {

        $response = new Response($request);

        /* To be able to delete an expense other than their own the user must 
         * meet one of these conditions:
         * - is the project manager
         * - can manage invoices
         */

        try {
            $exp = new PExpense($id);
            if (is_numeric($exp->getBillId())) {
                $response->code = Response::FORBIDDEN;
                return $response;
            }

            $prj_id = $exp->getPrjId();
            $owner = $exp->getPeId();
            unset($exp);
        } catch (Exception $e) {
            $prj_id = null;
        }
        $can_manage = ($owner==$_SESSION['auth_user']['pe_id']) || phpaga_project_canmanage($prj_id);

        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES) && !$can_manage) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;

        try {
            $expense = new PExpense($id);
            $expense->delete();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Deleted';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

}

?>
