<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /operation
 * @uri /operation/{id}
 */
class OperationResource extends PhPagaApi {

    protected $idFieldOp = 'op_id=';
    protected $map = array('prj_title' => 'projects.prj_title',
        'pe_person' => 'persons.pe_lastname');
    protected $basicSearchOptions = array(
            array('key' => 'prj_id', 'operator' => '='),
            array('key' => 'op_desc', 'operator' => '~'),
            array('key' => 'pe_id', 'operator' => '='),
            array('key' => 'bill_id', 'operator' => '=')
        );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return POperation::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {
        $project = $row->createPProject();
        $person = $row->createPPerson();
        $cat = $row->createPOperationCategory();

        $category = new stdClass;
        $category->id = $row->getOpcatId();
        $category->title = $cat->getOpcatTitle();
        
        $cell = new stdClass();
        $cell->op_id = (int)$row->getOpId(); 
        $cell->prj_id = (int)$row->getPrjId(); 
        $cell->prj_title = $project->getPrjTitle(); 
        $cell->op_desc = $row->getOpDesc(); 
        $cell->op_start = (string)$row->getOpStart();
        $cell->op_end = (string)$row->getOpEnd();
        $cell->pe_id = (int)$person->getPeId();
        $cell->pe_person = $person->getFullName();
        $cell->opcat_id = $row->getOpcatId();
        $cell->category = $category;
        $cell->minutes = $row->getOpDuration();
        $cell->can_edit = $row->canEdit();
        $cell->bill_id= (int)$row->getBillId();
        
        return $cell;
    }
}

?>
