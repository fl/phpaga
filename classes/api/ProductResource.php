<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /product
 * @uri /product/{id}
 */
class ProductResource extends PhPagaApi {

    protected $idFieldOp = 'prod_id=';
    protected $map = array();

    protected $basicSearchOptions = array(
        array('key' => 'prod_name', 'operator' => '~'),
        array('key' => 'prod_code', 'operator' => '~'),
        array('key' => 'prcat_id', 'operator' => '=')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PProduct::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {

        $cat = $row->createPProductCategory('prcat_id');

        $cell = new stdClass();
        $cell->prod_id = (int)$row->getProdId(); 
        $cell->prcat_id = is_numeric($row->getPrcatId()) ? (int)$row->getPrcatId() : null;
        $cell->prod_code = $row->getProdCode();
        $cell->prod_name = $row->getProdName(); 
        $cell->prod_price = $row->getProdPrice();
        $cell->prod_consumption= $row->getProdConsumption();
        $cell->prod_stock= $row->getProdStock();

        return $cell;
    }

    protected function post($request) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $product = new PProduct();
        $product->setProdName($data->prod_name);
        $product->setProdCode($data->prod_code);
        $product->setProdPrice($data->prod_price);
        $product->setProdConsumption($data->prod_consumption);
        $product->setProdStock($data->prod_stock);
        $product->setPrcatId($data->prcat_id);

        $r = new stdClass();
        $r->errors = null;

        try {
            $product->store();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Created';
        $response->code = Response::CREATED;
        $response->body = json_encode($r);
        return $response;
    }

    protected function put($request, $id) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;
        try {
            $product = new PProduct($id);
            $product->setProdName($data->prod_name);
            $product->setProdCode($data->prod_code);
            $product->setProdPrice($data->prod_price);
            $product->setProdConsumption($data->prod_consumption);
            $product->setProdStock($data->prod_stock);
            $product->setPrcatId($data->prcat_id);
            $product->store();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Updated';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

    protected function delete($request, $id) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;
        try {
            $product = new PProduct($id);
            $product->delete();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Deleted';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

}

?>
