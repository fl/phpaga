<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2012, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /person
 * @uri /person/{id}
 */
class PersonResource extends PhPagaApi {

    protected $idFieldOp = 'pe_id=';

    protected $map = array(
    'country.name' => 'countries{ctr_id}.ctr_name',
    'category.id' => 'personcat.pecat_id',
    'category.title' => 'personcat.pecat_id',
    'company.name' => 'companies.cpn_name');

    protected $basicSearchOptions = array(
        array('key' => 'pe_id', 'operator' => '='),
        array('key' => 'pe_lastname', 'operator' => '~')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PPerson::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {        
        $cat = $row->createPPersonCategory('pecat_id');
        $category = new stdClass;
        $category->id = $row->getPecatId();
        $category->title = $cat->getPecatTitle();

        $ctr = $row->createPCountry('ctr_id');
        $country = new stdClass;
        $country->id = $row->getCtrId();
        $country->name = $ctr->getCtrName();
        
        $cpn = $row->createPCompany('cpn_id');
        $company = new stdClass;
        $company->id = $row->getCpnId();
        $company->name = $cpn->getCpnName();

        $cell = new stdClass();
        $cell->pe_id = $row->getPeId();
        $cell->pe_lastname = $row->getPeLastname();
        $cell->pe_firstname = $row->getPeFirstname();
        $cell->pe_address = $row->getPeAddress();
        $cell->pe_address2 = $row->getPeAddress2();
        $cell->pe_email = $row->getPeEmail();
        $cell->pe_phonehome = $row->getPePhonehome();
        $cell->pe_phonework = $row->getPePhonework();
        $cell->pe_phonemobile = $row->getPePhonemobile();
        $cell->pe_city = $row->getPeCity();
        $cell->full_location = $row->getFullLocation();
        $cell->category = $category;
        $cell->country = $country;
        $cell->company = $company;
        
        return $cell;
    }
    
    protected function delete($request, $id) {

        $response = new Response($request);
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;

        try {
            $person = new PPerson($id);
            if (!$person->hasOwner($_SESSION['auth_user']['pe_id']) && !PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPERSONS)) {
                $response->code = Response::FORBIDDEN;
                return $response;
            }
            $person->delete();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Deleted';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

}

?>