<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /project
 * @uri /project/{id}
 */
class ProjectResource extends PhPagaApi {

    protected $idFieldOp = 'prj_id=';
    protected $map = array('status.title' => 'projectstatus.prjstat_title',
        'customer.name' => 'companies{cpn_id}.cpn_name',
        'status.id' => 'projectstatus.prjstat_id',
        'status.title' => 'projectstatus.prjstat_id',
        'category.id' => 'projectcat.prjcat_id',
        'category.title' => 'projectcat.prjcat_id');

    protected $basicSearchOptions = array(
        array('key' => 'prj_id', 'operator' => '='),
        array('key' => 'prj_title', 'operator' => '~'),
        array('key' => 'prj_startdate', 'operator' => '='),
        array('key' => 'prj_status', 'operator' => '=')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PProject::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {

        $parent = $row->createPProject('prj_prj_id');
        $cat = $row->createPProjectCategory('prjcat_id');
        $stat = $row->createPProjectStatus();
        $cust = $row->createPCompany('cpn_id');

        $category = new stdClass;
        $category->id = $row->getPrjcatId();
        $category->title = $cat->getPrjcatTitle();

        $status = new stdClass;
        $status->id = $row->getPrjStatus();
        $status->title = $stat->getPrjstatTitle();

        $customer = new stdClass;
        $customer->id = $row->getCpnId();
        $customer->name = $cust->getCpnName();
        
        $cell = new stdClass();
        $cell->prj_id = (int)$row->getPrjId(); 
        $cell->prj_title = $row->getPrjTitle(); 
        $cell->prj_startdate = (string)$row->getPrjStartdate(); 
        $cell->prj_enddate = (string)$row->getPrjEnddate(); 
        $cell->prj_deadline= (string)$row->getPrjDeadline(); 
        $cell->category = $category;
        $cell->status = $status;
        $cell->customer = $customer;
        $cell->prj_priority = (int)$row->getPrjPriority();
        $cell->minutes = (int)$row->getMinutes();
        $cell->is_overdue= (bool)$row->isOverdue();
        $cell->can_edit = $row->canEdit();

        return $cell;
    }
}

?>
