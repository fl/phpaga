<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @namespace PhPaga\API
 * @uri /projectcategory
 * @uri /projectcategory/{id}
 */
class ProjectCategoryResource extends PhPagaApi {

    protected $idFieldOp = 'prjcat_id=';
    protected $map = array();

    protected $basicSearchOptions = array(
        array('key' => 'prjcat_title', 'operator' => '~')
    );

    protected function getRecordSet($conditions, $orderBy, $limit, $page) {
        return PProjectCategory::find($conditions, $orderBy, $limit, $page);
    }

    protected function rowToCell($row) {

        $cell = new stdClass();
        $cell->prjcat_id = (int)$row->getPrjcatId(); 
        $cell->prjcat_title = $row->getPrjcatTitle(); 

        return $cell;
    }

    protected function post($request) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $projectCategory = new PProjectCategory();
        $projectCategory->setPrjcatTitle($data->prjcat_title);
        $r = new stdClass();
        $r->errors = null;

        try {
            $projectCategory->store();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Created';
        $response->code = Response::CREATED;
        $response->body = json_encode($r);
        return $response;
    }

    protected function put($request, $id) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;

        try {
            $projectCategory = new PProjectCategory($id);
            $projectCategory->setPrjcatTitle($data->prjcat_title);
            $projectCategory->store();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Updated';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

    protected function delete($request, $id) {

        $response = new Response($request);
        if (!PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }
        $data = json_decode($request->data);
        $r = new stdClass();
        $r->errors = null;
        $r->obj_id = $id;

        try {
            $projectCategory = new PProjectCategory($id);
            $projectCategory->delete();
        } catch (Exception $e) {
            $r->errors = array();
            $r->errors[] = $e->getMessage();
            $response->code = Response::OK;
            $response->body = json_encode($r);
            return $response;
        }

        $r->message = 'Deleted';
        $response->code = Response::OK;
        $response->body = json_encode($r);
        return $response;
    }

}

?>
