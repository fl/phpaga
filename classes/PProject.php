<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PProject extends PhPagaData {

     public static function find($conditions=array(), $orderBy=array('prj_title' => 'asc'), $limit=null, $page=null) {
        
        $viewall = (PUser::hasPerm(PHPAGA_PERM_VIEW_OTHERPROJECTS) 
            || PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPROJECTS));

        if (!$viewall) {
            $pe_id = $_SESSION['auth_user']['pe_id'];
            $conditions['projects.pe_id=|project_members.pe_id=|projects.prj_manager_pe_id='] = 
                array($pe_id, $pe_id, $pe_id);
        }

        $set = fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
        $set->precreatePCompanies('cpn_id');
        $set->precreatePCompanies('cpn_id_issuebill');
        $set->precreatePProjects('prj_prj_id');
        $set->precreatePPersons('pe_id');
        $set->precreatePPersons('prj_manager_pe_id');
        $set->precreatePProjectCategories('prjcat_id');
        $set->precreatePProjectStatuses('prj_status');
        return $set;
    }

    public function canEdit($pe_id=null) {
        if (is_null($pe_id))
            $pe_id = $_SESSION['auth_user']['pe_id'];

        $isowner = ($this->getPeId()==$pe_id);
        $ismanager = ($this->getPrjManagerPeId()==$pe_id);

        return($isowner && PUser::hasPerm(PHPAGA_PERM_MANAGE_PROJECTS)
            || $ismanager
            || PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPROJECTS));
    }

    public function getMinutes() {
        $db = PhPagaOrmDb::getDbInstance();
        $result = $db->query("SELECT SUM(op_duration) FROM operations WHERE prj_id=%i", (int)$this->getPrjId());
        return $result->fetchScalar();
    }

    /* A project is overdue when a deadline is set, an end date is not set, and 
     * the deadline is in the past.
     */
    public function isOverdue() {
        $deadline = $this->getPrjDeadline();
        $enddate = $this->getPrjEnddate();
        if (!strlen($deadline) || strlen($enddate))
            return false;

        return mktime() >= strtotime($deadline);
    }
}

?>
