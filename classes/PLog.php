<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PLog extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('logtime' => 'desc'), $limit=null, $page=null) {
        return fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
    }

    public static function add($msg, $priority=PHPAGA_LOG_INFO) {
        if (!self::toDb($msg, $priority))
            self::toLog($msg);
    }

    public static function toLog($msg) {
        error_log($msg, 0);
    }

    public static function logVar($msg) {
        self::toLog(print_r($msg, true));
    }

    public static function toFile($msg, $fname=null) {
        if (is_null($fname))
            $fname = PHPAGA_TMPDIR.'/phpaga.log';
        $ts = new fTimestamp('now');
        error_log($ts->format('Y-m-d H:i:s').' '.$msg."\n", 3, $fname);
    }

    public static function toDb($msg, $priority=PHPAGA_LOG_INFO) {

        if (defined('PHPAGA_DBUPGRADE'))
            return;

        if (isset($_SESSION['auth_user']['usr_login']))
            $ident = $_SESSION['auth_user']['usr_login'];
        else
            $ident = '_';

        try {
            $log = new PLog();
            $log->setLogtime(PhPagaOrmDb::getCurrentTimestamp());
            $log->setIdent($ident);
            $log->setPriority($priority);
            $log->setMessage($msg);
            $log->store();
            return true;
        } catch (Exception $e) {
            self::toLog($e->getMessage());
            return false;
        }
    }
}

?>
