<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PLateFee extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('fee_date' => 'desc'), $limit=null, $page=null) {
        return fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
    }

    /**
     * Add Most Recent Late Fee.
     *
     * @return array $info   A PhPagaError object on failure.
     *
     * @author Michael Kimmick <support@nichewares.com>
     * @author Florian Lanthaler <florian@phpaga.net> (adaptation to PLateFee class)
     * @since phpaga 0.4
     */
    public static function add($bill_id) {

        if (!isset($bill_id) || !is_numeric($bill_id))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_INVALID_PARAMTYPE);

        $d = new PhPagaDbData;

        /* Get Late Fee percentage */
        $r = $d->queryAll('Select cfg_value FROM config WHERE cfg_key = ?', 'PHPAGA_LATE_FEE');
        foreach ($r as $b) {
            $fee_pct = $b['cfg_value'];
        }

        /* Get bill_bill_endsum, any payments, and any previous late fees; calculate
        unpaid balance, compute new late fee from that */
        
        //  get endsum for bill
        try {
            $bill = new PBill($bill_id);
            $endsum = $bill->getBillEndsum();
        } catch (Exception $e) {
            return PhPagaError::raiseError($e->getMessage());
        }

        // get any pre-existing payments for bill
        $r = $d->queryAll('Select Sum(pmt_amt) as pre_pmts FROM payments WHERE bill_id = ?', $bill_id);
        foreach ($r as $b) {
            if ($b['pre_pmts'] <> null) {
                $pre_pmts = $b['pre_pmts'];
            } else {
                $pre_pmts = 0;
            }

        }
        // get any pre-existing latefees for bill
        $r = $d->queryAll('Select Sum(fee_amt) as pre_fees FROM latefees WHERE bill_id = ?', $bill_id);
        foreach ($r as $b) {
            if ($b['pre_fees'] <> null) {
                $pre_fees = $b['pre_fees'];
            } else {
                $pre_fees = 0;
            }
        }

        // unpaid balance
        $unpaid = $endsum - $pre_pmts + $pre_fees;
        $late_fee_amt = $unpaid * ($fee_pct/100);

        try {
            $lateFee = new PLateFee();
            $lateFee->setBillId($bill_id);
            $lateFee->setFeeAmt($late_fee_amt);
            $lateFee->store();
        } catch (Exception $e) {
            return PhPagaError::raiseError($e->getMessage());
        }

        return true;

    }
}

?>