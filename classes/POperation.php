<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class POperation extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('op_start' => 'desc'), $limit=null, $page=null) {

        $viewother = (PUser::hasPerm(PHPAGA_PERM_VIEW_ALLTASKS) || PUser::hasPerm(PHPAGA_PERM_MANAGE_ALLTASKS));
        if (!$viewother) {
            $prj_id = isset($conditions['prj_id=']) ? $conditions['prj_id='] : null;
            if (is_numeric($prj_id)) 
                $viewother = PUser::hasPerm(PHPAGA_PERM_VIEW_OTHERMEMTASKS) &&
                phpaga_project_is_member($prj_id, $_SESSION['auth_user']['pe_id']);
        }

        if (!$viewother)
            $conditions['pe_id='] = $_SESSION['auth_user']['pe_id'];

        $finalOrderBy = array();
        foreach($orderBy as $o => $s) {

            switch ($o) {
            case 'pe_person': 
                $finalOrderBy['persons.pe_lastname'] = $s; 
                $finalOrderBy['persons.pe_firstname'] = $s; 
                break;
            case 'category.title': 
                $finalOrderBy['operationcat.opcat_title'] = $s; 
                break;
            default: 
                $finalOrderBy[$o] = $s; 
                break;
            }
        }

        $set = fRecordSet::build(__CLASS__, $conditions, $finalOrderBy, $limit, $page);      
        $set->precreatePProjects('prj_id');
        $set->precreatePPersons('pe_id');
        $set->precreatePOperationCategories('opcat_id');
        return $set;
    }

    public function canEdit() {
        $perm_tasks = PUser::hasPerm(PHPAGA_PERM_MANAGE_ALLTASKS);
        return (!strlen($this->getBillId()) && ($perm_tasks || ($this->getPeId() == $_SESSION['auth_user']['pe_id'])));
    }
}

?>
