<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Base database class.
 * Sets up flourishlib's ORM layer.
 */
class PhPagaOrmDb {

    private static $db = null;

    public static function init() {
        switch(PHPAGA_DB_SYSTEM) {
        case 'pgsql': $_dbsystem = 'postgresql'; break;
        case 'mysql': $_dbsystem = 'mysql'; break;
        default:
            die(_('Unsupported database system: '.PHPAGA_DB_SYSTEM));
        }

        self::$db = new fDatabase($_dbsystem, PHPAGA_DB_NAME, PHPAGA_DB_LOGIN, PHPAGA_DB_PASSWD, PHPAGA_DB_HOST);
        fORMDatabase::attach(self::$db);

        fORM::mapClassToTable('PBill', 'bills');
        fORM::mapClassToTable('PBillingMethod', 'billing_methods');
        fORM::mapClassToTable('PCompany', 'companies');
        fORM::mapClassToTable('PCompanyCategory', 'companycat');
        fORM::mapClassToTable('PConfig', 'config');
        fORM::mapClassToTable('PCountry', 'countries');
        fORM::mapClassToTable('PCurrency', 'currencies');
        fORM::mapClassToTable('PExpense', 'expenses');
        fORM::mapClassToTable('PExpense', 'expenses');
        fORM::mapClassToTable('PFile', 'files');
        fORM::mapClassToTable('PJobCategory', 'jobcat');
        fORM::mapClassToTable('PLateFee', 'latefees');
        fORM::mapClassToTable('PLog', 'log');
        fORM::mapClassToTable('PMaterial', 'material');
        fORM::mapClassToTable('POperation', 'operations');
        fORM::mapClassToTable('POperationCategory', 'operationcat');
        fORM::mapClassToTable('PPayment', 'payments');
        fORM::mapClassToTable('PPerson', 'persons');
        fORM::mapClassToTable('PPersonCategory', 'personcat');
        fORM::mapClassToTable('PProduct', 'products');
        fORM::mapClassToTable('PProductCategory', 'productcat');
        fORM::mapClassToTable('PProject', 'projects');
        fORM::mapClassToTable('PProjectCategory', 'projectcat');
        fORM::mapClassToTable('PProjectMember', 'project_members');
        fORM::mapClassToTable('PProjectHourlyrates', 'projects_hourlyrates');
        fORM::mapClassToTable('PProjectStatus', 'projectstatus');
        fORM::mapClassToTable('PQuotation', 'quotations');
        fORM::mapClassToTable('PUser', 'users');

        fGrammar::addCamelUnderscoreRule('CpnCCatId', 'ccat_id');
        fGrammar::addCamelUnderscoreRule('CpnAddress2', 'cpn_address2');
        fGrammar::addCamelUnderscoreRule('PeAddress2', 'pe_address2');
    }

    public static function getDbInstance() { return self::$db; }

    public static function getCurrentTimestamp() {
        $res = self::$db->query('SELECT CURRENT_TIMESTAMP');
        return $res->fetchScalar();
    }

}

?>