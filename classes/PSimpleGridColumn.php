<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PSimpleGridColumn {

    public $hidden = false;
    public $name = '';
    public $index = '';

    public function __construct($name='', $hidden=false) {
        $this->name = $name;
        $this->index = $name;
        $this->hidden = $hidden;
    }

    public function enableSearch($opts) {
        if (!is_array($opts))
            $opts = array($opts);
        $searchoptions = new stdClass();
        //$searchoptions->sopt = $opts;
        $this->searchoptions = $opts;
    }

    public function setRequired($required=true) {
        if (!in_array('editrules', array_keys(get_object_vars($this))))
            $this->editrules = new stdClass();
        $this->editrules->required = true;
    }

    /**
     * Sets the field type for advanced search and edit forms.
     * Valid values are:
     * 'number', 'integer', 'email', 'url', 'date', 'time'
     *
     * @param string $type
     * @return void
     */
    public function setType($type) {
        if (!in_array('editrules', array_keys(get_object_vars($this))))
            $this->editrules = new stdClass();
        $this->editrules->$type = true;
        if (!in_array('searchrules', array_keys(get_object_vars($this))))
            $this->searchrules = new stdClass();
        $this->searchrules->$type = true;
        switch($type) {
        case 'number':
        case 'integer':
        case 'date':
        case 'time':
            if (!isset($this->align) && (!isset($this->stype) || ($this->stype!='select')))
                $this->align = 'right';
            break;
        default:
            break;
        }

    }

    public function setEditOptions($options) {
        if (is_array($options) && count($options) && !in_array('editoptions', array_keys(get_object_vars($this))))
            $this->editoptions = new stdClass();
        foreach ($options as $k => $v)
        $this->editoptions->$k = $v;
    }
}

?>
