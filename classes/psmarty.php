<?php
/**
 * phpaga
 *
 * smarty compiling template engine
 *
 * This file contains our little class derived from Smarty.

 * Smarty is an excellent template engine writte by Monte Ohrt and Andrei Zmievski.
 * You can find Smarty at http://www.phpinsider.com/php/code/Smarty/
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Configure smarty-gettext directory */
if (!defined('SMARTYGETTEXT_DIR'))
    define('SMARTYGETTEXT_DIR', PHPAGA_EXTPATH.'/smarty-gettext/');

/* Configure and include Smarty */
if (!defined('SMARTY_DIR'))
    define('SMARTY_DIR', PHPAGA_EXTPATH.'smarty/libs/');

/* include the original smarty class */
require_once(SMARTY_DIR.'Smarty.class.php');
require_once(SMARTY_DIR.'plugins/shared.make_timestamp.php');
require_once(SMARTYGETTEXT_DIR.'block.t.php');

/**
 * Smarty date_format modifier plugin (modified for PHPAGA)
 *
 * @author   Monte Ohrt <monte at ohrt dot com>, Christian Graffer
 * @param string
 * @param string
 * @param string
 * @return string|void
 * @uses smarty_make_timestamp()
 */
function phpaga_modifier_date_format($string, $format = '%b %e, %Y', $default_date = '')
{
    $oldlocale = setlocale(LC_TIME, 0);
    setlocale(LC_TIME, PHPAGA_LANGNAME);
    if ($string != '') {
        $timestamp = smarty_make_timestamp($string);
    } elseif ($default_date != '') {
        $timestamp = smarty_make_timestamp($default_date);
    } else {
        return;
    }
    if (DIRECTORY_SEPARATOR == '\\') {
        $_win_from = array('%D',       '%h', '%n', '%r',          '%R',    '%t', '%T');
        $_win_to   = array('%m/%d/%y', '%b', "\n", '%I:%M:%S %p', '%H:%M', "\t", '%H:%M:%S');
        if (strpos($format, '%e') !== false) {
            $_win_from[] = '%e';
            $_win_to[]   = sprintf('%\' 2d', date('j', $timestamp));
        }
        if (strpos($format, '%l') !== false) {
            $_win_from[] = '%l';
            $_win_to[]   = sprintf('%\' 2d', date('h', $timestamp));
        }
        $format = str_replace($_win_from, $_win_to, $format);
    }
    $timestring = strftime($format, $timestamp);
    setlocale(LC_TIME, $oldlocale);
    return $timestring;
}

/**
 * This class extends the Smarty class and sets some default variables (paths).
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @since phpaga 0.1
 */

/* extend it to set some default variables */
class PSmarty extends Smarty {


        function __construct() {
            parent::__construct();

            $this->template_dir = PHPAGA_TPLPATH;
            $this->compile_dir = PHPAGA_BASEPATH.'templates_c';
            $this->config_dir = SMARTY_DIR.'/configs';
            $this->plugins_dir = SMARTY_DIR.'/plugins';
            $this->force_compile = true;
            
            if (!defined('PHPAGA_ERR_VERBOSE') || (PHPAGA_ERR_VERBOSE === false))
                $this->error_reporting = E_ALL & ~E_NOTICE;
            else
                $this->error_reporting = E_ALL;

            $this->registerPlugin('modifier', 'trnc', 'phpaga_truncate');
            $this->registerPlugin('modifier', 'wash', 'phpaga_sanitize_htmlout');
            $this->registerPlugin('modifier', 'wash_popup', 'phpaga_popup_sanitizestr');
            $this->registerPlugin('modifier', 'wash_jsmsg', 'phpaga_jsmessage_sanitizestr');
            $this->unregisterPlugin('modifier', 'date_format');
            $this->registerPlugin('modifier', 'date_format', 'phpaga_modifier_date_format');

            $this->registerPlugin('block', 't', 'smarty_block_t');
        }
}




?>
