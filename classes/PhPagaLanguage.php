<?php
/**
 * phpaga
 *
 * Language / locale
 *
 * This class takes care of including the right language file for the current
 * user.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2012, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PhPagaLanguage {
    
    private static $languages = array(
        'en' => array('en_US.UTF8', 'en_US.UTF-8', 'en_GB.UTF8', 'en_GB.UTF-8', 'en_ZA.UTF8', 'en_ZW.UTF8',
        'en_AU.UTF8', 'en_AU.UTF-8', 'en_BW.UTF8', 'en_CA.UTF8', 'en_CA.UTF-8', 'en_DK.UTF8',
        'en_HK.UTF8', 'en_IE.UTF8', 'en_IE.UTF-8', 'en_NZ.UTF8', 'en_NZ.UTF-8', 'en_PH.UTF8',
        'en_SG.UTF8',
        'en_US.iso885915@euro', 'en_US.ISO88591-15',
        'en_US.ISO88591-1', 'en_US.iso88591',
        'en_US.ISO8859-15', 'en_US.ISO8859-1',
        'en_US', 'eng'),
        'it' => array('it_IT.UTF8', 'it_IT.UTF-8', 'it_IT', 'ita'),
        'es' => array('es_ES.UTF8', 'es_ES.UTF-8', 'es_ES', 'esp'),
        'hu' => array('hu_HU.UTF8', 'hu_HU.UTF-8', 'hu_HU', 'hungarian'),
        'no' => array('nb_NO.UTF8', 'nb_NO.UTF-8', 'nb_NO', 'norwegian'),
        'nl' => array('nl_NL.UTF8', 'nl_NL.UTF-8', 'nl_NL', 'nld'),
        'da' => array('da_DK.UTF8', 'da_DK.UTF-8', 'da_DK', 'dan'),
        'de' => array('de_DE.UTF8', 'de_DE.UTF-8', 'de_DE', 'deu', 'German_Germany.1252'),
        'fr' => array('fr_FR.UTF8', 'fr_FR.UTF-8', 'fr_FR', 'fra'),
        'ru' => array('ru_RU.UTF8', 'ru_RU.UTF-8', 'ru_RU', 'russian'),
        'sv' => array('sv_SE.UTF8', 'sv_SE.UTF-8', 'sv_SE', 'swedish')
    );

    private static $language_names = array(
        'en' => 'English',
        'it' => 'Italiano',
        'es' => 'Español (Castellano)',
        'hu' => 'Magyar',
        'no' => 'Norsk',
        'nl' => 'Nederlands',
        'da' => 'Dansk',
        'de' => 'Deutsch',
        'fr' => 'Française',
        'ru' => 'Русский язык',
        'sv' => 'Svenska'
    );

    public static function getLocaleArray() {
        return self::$languages;
    }

    public static function getList() {
        return self::$language_names;
    }
       
    public static function init() {
        $lng_code = defined('PHPAGA_LANGUAGE') && strlen('PHPAGA_LANGUAGE') ? PHPAGA_LANGUAGE : 'en';

        if ((basename($_SERVER['PHP_SELF']) == 'install.php') && isset($REQUEST_DATA['lang']) && strlen($REQUEST_DATA['lang']))
            $phpaga_selected_language = $REQUEST_DATA['lang'];
        else
            $phpaga_selected_language = $lng_code;

        if (!strlen($phpaga_selected_language))
            die('Unable to determine the selected language. Please report this error.');

        $langname = setlocale(LC_ALL, self::$languages[$phpaga_selected_language]);

        if ($langname === false) {
            print(sprintf("WARNING: Unable to set LC_ALL to '%s' make sure the desired locale is installed. Falling back to English...", self::$language_names[$phpaga_selected_language]));
            $langname = setlocale(LC_ALL, self::$languages['en']);
        }
        
        if (strstr(strtoupper(PHP_OS), 'WIN')) {
            putenv('LANG='.$langname);
            putenv('LANGUAGE='.$langname);
        }

        define('PHPAGA_LANGNAME', $langname);
        putenv("LC_ALL=$langname");
        putenv('LC_NUMERIC=C');
        putenv('LC_TIME=C');
        putenv('LC_MONETARY=C');

        if (setlocale(LC_NUMERIC, 'C') === false)
            die('Unable to set LC_NUMERIC to C');

        if (setlocale(LC_TIME, 'C') === false)
            die('Unable to set LC_TIME to C');

        if (setlocale(LC_MONETARY, 'C') === false)
            die('Unable to set LC_MONETARY to C');

        textdomain('messages');
        bindtextdomain('messages', PHPAGA_LNGPATH);
    }
}

?>