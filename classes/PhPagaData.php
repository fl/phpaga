<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Base class used by all other classes that give acces to data.
 */
class PhPagaData extends fActiveRecord {

    public function asArray() {
        return $this->values;
    }

    /**
     * Returns data as a string that can be used for a combo box (select) in a jqGrid.
     *
     * @param fRecordSet $records         This argument is required because Late Static Bindings are available only for PHP >= 5.3.0, and a call to self::find() would not work.
     * @param bool       $first           Should a custom entry be placed at the top of the selection?
     * @param string     $firstText       Text of the first entry
     * @param value      $firstValue      Value of the first entry
     * @param arrray     $buildItemStr    Array with method names. The first method name is called to determine the value, the second method name is called to fetch the text to be displayed. If this parameter is null, the method getSelectionItemStr() of the class is used to build an item option string.
     * @return string
     */
    public static function getGridSelectionStrBase($records, $first=true, $firstText=null, $firstValue='', $buildItemStr) {
        $s = '';
        if ($first) {
            if (is_null($firstText))
                $firstText = _('All');
            if (strlen($firstText))
                $firstText = '- '.$firstText." -";
            $s = $firstValue.':'.$firstText;
        }
        if (is_null($records))
            return $s;

        if (is_array($buildItemStr)) {
            $kfunc = $buildItemStr[0];
            $vfunc = $buildItemStr[1];
        }
        foreach ($records as $r) {
            if (strlen($s))
                $s .= ';';
            if (!is_array($buildItemStr)) 
                $s .= $r->getSelectionItemStr();
            else
                $s .= $r->$kfunc().':'.$r->$vfunc();
        }
        return $s;
    }

    protected function getSelectionItemStr() {
        return '-:-';
    }

}

?>
