<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PPersonCategory extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('pecat_title' => 'asc'), $limit=null, $page=null) {
        return fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
    }

    public static function getSimpleArray($assoc=true, $first=null) {
        $categories = array();
        if (!is_null($first) && $assoc)
            $categories[''] = $first;
        $set = self::find();
        foreach ($set as $c) {
            if ($assoc)
                $categories[$c->getPecatId()] = $c->getPecatTitle();
            else
                $categories[] = array('pecat_id' => $c->getPecatId(), 'pecat_title' => $c->getPecatTitle());
        }
        return $categories;
    }

    public static function getGridSelectionStr($first=true, $firstText=null, $firstValue='') {
        return parent::getGridSelectionStrBase(self::find(), $first, $firstText, $firstValue, array('getPecatId', 'getPecatTitle'));
    }

}

?>
