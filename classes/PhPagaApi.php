<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Contains various helper methods for the data API.
 *
 */
abstract class PhPagaApi extends Resource {

    /**
     * Maps fields names, needed for foreign key relationships and their tables.
     */

    protected $map = array();

    protected $basicSearchOptions = array();

    protected $idFieldOp = null;

    /* Required permissions for GET, POST, PUT, DELETE operations. */
    protected $permGet = null;
    protected $permPost = null;
    protected $permPut = null;
    protected $permDelete = null;
    

    /**
     * Builds search options valid for working with a Flourish fRecordSet.
     */

    private static function buildSearchOptions($param) {
        if (!is_array($param))
            return array();

        $conditions = array();
        foreach ($param as $p) {
            if (!isset($p['field']) || is_null($p['field']))
                $p['field'] = $p['key'];

            $val = fRequest::get($p['key']);
            if (!is_null($val))
                $conditions[$p['key'].$p['operator']] = $val;
        }
        return $conditions;
    }

    /**
     * Builds search options valid for working with a Flourish fRecordSet.
     * The data (filter) is coming from a jqGrid multiple search.
     */

    private static function buildMultipleSearchOptions($filters, $map=array()) {

        if (!is_array($map))
            $map = array($map);

        $conditions = array();
        $filters = json_decode($filters);
        $rules = $filters->rules;
        if (!is_array($rules) || !count($rules))
            return $conditions;

        $keys = array();
        $vals = array();
        foreach ($rules as $rule) {
            $field = isset($map[$rule->field]) && strlen($map[$rule->field]) ? $map[$rule->field] : $rule->field;
            $op = self::mapOperator($rule->op);
            if (!is_null($op) && strlen($op) && strlen($rule->data)) {
                $keys[] = $field.$op;
                $vals[] = $rule->data;
            }
        }
        if ($filters->groupOp=='OR') {
            $keys = implode('|', $keys);
            $conditions = array($keys => $vals);
        } else {
            foreach ($keys as $i => $k)
                $conditions[$k] = $vals[$i];
        }
        return $conditions;
    }

    /**
     * Maps operators from jqGrid to Flourish notation.
     */

    private static function mapOperator($op) {
        switch ($op) {
        /* 
         * Supported operators 
         * The following jqGrid operators can be mapped to Flourish operators 
         * (see http://flourishlib.com/docs/fRecordSet)
         */
        case 'eq': return '='; 
        case 'ne': return '!'; 
        case 'lt': return '<'; 
        case 'le': return '<='; 
        case 'gt': return '>'; 
        case 'ge': return '>='; 
        case 'bw': return '^~'; 
        case 'ew': return '$~'; 
        case 'cn': return '~'; 
        case 'nc': return '!~';
        /*
         * (Flourish's '=' and '!' could handle NULL and NOT NULL, 
         * but in the forms we cannot distinguish between an empty string and a NULL)
         */
        case 'nu': // is null 
        case 'nn': // is not null
            /* Currently not supported operators */
        case 'in': // is in
        case 'ni': // is not in
        case 'bn': // does not begin with
        case 'en': // does not end with
        default:
            return null;
        }
        return null;
    }

    /**
     * Maps column names between their grid/api name and their real column name.
     */

    private function mapColumnName($cn) {
        return !empty($this->map[$cn])?$this->map[$cn]:$cn;
    }

    /**
     * This method needs to be implemented individually by all child classes.
     * $row is a Flourish fActiveRecord
     * An associative array with the keys 'id' (int) and 'cell (array) must be 
     * returned. See OperationResource.php for a reference implementation.
     */
    protected abstract function rowToCell($row);

    /**
     * This method returns the data as a Flourish fRecordSet.
     *
     * This method must implemented in each single child class because it is 
     * possible to reference a class using a variable only for PHP >= 5.3.0.
     *
     * Otherwise we could address the class directly:
     *
     * protected $dataClass = 'PProject';
     * [...]
     * $this->dataClass::find($conditions, $orderBy, $limit, $page);
     */

    protected abstract function getRecordSet($conditions, $orderBy, $limit, $page);


    /**
     * This method handles the actual request, fetches and prepares the data as required, 
     * then returns a Tonic response with a json-encoded body.
     */

    protected function get($request, $id) {

        $response = new Response($request);
        if (!is_null($this->permGet) && !PUser::hasPerm($this->permGet)) {
            $response->code = Response::FORBIDDEN;
            return $response;
        }

        $map = $this->map;

        $page = fRequest::get('page', 'integer', 1);
        $limit = fRequest::get('rows', 'integer', 10);
        $sidx = fRequest::get('sidx');
        $sord = fRequest::get('sord', 'string', 'asc');
        $search = fRequest::get('_search', 'string') == 'true';
        $filters = (fRequest::get('filters'));
        $conditions = array();

        if ($search && !is_null($filters)) {
            $conditions = self::buildMultipleSearchOptions($filters, $map);
        }

        $optConditions = self::buildSearchOptions($this->basicSearchOptions );

        $conditions = array_merge($conditions, $optConditions);
        if (strlen($id))
            $conditions[$this->idFieldOp] = (int)$id;

        $orderBy = !is_null($sidx) ? array($this->mapColumnName($sidx) => $sord) : array();

        $etag = md5($request->uri);
        if ($request->ifNoneMatch($etag)) {
            $response->code = Response::NOTMODIFIED;
        } else {

            $response->code = Response::OK;
            $response->addHeader('Content-type', 'application/json');
            $response->addEtag($etag);
           
            $rows = array();
            $r = $this->getRecordSet($conditions, $orderBy, $limit, $page);
            foreach($r->getRecords() as $row)
                $rows[] = $this->rowToCell($row);
            
            $data = array('page' => $page, 'total' => $r->getPages(), 'records' => $r->count(true), 'rows' => $rows);
            $response->body = json_encode($data);
        }

        return $response;
    }
}

?>
