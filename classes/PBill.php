<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PBill extends PhPagaData {

    public static function find($conditions=array(), $orderBy=array('bill_date' => 'desc'), $limit=null, $page=null) {
        return fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
    }
    
    public function getPaymentSum() {
        $amount = 0;

        try {
            $result = PhPagaOrmDb::getDbInstance()->query('SELECT SUM(pmt_amt) FROM payments WHERE bill_id = %i', $this->getBillId());
            $amount  = $result->fetchScalar();
        } catch (Exception $e) {
            if (!($e instanceof fNoRowsException))
                throw $e;
        }

        return $amount;          
    }
    
    public function setBillPaydateAuto() {
        try {
            $result = PhPagaOrmDb::getDbInstance()->query('SELECT MAX(pmt_date) FROM payments WHERE bill_id = %i', $this->getBillId());
            $date  = $result->fetchScalar();            
            $this->setBillPaydate($date);
        } catch (Exception $e) {
            if (!($e instanceof fNoRowsException))
                throw $e;
        }
    }

    public function getLateFeeSum() {
        $amount = 0;

        try {
            $result = PhPagaOrmDb::getDbInstance()->query('SELECT SUM(fee_amt) FROM latefees WHERE bill_id = %i', $this->getBillId());
            $amount  = $result->fetchScalar();
        } catch (Exception $e) {
            if (!($e instanceof fNoRowsException))
                throw $e;
        }

        return $amount;                  
    }

}

?>