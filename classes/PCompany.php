<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This class handles company entities.
 */
class PCompany extends PhPagaData {

    /**
     * Configures special columns.
     */
    protected function configure() {
        fORMColumn::configureEmailColumn($this, 'cpn_email');
        fORMColumn::configureLinkColumn($this, 'cpn_url');
    }

    /**
     * Returns companies matching the search criteria.
     * 
     * @param array $conditions  Search criteria
     * @param array $orderBy     Sort order
     * @param int   $limit       Number of records to fetch
     * @param int   $page        Page offset when limiting records
     * @return fRecordSet        Set of fActiveRecord objects
     */
    public static function find($conditions=array(), $orderBy=array('cpn_name' => 'asc'), $limit=null, $page=null) {
        $set = fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
        $set->precreatePCountries('ctr_id');
        return $set;
    }

    public static function getSimpleArray($assoc=true, $first=null) {
        $companies = array();
        if (!is_null($first) && $assoc)
            $companies[''] = $first;
        $set = self::find();
        foreach ($set as $c) {
            if ($assoc) {
                $name = $c->getCpnName();
                if (strlen($c->getCpnCity()))
                    $name .= sprintf(' (%s)', $c->getCpnCity());
                $companies[$c->getCpnId()] = $name;
            } else
                $companies[] = array(
                    'cpn_id' => $c->getCpnId(), 
                    'cpn_name' => $c->getCpnName(),
                    'cpn_city' => $c->getCpnCity());
        }
        return $companies;
    }
    
    public function getFullLocation() {
        return phpaga_getFullLocation($this->getCpnCity(), $this->getCpnRegion(), $this->getCpnZip(), '');
    }
    
    public function hasOwner($pe_id) {
        return ((int)$pe_id == (int)$this->getPeId());
    }

    /**
    * Gets a company's information
    *
    * @param int    $cpnID   Company ID
    *
    * @return array  $peInfo Array containing the results. A PhPagaError object on failure.
    *
    * @author Florian Lanthaler <florian@phpaga.net>
    * @since phpaga 0.1
    */

    public static function getInfo($cpnID) {
        $cpnInfo = array();
        $prjInfo = array();
        $billInfo = array();
        $rows = array();
        $count = 0;
        $summary = array();

        if (!isset($cpnID) || !is_numeric($cpnID))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_COMPANY_INVALIDID);

        $query = 'SELECT companies.cpn_id, cpn_name, cpn_address, cpn_address2, cpn_city,
            cpn_zip, cpn_region, companies.ctr_id, cpn_defaultpayterm,
            ctr_name, ctr_code, cpn_logoname,
            companies.ccat_id, ccat_title, cpn_taxnr,
            cpn_url, cpn_phone, cpn_fax, cpn_email, cpn_owner_taxnr, companies.pe_id,
            p2.pe_lastname as owner_pe_lastname,
            p2.pe_firstname as owner_pe_firstname
            FROM companies
            LEFT JOIN companycat ON companycat.ccat_id = companies.ccat_id
            LEFT JOIN countries on countries.ctr_id = companies.ctr_id
            LEFT JOIN persons p2 ON p2.pe_id = companies.pe_id
            WHERE companies.cpn_id = ?';

        $d = new PhPagaDbData;
        $cpnInfo = $d->queryRow($query, $cpnID);

        if (PhPagaError::isError($cpnInfo) || !count($cpnInfo))
            return $cpnInfo;

        $cpnInfo['location'] = phpaga_getLocation($cpnInfo['cpn_city'], $cpnInfo['cpn_region']);

        $cpnInfo['full_location'] = phpaga_getFullLocation($cpnInfo['cpn_city'],
                                                        $cpnInfo['cpn_region'],
                                                        $cpnInfo['cpn_zip'],
                                                        $cpnInfo['ctr_name']);

        $cpnInfo['owner_pe_person'] = PPerson::getFormattedName($cpnInfo['owner_pe_lastname'], $cpnInfo['owner_pe_firstname']);

        phpaga_sanitizeArray($cpnInfo);

        $billInfo = array('cpn_id' => $cpnID);

        $rows = phpaga_bills_search($count, $billInfo);

        if (!PhPagaError::isError($rows))
            $cpnInfo['bills'] = $rows;

        $quotInfo = array('cpn_id' => $cpnID);

        $rows = phpaga_quotations_search($count, $quotInfo);

        if (!PhPagaError::isError($rows))
            $cpnInfo['quotations'] = $rows;

        $prjInfo['cpn_id'] = $cpnID;

        $rows = phpaga_projects_search($count, $duration, $prjIds, $prjInfo);

        if (!PhPagaError::isError($rows))
            $cpnInfo['projects'] = $rows;
        else
            return PhPagaError::raiseErrorByCode($rows);

        $summary = phpaga_bills_getSummaryCpn($cpnID);

        if (!PhPagaError::isError($summary))
            $cpnInfo['billing_summary'] = $summary;
        else
            return PhPagaError::raiseErrorByCode($summary);

        return $cpnInfo;
    }

}

?>