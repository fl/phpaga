<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class PPerson extends PhPagaData {

    protected function configure() {
        fORMColumn::configureEmailColumn($this, 'pe_email');
        fORMColumn::configureLinkColumn($this, 'pe_url');
    }

    public static function find($conditions=array(), $orderBy=array('pe_lastname' => 'asc'), $limit=null, $page=null) {
        $set = fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
        $set->precreatePCountries('ctr_id');
        $set->precreatePCompanies('cpn_id');
        return $set;
    }

    public function hasOwner($pe_pe_id) {
        return ((int)$pe_pe_id == (int)$this->getPeId());
    }

    public function getFullName() {

        $lastname = $this->getPeLastname();
        $firstname = $this->getPeFirstname();
        $formattedname = '';

        if (strlen($lastname) && strlen($firstname)) {
            $formattedname = str_replace("%LASTNAME", $lastname, PHPAGA_PENAME_FORMAT);
            $formattedname = str_replace("%FIRSTNAME", $firstname, $formattedname);
        }
        elseif (strlen($lastname))
            $formattedname = $lastname;
        elseif (strlen($firstname))
            $formattedname = $firstname;

        return $formattedname;
    }
    
    public function getFullLocation() {
        return phpaga_getFullLocation($this->getPeCity(), $this->getPeRegion(), $this->getPeZip(), '');
    }

    
    /**
    * Return a person's name formatted according to PHPAGA_PENAME_FORMAT
    *
    * @param  string  $lastname   Person's last name
    * @param  string  $firstname  Person's first name
    *
    * @return string  $name       Formatted name
    *
    * @author Florian Lanthaler <florian@phpaga.net>
    * @since phpaga 0.1.2
    */

    public static function getFormattedName($lastname = '', $firstname = '') {

        $formattedname = '';

        /* Format the name if both last and first name are defined,
        * otherwise just return whatever is available
        */

        if (strlen($lastname) && strlen($firstname)) {
            $formattedname = str_replace("%LASTNAME", $lastname, PHPAGA_PENAME_FORMAT);
            $formattedname = str_replace("%FIRSTNAME", $firstname, $formattedname);
        }
        elseif (strlen($lastname))
            $formattedname = $lastname;
        elseif (strlen($firstname))
            $formattedname = $firstname;

        return $formattedname;
    }


    /**
    * Gets a person's information
    *
    * @param  int    $peID   person ID
    *
    * @return array  $peInfo Array containing the results. A PhPagaError object on failure.
    *
    * @author Florian Lanthaler <florian@phpaga.net>
    * @since phpaga 0.1
    */

    public static function getInfo($peID) {
        $peInfo = array();
        $persprj = array();

        if (!isset($peID) || !is_numeric($peID))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_PERSON_INVALIDID);

        $query = 'SELECT p.pe_id, p.pe_lastname, p.pe_firstname,
            p.pe_address, p.pe_address2, p.pe_city, p.pe_zip, p.pe_region,
            p.ctr_id, ctr_name, ctr_code,
            p.pecat_id, pecat_title, p.pe_taxnr, p.pe_url,
            p.pe_phonework, p.pe_phonemobile, p.pe_phonehome,
            p.pe_fax, p.pe_email, p.jcat_id, p.cpn_id,
            p.pe_note, p.pe_hourlyrate, cpn_name, jcat_title,
            p.pe_pe_id, p2.pe_id as owner_pe_id, p2.pe_lastname as owner_pe_lastname,
            p2.pe_firstname as owner_pe_firstname
            FROM persons p
            LEFT JOIN personcat ON personcat.pecat_id = p.pecat_id
            LEFT JOIN countries ON countries.ctr_id = p.ctr_id
            LEFT JOIN companies ON companies.cpn_id = p.cpn_id
            LEFT JOIN jobcat ON jobcat.jcat_id = p.jcat_id
            LEFT JOIN persons p2 ON p2.pe_id = p.pe_pe_id
            WHERE p.pe_id = ?';

        $d = new PhPagaDbData;

        $peInfo = $d->queryRow($query, $peID);

        if (PhPagaError::isError($peInfo) || ($peInfo===false ) || !count($peInfo))
            return $peInfo;

        $peInfo['pe_person'] = PPerson::getFormattedName($peInfo['pe_lastname'], $peInfo['pe_firstname']);
        $peInfo['owner_pe_person'] = PPerson::getFormattedName($peInfo['owner_pe_lastname'], $peInfo['owner_pe_firstname']);
        $peInfo['pe_location'] = phpaga_getLocation($peInfo['pe_city'], $peInfo['pe_region']);
        $peInfo['pe_full_location'] = phpaga_getFullLocation($peInfo['pe_city'],
                                                            $peInfo['pe_region'],
                                                            $peInfo['pe_zip'],
                                                            $peInfo['ctr_name']);

        $peInfo['pe_duration'] = 0;
        $peInfo['projects'] = array();

        $d = new PhPagaDbData;
        $pedur = $d->queryAll('SELECT SUM(op_duration) AS duration FROM operations WHERE pe_id = ?', $peInfo['pe_id']);

        if (!PhPagaError::isError($pedur) && is_array($pedur) && count($pedur) && isset($pedur[0]['duration']))
            $peInfo['pe_duration'] = (int)$pedur[0]['duration'];

        $persprj = phpaga_projects_getpersonprojects($peID);

        if (!PhPagaError::isError($persprj))
            $peInfo['projects'] = $persprj;

        $files = PFile::getAllRel(PHPAGA_RELTYPE_PERSON, $peID);

        if (!PhPagaError::isError($files))
            $peInfo['files'] = $files;

        phpaga_sanitizeArray($peInfo);

        return $peInfo;
    }

    /**
    * Gets a list (array) of all persons of a certain category or company
    * (from $offset to $limit, if set)
    *
    * @param int    $count     Number of records matching the query
    * @param int    $pecat     person category
    * @param int    $pecpn     company
    * @param int    $offset    Offset
    * @param int    $limit     Limit
    *
    * @return array $rows      Array containing the results. A PhPagaError object on failure.
    *
    * @author Florian Lanthaler <florian@phpaga.net>
    * @since phpaga 0.1
    */

    public static function getList(&$count, $pecat, $pecpn, $offset = null, $limit = null) {
        $rows = array();
        $count = 0;
        $where = array();
        $qparam = array();
        $count_pe = array();

        $countquery = 'SELECT COUNT(pe_id) AS count_pe FROM persons ';

        $query = 'SELECT persons.pe_id, pe_lastname, pe_firstname, pe_address, pe_address2,
            pe_city, pe_region, pe_zip, persons.ctr_id, ctr_name, ctr_code,
            persons.pecat_id, pecat_title, pe_taxnr, pe_url,
            pe_phonework, pe_phonemobile, pe_phonehome,
            pe_fax, pe_email, persons.jcat_id, persons.cpn_id,
            pe_note, cpn_name, jcat_title
            FROM persons
            LEFT JOIN personcat ON personcat.pecat_id = persons.pecat_id
            LEFT JOIN countries ON countries.ctr_id = persons.ctr_id
            LEFT JOIN companies ON companies.cpn_id = persons.cpn_id
            LEFT JOIN jobcat ON jobcat.jcat_id = persons.jcat_id ';

        if (strlen($pecat)) {
            $where[] = '(persons.pecat_id = ?)';
            $qparam[] = $pecat;
        }

        if (strlen($pecpn)) {
            $where[] = '(persons.cpn_id = ?)';
            $qparam[] = $pecpn;
        }

        if (count($where) > 0) {
            $h = sprintf('WHERE %s ', implode(' AND ', $where));
            $countquery .= $h;
            $query .= $h;
        }

        $query .= 'ORDER BY pe_lastname, pe_firstname, pe_city, ctr_name ';

        $d = new PhPagaDbData;
        $count_pe = $d->queryOneVal($countquery, $qparam);
        if (!PhPagaError::isError($count_pe))
            $count = $count_pe;

        $persons = $d->queryAll($query, $qparam, $offset, $limit);
        if (PhPagaError::isError($persons))
            return $persons;

        foreach($persons as $qrow) {

            phpaga_sanitizearray($qrow);

            $qrow['pe_person'] = PPerson::getFormattedName ($qrow['pe_lastname'], $qrow['pe_firstname']);
            $qrow['pe_location'] = phpaga_getLocation ($qrow['pe_city'], $qrow['pe_region']);
            $qrow['pe_full_location'] = phpaga_getFullLocation ($qrow['pe_city'],
                                                                $qrow['pe_region'],
                                                                $qrow['pe_zip'],
                                                                $qrow['ctr_name']);

            $rows[] = $qrow;
        }

        phpaga_sanitizearray($rows);
        return $rows;
    }


    /* FIXME overwrite delete() ?


        if (!isset($pe_id) || !is_numeric($pe_id))
        return PhPagaError::raiseErrorByCode(PHPAGA_ERR_PERSON_INVALIDID);

    $d = new PhPagaDbData;


    $depqueries = array('SELECT COUNT(cpn_id) FROM companies WHERE pe_id = ?',
    'SELECT COUNT(op_id) FROM operations WHERE pe_id = ?',
    'SELECT COUNT(pe_id) FROM persons where pe_pe_id = ?',
    'SELECT COUNT(prj_id) FROM projects WHERE pe_id = ?',
    'SELECT COUNT(prj_id) FROM project_members WHERE pe_id = ?',
    'SELECT COUNT(usr_id) FROM users WHERE pe_id = ?',
    'SELECT COUNT(usr_id) FROM users WHERE usr_pe_id = ?');

    foreach ($depqueries as $dpq) {

    $n = $d->queryOneVal($dpq, $pe_id);

    if (PhPagaError::isError($n))
    return $n;

    if ($n > 0)
    return PhPagaError::raiseErrorByCode(PHPAGA_ERR_DEL_DEPENDENCIES);

    */

    /**
     * FIXME: migrate to use flourish's db API
     *
     * Get some manhours statistics for a specific person.
     *
     * @param int    $pe_id     Person ID
     *
     * @param int    $fy        Fiscal year (optional, default is current FY)
     *
     * @return array $stats     Array with manhours per FY, per current month and per current week.
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.5.2
     */

    function getBillableManhoursStats($fy=null) {

        if (strlen($fy) && !is_numeric($fy))
            return false;

        if (is_null($fy))
            $fy = phpaga_finances_getfiscalyear(date('Y-m-d'));

        $stats = new stdClass();
        $stats->fy = $fy;
        $stats->curr_fy = null;
        $stats->curr_month = null;
        $stats->curr_week = null;
        $stats->spark_month = null;
        $stats->op = new stdClass();

        $fydates = phpaga_finances_getfiscalyeardates($fy);

        $d = new PhPagaDbData;

        $op = $d->queryRow('select min(op_start) as begin_op, max(op_start) as end_op from operations where pe_id=?', $this->getPeId());

        if (PhPagaError::isError($op))
            return $op;

        $stats->op->begin = strftime(PHPAGA_DATEFORMAT_SHORT, strtotime($op['begin_op']));
        $stats->op->end = strftime(PHPAGA_DATEFORMAT_SHORT, strtotime($op['end_op']));

        $val = $d->queryOneVal('SELECT SUM(op_duration) FROM operations WHERE pe_id=? AND op_billabletype<>? AND op_start>=? and op_start<=?',
            array($this->getPeId(), PHPAGA_BILLABLETYPE_NO, $fydates['start'], $fydates['end']));

        if (PhPagaError::isError($val))
            return $val;

        $stats->curr_fy = phpaga_minutes2duration($val);

        $val = $d->queryOneVal('SELECT SUM(op_duration) FROM operations
        WHERE pe_id=?
        AND op_billabletype<>?
        AND YEAR(op_start)=YEAR(CURRENT_TIMESTAMP)
        AND MONTH(op_start)=MONTH(CURRENT_TIMESTAMP)',
            array($this->getPeId(), PHPAGA_BILLABLETYPE_NO));

        if (PhPagaError::isError($val))
            return $val;

        $stats->curr_month = phpaga_minutes2duration($val);

        $query = 'SELECT YEAR(op_start) as y, MONTH(op_start) AS m, SUM(op_duration) as s
        FROM operations
        WHERE pe_id=?
        AND op_billabletype<>?
        group by year(op_start), month(op_start) order by year(op_start), month(op_start)';
        $val = $d->queryAll($query, array($this->getPeId(), PHPAGA_BILLABLETYPE_NO));

        if (PhPagaError::isError($val))
            return $val;

        $stats->spark_month = array();
        $beginy = (int)date('Y', strtotime($op['begin_op']));
        $beginm = (int)date('m', strtotime($op['begin_op']));
        $endy = (int)date('Y', strtotime($op['end_op']));
        $endm = (int)date('m', strtotime($op['end_op']));

        $_s = array();
        $y = $beginy;
        $m = $beginm;

        while ($y <= $endy) {
            $_s[$y][$m] = 0;
            if (($y == $endy) && ($m == $endm))
                break;

            if ($m == 12) {
                $m = 1;
                $y++;
            } else
                $m++;
        }

        foreach ($val as $v)
            $_s[$v['y']][$v['m']] = $v['s'];

        $_sm = array();
        foreach ($_s as $y => $ya)
            foreach ($ya as $m => $v)
                $_sm[] = $v;

        $stats->spark_month = $_sm;

        $query = '';
        $param = array();

        switch(PHPAGA_DB_SYSTEM) {
            case 'pgsql':
                $query = "SELECT SUM(op_duration) FROM operations
                WHERE pe_id=?
                AND op_billabletype<>?
                AND YEAR(op_start)=YEAR(CURRENT_TIMESTAMP)
                AND TO_CHAR(op_start, 'iw')=TO_CHAR(CURRENT_TIMESTAMP, 'iw')";
                $param = array($this->getPeId(), PHPAGA_BILLABLETYPE_NO);
                break;
            case "mysql":
                $query = "SELECT SUM(op_duration) FROM operations
                WHERE pe_id=?
                AND op_billabletype<>?
                AND YEAR(op_start)=YEAR(CURRENT_TIMESTAMP)
                AND DATE_FORMAT(op_start, '%v')=DATE_FORMAT(CURRENT_TIMESTAMP, '%v')";
                $param = array($this->getPeId(), PHPAGA_BILLABLETYPE_NO);
                break;

            default:
                return PhPagaError::raiseError('Unsupported database system');
                break;
        }

        $val = $d->queryOneVal($query, $param);
        if (PhPagaError::isError($val))
            return $val;

        $stats->curr_week = phpaga_minutes2duration($val);

        return $stats;
    }


}

?>
