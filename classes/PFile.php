<?php
/**
 * phpaga
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This class handles files that are attached to an object (project, person, ...) or to a system setting.
 */
class PFile extends PhPagaData {

    /**
     * Returns companies matching the search criteria.
     * 
     * @param array $conditions  Search criteria
     * @param array $orderBy     Sort order
     * @param int   $limit       Number of records to fetch
     * @param int   $page        Page offset when limiting records
     * @return fRecordSet        Set of fActiveRecord objects
     */
    public static function find($conditions=array(), $orderBy=array('file_original_name' => 'asc'), $limit=null, $page=null) {
        $set = fRecordSet::build(__CLASS__, $conditions, $orderBy, $limit, $page);      
        $set->precreatePPersons('pe_id');
        return $set;        
    }

    /**
     * Returns the filename including the full path.
     * 
     * @return string 
     */
    public function getFullPath() {
        return PHPAGA_FILEPATH.$this->getFileRelType().'/'.$this->getFileRelId().'/'.$this->getFileLocalName();
    }

    /**
     * Returns an array with information about the file.
     * 
     * @param mixed $file_id   The file id, can be numeric for files that are attached to an object, or a string for files attached to a setting.
     * @return array           File information 
     */
    public static function getInfo($file_id) {

        if (!strlen($file_id))
            return PhPagaError::raiseError(_('Missing file ID.'));

        if (!is_numeric($file_id)) {
            switch($file_id) {
            case 'LETTERHEAD_LOGO_FILE': 
            case 'PDF_PREPEND_INVOICE':
            case 'PDF_APPEND_INVOICE':
                $perm = PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS);
                if (!$perm)
                    return PhPagaError::raiseError(_('You do not have the permission to remove this file.'));
                $value = constant('PHPAGA_'.$file_id);
                if (!strlen($value))
                    return null;
                $full_path = PHPAGA_FILEPATH.'00/'.$value;
                $f = new fFile($full_path);
                $file = array('file_original_name' => $value,
                    'full_path' => $full_path,
                    'thumbnail' => $full_path.'.jpg',
                    'file_mimetype' => $f->getMimeType(),
                    'file_rel_type' => 0);
                break;
            default:
                return PhPagaError::raiseError(_('Invalid file ID.'));
            }

        } else {

            if (PHPAGA_DB_SYSTEM == 'pgsql')
                $t_file_added = "to_char(file_added, 'yyyy-mm-dd hh24:mi:ss'::text) as file_added";
            else
                $t_file_added = 'file_added';

            $d = new PhPagaDbData;

            $query = 'SELECT file_id, '.$t_file_added.', files.pe_id, file_original_name,
                file_local_name, file_mimetype, persons.pe_lastname, persons.pe_firstname,
                file_rel_type, file_rel_id
                FROM files
                LEFT JOIN persons ON persons.pe_id = files.pe_id
                WHERE file_id = ?';

            $file = $d->queryRow($query, $file_id);

            if (PhPagaError::isError($file))
                return $file;

            $path = PHPAGA_FILEPATH.$file['file_rel_type'].'/'.$file['file_rel_id'].'/';
            $file['pe_person'] = PPerson::getFormattedName($file['pe_lastname'], $file['pe_firstname']);
            $file['full_path'] = $path.$file['file_local_name'];
            $file['thumbnail'] = $path.'.thumbs/'.$file['file_local_name'].'.jpg';

        }
        return $file;
    }

    /**
     * Deletes a file both from the file system and from the database.
     *
     * Administrators can always remove files.
     * Users can remove their own files only.
     * Project managers can remove files from their projects.
     *
     * @param mixed  $file_id    File ID
     * @return                   True if successful, PhPagaError otherwise.
     * @since phpaga 0.7
     */
    public static function remove($file_id) {
        if (!strlen($file_id))
            return false;

        $isadmin = PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS);

        if (!is_numeric($file_id)) {
            if (!$isadmin)
                return PhPagaError::raiseError(_("You don't have the necessary permission to delete this file."));

            switch($file_id) {
            case 'LETTERHEAD_LOGO_FILE': 
            case 'PDF_PREPEND_INVOICE':
            case 'PDF_APPEND_INVOICE':
                $value = constant('PHPAGA_'.$file_id);
                if (!strlen($value))
                    return false;
                $fullPath = PHPAGA_FILEPATH.'00/'.$value;

                $result = PConfig::updateItem($file_id, '');
                if (PhPagaError::isError($result))
                    return $result;
                break;
            default:
                return PhPagaError::raiseError(_('Invalid file ID.'));
            }
        } else {

            $fl = new PFile($file_id);

            $isowner = $fl->getPeId() == $_SESSION['auth_user']['pe_id'];
            $isprjmanager = ($fl->getFileRelType()==PHPAGA_RELTYPE_PROJECT) ? phpaga_project_canmanage($fl->getFileRelId()) : false;

            if (!$isadmin && !$isowner && !$isprjmanager)
                return PhPagaError::raiseError(_("You don't have the necessary permission to delete this file."));

            $fullPath = $fl->getFullPath();
        }

        if (file_exists($fullPath) && !unlink($fullPath))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_FILE_REMOVE);

        if (!is_numeric($file_id))
            return true;

        try {
            $fl->delete();
            unset($fl);
            return true;
        } catch (Exception $e)  {
            return PhPagaError::raiseError($e->getMessage());
        }
    }

    /**
     * Gets a list with information about all files that belong to a
     * certain relation.
     *
     * @param  int     $file_rel_type   Type of the related object
     * @param  int     $file_rel_id     ID of the related object
     * @return array                    File(s) information. A PhPagaError object on failure.
     * @since phpaga 0.3
     */
    public static function getAllRel($file_rel_type, $file_rel_id) {

        global $phpaga_preview_types;
        global $phpaga_mime_image_types;

        $files = array();

        if (!isset($file_rel_type) || !is_numeric($file_rel_type) || !isset($file_rel_id) || !is_numeric($file_rel_id))
            return PhPagaError::raiseErrorByCode(PHPAGA_ERR_INVALID_PARAMTYPE);

        if (PHPAGA_DB_SYSTEM == 'pgsql')
            $t_file_added = "to_char(file_added, 'yyyy-mm-dd hh24:mi:ss'::text) as file_added";
        else
            $t_file_added = 'file_added';

        $query = 'SELECT file_id, '.$t_file_added.', files.pe_id, file_original_name,
            file_local_name, file_mimetype, persons.pe_lastname, persons.pe_firstname
            FROM files
            LEFT JOIN persons ON persons.pe_id = files.pe_id
            WHERE file_rel_type = ?
            AND file_rel_id = ?';

        $d = new PhPagaDbData;
        $result = $d->queryAll($query, array($file_rel_type, $file_rel_id));

        if (PhPagaError::isError($result))
            return $result;

        foreach ($result as $qrow) {
            $qrow['pe_person'] = PPerson::getFormattedName($qrow['pe_lastname'], $qrow['pe_firstname']);
            $qrow['preview'] = in_array($qrow['file_mimetype'], $phpaga_preview_types);
            $qrow['is_image'] = in_array($qrow['file_mimetype'], $phpaga_mime_image_types);

            $files[] = $qrow;
        }
        return $files;
    }

}

?>
