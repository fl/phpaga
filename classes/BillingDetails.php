<?php
/**
 * phpaga
 *
 * billing functionality.
 *
 * This file contains the necessary routines for billing and invoice management.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @author Michael Kimmick <support@nichewares.com>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * This class allows the management of billing details as shown on invoices and 
 * quotations.
 *
 * Against common coding conventions, methods are not named in camelCode but in 
 * lower_letters in order to be make the migration of pre-0.6 plugins easy.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @since phpaga 0.6
 */

abstract class BillingDetails {

    protected $curr_name;
    protected $curr_decimals;
    protected $details = array();
    protected $startsum=0;
    protected $expensesum=0;
    protected $paymentsum=0;
    protected $feesum=0;
    protected $balance_due=0;
    protected $endsum=null;

    /* The member $plugin_info must be overwritten by each child class. It is 
     * an array with the following keys:
     *
     * author: Author name and email address in the format Name Lastname <emailaddress>
     * description: A short description
     * country: Two-letter country code
     *
     */
    static $plugin_info = array('author' => '', 'description' => '', 'country' => '');


    /**
     * Returns the plugin info as specified by each plugin.
     *
     * @return array $info   Plugin information
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    static public function get_plugin_info() { return self::$plugin_info; }


    /**
     * Constructor
     *
     * @param int   $curr_id     Currency ID
     * @param float $startsum    Start amount as base of calculation
     * @param float $expensesum  Total amount of expenses
     * @param float $paymentsum  Total amount of payments 
     * @param float $feesum      Total amount of (late) fees
     *
     * @return void
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    public function __construct($curr_id, $startsum, $expensesum=0, $paymentsum=0, $feesum=0) {

        if (!isset($curr_id) || !is_numeric($curr_id)) {
            $this->curr_name = '';
            $this->curr_decimals = 0;
        } else {
            $currInfo = new PCurrency($curr_id);
            $this->curr_name = $currInfo->getCurrName();
            $this->curr_decimals = $currInfo->getCurrDecimals();
        }

        $this->startsum=$startsum;
        $this->expensesum=$expensesum;
        $this->paymentsum=$paymentsum;
        $this->feesum=$feesum;

        $this->calculate();
    }


    /**
     * Returns the plugin's filename.
     *     
     * @return string $filename  The plugin's filename.
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    abstract public static function get_filename();


    /**
     * Returns the calculated end sum.
     *     
     * @return float $endsum  The calculated end sum.
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    public function get_endsum() { return $this->endsum; }


    /**
     * Returns the calculated balance due.
     *     
     * @return float $balance_due  The calculated balance due.
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    public function get_balance_due() { return $this->balance_due; }


    /**
     * Returns the billing details.
     *     
     * @return array $billing_details  Array with billing details.
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    public function get_details() { return $this->details; }


    /**
     * Calculates the end sum, the balance due, and the single details of the bill.
     *
     * This abstract method must be overwritten in the custom billing plugins 
     * that extend this class.
     *
     * See an example implementation at plugins/billing/notaxes.php
     *
     * @return void
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    abstract public function calculate();

    /**
     * Adds a detail line.
     *
     * @param string   $text      Text to be displayed
     * @param real     $amount    Amount
     *
     * @return void
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.1
     */

    function add_detail($text = "", $amount = "") {

	$config = PConfig::getArray();
    
    if ($amount == "")
            $this->details[] = array("text" => $text, "amount" => "");
        else {
            if ($config['SHOW_CURRENCY'] == 'true' && $text == 'Balance Due') {
                $this->details[] = array("text" => $text,
                    "amount" => $config['MONETARY_SYMBOL'] .
                    number_format((double)$amount, $this->curr_decimals, PHPAGA_SEPARATOR_DECIMALS, PHPAGA_SEPARATOR_THOUSANDS) .
                    " " .
                    $this->curr_name);
            } else {
                $this->details[] = array("text" => $text,
                    "amount" => $config['MONETARY_SYMBOL'] .
                    number_format((double) $amount, $this->curr_decimals, PHPAGA_SEPARATOR_DECIMALS, PHPAGA_SEPARATOR_THOUSANDS));
            }
        }
    }


    /**
     * Adds a payment detail line.
     *
     * @param string   $text      Text to be displayed
     * @param real     $amount    Amount - negative
     *
     * @return void
     *
     * @author Michael Kimmick <support@nichewares.com>
     * @since phpaga 0.4
     */

     function add_payment_detail($text = "", $amount = "") {

	$config = PConfig::getArray();
	$this->details[] = array("text" => $text,
				"amount" => "-".$config['MONETARY_SYMBOL'].
				number_format($amount,
					      $this->curr_decimals,
					      PHPAGA_SEPARATOR_DECIMALS,
					      PHPAGA_SEPARATOR_THOUSANDS),
					      " ".
					      $this->curr_name);
     }


    /**
     * Adds a latefee detail line
     *
     * @param string   $text      Text to be displayed
     * @param real     $amount    Amount - positive
     *
     * @return void
     *
     * @author Michael Kimmick <support@nichewares.com>
     * @since phpaga 0.4
     */

     function add_fee_detail($text = "", $amount = "") {

	$config = PConfig::getArray();
	$this->details[] = array("text" => $text,
				 "amount" => "+".$config['MONETARY_SYMBOL'].
				 number_format($amount,
					       $this->curr_decimals,
					       PHPAGA_SEPARATOR_DECIMALS,
					       PHPAGA_SEPARATOR_THOUSANDS));
     }


    /**
     * Adds a separator line.
     *
     * @return void
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.1
     */

    function add_separator() { $this->details[] = array("text" => "", "amount" => ""); }

}

phpaga_load_plugins('billing');

?>
