<?php
/**
 * phpaga
 *
 * This is an invoice PDF template file.
 *
 * Invoice templates extend the base class InvoicePDF and overwrite (or add) 
 * one or more methods or members.
 *
 * This file can be used as an example on how to create a custom PDF class.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2010, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class InvoiceExamplePDF extends InvoicePDF {

    /**
     * Adds a custom letterhead.
     *
     * If the parameter $logofile is set, the logo is inserted in the 
     * letterhead.
     * If $logofile is not set and a default logo file is configured, the 
     * default logo is inserted.
     *
     * @param string  $logofile     Logo filename (optional)
     *
     * @return void
     */
    public function AddLetterhead($logofile=null) {
        if (is_null($logofile))
            if (strlen(PHPAGA_LETTERHEAD_LOGO_FILE))
                $logofile = PHPAGA_FILEPATH.'00/'.PHPAGA_LETTERHEAD_LOGO_FILE;

        if (strlen($logofile) && is_readable($logofile))
            $this->Image($logofile, 155, 5, 40, 0);

        $this->SetXY(20, 12);
        $this->SetFont($this->defaultFontName, '', 24);
        $this->MultiCell(0, 24, PHPAGA_LETTERHEAD_COMPANY_NAME, 0, 'L', 0, 1, 20);
        $this->SetFont($this->defaultFontName, '', 8);

        $this->HorizontalLine(0, 160, 25, array(0,0,255), 0.4);

        $this->SetXY(20, 26);
        $this->MultiCell(40, 10, PHPAGA_LETTERHEAD_COMPANY_STREET."\n".PHPAGA_LETTERHEAD_COMPANY_FULLLOCATION, 0, 'L', 0, 0);
        $this->MultiCell(40, 10, PHPAGA_LETTERHEAD_COMPANY_PHONE."\n".PHPAGA_LETTERHEAD_COMPANY_EMAIL, 0, 'L', 0, 0);
        $this->MultiCell(60, 10, PHPAGA_LETTERHEAD_OWNER_PERSONALTAXNR."\n".PHPAGA_LETTERHEAD_COMPANY_TAXNR, 0, 'L', 0, 1);
    }
}

?>
