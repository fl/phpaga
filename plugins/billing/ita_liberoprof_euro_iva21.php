<?php
/**
 * phpaga
 *
 * billing plugin
 *
 * This file is a reference implementation and can be used as
 * an inspiration to create new plugins.
 *
 * This plugin contains the necessary routines to calculate a bill for:
 *
 * Liberi professionisti / ditte individuali (Italy) / calcolazione in Euro
 * IVA al 21% (dal 17 settembre 2011)
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2011, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class ItaLiberoprofEuroIva21 extends BillingDetails {

    public static $plugin_info = array(
        'author' => 'Florian Lanthaler <florian@phpaga.net>', 
        'description' => 'Liberi professionisti / ditte individuali, IVA 21%', 
        'country' => 'IT'
    );

    public static function get_filename() { return basename(__FILE__); }

    public function calculate() {

        $inps = 0;
        $imponibile = 0;
        $iva = 0;
        $irpef = 0;

        /* 4% INPS/NISF */
        $inps = sprintf("%.2f", $this->startsum * 0.04);

        /* imponibile */
        $imponibile = sprintf("%.2f", $this->startsum + $inps);

        /* IVA 21 % */
        $iva = sprintf("%.2f", $imponibile * 0.21);

        /* IRPEF 20 % */
        $irpef = sprintf("%.2f", $imponibile * 0.2);

        /* End sum without expenses*/
        $preendsum = sprintf("%.2f", $imponibile + $iva - $irpef);

        /* End sum */
        $this->endsum = $preendsum + $this->expensesum;

        $this->add_detail("Competenze", $this->startsum);
        $this->add_detail("4% INPS Art. 4 DL 28.3.96 n 166", $inps);
        $this->add_separator();
        $this->add_detail("Imponibile", $imponibile);
        $this->add_detail("IVA 21%", $iva);
        $this->add_separator();
        $this->add_detail("Totale", sprintf("%.2f", $imponibile + $iva));
        $this->add_detail("- Ritenuta d'acconto 20%", $irpef * -1);
        $this->add_separator();

        if (is_numeric($this->expensesum) && ($this->expensesum > 0)) {
            $this->add_detail("Parziale", $preendsum);
            $this->add_detail("+ Rimborso spese", $this->expensesum);
            $this->add_separator();
        }

        $sep = false;

        /* Add Latefees to bill details */
        if (is_numeric($this->feesum) &&($this->feesum > 0)) {
            $sep = true;
            $this->add_fee_detail(_("+ Mora"), $this->feesum);
        }

        /* Add Payments to bill details */
        if (is_numeric($this->paymentsum) &&($this->paymentsum > 0)) {
            $sep = true;
            $this->add_payment_detail(_("- Pagamenti"), $this->paymentsum);
        }

        if ($sep)
            $this->add_separator();

        $this->balance_due = $this->endsum - $this->paymentsum + $this->feesum;
        $this->add_detail("Importo complessivo", $this->balance_due);
    }
}

?>
