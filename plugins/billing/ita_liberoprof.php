<?php
/**
 * phpaga
 *
 * billing plugin
 *
 * This file is a reference implementation and can be used as
 * an inspiration to create new plugins.
 *
 * This plugin contains the necessary routines to calculate a bill for:
 *
 * Liberi professionisti / ditte individuali (Italy)
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class ItaLiberoprof extends BillingDetails {

    public static $plugin_info = array(
        'author' => 'Florian Lanthaler <florian@phpaga.net>', 
        'description' => 'Lire: Liberi professionisti / ditte individuali', 
        'country' => 'IT'
    );

    public static function get_filename() { return basename(__FILE__); }

    public function calculate() {

        $inps = 0;
        $imponibile = 0;
        $iva = 0;
        $irpef = 0;

        /* 4% INPS/NISF */
        $inps = $this->startsum * 0.04;

        /* imponibile */
        $imponibile = $this->startsum + $inps;

        /* IVA 20 % */
        $iva = $imponibile * 0.2;

        /* IRPEF 20 % */
        $irpef = $imponibile * 0.2;

        /* end sum */
        $this->endsum = $imponibile + $iva - $irpef;

        $this->add_detail ("Honorar", $this->startsum);
        $this->add_detail ("4% INPS/NISF Art. 4 DL 28.3.96 n 166", $inps);
        $this->add_separator ();
        $this->add_detail ("Besteuerbarer Betrag", $imponibile);
        $this->add_detail ("Mehrwertsteuer 20%", $iva);
        $this->add_separator ();
        $this->add_detail ("Gesamtsumme", $imponibile + $iva);
        $this->add_detail ("- Steuerrückbehalt 20%", $irpef);
        $this->add_separator ();

        /* Add Latefees to bill details */
        if (is_numeric($this->feesum) && ($this->feesum > 0)) {
            $this->add_fee_detail(_("+ Late Fees"), $this->feesum);
        }

        /* Add Payments to bill details */
        if (is_numeric($this->paymentsum) && ($this->paymentsum > 0)) {
            $this->add_payment_detail(_("- Payments"), $this->paymentsum);
            $this->add_separator ();
        } else {
            $this->add_separator ();
        }

        $this->balance_due = $this->endsum - $this->paymentsum + $this->feesum;
        $this->add_detail ("Guthaben", $this->balance_due);
    }
}

?>
