<?php
/**
 * phpaga
 *
 * billing plugin
 *
 * This file is a reference implementation and can be used as
 * an inspiration to create new plugins. You may prefer looking at
 * one of the other reference implementations as this one is too
 * basic.
 *
 * This plugin contains the necessary routines to calculate a bill for:
 *
 * No taxes.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @author Michael Kimmick <support@nichewares.com>
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


class NoTaxes extends BillingDetails {

    public static $plugin_info = array(
        'author' => 'Florian Lanthaler <florian@phpaga.net>, Michael Kimmick <support@nichewares.com>', 
        'description' => 'No taxes (reference plugin implementation)', 
        'country' => 'US'
    );

    public static function get_filename() { return basename(__FILE__); }

    /**
     * Calculates the end sum, the balance due, and the single details of the bill.
     *
     * This overwrites the abstract method of the parent class BillingDetails. 
     *
     * @return void
     *
     * @author Florian Lanthaler <florian@phpaga.net>
     * @since phpaga 0.6
     */

    public function calculate() {

        /* End sum and balance due */
        $preendsum = sprintf("%.2f", $this->startsum);
        $this->endsum = $preendsum + $this->expensesum + $this->feesum;
        $this->balance_due = $this->endsum - $this->paymentsum;

        /* Time and material */
        $this->add_detail(_("Time and Materials"), $this->startsum);

        /* Add expenses */
        if (is_numeric($this->expensesum) && ($this->expensesum > 0))
            $this->add_detail(_("+ Reimbursable expenses"), $this->expensesum);

        /* Add late fees */
        if (is_numeric($this->feesum) && ($this->feesum > 0))
            $this->add_fee_detail(_("+ Late Fees"), $this->feesum);

        /* Add payments */
        if (is_numeric($this->paymentsum) && ($this->paymentsum > 0)) {
            $this->add_payment_detail(_("- Payments"), $this->paymentsum);
            $this->add_separator ();
        } else
            $this->add_separator ();

        $this->add_detail (_("Balance Due"), $this->balance_due);
    }
}

?>
