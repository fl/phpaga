<?php
/**
 * phpaga
 *
 * billing plugin
 *
 * This plugin contains the necessary routines to calculate a bill for:
 *
 * NL VAT
 *
 * @author Angelo Hoengens
 *
 * Copyright (c) 2007, Angelo Höngens
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


class NldBtwHoog extends BillingDetails {

    public static $plugin_info = array(
        'author' => 'Angelo Hoengens', 
        'description' => 'NL VAT', 
        'country' => 'NL'
    );

    public static function get_filename() { return basename(__FILE__); }

    public function calculate() {

        $inps = 0;
        $imponibile = 0;
        $iva = 0;
        $irpef = 0;

        /* 19% VAT */
        $inps = $this->startsum * 0.19;

        /* end sum */
        $preendsum = $this->startsum + $inps;
        $this->endsum = $preendsum + $this->expensesum;

        $this->add_detail ("Totaal excl. BTW", $this->startsum);
        $this->add_detail ("BTW 19%", $inps);
        $this->add_separator ();

        if (is_numeric($this->expensesum) && ($this->expensesum > 0)) {
            $this->add_detail(_("Subtotal"), $preendsum);
            $this->add_detail(_("+ Reimbursable expenses"), $this->expensesum);
            $this->add_separator ();
        }

        /* Add Latefees to bill details */
        if (is_numeric($this->feesum) && ($this->feesum > 0)) {
            $this->add_fee_detail(_("+ Late Fees"), $this->feesum);
        }

        /* Add Payments to bill details */
        if (is_numeric($this->paymentsum) && ($this->paymentsum > 0)) {
            $this->add_payment_detail(_("- Payments"), $this->paymentsum);
            $this->add_separator ();
        } else {
            $this->add_separator ();
        }

        $this->balance_due = $this->endsum - $this->paymentsum + $this->feesum;
        $this->add_detail ("Totaal incl. BTW", $this->balance_due);
    }
}

?>
