<?php
/**
 * phpaga
 *
 * Settings 
 *
 * This is the sitewide settings management console.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define('PHPAGA_COMPRESSOUT_DISABLED', true);
include_once('./config.php');
PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);

include_once(PHPAGA_LIBPATH.'pdf.php');

$options_months = array();
$options_days = array();
$options_prjstat = array();
$options_pdfmerge_backends = array('' => '',
    'pdftk' => 'Pdftk',
    'gs' => 'Ghostscript');

$oldlocale = setlocale(LC_TIME, 0);
setlocale(LC_TIME, PHPAGA_LANGNAME);

foreach(range(1, 12) as $i)
    $options_months[$i] = strftime("%B", strtotime("2005-$i-01"));

setlocale(LC_TIME, $oldlocale);

$numdays = date('t', strtotime(sprintf('2005-%s-01', PHPAGA_FIN_FISCYEAR_MONTH)));

foreach(range(1, $numdays) as $i)
    $options_days[$i] = $i;

$config = array();
$update_result = null;

if (isset($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == ACTION_SAVECONFIG)
    && (isset($REQUEST_DATA['cfgitem']) && (count($REQUEST_DATA['cfgitem'])))) {

    foreach($REQUEST_DATA['cfgitem'] as $key => $value) {

        $result = PConfig::updateItem($key, $value);

        if (PhPagaError::isError($result)) {
            $result->printMesssage();
            $update_result = $result;
        }
    }

    /* 
     * Check for uploaded files 
     * Files related to system settings go directly into PHPAGA_FILEPATH/00/
     */

    if (fUpload::check('logo') || fUpload::check('invoiceprepend') || fUpload::check('invoiceappend')) { 

        $dest_dir = PHPAGA_FILEPATH.'00/';
        $status = null;

        if (!is_dir(PHPAGA_FILEPATH))
            $status = sprintf(_("%s: The directory does not exist"), PHPAGA_FILEPATH);

        elseif (!is_dir($dest_dir) && !mkdir($dest_dir))
            $status = sprintf(_("%s: The directory does not exist and can't be created"), $dest_dir);

        elseif (!is_writable($dest_dir))
            $status = sprintf(_("%s: The directory is not writable"), $dest_dir);

        if (strlen($status)) {
            phpaga_error($status);
        } else {
            $uploadLogo = new fUpload();
            $uploadLogo->setOptional();
            $imageLogo = $uploadLogo->move($dest_dir, 'logo');
            if (!is_null($imageLogo)) {
                $result = PConfig::updateItem('LETTERHEAD_LOGO_FILE', $imageLogo->getName());
                if (PhPagaError::isError($result))
                    $result->printMesssage();
            }

            $uploadInvoicePrepend = new fUpload();
            $uploadInvoicePrepend->setOptional();
            $uploadInvoicePrepend->setMIMETypes(array('application/pdf'), _('The file is not a valid PDF document.'));
            try {
                $imageInvoicePrepend = $uploadInvoicePrepend->move($dest_dir, 'invoiceprepend');
                if (!is_null($imageInvoicePrepend)) {
                    $result = PConfig::updateItem('PDF_PREPEND_INVOICE', $imageInvoicePrepend->getName());
                    if (PhPagaError::isError($result))
                        $result->printMesssage();
                }
            } catch (Exception $e) {
                $error = PhPagaError::raiseError($e->getMessage());
                $error->printMessage();
            }


            $uploadInvoiceAppend = new fUpload();
            $uploadInvoiceAppend->setOptional();
            $uploadInvoiceAppend->setMIMETypes(array('application/pdf'), _('The file is not a valid PDF document.'));
            try {
                $imageInvoiceAppend = $uploadInvoiceAppend->move($dest_dir, 'invoiceappend');
                if (!is_null($imageInvoiceAppend)) {
                    $result = PConfig::updateItem('PDF_APPEND_INVOICE', $imageInvoiceAppend->getName());
                    if (PhPagaError::isError($result))
                        $result->printMesssage();
                }
            } catch (Exception $e) {
                $error = PhPagaError::raiseError($e->getMessage());
                $error->printMessage();
            }
        }
    }

    if (!PhPagaError::isError($update_result)) {
        header("Location: siteconfig.php?action=".ACTION_OKMSG);
        exit;
    }
} else {

    $pdfclasses = array('invoice' => array('InvoicePDF' => 'InvoicePDF'), 'quotation' => array('QuotationPDF' => 'QuotationPDF'));

    foreach (get_declared_classes() as $class) {
        if (is_subclass_of($class, 'InvoicePDF'))
            $pdfclasses['invoice'][$class] = $class;
        elseif (is_subclass_of($class, 'QuotationPDF'))
            $pdfclasses['quotation'][$class] = $class;
    }

    phpaga_header(array('menuitem' => 'administration'));

    if (isset($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == ACTION_OKMSG))
        phpaga_message(_('Settings saved'),
                       _('The configuration settings have been saved successfully.'));

    $options_truefalse = array();
    $options_truefalse['true'] = _('Yes');
    $options_truefalse['false'] = _('No');

    $options_imagetypes = array('jpg' => 'jpg', 'gif' => 'gif', 'png' => 'png');

    $options_pdfpageformat = array('a4' => 'a4', 'letter' => 'letter');

    $styles = phpaga_getSkinNames();
    $templatesets = phpaga_getTemplateSetNames();

    $options_bmt = phpaga_bills_getInvoiceTypeList();
    $options_prjstat = phpaga_projects_getStatusList();

    $config = PConfig::getArray();

    $tpl = new PSmarty;

    $tpl->assign('admin', true);
    $tpl->assign('config', $config);
    $tpl->assign('options_truefalse', $options_truefalse);
    $tpl->assign('options_styles', $styles);
    $tpl->assign('options_templatesets', $templatesets);
    $tpl->assign('options_imagetypes', $options_imagetypes);
    $tpl->assign('options_countries', PCountry::getSimpleArray());
    $tpl->assign('options_currencies', PCurrency::getList());
    $tpl->assign('options_languages', PhPagaLanguage::getList());
    $tpl->assign('options_months', $options_months);
    $tpl->assign('options_days', $options_days);
    $tpl->assign('options_bmt', $options_bmt);
    $tpl->assign('options_prjstat', $options_prjstat);
    $tpl->assign('options_pdfpageformat', $options_pdfpageformat);
    $tpl->assign('pdfclasses', $pdfclasses);
    $tpl->assign('options_pdfmerge_backends', $options_pdfmerge_backends);

    $tpl->display('siteconfig.tpl.html');
    phpaga_footer();
}

?>