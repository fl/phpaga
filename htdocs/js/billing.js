/*
 * phpaga
 *
 * This file contains some JavaScript functionality for the billing
 * pages of phpaga.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2006, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

function addLineItem() {
    var rnum = $('#tbllineitems tbody tr').length + 1;

    $('#tbllineitems > tbody:last').append('<tr><td> \
        <input type="hidden" name="lit_op_id[]" value="" /> \
        <input type="hidden" name="lit_mat_id[]" value="" /> \
         \
        <input type="text" class="tinput prodcode" name="lit_prodcode[]" value="" size="18" maxlength="18" id="prodcode'+rnum+'"> \
        <a class="ptr prodsel" id="prc'+rnum+'"><img src="img/phpaga/catalog.png" width="16" height="16"></a> \
         </td> \
            <td><textarea name="lit_desc[]" cols="60" rows="2" wrap="virtual" id="prodname'+rnum+'"></textarea></td> \
            <td><input type="text" class="tinput number prodqty" name="lit_qty[]" value="" size="10" maxlength="10" id="prodqty'+rnum+'"></td> \
            <td class="number"> \
                <span id="unitnetpricetxt'+rnum+'"></span> \
                <input type="hidden" name="lit_unitnetprice[]" value="" size="15" id="unitnetprice'+rnum+'"> \
            </td> \
            <td><input type="text" class="tinput number" name="lit_netprice[]" value="" size="15" maxlength="15" id="prodprice'+rnum+'"></td> \
            <td> \
                <a class="del_item" href="#"><img src="img/phpaga/delete.png" width="16" height="16"></a> \
            </td></tr>');
}

function companyChanged() {
    getPayTerm();
    getEmployees();
}

function getPayTerm() {

    $.ajax({
        url: 'service.php?saction=35&id='+$('#cpn').val(),
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(result) {

            if (result == null) {
                alert('An unexpected error has occurred.');
            } else if (result.status != 0 && result.status != 30) {
                alert(result.message);
            } else {

                if (result.status == 0) {
                    $('#payterm').val(result.data.term);
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

            msg = textStatus;
            if (textStatus == 'parsererror')
                msg = 'Invalid response from the server.';

            alert(msg);
        }
    });
}

function getEmployees() {

    $.ajax({
        url: 'service.php?saction=48&id='+$('#cpn').val(),
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(result) {

            if (result == null) {
                alert('An unexpected error has occurred.');
            } else if (result.status != 0 && result.status != 30) {
                alert(result.message);
            } else {

                el = $('#pe_id_recipient').get(0);
                el.options.length = 0;
                items = result.data.persons;

                if (items.length > 0) {
                    el.options[0] = new Option('', '');
                    $.each(items, function () { el.options[el.options.length] = new Option(this.name, this.id); });
                    $('#pe_id_recipient').removeAttr('disabled');
                } else 
                    $('#pe_id_recipient').attr('disabled', 'disabled');
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

            msg = textStatus;
            if (textStatus == 'parsererror')
                msg = 'Invalid response from the server.';

            alert(msg);
        }
    });
}

function getProdInfo(elem_num) {

    /* If prod_code is set and prod_name is not set then fetch product information.
       Do not fetch data for rows that have already been filled out. */

    prodcode = $('#prodcode'+elem_num).val();
    prodname = $('#prodname'+elem_num).val();

    if ((prodcode.length > 0) && (prodname.length == 0)) {

        $.ajax({
            url: 'service.php?saction=40&prod_code='+prodcode,
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function(result) {

                if (result == null) {
                    alert('An unexpected error has occurred.');
                } else if (result.status != 0 && result.status != 30) {
                    alert(result.message);
                } else {

                    if (result.status == 0) {
                        price = result.data.prod_price;
                        name = result.data.prod_name;
                    } else {
                        price = null;
                        name = '';
                    }

                    var qty = $('#prodqty'+elem_num).val();

                    if ((qty == null) || (qty.length == 0))
                        $('#prodqty'+elem_num).val(1);

                    $('#prodname'+elem_num).val(name);

                    if (price != null) {
                        $('#unitnetprice'+elem_num).val(price);
                        $('#unitnetpricetxt'+elem_num).html(price);
                    } else {
                        $('#unitnetprice'+elem_num).val('');
                        $('#unitnetpricetxt'+elem_num).html('');
                    }

                    if ((price != null) && (price.length > 0)) {
                        if (parseInt(qty) != qty)
                            $('#prodprice'+elem_num).val(price);
                        else
                            $('#prodprice'+elem_num).val(qty * price);
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

                msg = textStatus;
                if (textStatus == 'parsererror')
                    msg = 'Invalid response from the server.';

                alert(msg);
            }
        });
    }
}

function calcLineItemPrice(elem_num) {
    var qty = $('#prodqty'+elem_num).val();
    var unitprice = $('#unitnetprice'+elem_num).val();
    if (isNumeric(qty) && isNumeric(unitprice)) {
        var res = (qty * unitprice);
        if (res.toFixed)
            res = res.toFixed(2);
        $('#prodprice'+elem_num).val(res);
    }
}

