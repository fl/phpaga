/*
 * phpaga
 *
 * This file contains some core JavaScript functionality for phpaga.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2006, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

function min2hrs(minutes) {
    var m = padNum(minutes % 60);
    return Math.floor(minutes/60)+':'+m;
}

function setCurrentDateTime(did, tid) {
    $('#'+did).val(Date.today().toString("yyyy-MM-dd"));
    $('#'+tid).val(new Date().toString("HH:mm"));
}

function setSelectDefaultOption(el, val) {
    var defOption = $("option[value='"+val+"']",el);
    defOption.attr("selected", "selected");
    setTimeout(function(){ $(el).trigger('change'); },2500);
}

function getDateTimeDiff(date2, date1) {
    var B, D, E, F = 0;
    var diff, hours, mins = 0;

    B = /^(\d{4})-(\d{1,2})-(\d{1,2}) (\d{1,2})[-:]{1}(\d\d)$/.test(date1);
    if (B) {
        with (RegExp) {
            D = new Date($1, $2 - 1, $3, $4, $5);
        }
    }

    E = /^(\d{4})-(\d{1,2})-(\d{1,2}) (\d{1,2})[-:]{1}(\d\d)$/.test(date2);
    if (E) {
        with (RegExp) {
            F = new Date($1, $2 - 1, $3, $4, $5);
        }
    }

    if (!D || !E)
        return('');
    else {
        diff = ((D.getTime() - F.getTime()) / 1000) / 60;
        hours = padNum(Math.floor(diff / 60));
        mins = padNum(diff % 60);
        return hours + ':' + mins;
    }
}

function setmenu(mainmenu) {
    var items = '<ul>';
    var mitem;
    var selmenu = menu[mainmenu];
    for (mitem in selmenu) {
        if (mitem != 'each') {
            items += '<li><a href="'+ mitem + '">' + selmenu[mitem] + '</a></li>';
        }
    }
    items += '</ul>';
    $("#ctxtnav").html(items);
    $("#navitems > li").each(function() { $(this).removeClass('active'); });
    $("#nav"+mainmenu).addClass('active');
}

function clockTimerAction() {
    $.ajax({
            url: 'service.php?saction=49',
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function(result) {
                $("#clock").html(result.data.time);
            }
        }
    );
}

/* Various helper funtions */

function isNumeric(val) {
    var charpos = val.search("[^0-9.]");
    if(val.length > 0 &&  charpos >= 0)
        return false;
    else
        return true;
}

function padNum(val, len) {
    val = String(val);
    len = len || 2;
    while (val.length < len) val = "0" + val;
    return val;
}


function projectFmt(cellvalue, options, rowObject) { return '<a href="project.php?id='+rowObject.prj_id+'">' + cellvalue + '</a>'; }

function personFmt(cellvalue, options, rowObject) { return '<a href="person.php?id='+rowObject.pe_id+'">' + cellvalue + '</a>'; }


/* jQuery UI slider handling adapted from
 * http://www.switchonthecode.com/tutorials/using-jquery-slider-to-scroll-a-div
 */

function handleSliderChange(e, ui) {
    var maxScroll = $("#content-scroll").attr("scrollWidth") -
    $("#content-scroll").width();
    $("#content-scroll").animate({scrollLeft: ui.value * (maxScroll / 100) }, 1000);
}

function handleSliderSlide(e, ui) {
    var maxScroll = $("#content-scroll").attr("scrollWidth") -
    $("#content-scroll").width();
    $("#content-scroll").attr({scrollLeft: ui.value * (maxScroll / 100) });
}

