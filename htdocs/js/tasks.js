/*
 * phpaga
 *
 * This file contains some JavaScript functionality for phpaga.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2006, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


function submitTaskFormDyn(addDialog, viewDialog) {
    var prj_id = $("#prj_id");
    var pe_id = $("#pe_id");
    var op_startdate = $("#op_startdate");
    var op_starthours = $("#op_starthours");
    var op_enddate = $("#op_enddate");
    var op_endhours = $("#op_endhours");
    var opcat_id = $("#opcat_id");
    var op_duration = $("#op_duration");
    var op_desc = $("#op_desc");

    var data = 'saction=47&prj_id='+prj_id.val()+'&pe_id='+pe_id.val()
        +'&op_startdate='+op_startdate.val()+'&op_starthours='+op_starthours.val()
        +'&op_enddate='+op_enddate.val()+'&op_endhours='+op_endhours.val()
        +'&opcat_id='+opcat_id.val()+'&op_duration='+op_duration.val()
        +'&op_desc='+op_desc.val();

    if ($("#op_billabletype").attr('checked'))
        data += '&op_billabletype='+$("#op_billabletype").val()

    $.ajax({
        url: 'service.php',
        type: 'POST',
        data: data,
        dataType: 'json',
        cache: false,
        success: function(result) {
            data = result.data;
            if (result.status != 0) {
                alert(result.message);
            } else {
                $("#prjlist").jqGrid().trigger("reloadGrid",[{page:1}]);
                $("#oplist").jqGrid().trigger("reloadGrid",[{page:1}]);
                addDialog.dialog('close');
                reload_main_charts();
            }
        }
    });

    return false;
}

