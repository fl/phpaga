/*
 * This file contains some wrapper functions that help with creating charts
 * (using jqPlot).
 */


function get_simple_bar_chart(id, data) {

    var plot = $.jqplot(id, [data.values], {
            title: data.title, 
            grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer, 
                rendererOptions:{barPadding: 2, barMargin: 2},
                shadow: false,
            },
            axes:{
                xaxis:{
                    renderer:$.jqplot.CategoryAxisRenderer,
                    rendererOptions: { tickRenderer: $.jqplot.CanvasAxisTickRenderer },
                    ticks: data.ticks,
                    tickOptions: { angle: -90 },
                },
                yaxis: { autoscale:true, min:0, tickOptions:{formatString:'%d'} },
            },
            highlighter: {show:true, sizeAdjust: 7.5, useAxesFormatters:false, tooltipAxes: 'y'},
    });
    return plot;
}

function get_multi_bar_chart(id, data) {

    var h_series = [];

    $.each(data.series_labels, function(l, v) {
        h_series.push({label:v});
    });

    var plot = $.jqplot(id, data.values, {
            title: data.title, 
            grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
            series: h_series,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer, 
                rendererOptions:{barPadding: 2, barMargin: 2},
                shadow: false,
            },
            legend:{show:true},
            axes:{
                xaxis:{
                    renderer:$.jqplot.CategoryAxisRenderer,
                    ticks: data.ticks,
                },
                yaxis: { autoscale:true, min:0, tickOptions:{formatString:'%d'} },
            },
            highlighter: {show:true, sizeAdjust: 7.5, useAxesFormatters:false, tooltipAxes: 'y'},
    });
    return plot;
}


function get_pie_chart(id, data) {

    var plot = $.jqplot(id, [data.values], {
            title: data.title,
            grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
            seriesDefaults:{renderer:$.jqplot.PieRenderer, shadow: false, rendererOptions:{sliceMargin:8}},
            legend:{show:true}
    });
    return plot;
}


function get_mhrsplot_year(id, data) {  
     ticks = [];
     $.each(data.ticks, function(i,item){ ticks.push(String(item)); });

     var calweekmhplot = $.jqplot(id, [data.values], {
        title: data.title, 
        grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer, 
            rendererOptions:{barPadding: 2, barMargin: 2},
            shadow: false,
        },
        axes:{
            xaxis:{
                renderer:$.jqplot.CategoryAxisRenderer,
                rendererOptions: { tickRenderer: $.jqplot.CanvasAxisTickRenderer },
                ticks: ticks,
                tickOptions: { angle: -90 },
                label: data.xlabel,
                },
            yaxis: { autoscale:true, min:0, tickOptions:{formatString:'%d'} },
        },
        highlighter: {show:true, sizeAdjust: 7.5, useAxesFormatters:false, tooltipAxes: 'y'}
    });

    return calweekmhplot;
}


function get_mhrsplot_month(id, data) {  
    ticks = [];
    dmin = null;
    dmax = null;
    $.each(data.ticks, function(i,item){ d = Date.parse(item); ticks.push(d.toString('d')); });

    var daymhplot = $.jqplot(id, [data.values], {
       title: data.title,
       grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
       seriesDefaults:{
           renderer:$.jqplot.BarRenderer, 
           //rendererOptions:{barPadding: 8, barMargin: 20}
           shadow: false,
       },
       axes:{
           xaxis:{
               renderer:$.jqplot.CategoryAxisRenderer,
               rendererOptions: { tickRenderer: $.jqplot.CanvasAxisTickRenderer },
               ticks: ticks,
               tickOptions: { angle: -90 },
               label: data.xlabel,
               },
           yaxis: { autoscale:true, min:0, tickOptions:{formatString:'%d'} },
       },
       highlighter: {show:true, sizeAdjust: 7.5, useAxesFormatters:false, tooltipAxes: 'y'}
    });
    return daymhplot;
}


function get_opcat_plot(id, data) {  
    var ocplot = $.jqplot(id, [data.values], {
        width: $(id).width(),
        height:$(id).height(),
        title: data.title, 
        grid: { borderWidth: 0, gridLineColor: '#dddddd', shadow: false },
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer, 
            rendererOptions:{barPadding: 2, barMargin: 2},
            shadow: false,
        },
        axes:{
            xaxis:{
                renderer:$.jqplot.CategoryAxisRenderer,
                rendererOptions: { tickRenderer: $.jqplot.CanvasAxisTickRenderer },
                ticks: data.ticks,
                tickOptions: { angle: -45 },
            },
            yaxis: { autoscale:true, min:0, tickOptions:{formatString:'%d'} },
        },
        highlighter: {show:true, sizeAdjust: 7.5, useAxesFormatters:false, tooltipAxes: 'y'},
    });
    return ocplot;
}


function reload_main_charts() {
    var year = $('#year').html();
    var month = parseInt($('#month').html());
    var pe_id = parseInt($('#currperson').val());

    $('#mhchartyear').empty();
    $.getJSON("data_service.php?req=mhrs&pe_id="+pe_id+"&type=4&date="+year+"-01-01",  
        function(data) { mhrsplot = get_mhrsplot_year('mhchartyear', data); });

    $('#mhchartmonth').empty();
    $.getJSON("data_service.php?req=mhrs&pe_id="+pe_id+"&type=2&date="+year+"-"+month+"-01", 
        function(data) { mhrsplot = get_mhrsplot_month('mhchartmonth', data); });

    $.getJSON("data_service.php?req=mhrsstatpe&pe_id="+pe_id,
        function(data) { 
            $('#mh_fy').html(data.fy);
            $('#mh_curr_fy').html(data.curr_fy);
            $('#mh_curr_month').html(data.curr_month);
            $('#mh_curr_week').html(data.curr_week);
            $('#mh_spark').sparkline(data.spark_month);
            $('#mh_oprange').html(data.op.begin+' - '+data.op.end);
        });
}
