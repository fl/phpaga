<?php
/**
 * phpaga
 *
 * file preview
 *
 * Preview a file that has been associated to an internal object
 * (for example a project, a task, a company, a person, ...)
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2006, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS
 * IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');
ob_start();

$file = PFile::getInfo(phpaga_fetch_REQUEST('id'));
if (PhPagaError::isError($file)) {
    $file->printMessage();
    die();
}

switch ($file['file_rel_type']) {
case PHPAGA_RELTYPE_PROJECT:
    if (!phpaga_project_canview($file['file_rel_id'])) {
        phpaga_error(_('You are not allowed to access this file.'));
        die();
    }
default:
    break;
}

$fObj = null;
try {
    $fObj = new fFile($file['full_path']);
} catch (Exception $e) {
    phpaga_error($e->getMessage());
    die();
}

if (!in_array($fObj->getMimeType(), $phpaga_preview_types))
    die();

$fThumb = null;
$thumbnail = $file['thumbnail'];
$thumb_dir = dirname($thumbnail);
if (!is_dir($thumb_dir) && !mkdir($thumb_dir)) {
    phpaga_error(_('Unable to create thumbnail directory'));
    exit;
}

if (file_exists($thumbnail)) {
    try {
        $fThumb = new fFile($thumbnail);
        if ($fThumb->getMTime() < $fObj->getMtime())
            $fThumb->delete();
    } catch (Exception $e) { }
}

if (!file_exists($thumbnail)) {
    $full_path = $file['full_path'];
    if (in_array($fObj->getMimeType(), $phpaga_preview_types_firstpage))
        $full_path .='[0]'; // first page only

    $command = sprintf('%s -size 100x100 %s -resize 100x100 %s',
        PHPAGA_BIN_CONVERT,
        escapeshellcmd($full_path),
        escapeshellcmd($thumbnail));
    $ex = `$command`;

    $fThumb = new fFile($thumbnail);
}

//header('Expires: Thu, 26 Jul 2007 11:00:00 GMT');
//header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
//header('Cache-Control: no-cache, must-revalidate');
//header('Cache-Control: post-check=0, pre-check=0', false);
//header('Pragma: no-cache');
//header('Content-Disposition: inline');
//header('Content-length: '.filesize($thumbnail));

header('Expires: ');
header('Last-Modified: '.$fThumb->getMTime()->format('D, d M Y H:i:s'));
header('Pragma: ');
header('Content-Disposition: inline; filename='.$fThumb->getName());
header('Content-type: image/jpg');

if ($f = fopen($thumbnail, 'rb'))
    while(!feof($f))
        print fread($f, 4096);
ob_end_flush();

?>
