<?php
/**
 * phpaga
 *
 * Print invoice(s) to PDF
 *
 * Prints one or more invoices to a PDF file.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define('PHPAGA_COMPRESSOUT_DISABLED', true);
include_once("./config.php");

PUser::protectPage(PHPAGA_PERM_MANAGE_INVOICES);

include_once(PHPAGA_LIBPATH."pdf.php");

$append_oplist = phpaga_fetch_REQUEST('oplist', false);
$append_timesheet = phpaga_fetch_REQUEST('timesheet', false);
$letterhead = phpaga_fetch_REQUEST('letterhead', false);
$download = phpaga_fetch_REQUEST('download', false);
$errors = array();
$filename = 'invoices.pdf';

$bills = phpaga_fetch_REQUEST('id');

if (is_null($bills) || (!is_array($bills)  && !is_numeric($bills))) {
    phpaga_error(_('No invoice has been selected to be printed to PDF.'));
    exit;
}

if (!is_array($bills))
    $bills = array($bills);

$cls = PHPAGA_PDF_INCOICE_TPL;
$pdf = new $cls(PDF_PAGE_ORIENTATION, PDF_UNIT, strtoupper(PHPAGA_PDF_DOCFORMAT), true, 'UTF-8', false);

$pdf->SetTitle('Invoice');

foreach ($bills as $bill_id) {

    $billInfo = phpaga_bills_getinfo($bill_id);

    if (PhPagaError::isError($billInfo))
        $errors[] = $billInfo->getMessage();
    else {

        $customer = PCompany::getInfo($billInfo["cpn_id"]);

        if (PhPagaError::isError($customer))
            $errors[] = $customer->getMessage();
        else {

            $address = array();

            if (isset($billInfo["bill_person_recipient"]) && strlen($billInfo["bill_person_recipient"]))
                $address[] = $billInfo["bill_person_recipient"];
            if (isset($customer["cpn_address"]) && strlen($customer["cpn_address"]))
                $address[] = $customer["cpn_address"];
            if (isset($customer["full_location"]) && strlen($customer["full_location"]))
                $address[] = $customer["full_location"];
            if (isset($customer["cpn_taxnr"]) && strlen($customer["cpn_taxnr"]))
                $address[] = _('VAT number').' '.$customer["cpn_taxnr"];

            $recipient = array('name' => $billInfo['cpn_name'],
                'address' => $address);

            $pdf->AddPage();
            $pdf->setPrintHeader(false);
            if ($letterhead)
                $pdf->AddLetterhead();
            else
                $pdf->setPrintHeader(false);

            if (count($bills) == 1)
                $filename = sprintf("invoice_%s-%s.pdf", date("Y", strtotime($billInfo["bill_date"])), $billInfo["bill_number"]);

            $pdf->AddRecipient($recipient);
            $pdf->AddBody($billInfo);

            if ((count($billInfo["operations"]["oprows"]) > 0) && ($append_oplist || $append_timesheet)) {
                if ($append_oplist) {
                    $pdf->AddPage();
                    $pdf->setPrintFooter(false);
                    $pdf->AddTaskList($billInfo['operations']['oprows']);
                }

                if ($append_timesheet) {
                    $pdf->AddPage();
                    $pdf->setPrintFooter(false);
                    $pdf->AddTimeSheet($billInfo['operations']['oprows']);
                }
            }
        }
    }
}

if (count($errors) > 0)
    phpaga_error($errors);
else {
    $dest = $download ? 'D' : 'I';
    $pdf->Output($filename, $dest);
}

?>
