<?php
  /**
   * phpaga
   *
   * requirements test
   *
   * This file tests if all necessary requirements for running phpaga are
   * fulfilled.
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2003, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

session_name("phpaga_test");
session_start();

if (!isset($_SESSION["phpaga_test"])) {
    $_SESSION["phpaga_test"] = 1;
}

$_SESSION["phpaga_test"]++;

$phpaga_test = $_SESSION["phpaga_test"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>phpaga: System Requirements Test</title>
    <style type="text/css">
    <!--
body {
  font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans",Verdana,Arial,sans-serif;
  color: #444;
  background-color: #fff;
  margin: 10px 20px;
  font-size: 0.8em;
}

#header { border-bottom: solid 1px #ccc; }
#header h1 { color: #358695; }
#header h1 span { color: #bbcacd; }

h2 {font-size: 90%;}

ul li {font-weight: normal;color:#000;}
ul li em {font-weight: normal;color:#666;margin-right: 5px;}

.yes {color: green;}
.no {color: red;}

a:link, a:visited, a:active {color: #222;}
-->
</style>
</head>
<body>
 <div id="header"><h1><span>ph</span>paga</h1>
<p>phpaga: System Requirements Test</p>
</div>

    <?php

    $definitions = array("PHPAGA_BASEPATH",
                         "PHPAGA_DB_SYSTEM",
                         "PHPAGA_DB_HOST",
                         "PHPAGA_DB_NAME",
                         "PHPAGA_DB_LOGIN",
                         "PHPAGA_DB_PASSWD"
        );

function fulfilled($text = "", $bool) {
    $class = $bool ? 'yes' : 'no';
    return("<span class=\"$class\">$text</span>");
}

function yesno($bool) {
    $yn = $bool ? 'Yes' : 'No';
    return(fulfilled($yn, $bool));
}

function sectionstart($text = "") { print("<h2>$text</h2>\n<ul>\n"); }

function sectionend() { print("</ul>\n"); }

function printrow($label = "", $value = "") {
    print("<li><em>$label</em> $value</li>\n");
}

function contains_illegal_whitespace($filename) {
    $content = implode("", file($filename));
    return (($content[0] != "<")
            || (($content[strlen($content)-2] != ">")
                && ($content[strlen($content)-1] != ">") ));
}

/* ----------------------------------------------------------------- */

error_reporting(E_ALL);

$illegal_whitespace_message = "The file %s contains illegal whitespace or ".
    "other characters outside the PHP tags &lt;?php and ?&gt;. Please remove ".
    "all illegal characters outside these tags.";

$htdocs_config_filename = "./config.php";
$etc_config_filename = "";
$etc_config = false;

$etc_config_local_filename = "";
$etc_config_local = false;

$pgsql = extension_loaded("pgsql");
$mysql = extension_loaded("mysql");

$pdo_pgsql = extension_loaded("pdo_pgsql");
$pdo_mysql = extension_loaded("pdo_mysql");

$gd = extension_loaded("gd");
$gettext = extension_loaded("gettext");

$basepath = "";

$htdocs_config = (is_file($htdocs_config_filename) && is_readable($htdocs_config_filename));

if ($htdocs_config)
{

    $htdocs_config_content = implode('', file("./config.php"));

    if (preg_match("/[^\*]*(require|include).*[\"\'](.*)[\"\']/", $htdocs_config_content, $matches))
    {
        $etc_config_filename = $matches[2];
        $etc_config = (is_file($etc_config_filename) && is_readable($etc_config_filename));

        $etc_config_local_filename = dirname($etc_config_filename)."/config.local.php";
        $etc_config_local = (is_file($etc_config_local_filename) && is_readable($etc_config_local_filename));
    }
}

/* ----------------------------------------------------------------- */

sectionstart("phpaga");
$version = implode('', file("../VERSION"));
printrow("phpaga version", $version);
sectionend();

/* ----------------------------------------------------------------- */

sectionstart("Configuration files");

if (!$htdocs_config)
{
    printrow($htdocs_config_filename." readable", fulfilled("No -> You need to create the file config.php in your htdocs/ ".
                                                            "directory (see INSTALL).", false));
    sectionend();
}
else
{
    printrow($htdocs_config_filename." readable", yesno($htdocs_config));

    if (contains_illegal_whitespace($htdocs_config_filename))
        printrow("$htdocs_config_filename format",
                 fulfilled(sprintf($illegal_whitespace_message,
                                   $htdocs_config_filename), false));

    if ($etc_config)
    {
        printrow("etc/config.php location", $etc_config_filename);

        if (contains_illegal_whitespace($etc_config_filename))
            printrow("etc/config.php format",
                     fulfilled(sprintf($illegal_whitespace_message,
                                       $etc_config_filename), false));

    }

    printrow("etc/config.php readable", yesno($etc_config));


    if (!$etc_config_local)
        printrow($etc_config_local_filename." readable", fulfilled("No -> You need to create the file config.local.php in your etc/ ".
                                                                   "directory (see INSTALL).", false));
    else
    {
        printrow("etc/config.local.php location", $etc_config_local_filename);
        if (contains_illegal_whitespace($etc_config_local_filename))
            printrow("etc/config.local.php format",
                     fulfilled(sprintf($illegal_whitespace_message,
                                       $etc_config_local_filename), false));
        include_once($etc_config_local_filename);

        printrow("etc/config.local.php readable", yesno($etc_config_local));

        sectionend();


/* ----------------------------------------------------------------- */

        sectionstart("phpaga settings");

        foreach ($definitions as $def)
        {
            if (defined($def))
            {
                if ($def == "PHPAGA_DB_PASSWD")
                {
                    if (strlen(constant($def)))
                        printrow($def, fulfilled("[set]", true));
                    else
                        printrow($def, "[empty]");
                }
                elseif (($def == "PHPAGA_BASEPATH") && (!strlen(constant($def)) || !is_dir(constant($def))))
                    printrow($def, fulfilled("Invalid base path: ".constant($def), false));
                else
                    printrow($def, fulfilled(constant($def), true));
            }
            else
                printrow($def, fulfilled("[not set]", false));
        }

        $tmp_tpl_c_dir = PHPAGA_BASEPATH."templates_c";
        if (!is_dir($tmp_tpl_c_dir))
            printrow("directory for compiled templates", fulfilled("$tmp_tpl_c_dir does not exist", false));
        elseif (!is_writable($tmp_tpl_c_dir))
            printrow("directory for compiled templates", fulfilled("$tmp_tpl_c_dir is not writable", false));
        else
            printrow("directory for compiled templates", fulfilled("$tmp_tpl_c_dir exists and is writable", true));

        if (!defined("PHPAGA_FILEPATH"))
            define("PHPAGA_FILEPATH", PHPAGA_BASEPATH."files/");

        if (!is_dir(PHPAGA_FILEPATH))
            printrow("upload directory for files", fulfilled(PHPAGA_FILEPATH." does not exist", false));
        elseif (!is_writable(PHPAGA_FILEPATH))
            printrow("upload directory for files", fulfilled(PHPAGA_FILEPATH." is not writable", false));
        else
            printrow("upload directory for files", fulfilled(PHPAGA_FILEPATH." exists and is writable", true));

        if (!defined("PHPAGA_FILES_PREVIEW") || PHPAGA_FILES_PREVIEW) // enabled by default
            printrow("file thumbnails", fulfilled("enabled", true));
        else
            printrow("file thumbnails", fulfilled("disabled", false));

        if (!defined("PHPAGA_BIN_CONVERT")) {
            if (!file_exists('/usr/bin/convert'))
                printrow("file thumbnails creation tool", fulfilled("Full path to the executable is not defined - using default value /usr/bin/convert - file does not exist", false));
            else
                printrow("file thumbnails creation tool", fulfilled("Full path to the executable is not defined - using default value /usr/bin/convert - file exists", true));
        }
        elseif (!file_exists(PHPAGA_BIN_CONVERT))
            printrow("file thumbnails creation tool", fulfilled(PHPAGA_BIN_CONVERT." does not exist - you will not be able to see thumbnails for uploaded files", false));
        else
            printrow("file thumbnails creation tool", fulfilled(PHPAGA_BIN_CONVERT." exists", true));

        sectionend();

/* ----------------------------------------------------------------- */

        $mem = preg_match('/^(\d+)([a-zA-Z]+)/', trim(ini_get("memory_limit")), $memlimit);

        sectionstart("PHP");

        printrow("PHP OS", PHP_OS);
        printrow("PHP Version", PHP_VERSION);
        printrow("PHP uname", php_uname());
        printrow("PHP SAPI name", php_sapi_name());
        printrow("Server", $_SERVER["SERVER_SOFTWARE"]);
        printrow("PostgreSQL enabled", yesno($pgsql));
        printrow("PDO PostgreSQL enabled", yesno($pdo_pgsql));
        printrow("MySQL enabled", yesno($mysql));
        printrow("PDO MySQL enabled", yesno($pdo_mysql));
        printrow("GD enabled", yesno($gd));
        printrow("gettext enabled", yesno($gettext));
        printrow("include_path", ini_get("include_path"));
        printrow("session test counter", $phpaga_test);
        printrow("magic_quotes_gpc", get_magic_quotes_gpc() ? "On - phpaga does not need magic_quotes_gpc to be on and will strip slashes from \$REQUEST_DATA, \$_POST and \$_GET" : "Off (good)");
        printrow("register_globals", ini_get("register_globals") ? "On - phpaga does not need register_globals to be on, but other installed applications on your server might." : "Off (good)");

        if (isset($memlimit[1]) && ($memlimit[1] > 12))
            printrow("memory_limit", ini_get("memory_limit"));
        else
        {
            $str = sprintf("%s - this might not be sufficient; if you encounter problems like blank pages, try to increase the value of memory_limit in php.ini", ini_get("memory_limit"));
            printrow("memory_limit", fulfilled($str, false));
        }

        sectionend();

/* ----------------------------------------------------------------- */

        sectionstart("Languages and locales");

        $langfile = dirname($etc_config_filename)."/../classes/PhPagaLanguage.php";

        if (!is_file($langfile)
            || !is_readable($langfile))
        {
            printrow($langfile, fulfilled("Unable to find language file (this is fine for phpaga 0.2 and previous verisons)", false));
        }
        else
        {
            include_once($langfile);

            $phpaga_languages = PhPagaLanguage::getLocaleArray();
            if (!isset($phpaga_languages) || (count($phpaga_languages) == 0))
                printrow("Languages", fulfilled("NO AVAILABLE LANGUAGES", false));
            else
            {
                foreach($phpaga_languages aS $langid => $langarr)
                {

                    $langname = setlocale(LC_ALL, $langarr);

                    if (strstr(PHP_OS, 'WIN'))
                    {
                        putenv("LANG=".$langname);
                        putenv("LANGUAGE=".$langname);
                    }

                    $l = !($langname === false);

                    if ($l)
                        printrow($langname, yesno($l));
                    else
                        printrow($langid, fulfilled("NO - you need to install the proper locale if you want to offer this language to your users", false));
                }
            }
        }

        sectionend();

/* ----------------------------------------------------------------- */

        sectionstart("Database");

        $v = null;

        try {

            $db = new PDO(sprintf('%s:host=%s;dbname=%s',
                                  PHPAGA_DB_SYSTEM, PHPAGA_DB_HOST, PHPAGA_DB_NAME),
                          PHPAGA_DB_LOGIN, PHPAGA_DB_PASSWD);

            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $db->query("SELECT version()");
            $v = $stmt->fetchColumn();

            printrow("Database system", PHPAGA_DB_SYSTEM);
            printrow("Database connection", fulfilled("OK", true));
            printrow("Server version", $v);

        } catch (PDOException $e) {
            $db = null;
            printrow("Database connection", fulfilled("Error: ".$e->getMessage(), false));
        }

        sectionend();

/* ----------------------------------------------------------------- */

        sectionstart("3rd party libraries");

        define('PHPAGA_EXTPATH', PHPAGA_BASEPATH.'ext/');

        $sf = PHPAGA_EXTPATH.'smarty/libs/Smarty.class.php';
        $smarty = (is_file($sf) && is_readable($sf));

        $gf = PHPAGA_EXTPATH.'smarty-gettext/block.t.php';
        $smarty_g= (is_file($gf) && is_readable($gf));


        $tf = PHPAGA_EXTPATH.'tcpdf/tcpdf.php';
        $tcpdf = (is_file($tf) && is_readable($tf));

        $fl = PHPAGA_EXTPATH.'flourish/fCore.php';
        $flourish = (is_file($fl) && is_readable($fl));

        printrow("Flourish", yesno($flourish));
        printrow("Smarty", yesno($smarty));
        printrow("smarty-gettext", yesno($smarty_g));
        printrow("TCPDF", yesno($tcpdf));

        sectionend();
    }
}
?>
<div class="footer"><a href="http://phpaga.net/">phpaga.net</a></div>
</body>
</html>
