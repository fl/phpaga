<?php
/**
 * phpaga
 *
 * Bills
 *
 * This is the invoices (bills) management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$perms = array(PHPAGA_PERM_VIEW_INVOICES, PHPAGA_PERM_MANAGE_INVOICES);
PUser::protectPage($perms);

$search_args = "";
$rows = "";
$count = "";
$browse = "";
$select_paid = array(PHPAGA_BILL_PAID => _("paid"),
                     PHPAGA_BILL_NOTPAID => _("not paid"));
$offset = 0;
$billInfo = array('cpn_id' => '',
                  'bmt_id' => '',
                  'curr_id' => '',
                  'bill_additional_line' => '',
                  'bill_number' => '',
                  'bill_paid' => '',
                  'bill_sent' => '',
                  'bill_datefrom' => '',
                  'bill_dateto' => '',
                  'bill_year' => null);

phpaga_arrayAddOption($select_paid, PHPAGA_OPTION_ALL);

if (isset($REQUEST_DATA["offset"]) && is_numeric($REQUEST_DATA["offset"]))
    $offset = $REQUEST_DATA["offset"];

if (isset($REQUEST_DATA["cpn_id"]) && is_numeric($REQUEST_DATA["cpn_id"]))
    $billInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];

if (isset($REQUEST_DATA["bmt_id"]) && is_numeric($REQUEST_DATA["bmt_id"]))
    $billInfo["bmt_id"] = $REQUEST_DATA["bmt_id"];

if (isset($REQUEST_DATA["curr_id"]) && is_numeric($REQUEST_DATA["curr_id"]))
    $billInfo["curr_id"] = $REQUEST_DATA["curr_id"];

if (isset($REQUEST_DATA["bill_number"]) && strlen ($REQUEST_DATA["bill_number"]))
    $billInfo["bill_number"] = $REQUEST_DATA["bill_number"];

if (isset($REQUEST_DATA["bill_paid"]) && is_numeric($REQUEST_DATA["bill_paid"]) &&
    (($REQUEST_DATA["bill_paid"] ==  PHPAGA_BILL_PAID) ||
     ($REQUEST_DATA["bill_paid"] == PHPAGA_BILL_NOTPAID)))

    $billInfo["bill_paid"] = $REQUEST_DATA["bill_paid"];

if (isset($REQUEST_DATA["bill_sent"]) && is_numeric($REQUEST_DATA["bill_sent"]) &&
    (($REQUEST_DATA["bill_sent"] == PHPAGA_BILL_SENT) ||
     ($REQUEST_DATA["bill_sent"] == PHPAGA_BILL_NOTSENT)))

    $billInfo["bill_sent"] = $REQUEST_DATA["bill_sent"];

if (isset($REQUEST_DATA["bill_datefrom"]) && strlen($REQUEST_DATA["bill_datefrom"]))
    $billInfo["bill_datefrom"] = rawurldecode($REQUEST_DATA["bill_datefrom"]);

if (isset($REQUEST_DATA["bill_dateto"]) && strlen($REQUEST_DATA["bill_dateto"]))
    $billInfo["bill_dateto"] = rawurldecode($REQUEST_DATA["bill_dateto"]);

if (isset($REQUEST_DATA["bill_additional_line"]) && strlen($REQUEST_DATA["bill_additional_line"]))
    $billInfo["bill_additional_line"] = rawurldecode($REQUEST_DATA["bill_additional_line"]);

foreach ($billInfo as $key =>$value) {
    if (isset($value) && strlen($value))
        $search_args .= "&amp;$key=".rawurlencode ($value);
}

$rows = phpaga_bills_search($count, $billInfo, $offset, PHPAGA_RECORDS_PERPAGE);

phpaga_header(array('menuitem' => 'finance'));

if (PhPagaError::isError($rows))
    $rows->printMessage();

$tpl = new PSmarty;

$config = PConfig::getArray();

$tpl->assign('perm_invoices', PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES));
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));
$tpl->assign('billInfo', $billInfo);
$tpl->assign('config', $config);
$tpl->assign('select_cpn', PCompany::getSimpleArray(true, ''));
$tpl->assign('select_paid', $select_paid);
$tpl->assign('search_args', $search_args);
$tpl->assign('rows', $rows);
$tpl->assign('BROWSE', phpaga_navigate(basename($_SERVER['PHP_SELF']). '?$search_args',
                                       $count,
                                       $offset, PHPAGA_RECORDS_PERPAGE));

$tpl->display('bills.tpl.html');

phpaga_footer();

?>
