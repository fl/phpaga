<?php
  /**
   * phpaga
   *
   * Add a project member
   *
   * This modules adds persons to a project (members).
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */


include_once ("./config.php");

$member = array();
$member["pe_id"] = "";
$member["jcat_id"] = "";
$member["prjmem_hourlyrate"] = "";

$mode_edit = false;

$persons = array();
$jobcats = array();
$prjInfo = array();
$prjobcats = array();
$opcats = array();
$peopcats = array();

$error = array();

if (isset($REQUEST_DATA["prj_id"]) && strlen($REQUEST_DATA["prj_id"])) {

    $prjInfo = phpaga_projects_getInfo($REQUEST_DATA["prj_id"]);

    if (PhPagaError::isError($prjInfo))
        $prjInfo->printMessage();

} else {

    phpaga_header(array("menuitem" => "core"));
    phpaga_error(_("Invalid project"));
    phpaga_footer();
    exit;
}

if (!phpaga_project_canmanage($REQUEST_DATA["prj_id"]))
    PUser::protectPage(-1);
else {

    if (isset($REQUEST_DATA["edit"]) && ((int)$REQUEST_DATA["edit"] == ACTION_EDIT)) {
        $mode_edit = true;

        $member = phpaga_project_getMemberInfo($REQUEST_DATA["prj_id"], $REQUEST_DATA["pe_id"]);

        if (PhPagaError::isError($member))
            $member->printMessage();
        else {

            $prjopcats = phpaga_projects_gethourlyrates($REQUEST_DATA["prj_id"]);

            foreach ($prjopcats as $p) {

                $opcats[$p["opcat_id"]] = array("opcat_id" => $p["opcat_id"],
                                                "opcat_title" =>  $p["opcat_title"],
                                                "opcat_color" =>  $p["opcat_color"],
                                                "opcat_hourlyrate_person" => null,
                                                "opcat_hourlyrate_project" => $p["opcat_hourlyrate"]);

            }


            $peopcats = phpaga_projects_gethourlyrates($REQUEST_DATA["prj_id"], $REQUEST_DATA["pe_id"]);

            foreach ($peopcats as $p) {
                if (isset($opcats[$p["opcat_id"]])) {
                    $opcats[$p["opcat_id"]]["opcat_hourlyrate_person"] = $p["opcat_hourlyrate"];

                    $peopcats[] = array("opcat_id" => $p["opcat_id"],
                                        "opcat_hourlyrate" => $p["opcat_hourlyrate"]);
                }


            }
        }

    } else {

        if (isset($REQUEST_DATA["pe_id"]) && strlen($REQUEST_DATA["pe_id"])
            && isset($REQUEST_DATA["substatus"]) && ($REQUEST_DATA["substatus"] == 0)) {

            $peInfo = PPerson::getInfo($REQUEST_DATA["pe_id"]);
            if (PhPagaError::isError($peInfo))
                $peInfo->printMessage();
            else {
                $member["jcat_id"] = $peInfo["jcat_id"];
                $member["prjmem_hourlyrate"] = $peInfo["pe_hourlyrate"];
            }
        }

        $prjopcats = phpaga_projects_gethourlyrates($REQUEST_DATA["prj_id"]);

        foreach ($prjopcats as $p) {

            $opcats[$p["opcat_id"]] = array("opcat_id" => $p["opcat_id"],
                                            "opcat_title" =>  $p["opcat_title"],
                                            "opcat_color" =>  $p["opcat_color"],
                                            "opcat_hourlyrate_person" => null,
                                            "opcat_hourlyrate_project" => $p["opcat_hourlyrate"]);

        }

        if (isset($REQUEST_DATA["action"]) &&
            (($REQUEST_DATA["action"] == ACTION_PRJ_MEMBER_ADD) || ($REQUEST_DATA["action"] == ACTION_UPDATE))) {

            $iod = null;
            $orate = null;

            if (isset($REQUEST_DATA["opcat_rates"]) && is_array($REQUEST_DATA["opcat_rates"]))
                foreach ($REQUEST_DATA["opcat_rates"] as $oid => $orate) {
                    if (isset($opcats[$oid])) {
                        $opcats[$oid]["opcat_hourlyrate_person"] = $orate;

                        $peopcats[] = array("opcat_id" => $oid,
                                            "opcat_hourlyrate" => $orate);
                    }
                }
        }

        if (isset($REQUEST_DATA["prj_id"]) && strlen($REQUEST_DATA["prj_id"]))
            $member["prj_id"] = $REQUEST_DATA["prj_id"];

        if (isset($REQUEST_DATA["pe_id"]) && is_numeric($REQUEST_DATA["pe_id"]))
            $member["pe_id"] = $REQUEST_DATA["pe_id"];

        /* the current user's pe_id is "parent" of the new member */
        $member["prjmem_pe_id"] = $_SESSION["auth_user"]["pe_id"];

        if (isset($REQUEST_DATA["jcat_id"]) && is_numeric($REQUEST_DATA["jcat_id"]))
            $member["jcat_id"] = $REQUEST_DATA["jcat_id"];

        if ((!isset($REQUEST_DATA["substatus"]) || ($REQUEST_DATA["substatus"] != 0))
            && (isset($REQUEST_DATA["prjmem_hourlyrate"]) && strlen($REQUEST_DATA["prjmem_hourlyrate"]))) {

            /* get hourly rate, eventually convert all semicolons to dots */
            $member["prjmem_hourlyrate"] = str_replace(",", ".", $REQUEST_DATA["prjmem_hourlyrate"]);

            if (!is_numeric($member["prjmem_hourlyrate"]))
                $error[] = PHPAGA_ERR_INVALID_HOURLYRATE;

        }

        /* no errors? any action to perform? add member to project. */

        if (isset($REQUEST_DATA["substatus"]) && ($REQUEST_DATA["substatus"] == 1)
            && isset($REQUEST_DATA["action"])
            && (($REQUEST_DATA["action"] == ACTION_PRJ_MEMBER_ADD) || ($REQUEST_DATA["action"] == ACTION_UPDATE))
            && (count($error) == 0))
        {

            $member["opcats"] = $peopcats;

            if ($REQUEST_DATA["action"] == ACTION_UPDATE)
                $result = phpaga_projects_updatemember($member);
            else
                $result = phpaga_projects_addmember($member);

            if (PhPagaError::isError($result))
                $error = $result->getCode();
            else {

                header("Location: project.php?id=".$member["prj_id"]);
                exit;
            }
        }

        $s_userInfo = array("sort" => "person",
                            "notmemberof_prj_id" => $member["prj_id"]);


        // FIXME: The following is a hack to weed out those who are already members of this project.

        $h_persons = PUser::findAsArray(array('persons{pe_id}=>project_members.prj_id=' => $member['prj_id']), 
            array('persons{pe_id}.pe_lastname' => 'asc', 'persons{pe_id}.pe_firstname' => 'asc'));
        $alreadyMembers = array();
        foreach ($h_persons as $person)
            $alreadyMembers[$person["pe_id"]] = true;

        $h_persons = PUser::findAsArray(array(), array('persons{pe_id}.pe_lastname' => 'asc', 
            'persons{pe_id}.pe_firstname' => 'asc'));
        foreach ($h_persons as $person) {
            if (!isset($alreadyMembers[$person['pe_id']]))
                $persons[$person['pe_id']] = sprintf('%s (%s)', $person['pe_name'], $person['usr_login']);
        }

        phpaga_arrayAddOption($persons, PHPAGA_OPTION_SELECT);
    }

    $jobcats = phpaga_jobcat_getList();
    phpaga_arrayAddOption($jobcats, PHPAGA_OPTION_SELECT);

    phpaga_header(array("menuitem" => "core"));

    if (PhPagaError::isError($error))
        $error->printMessage();
    else if (count($error))
        phpaga_error($error);

    $tpl = new PSmarty;

    $tpl->assign("SELF", basename($_SERVER["PHP_SELF"]));

    $tpl->assign("member", $member);
    $tpl->assign("persons", $persons);
    $tpl->assign("jobcats", $jobcats);
    $tpl->assign("opcatlist", $opcats);
    $tpl->assign("mode_edit", $mode_edit);
    $tpl->display("prjmember.tpl.html");

}

phpaga_footer();

?>
