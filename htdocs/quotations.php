<?php
/**
 * phpaga
 *
 * Quotations
 *
 * This is the quotations management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$perms = array(PHPAGA_PERM_MANAGE_QUOTATIONS, PHPAGA_PERM_VIEW_QUOTATIONS);
PUser::protectPage($perms);

$search_args = "";
$rows = "";
$count = "";
$browse = "";
$select_company = "";
$offset = 0;

$quotInfo = array(
    'cpn_id' => '',
    'bmt_id' => '',
    'curr_id' => '',
	'quot_additional_line' => '',
    'quot_number' => '',
    'quot_sent' => '',
    'quot_datefrom' => '',
    'quot_dateto' => '',
    'quot_year' => null);


if (isset($REQUEST_DATA["offset"]) && is_numeric($REQUEST_DATA["offset"]))
    $offset = $REQUEST_DATA["offset"];

if (isset($REQUEST_DATA["cpn_id"]) && is_numeric($REQUEST_DATA["cpn_id"]))
    $quotInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];

if (isset($REQUEST_DATA["bmt_id"]) && is_numeric($REQUEST_DATA["bmt_id"]))
    $quotInfo["bmt_id"] = $REQUEST_DATA["bmt_id"];

if (isset($REQUEST_DATA["curr_id"]) && is_numeric($REQUEST_DATA["curr_id"]))
    $quotInfo["curr_id"] = $REQUEST_DATA["curr_id"];

if (isset($REQUEST_DATA["quot_number"]) && strlen($REQUEST_DATA["quot_number"]))
    $quotInfo["quot_number"] = $REQUEST_DATA["quot_number"];

if (isset($REQUEST_DATA["quot_additional_line"]) && strlen($REQUEST_DATA["quot_additional_line"]))
    $quotInfo["quot_additional_line"] = $REQUEST_DATA["quot_additional_line"];

if (isset($REQUEST_DATA["quot_sent"]) && is_numeric($REQUEST_DATA["quot_sent"]) &&
    (($REQUEST_DATA["quot_sent"] == PHPAGA_QUOTATION_SENT) ||
     ($REQUEST_DATA["quot_sent"] == PHPAGA_QUOTATION_NOTSENT)))

    $quotInfo["quot_sent"] = $REQUEST_DATA["quot_sent"];

if (isset($REQUEST_DATA["quot_datefrom"]) && ($REQUEST_DATA["quot_datefrom"] != ""))
    $quotInfo["quot_datefrom"] = rawurldecode($REQUEST_DATA["quot_datefrom"]);

if (isset($REQUEST_DATA["quot_dateto"]) && ($REQUEST_DATA["quot_dateto"] != ""))
    $quotInfo["quot_dateto"] = rawurldecode($REQUEST_DATA["quot_dateto"]);

foreach ($quotInfo as $key =>$value) {
    if (isset($value) && ($value != "")) {
        $search_args .= "&$key=".rawurlencode($value);
    }
}

$rows = phpaga_quotations_search($count, $quotInfo, $offset, PHPAGA_RECORDS_PERPAGE);

phpaga_header(array("menuitem" => "finance"));

if (PhPagaError::isError($rows)) {
    $rows->printMessage();
    $rows = null;
}

$tpl = new PSmarty;

$tpl->assign('perm_quotations', PUser::hasPerm(PHPAGA_PERM_MANAGE_QUOTATIONS));
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));
$tpl->assign('quotInfo', $quotInfo);
$tpl->assign('select_cpn', PCompany::getSimpleArray(true, ''));
$tpl->assign('rows', $rows);
$tpl->assign("BROWSE", phpaga_navigate(basename($_SERVER["PHP_SELF"])."?$search_args",
                                                  $count,
                                                  $offset, PHPAGA_RECORDS_PERPAGE));

$tpl->display('quotations.tpl.html');

phpaga_footer();

?>
