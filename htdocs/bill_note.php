<?php
  /**
   * phpaga
   *
   * Bill note
   *
   * Set a bill's note.
   *
   * @author Michael Kimmick <support@nichewares.com>
   * @version $Id: bill_note.php 1 2008-10-01 15:17:45 Michael Kimmick$
   *
   * Copyright (c) 2008, Michael Kimmick <support@nichewares.com>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

include_once ("./config.php");
PUser::protectPage(PHPAGA_PERM_MANAGE_INVOICES);

$bill_id = null;
$bill_note = null;
$update_result = null;
$billInfo = array ();
$rows = array ();

if (!isset($REQUEST_DATA["id"]) || !is_numeric($REQUEST_DATA["id"]))
    phpaga_error(PHPAGA_ERR_INVALID_INVOICEID);
else {

    $bill_id = $REQUEST_DATA["id"];

    if (isset($REQUEST_DATA["bill_note"]) ) {

        $bill_note = $REQUEST_DATA["bill_note"];
        $update_result = phpaga_bills_setInvoiceNote($bill_id, $bill_note);

        if (!PhPagaError::isError($update_result)) {
            header("Location: bill.php?id=".$bill_id);
            exit;
        }
    }

    phpaga_header(array("menuitem" => "finance"));

    $billInfo = phpaga_bills_getinfo($bill_id);

    if (!isset($REQUEST_DATA["bill_note"]) && is_null($bill_note))
        $bill_note = $billInfo['bill_note'];

    if (PhPagaError::isError($billInfo))
        $billInfo->printMessage();
    else {

        $tpl = new PSmarty;
        $tpl->assign("ACTION", basename($_SERVER["PHP_SELF"]));
        $tpl->assign("BILL_INFO", sprintf(_("Modify note for invoice %s"),
            $billInfo["bill_number"]."/".
            $billInfo["bill_date"]));
        $tpl->assign("billInfo", $billInfo);
        $tpl->display("bill_note.tpl.html");
    }

    phpaga_footer ();
}

?>
