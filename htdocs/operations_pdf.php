<?php
  /**
   * phpaga
   *
   * Print operations
   *
   * Print operations matching query options to pdf file
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

define('PHPAGA_COMPRESSOUT_DISABLED', true);
include_once ("./config.php");
include_once(PHPAGA_LIBPATH."pdf.php");

$fields = array('cpn_id',
                'cpn_id_issuebill',
                'prj_id',
                'pe_id',
                'prj_status',
                'prjcat_id',
                'jcat_id',
                'opcat_id',
                'op_desc',
                'op_billed',
                'op_startdatefrom',
                'op_startdateto');

$opInfo = array();
foreach ($fields as $f)
    $opInfo[$f] = phpaga_fetch_REQUEST($f);

$search_params_pdf = array();

if (is_numeric($opInfo['cpn_id'])) {
    $cpnInfo = PCompany::getInfo($opInfo['cpn_id']);
    if (!PhPagaError::isError($cpnInfo))
        $search_params_pdf[] = array('param' => _('Company'),
                                     'value' => $cpnInfo['cpn_name']);
}

if (is_numeric($opInfo['cpn_id_issuebill'])) {
    $cpnInfo = PCompany::getInfo ($opInfo['cpn_id_issuebill']);
    if (!PhPagaError::isError($cpnInfo))
        $search_params_pdf[] = array('param' => _('Company issuing the bill'),
                                     'value' => $cpnInfo['cpn_name']);
}

if (is_numeric($opInfo['prj_id'])) {
    $prjInfo = phpaga_projects_getInfo($opInfo['prj_id']);
    if (!PhPagaError::isError($prjInfo) && isset($prjInfo['prj_title']))
        $search_params_pdf[] = array('param' => _('Project'),
                                     'value' => $prjInfo['prj_title']);
} 

if (is_numeric($opInfo['pe_id'])) {
    $peInfo = PPerson::getInfo ($opInfo['pe_id']);
    if (!PhPagaError::isError($peInfo))
        $search_params_pdf[] = array('param' => _('Person'),
                                     'value' => $peInfo['pe_person']);
} 

if (is_numeric($opInfo['prj_status'])) {
    $prjstatList = phpaga_projects_getStatusList();
    if ((!PhPagaError::isError($prjstatList)) && isset($prjstatList[$opInfo['prj_status']]))
        $search_params_pdf[] = array('param' => _('Project status'),
                                     'value' => $prjstatList[$opInfo['prj_status']]);
}

if (is_numeric($opInfo['prjcat_id'])) {
    $prjcatList = phpaga_projects_getCategoryList();
    if ((!PhPagaError::isError($prjcatList)) && isset($prjcatList[$opInfo['prjcat_id']]))
        $search_params_pdf[] = array('param' => _('Project category'),
                                     'value' => $prjcatList[$opInfo['prjcat_id']]);
}

if (is_numeric($opInfo['jcat_id'])) {
    $jcatList = phpaga_jobcat_getList();
    if (!PhPagaError::isError($jcatList) && isset($jcatList[$opInfo['jcat_id']]))
        $search_params_pdf[] = array('param' => _('Job category'),
                                     'value' => $jcatList[$opInfo['jcat_id']]);
}

if (is_numeric($opInfo['opcat_id'])) {
    $opcatList = phpaga_operations_getCategoryList();
    if ((!PhPagaError::isError($opcatList)) && isset($opcatList[$opInfo['opcat_id']]))
        $search_params_pdf[] = array('param' => _('Task category'),
                                     'value' => $opcatList[$opInfo['opcat_id']]);
}

if (strlen($opInfo['op_desc'])) {
    $search_params_pdf[] = array('param' => _('Task description'),
                                 'value' => $opInfo['op_desc']);
}

if (is_numeric($opInfo['op_billed'])
    && (($opInfo['op_billed'] == PHPAGA_BILL_BILLED)
        || ($opInfo['op_billed'] == PHPAGA_BILL_NOTBILLED))) {
    $search_params_pdf[] = array('param' => _('Billing status'),
                                 'value' =>
                                 ($opInfo['op_billed'] == PHPAGA_BILL_BILLED) ? _('billed') : _('not billed'));
}

if (strlen($opInfo['op_startdatefrom'])) {
    $search_params_pdf[] = array('param' => _('Date from'),
                                 'value' => $opInfo['op_startdatefrom']);
}

if (strlen($opInfo['op_startdateto'])) {
    $search_params_pdf[] = array('param' => _('Date to'),
                                 'value' => $opInfo['op_startdateto']);
}

$letterhead = phpaga_fetch_REQUEST('letterhead', false);
$download = phpaga_fetch_REQUEST('download', false);

$rows = phpaga_operations_search($count, $duration, $opIds, $opInfo);

if (PhPagaError::isError($rows))
    $rows->printMessage();
else if ($count > 0) {
    $pdf = new PhPagaPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, strtoupper(PHPAGA_PDF_DOCFORMAT), true, 'UTF-8', false);
    $pdf->SetTitle('Tasks');

    $pdf->AddPage();
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->AddTable(array(
            array('width' => 40, 'name' => 'date', 'title' => _('Date')),
            array('width' => 10, 'name' => 'op_duration_hrs', 'title' => _('Duration'), 'align' => 'R'),
            array('width' => 30, 'name' => 'prj_title', 'title' => _('Project')),
            array('width' => 30, 'name' => 'op_person', 'title' => _('Person')),
            array('width' => 60, 'name' => 'op_desc', 'title' => _('Task'))),
        $rows, 
        array('title' => _('Tasks')));
    $dest = $download ? 'D' : 'I';
    $pdf->Output('tasks.pdf', $dest);
}

?>
