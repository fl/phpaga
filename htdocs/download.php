<?php
  /**
   * phpaga
   *
   * file download
   *
   * Download a file that has been associated to an internal object
   * (for example a project, a task, a company, a person, ...)
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2005, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

include_once("./config.php");

ob_start();

$id = phpaga_fetch_REQUEST('id');
if (!is_numeric($id)) {
    phpaga_error(_("Invalid file ID."));
    exit;
}

$file = PFile::getInfo($id);
if (PhPagaError::isError($file)) {
    $file->printMessage();
    exit;
}

switch ($file["file_rel_type"]) {
    case PHPAGA_RELTYPE_PROJECT:

        if (!phpaga_project_canview($file["file_rel_id"])) {
            phpaga_error(_("You are not allowed to access this file."));
            exit;
        }

    default:
        break;

}

$dest_dir = PHPAGA_FILEPATH.$file["file_rel_type"]."/".$file["file_rel_id"]."/";

if (!file_exists($dest_dir.$file["file_local_name"]))
    die(sprintf(_("%s: File does not exist"), $dest_dir.$file["file_local_name"]));

if (!is_readable($dest_dir.$file["file_local_name"]))
    die(sprintf(_("%s: File is not readable"), $dest_dir.$file["file_local_name"]));

header("Expires: Thu, 26 Jul 2007 11:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-Disposition: filename=".$file["file_original_name"]);
//header("Content-length: ".filesize($dest_dir.$file["file_local_name"]));

$mt = $file["file_mimetype"];

if (!strlen($mt)) {
    $mt = mime_content_type($dest_dir.$file["file_local_name"]);
}

header("Content-type: ".$mt);

if ($f = fopen($dest_dir.$file["file_local_name"], "rb"))

    while(!feof($f)) {
        $buffer = fread($f, 4096);
        print $buffer;
    }

ob_end_flush();

?>
