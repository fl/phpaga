<?php
/**
 * phpaga
 *
 * Products
 *
 * This is the products management module
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2005, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

phpaga_header(array('menuitem' => 'core'));

$gridconf = new PSimpleGridConf();
$gridconf->apiUrl = 'api.php/product';
$gridconf->idField = 'prod_id';
$gridconf->title = _('Products');
$gridconf->sortName = 'prod_name';
$gridconf->colNames = array('Id',  _('Category'), _('Code'), _('Name'), _('Price'), _('Consumption'), _('Stock'));
$gridconf->canManage = PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS);
$gridconf->width = 600;

$_id = new PSimpleGridColumn('prod_id', true);

$_code = new PSimpleGridColumn('prod_code');
$_code->width = 100;
$_code->editable = true;
$_code->enableSearch(array('sopt' => array('cn','bw','eq','ew','ne')));
$_code->setRequired();
$_code->setEditOptions(array('maxlength' => 18));

$_name = new PSimpleGridColumn('prod_name');
$_name->width = 160;
$_name->editable = true;
$_name->enableSearch(array('sopt' => array('cn','bw','eq','ew','ne')));
$_name->setRequired();
$_name->setEditOptions(array('maxlength' => 100));

$_catid = new PSimpleGridColumn('prcat_id');
$_catid->width = 100;
$_catid->editable = true;
$_catid->edittype = 'select';
$_catid->stype = 'select';
$_catid->formatter = 'select';
$_catid->enableSearch(array('sopt' => array('eq'), 'value' => PProductCategory::getGridSelectionStr()));
$_catid->setType('number');
$_catid->setEditOptions(array('value' => PProductCategory::getGridSelectionStr(true, '')));

$_price = new PSimpleGridColumn('prod_price');
$_price->width = 60;
$_price->editable = true;
$_price->enableSearch(array('sopt' => array('eq', 'lt','le','gt','ge')));
$_price->setEditOptions(array('maxlength' => 15));
$_price->searchrules = new stdClass();
$_price->searchrules->number = true;
$_price->setType('number');

$_consumption = new PSimpleGridColumn('prod_consumption');
$_consumption->width = 60;
$_consumption->editable = true;
$_consumption->enableSearch(array('sopt' => array('eq', 'lt','le','gt','ge')));
$_consumption->setEditOptions(array('maxlength' => 15));
$_consumption->setType('number');

$_stock = new PSimpleGridColumn('prod_stock');
$_stock->width = 60;
$_stock->editable = true;
$_stock->enableSearch(array('sopt' => array('eq', 'lt','le','gt','ge')));
$_stock->setEditOptions(array('maxlength' => 15));
$_stock->setType('integer');

$gridconf->colModel = array($_id, $_catid, $_code, $_name, $_price, $_consumption, $_stock);

$tpl = new PSmarty;
$tpl->assign('gridconf', $gridconf->build());
$tpl->display('simple_item_grid.tpl.html');


/*  FIXME
    $config = PConfig::getArray();
    monetary symbol
 */

phpaga_footer();

?>
