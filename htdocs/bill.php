<?php
/**
 * phpaga
 *
 * Bill info
 *
 * Show a bill's information.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @author Michael Kimmick <support@nichewares.com>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once ("./config.php");

$perms = array(PHPAGA_PERM_VIEW_INVOICES, PHPAGA_PERM_MANAGE_INVOICES);
PUser::protectPage($perms);

$bill_id = null;
$billInfo = array();
$opInfo = array();
$acerror = null;
$action = null;
$pmt_error = null;

if (isset ($REQUEST_DATA["id"]) && is_numeric($REQUEST_DATA["id"])) {

    $bill_id = $REQUEST_DATA["id"];

    if (isset($REQUEST_DATA["action"]) && is_numeric($REQUEST_DATA["action"])) {

        $action = $REQUEST_DATA["action"];

        switch($action) {

            case ACTION_DELETE:

                if (PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES) && is_numeric($bill_id)) {
                    $acerror = phpaga_bills_delete($bill_id);
                    if (!PhPagaError::isError($acerror))
                        header('Location: bills.php');
                }
                break;

            default:
                break;
        }
    }
    elseif (isset($REQUEST_DATA["add_latefee"]) && $REQUEST_DATA["add_latefee"] == "true") {
	//$acerror = phpaga_add_latefee($bill_id);
        $acerror = PLateFee::add($bill_id);
	if (!PhPagaError::isError($acerror))
		header('Location: bill.php?id='.$bill_id);
    }

    phpaga_header(array("menuitem" => "finance"));

    if (isset($REQUEST_DATA["pmt_error"]) && strlen($REQUEST_DATA["pmt_error"])) {
	$pmt_error = $REQUEST_DATA["pmt_error"];
	if ($pmt_error == PHPAGA_ERR_DEL_DEPENDENCIES)
		 phpaga_error(_('Payment cannot be deleted because this Invoice has been paid in full i.e. payment date set.'));
    }

    if (PhPagaError::isError($acerror)) {

        if (($acerror->getCode() == PHPAGA_ERR_DB_DUPLICATE) && ($action == ACTION_DELETE))
            phpaga_error(_('This invoice cannot be deleted as it is the base of a recurring invoice. Remove the recurring invoice first.'));
        else
            $acerror->printMessage();
    }

    $billInfo = phpaga_bills_getinfo($bill_id);

    if (PhPagaError::isError($billInfo))
        $billInfo->printMessage();
    else {
	$config = PConfig::getArray();

        $tpl = new PSmarty;
        $tpl->assign("perm_invoices", PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES));
	$tpl->assign('config', $config);
        $tpl->assign("billInfo", $billInfo);
        $tpl->display("bill.tpl.html");
    }

    phpaga_footer();
} else {
    phpaga_header(array("menuitem" => "finance"));
    phpaga_error(PHPAGA_ERR_INVALID_INVOICEID);
    phpaga_footer();
}

?>
