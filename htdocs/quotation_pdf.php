<?php
/**
 * phpaga
 *
 * Print quotation to pdf
 *
 * Prints a quotation to a pdf file
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define('PHPAGA_COMPRESSOUT_DISABLED', true);
include_once("./config.php");
include_once(PHPAGA_LIBPATH."pdf.php");

PUser::protectPage(PHPAGA_PERM_MANAGE_QUOTATIONS);

$letterhead = phpaga_fetch_REQUEST('letterhead', false);
$download = phpaga_fetch_REQUEST('download', false);
$errors = array();
$filename = 'quotations.pdf';

$quotations = phpaga_fetch_REQUEST('id');

if (is_null($quotations) || (!is_array($quotations)  && !is_numeric($quotations))) {
    phpaga_error(_('No valid quotation has been selected to be printed to PDF.'));
    exit;
}

if (!is_array($quotations))
    $quotations = array($quotations);

$cls = PHPAGA_PDF_QUOTATION_TPL;
$pdf = new $cls(PDF_PAGE_ORIENTATION, PDF_UNIT, strtoupper(PHPAGA_PDF_DOCFORMAT), true, 'UTF-8', false);

$pdf->SetTitle('Quotation');

foreach ($quotations as $quot_id) {

    $quotInfo = phpaga_quotations_getinfo($quot_id);

    if (PhPagaError::isError($quotInfo))
        $errors[] = $quotInfo->getMessage();
    else {

        $customer = PCompany::getInfo($quotInfo["cpn_id"]);

        if (PhPagaError::isError($customer))
            $errors[] = $customer->getMessage();
        else {

            $address = array();

            if (isset($quotInfo["quot_person_recipient"]) && strlen($quotInfo["quot_person_recipient"]))
                $address[] = $quotInfo["quot_person_recipient"];
            if (isset($customer["cpn_address"]) && strlen($customer["cpn_address"]))
                $address[] = $customer["cpn_address"];
            if (isset($customer["full_location"]) && strlen($customer["full_location"]))
                $address[] = $customer["full_location"];
            if (isset($customer["cpn_taxnr"]) && strlen($customer["cpn_taxnr"]))
                $address[] = _('VAT number').' '.$customer["cpn_taxnr"];

            $recipient = array('name' => $quotInfo['cpn_name'],
                'address' => $address);

            $pdf->AddPage();
            if ($letterhead)
                $pdf->AddLetterhead();
            else
                $pdf->setPrintHeader(false);

            if (count($quotations) == 1)
                $filename = sprintf("quotation_%s-%s.pdf", date("Y", strtotime($quotInfo["quot_date"])), $quotInfo["quot_number"]);

            $pdf->AddRecipient($recipient);
            $pdf->AddBody($quotInfo);
        }
    }
}

if (count($errors) > 0)
    phpaga_error($errors);
else {
    $dest = $download ? 'D' : 'I';
    $pdf->Output($filename, $dest);
}

?>
