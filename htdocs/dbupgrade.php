<?php
/**
 * phpaga
 *
 * Db upgrade
 *
 * This file checks for available database upgrades and applies them.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");
require_once(PHPAGA_LIBPATH.'dbtools.php');
define('PHPAGA_DBUPGRADE', true);

PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);

$result = null;
$error = false;
$errormsg = null;
$dberror = null;

if ((PHPAGA_DB_SYSTEM == 'mysql') && !defined("PHPAGA_MYSQLBIN"))
    $errormsg = phpaga_returnerror(sprintf(_("Please configure PHPAGA_MYSQLBIN in the configuration file %s"), PHPAGA_ETCPATH.'config.local.php'));
else {
    if (isset($REQUEST_DATA["action"]) && ($REQUEST_DATA["action"] == 1)) {

        $dbupgrade = phpaga_db_getUpgradeInfo();

        foreach($dbupgrade["files"] as $file) {
            $result = phpaga_db_applyUpgrade($file["name"], $dberror);

            if (PhPagaError::isError($result)) {
                $error = true;
                break;
            }
        }

        if ($error) {
            $errormsg = $result->getMessage()."\n".$dberror;
            $errormsg = phpaga_returnerror($errormsg);
        } else {
            header('Location: dbupgrade.php');
            exit;
        }
    }
}

$dbupgrade = phpaga_db_getUpgradeInfo();

phpaga_header(array("menuitem" => "system"));

if (PhPagaError::isError($result))
    $result->printMessage();

$tpl = new PSmarty;
$tpl->assign("errormsg", $errormsg);
$tpl->assign("dbversion", PHPAGA_VERSION_DB);
$tpl->assign("dbupgrade", $dbupgrade);
$tpl->display("dbupgrade.tpl.html");

phpaga_footer();

?>
