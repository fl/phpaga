<?php
/**
 * phpaga
 *
 * Installation wizard
 *
 * This is the first-time installation wizard that helps creating the
 * first user.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2004, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


define('PHPAGA_INSTALL', true);

include_once('./config.php');
require_once(PHPAGA_LIBPATH.'dbtools.php');

$errmsg = '';
$struct = false;
$structsuccess = false;
$errormessage = null;

if (isset ($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == 'dbinst'))
{

    if ((PHPAGA_DB_SYSTEM == 'mysql') && !defined('PHPAGA_MYSQLBIN')) {
        phpaga_error(_('Please set PHPAGA_MYSQLBIN in etc/config.local.php to the full path to mysql (the client binary).'));
        $tpl = new PSmarty;
        $tpl->display('install_dbstructmiss.tpl.html');
        exit;
    }

    if ((PHPAGA_DB_SYSTEM == 'mysql') && defined('PHPAGA_MYSQLBIN') && (!is_file(PHPAGA_MYSQLBIN) || !is_readable(PHPAGA_MYSQLBIN))) {
        phpaga_error(sprintf(_("Wrong setting for PHPAGA_MYSQLBIN in etc/config.local.php: Can't open %s"), PHPAGA_MYSQLBIN));
        exit;
    }

    $instres = phpaga_db_install($errormessage);

    if (PhPagaError::isError($instres)) {
        $instres->printMessage();
        phpaga_error($errormessage);
        exit;
    } else {
        /* Register the billing plugins */
        phpaga_bills_getPluginList();
        header('Location: install.php');
    }
}

$struct = phpaga_db_checkstructure();

if (PhPagaError::isError($struct)) {
    $struct->printMessage();
    exit;
}

if (!$struct) {
    $tpl = new PSmarty;
    $tpl->display('install_dbstructmiss.tpl.html');
    exit;
}

define('PHPAGA_ERRHANDLER_STANDARD', true);

$numusers = PUser::count();
if (PhPagaError::isError($numusers)) {
    $numusers->printMessage();
    exit;
}

if ($numusers > 0) {
    $tpl = new PSmarty;
    $tpl->display('install_complete.tpl.html');
    exit;
}

$error = array();
$action = phpaga_fetch_REQUEST('action');

if ($action==ACTION_ADD) {

    $person = new PPerson();
    $person->setPeLastname(phpaga_fetch_POST('pe_lastname'));
    $person->setPeFirstname(phpaga_fetch_POST('pe_firstname'));
    $person->setCtrId(phpaga_fetch_POST('ctr_id'));

/*

    if (isset($REQUEST_DATA["pe_lastname"]) && strlen($REQUEST_DATA["pe_lastname"]))
        $person["pe_lastname"] = $REQUEST_DATA["pe_lastname"];
    else
        $error[] = _("Insert your last name.");

    if (isset($REQUEST_DATA["pe_firstname"]) && strlen($REQUEST_DATA["pe_firstname"]))
        $person["pe_firstname"] = $REQUEST_DATA["pe_firstname"];


    if (isset($REQUEST_DATA["ctr_id"]) && is_numeric($REQUEST_DATA["ctr_id"]))
    {
        $person["ctr_id"] = $REQUEST_DATA["ctr_id"];
        $company["ctr_id"] = $REQUEST_DATA["ctr_id"];
    }
 */

    $company = new PCompany();
    $company->setCpnName(phpaga_fetch_POST('cpn_name'));
    $company->setCcatId(phpaga_fetch_POST('ccat_id'));
    $company->setCtrId(phpaga_fetch_POST('ctr_id'));

    $user = new PUser();
    $user->setUsrLogin(phpaga_fetch_POST('usr_login'));
    $user->setUsrSkin(defined('PHPAGA_DEFAULT_SKIN') ? PHPAGA_DEFAULT_SKIN : 'phpaga');

//        $error[] = _("Select a category for your company.");

    $pwd_new = phpaga_fetch_POST('usr_pwd_new');
    $pwd_confirm = phpaga_fetch_POST('usr_pwd_confirm');

    if (!strlen($pwd_new))
        $error[] = PHPAGA_ERR_AUTH_PASSWDLENGTH;
    else if (!strlen($pwd_confirm) || ($pwd_new!=$pwd_confirm))
        $error[] = _("The password and its confirmation do not match.");
    else
        $user->setPassword($pwd_new);

    if (count($error) == 0) {
        try {

            $db = PhPagaOrmDb::getDbInstance();

            $db->query('BEGIN');

            $person->store();
            $pe_id = $person->getPeId();

            $company->setPeId($pe_id);
            $company->store();
            $cpn_id = $company->getCpnId();

            $person->setCpnId($cpn_id);
            $person->store();

            $user->setPeId($pe_id);
            $user->setUsrPeId($pe_id);
            $user->setUsrPerm(implode('|', array_keys($phpaga_perm)));
            $user->store();
            $usr_id = $user->getUsrId();
            $db->query('COMMIT');

            $userconfig = phpaga_fetch_POST('userconfig');

            if (is_array($userconfig) && count($userconfig)) {
                foreach($userconfig as $key => $value) {
                    $result = PConfig::setUserItem($key, $value, $usr_id);
                    if (PhPagaError::isError($result))
                        $error[] = $result->printMesssage();
                }
            }

            if (count($error) == 0) {
                header("Location: ".basename($_SERVER["PHP_SELF"]));
                exit;
            }
        } catch (Exception $e) {
            $db->query('ROLLBACK');
            $error[] = $e->getMessage();
        }
    }
}

$userdata = array('usr_login' => phpaga_fetch_POST('usr_login'),
                  'usr_passwd' => phpaga_fetch_POST('usr_passwd'),
                  'pe_firstname' => phpaga_fetch_POST('pe_firstname'),
                  'pe_lastname' => phpaga_fetch_POST('pe_lastname'),
                  'ctr_id' => phpaga_fetch_POST('ctr_id', PHPAGA_DEFAULTCOUNTRY),
                  'cpn_name' => phpaga_fetch_POST('cpn_name'),
                  'ccat_id' => phpaga_fetch_POST('ccat_id')
    );

$tpl = new PSmarty;
$tpl->assign('SELF', basename($_SERVER['PHP_SELF']));

if (count($error) > 0)
    $tpl->assign('errmsg', phpaga_returnerror($error));
else
    $tpl->assign('errmsg', false);

$tpl->assign('userdata', $userdata);
$tpl->assign('select_ctr', PCountry::getSimpleArray());
$tpl->assign('select_pecat', PPersonCategory::getSimpleArray(true, ''));
$tpl->assign('select_ccat', PCompanyCategory::getSimpleArray());
$tpl->assign('jobcategories', PJobCategory::getSimpleArray());
$tpl->assign('skins', phpaga_getSkinNames());
$tpl->assign('languages', PhPagaLanguage::getList());

$tpl->display('install.tpl.html');

?>
