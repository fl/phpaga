<?php
/**
 * phpaga
 *
 * edit company
 *
 * Edit a company's data
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, 2003, 2004, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

$status = null;
$cpnInfo = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = $_POST['cpn_id'];
    try {
        $company = new PCompany($id);
        
        if (!$company->hasOwner($_SESSION['auth_user']['pe_id']) && !PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERCOMPANIES)) {
            phpaga_header(array('menuitem' => 'core'));
            phpaga_message(_('No access'), _('You are not allowed to modify this record.'));
            phpaga_footer();
            exit;
        }

        $company->setCpnName(phpaga_fetch_POST('cpn_name'));
        $company->setCpnAddress(phpaga_fetch_POST('cpn_address'));
        $company->setCpnAddress2(phpaga_fetch_POST('cpn_address2'));
        $company->setCpnCity(phpaga_fetch_POST('cpn_city'));
        $company->setCpnRegion(phpaga_fetch_POST('cpn_region'));
        $company->setCpnZip(phpaga_fetch_POST('cpn_zip'));
        $company->setCtrId(phpaga_fetch_POST('ctr_id'));
        $company->setCcatId(phpaga_fetch_POST('ccat_id'));
        $company->setCpnTaxnr(phpaga_fetch_POST('cpn_taxnr'));
        $company->setCpnPhone(phpaga_fetch_POST('cpn_phone'));
        $company->setCpnFax(phpaga_fetch_POST('cpn_fax'));
        $company->setCpnUrl(phpaga_fetch_POST('cpn_url'));
        $company->setCpnEmail(phpaga_fetch_POST('cpn_email'));
        $company->setCpnDefaultpayterm(phpaga_fetch_POST('cpn_defaultpayterm'));
        
        $company->store();

        header('Location: company.php?id='.$company->getCpnId());
        exit;
    } catch (Exception $e) {
        $status = phpaga_returnerror($e->getmessage());

        $cpnInfo = array();
        $cpnInfo['cpn_name'] = phpaga_fetch_POST('cpn_name');
        $cpnInfo['cpn_address'] = phpaga_fetch_POST('cpn_address');
        $cpnInfo['cpn_address2'] = phpaga_fetch_POST('cpn_address2');
        $cpnInfo['cpn_city'] = phpaga_fetch_POST('cpn_city');
        $cpnInfo['cpn_region'] = phpaga_fetch_POST('cpn_region');
        $cpnInfo['cpn_zip'] = phpaga_fetch_POST('cpn_zip');
        $cpnInfo['ctr_id'] = phpaga_fetch_POST('ctr_id');
        $cpnInfo['ccat_id'] = phpaga_fetch_POST('ccat_id');
        $cpnInfo['cpn_taxnr'] = phpaga_fetch_POST('cpn_taxnr');
        $cpnInfo['cpn_phone'] = phpaga_fetch_POST('cpn_phone');
        $cpnInfo['cpn_fax'] = phpaga_fetch_POST('cpn_fax');
        $cpnInfo['cpn_url'] = phpaga_fetch_POST('cpn_url');
        $cpnInfo['cpn_email'] = phpaga_fetch_POST('cpn_email');
        $cpnInfo['cpn_defaultpayterm'] = phpaga_fetch_POST('cpn_defaultpayterm');
    }
} else {
    $cpnInfo = PCompany::getInfo(phpaga_fetch_GET('id'));
    if (PhPagaError::isError($cpnInfo))
        $status = $cpnInfo->getFormattedMessage();
}

phpaga_header(array('menuitem' => 'core'));

if (PhPagaError::isError($cpnInfo)) {
    $cpnInfo->printMessage();
} else if (($_SESSION['auth_user']['pe_id'] != $cpnInfo['pe_id']) && !PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERCOMPANIES))
    phpaga_message(_('No access'), _('You are not allowed to modify this record.'));
else {

    $tpl = new PSmarty;

    $tpl->assign('cpnInfo', $cpnInfo);
    $tpl->assign('select_ctr', PCountry::getSimpleArray());
    $tpl->assign('select_ccat', PCompanyCategory::getSimpleArray());
    $tpl->assign('STATUSMSG', $status);
    $tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

    $tpl->display('edit_company.tpl.html');
}

phpaga_footer ();

?>