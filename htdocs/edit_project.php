<?php
/**
 * phpaga
 *
 * add project
 *
 * Add projects
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$result = null;
$status = null;

$prjInfo = array();
$prjInfo["opcatlist"] = array();

if (!isset($REQUEST_DATA["prj_id"])) {
    phpaga_header(array("menuitem" => "core"));
    phpaga_error(_("Invalid project"));
    phpaga_footer();
    exit;
}

$prj_id = $REQUEST_DATA["prj_id"];

if (!phpaga_project_canmanage($prj_id))
    PUser::protectPage(-1);

/* if we're invoked by post, we have to save the modifications */
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $prjInfo["prj_id"] = $REQUEST_DATA["prj_id"];;
    $prjInfo["prjcat_id"] = $REQUEST_DATA["prjcat_id"];;
    $prjInfo["prj_desc"] = $REQUEST_DATA["prj_desc"];
    $prjInfo["prj_title"] = $REQUEST_DATA["prj_title"];
    $prjInfo["prj_startdate"] = $REQUEST_DATA["prj_startdate"];
    $prjInfo["prj_estimatedend"] = $REQUEST_DATA["prj_estimatedend"];
    $prjInfo["prj_deadline"] = $REQUEST_DATA["prj_deadline"];
    $prjInfo["prj_enddate"] = $REQUEST_DATA["prj_enddate"];
    $prjInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];
    $prjInfo["cpn_id_issuebill"] = $REQUEST_DATA["cpn_id_issuebill"];
    $prjInfo["prj_status"] = $REQUEST_DATA["prj_status"];
    $prjInfo["pe_id"] = $_SESSION["auth_user"]["pe_id"];
    $prjInfo["prj_prj_id"] = $REQUEST_DATA["prj_prj_id"];
    $prjInfo["prj_planned_manhours"] = $REQUEST_DATA["prj_planned_manhours"];
    $prjInfo["prj_planned_cost_manhours"] = $REQUEST_DATA["prj_planned_cost_manhours"];
    $prjInfo["prj_planned_cost_material"] = $REQUEST_DATA["prj_planned_cost_material"];
    $prjInfo["prj_billabletype"] = $REQUEST_DATA["prj_billabletype"];
    $prjInfo["prj_priority"] = $REQUEST_DATA["prj_priority"];
    $prjInfo["prj_manager_pe_id"] = $REQUEST_DATA["prj_manager_pe_id"];

    $oid = null;
    $orate = null;
    $otitle = null;
    $ocolor = null;

    $tmp_opcat = phpaga_operations_getOpcats(null, true);

    if (isset($REQUEST_DATA["opcat_rates"]) && is_array($REQUEST_DATA["opcat_rates"])) {
        foreach ($REQUEST_DATA["opcat_rates"] as $oid => $orate) {

            if (isset($tmp_opcat[$oid])) {
                $otitle = $tmp_opcat[$oid]["opcat_title"];
                $ocolor = $tmp_opcat[$oid]["opcat_color"];
            }

            $prjInfo["opcatlist"][] = array("opcat_id" => $oid,
                                            "opcat_title" => $otitle,
                                            "opcat_color" => $ocolor,
                                            "opcat_hourlyrate" => $orate);
        }

    }

    $result = phpaga_projects_validdata($prjInfo);

    if (PhPagaError::isError($result))
        $status = $result->getFormattedMessage();
    else {

        $update_result = phpaga_projects_update($prjInfo);

        if (PhPagaError::isError($update_result))
            $status = $update_result->getFormattedMessage();
        else {
            header("Location: project.php?id=".$prjInfo["prj_id"]);
            exit;
        }
    }
}
else
{

    /* this is the first invocation */

    $prjInfo = phpaga_projects_getInfo($prj_id);

    if (PhPagaError::isError($prjInfo))
        $status = $prjInfo->getFormattedMessage();
}

phpaga_header(array("menuitem" => "core"));

/* if an error occured while searching for the record then don't let the user
 * edit enything, otherwise let him edit */


if (PhPagaError::isError($prjInfo))
    $prjInfo->printMessage();
else if (!count($prjInfo))
    phpaga_error(_("The project you are looking for could not be found."));
else {

    $select_prjstat = phpaga_projects_getStatusList();
    $select_prjcat = phpaga_projects_getCategoryList();
    phpaga_arrayAddOption($select_prjcat, PHPAGA_OPTION_SELECT);

    $select_parent_project = phpaga_projects_getProjectList();
    phpaga_arrayAddOption($select_parent_project, PHPAGA_OPTION_NONE);

    $select_priority = phpaga_projects_getPriorityList();
    $select_persons = array();
    $h_persons = PUser::findAsArray(array(), array('persons{pe_id}.pe_lastname' => 'asc', 
        'persons{pe_id}.pe_firstname' => 'asc'));
    foreach ($h_persons as $person)
        $select_persons[$person['pe_id']] = sprintf('%s (%s)', $person['pe_name'], $person['usr_login']);

    phpaga_arrayAddOption($select_persons, PHPAGA_OPTION_SELECT);

    $tpl = new PSmarty;

    $tpl->assign('prjinfo', $prjInfo);
    $tpl->assign('from_project', true);
    $tpl->assign('select_cpn', PCompany::getSimpleArray());
    $tpl->assign('select_cpn_issuebill', PCompany::getSimpleArray(true, ''));
    $tpl->assign('select_prjcat', $select_prjcat);
    $tpl->assign('select_prjstat', $select_prjstat);
    $tpl->assign('select_parent_project', $select_parent_project);
    $tpl->assign('select_prj_billabletype', $phpaga_projects_billabletype);
    $tpl->assign('select_priority', $select_priority);
    $tpl->assign('select_prj_manager', $select_persons);
    $tpl->assign('STATUSMSG', $status);
    $tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

    $tpl->display('edit_project.tpl.html');
}

phpaga_footer();

?>
