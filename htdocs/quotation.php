<?php
/**
 * phpaga
 *
 * Quotation info
 *
 * Show a quotation's information.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

$perms = array(PHPAGA_PERM_MANAGE_QUOTATIONS, PHPAGA_PERM_VIEW_QUOTATIONS);
PUser::protectPage($perms);

$quotInfo = array();
$acerror = null;

if (isset($REQUEST_DATA['id']) && is_numeric($REQUEST_DATA['id'])) {

    if (isset($REQUEST_DATA['action']) && is_numeric($REQUEST_DATA['action'])) {

        $action = $REQUEST_DATA['action'];

        switch($action) {

        case ACTION_DELETE:

            if (PUser::hasPerm(PHPAGA_PERM_MANAGE_QUOTATIONS) && is_numeric($REQUEST_DATA['id'])) {

                $acerror = phpaga_quotations_delete($REQUEST_DATA['id']);

                if (!PhPagaError::isError($acerror)) {
                    header("Location: quotations.php");
                    exit;
                }
            }
            break;

        default:
            break;
        }
    }

    phpaga_header(array('menuitem' => 'finance'));

    if (PhPagaError::isError($acerror))
        $acerror->printMessage();

    $quotInfo = phpaga_quotations_getinfo($REQUEST_DATA['id']);

    if (PhPagaError::isError($quotInfo))
        $quotInfo->printMessage();
    else {
        $tpl = new PSmarty;

	$config = PConfig::getArray();

        $tpl->assign('perm_quotations', PUser::hasPerm(PHPAGA_PERM_MANAGE_QUOTATIONS));
        $tpl->assign('quotInfo', $quotInfo);
	$tpl->assign('config', $config);
        $tpl->display('quotation.tpl.html');
    }

    phpaga_footer();
} else {
    phpaga_header(array('menuitem' => 'finance'));
    phpaga_error(PHPAGA_ERR_INVALID_QUOTATIONID);
    phpaga_footer();
}

?>
