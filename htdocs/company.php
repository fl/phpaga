<?php
/**
 * phpaga
 *
 * company
 *
 * View company information
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

if (!isset ($REQUEST_DATA['id'])) {
    phpaga_error(_('Missing company ID'));
    exit;
}

$sum_duration_prj = null;
$cpn_id = $REQUEST_DATA['id'];

phpaga_header(array('menuitem' => 'core'));

$cpnInfo = PCompany::getInfo($cpn_id);

if (PhPagaError::isError($cpnInfo))
    phpaga_error($cpnInfo);
else {

    $tpl = new PSmarty;

    $cpn_opcatstats = phpaga_operations_getOpcatStats(array('cpn_id' => $cpn_id), $sum_duration_prj, PHPAGA_OPCGRP_PROJECT);
    if (!PhPagaError::isError($cpn_opcatstats)) {
        $tpl->assign('opcats', $cpn_opcatstats);
        $tpl->assign('TOTAL_DURATION_PRJ', $sum_duration_prj);
    }

    $tpl->assign('cpnInfo', $cpnInfo);
    $tpl->assign('perm_companies', PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERCOMPANIES));
    $tpl->assign('perm_financial', PUser::hasPerm(PHPAGA_PERM_VIEW_FINANCE));

    $persons = PPerson::getList($count, null, $cpnInfo['cpn_id']);

    if (!PhPagaError::isError($persons))
        $tpl->assign('persons', $persons);
    else
        $tpl->assign('persons', null);

    $tpl->display('company.tpl.html');
}

phpaga_footer ();

?>