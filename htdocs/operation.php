<?php
/**
 * phpaga
 *
 * View/edit operation
 *
 * View or edit an operation
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$result = null;
$status = "";
$ref = null;
$refclear = null;
$count = 0;
$op_id = null;

if (isset($REQUEST_DATA["ref"]) && strlen($REQUEST_DATA["ref"]) > 0) {
    $ref = $REQUEST_DATA["ref"];
    $refclear = rawurldecode(str_replace('|', '&', $ref));
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $op_id = $_POST["op_id"];
    $opInfo["op_id"] = $_POST["op_id"];
    $opInfo["prj_id"] = $_POST["prj_id"];
    $opInfo["pe_id"] = $_POST["pe_id"];
    $opInfo["opcat_id"] = $_POST["opcat_id"];
    $opInfo["op_desc"] = $_POST["op_desc"];

    if (isset($REQUEST_DATA["op_billabletype"]) && ($REQUEST_DATA["op_billabletype"] == 0)) {

        $opInfo["op_billabletype"] = 0;
        $opInfo["billable_checked"] = true;
    } else {
        $opInfo["op_billabletype"] = 10;
        $opInfo["billable_checked"] = false;
    }

    $opInfo["op_duration"] = $_POST["op_duration"];

	/* if the duration is numeric, convert it from fractional hours into hours:minutes */

	if (is_numeric($opInfo["op_duration"])) {
	// Under 24 - assume the duration was given in hours
		if ($opInfo["op_duration"] < 24)
			$opInfo["op_duration"] = (int)$opInfo["op_duration"].':'.sprintf('%02d', 60 * ($opInfo["op_duration"] - (int)$opInfo["op_duration"]));
	// More than 60 minutes, different calculation
		elseif ($opInfo["op_duration"] >= 60)
			$opInfo["op_duration"] = (int)($opInfo["op_duration"] / 60).':'.sprintf('%02d', $opInfo["op_duration"] % 60);
	// duration given in minutes
		else
			$opInfo["op_duration"] = '0:'.(int)$opInfo["op_duration"];
	}

	/* if there is a valid start date and time then convert it to a timestamp */

    $opInfo["op_startdate"] = $_POST["op_startdate"];
    $opInfo["op_starthours"] = $_POST["op_starthours"];

    $start_tmp = "";

    if ($opInfo["op_startdate"] != "") {
        $start_tmp = $opInfo["op_startdate"];

        if ($opInfo["op_starthours"] != "")
            $start_tmp .= " ".$opInfo["op_starthours"];

        $opInfo["op_start"] = $start_tmp;
    } else
        $opInfo["op_start"] = null;

    /* if there is a valid end date and time then convert it to a timestamp */

    $opInfo["op_enddate"]  = $_POST["op_enddate"];
    $opInfo["op_endhours"] = $_POST["op_endhours"];

    $end_tmp = "";

    if ($opInfo["op_enddate"] != "") {
        $end_tmp = $opInfo["op_enddate"];

        if ($opInfo["op_endhours"] != "")
            $end_tmp .= " ".$opInfo["op_endhours"];

        $opInfo["op_end"] = $end_tmp;
    } else
        $opInfo["op_end"] = null;

    $result = phpaga_operations_validdata($opInfo);

    if (PhPagaError::isError($result))
        $status = $result->getFormattedMessage();
    else {

        $edit_result = phpaga_operations_update($opInfo);

        if (PhPagaError::isError($edit_result))
            $status = $edit_result->getFormattedMessage();
        else {
            if (strlen($refclear)) {
                header("Location: ".$refclear);
                exit;
            } else {
                $status = phpaga_returnmessage(_("Information"), _("The task has been updated successfully."));
            }
        }
    }

} else {

    if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]))
        $op_id = $_GET["id"];
    else
        $op_id = null;

    $opInfo = phpaga_operations_getInfo($op_id);
    if (PhPagaError::isError($opInfo))
        $status = $opInfo->getFormattedMessage();
}

$select_opcat = phpaga_operations_getCategoryList();
phpaga_arrayAddOption($select_opcat, PHPAGA_OPTION_ALL);

phpaga_header();
if (is_null($op_id) || ($opInfo === false)) {
    phpaga_error(_('Invalid task.'));
    phpaga_footer();
    exit;
} else if (PhPagaError::isError($opInfo)) {
    $opInfo->printError();
    phpaga_footer();
    exit;
}

$prjInfo = phpaga_projects_getInfo($opInfo["prj_id"]);
if (PhPagaError::isError($prjInfo))
    $prjInfo->printMessage();

$duration = phpaga_projects_getDuration($prjInfo["prj_id"]);

if (isset($opInfo["op_billabletype"]) && ($opInfo["op_billabletype"] != 10))
    $opInfo["billable_checked"] = true;
else
    $opInfo["billable_checked"] = false;

$is_owner_or_manager = ((($prjInfo["pe_id"] == $_SESSION["auth_user"]["pe_id"])
    && PUser::hasPerm(PHPAGA_PERM_MANAGE_PROJECTS))
    || PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPROJECTS));

/* Only the project owner and general project managers or the
 * task's owner can change this task's data
 */

if (!$is_owner_or_manager && ($opInfo["pe_id"] != $_SESSION["auth_user"]["pe_id"]))
    phpaga_error(_("You are not the owner of this task, and you do not have the required permissions to change other people's tasks."));
else {
    /* Only the project owner and general project managers can
     * set an operation's user
     */

    $select_user = array();
    if (!$is_owner_or_manager) {

        $peInfo = PPerson::getInfo($_SESSION["auth_user"]["pe_id"]);

        $select_user[$peInfo["pe_id"]] = sprintf("%s (%s)",
            $peInfo["pe_person"],
            $_SESSION["auth_user"]["usr_login"]);
    } else {
        $users = PUser::findAsArray(array('persons{pe_id}=>project_members.prj_id=' => $opInfo['prj_id']), 
            array('persons{pe_id}.pe_lastname' => 'asc', 'persons{pe_id}.pe_firstname' => 'asc'));
        foreach ($users as $u)
            $select_user[$u["pe_id"]] = $u["pe_name"]." (".$u["usr_login"].")";
    }

    $tpl = new PSmarty;
    $tpl->assign("PRJ_OPERATION", _("Edit task"));
    $tpl->assign("FORM_ACTION", basename($_SERVER["PHP_SELF"]));
    $tpl->assign("prjInfo", $prjInfo);
    $tpl->assign("opInfo", $opInfo);
    $tpl->assign("STATUSMSG", $status);
    $tpl->assign("HOURS_SO_FAR", $duration);
    $tpl->assign("select_person", $select_user);
    $tpl->assign("select_opcat", $select_opcat);
    $tpl->assign("ref", $ref);
    $tpl->assign("refclear", $refclear);
    $tpl->display("edit_operation.tpl.html");
}

phpaga_footer();

?>
