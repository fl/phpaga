<?php
/**
 * phpaga
 *
 * Users
 *
 * This is the user management interface (list and search).
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');
PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);

$conditions = array();
$userInfo = array('pe_lastname' => phpaga_fetch_REQUEST('pe_lastname'),
                  'usr_login' => phpaga_fetch_REQUEST('usr_login'));

phpaga_header(array('menuitem' => 'administration'));

if (strlen($userInfo['usr_login']))
    $conditions['usr_login~'] = $userInfo['usr_login'];

if (strlen($userInfo['pe_lastname']))
    $conditions['persons{pe_id}.pe_lastname~'] = $userInfo['pe_lastname'];

try {
    $rows = PUser::findAsArray($conditions, array('usr_login' => 'asc'));
} catch (Exception $e) {
    phpaga_error($e->getMessage());
    $rows = array();
}

$tpl = new PSmarty;
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));
$tpl->assign('s_userInfo', $userInfo);
$tpl->assign('rows', $rows);
$tpl->display('users.tpl.html');

phpaga_footer();

?>
