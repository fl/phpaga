<?php
/**
 * phpaga
 *
 * add material
 *
 * Add materials to a project
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2005, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

$prj_id = phpaga_fetch_REQUEST('prj_id');

if (!is_numeric($prj_id)) {
    phpaga_header(array('menuitem' => 'core'));
    phpaga_error(_('Invalid project ID'));
    phpaga_footer();
    exit;
}

$ismember = phpaga_project_is_member($prj_id, $_SESSION['auth_user']['pe_id']);
$can_manage = phpaga_project_canmanage($prj_id);
if (!$ismember && !$can_manage) {
    phpaga_header(array('menuitem' => 'core'));
    phpaga_message (_('No access'),
                    _("You can't file materials for this project because you are not a project member."));
    phpaga_footer();
    exit;
}

$status = '';
$prjInfo = array();
$users = array();
$select_user = array();
$ismember = false;
$products = array();

$matInfo = array(
    'prod_id' => null,
    'mat_name' => null,
    'mat_qty' => null,
    'pe_id' => $_SESSION['auth_user']['pe_id'],
    'prj_id' => $prj_id,
    'mat_id' => null,
    'mat_desc' => null,
    'mat_price' => null,
    'curr_id' => PHPAGA_DEFAULTCURRENCY,
    'mat_prodcode' => null);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {




    #### FIXME
    die("#FIXME");







    $matInfo['prj_id'] = $prj_id;
    $matInfo['prod_id'] = $REQUEST_DATA['prod_id'];
    $matInfo['mat_name'] = $REQUEST_DATA['mat_name'];

    /* user can file materials for other users if she has the right to
     * manage other people's projects or when this is her project */

    if (!$can_manage)
        $matInfo['pe_id'] = $_SESSION['auth_user']['pe_id'];
    else
        $matInfo['pe_id'] = $REQUEST_DATA['pe_id'];

    $matInfo['mat_qty'] = $REQUEST_DATA['mat_qty'];
    $matInfo['mat_desc'] = $REQUEST_DATA['mat_desc'];
    $matInfo['mat_price'] = $REQUEST_DATA['mat_price'];
    $matInfo['curr_id'] = $REQUEST_DATA['curr_id'];
    $matInfo['mat_prodcode'] = $REQUEST_DATA['mat_prodcode'];

    $result = phpaga_materials_validdata($matInfo);

    if (PhPagaError::isError($result))
        $status = $result->getFormattedMessage();
    else {

        $add_result = phpaga_materials_add($matInfo);

        if (PhPagaError::isError($add_result))
            $status = $add_result->getFormattedMessage();
        else {

            $status = phpaga_returnmessage(_('Information'), _('The material has been inserted successfully.'));

            $matInfo['prod_id'] = '';
            $matInfo['mat_name'] = '';
            $matInfo['mat_qty'] = '';
            $matInfo['mat_desc'] = '';
            $matInfo['mat_price'] = '';
            $matInfo['curr_id'] = PHPAGA_DEFAULTCURRENCY;
            $matInfo['mat_prodcode'] = '';
        }
    }
} else {


}

$prjInfo = phpaga_projects_getInfo($prj_id);
if (PhPagaError::isError($prjInfo)) {
    phpaga_header(array('menuitem' => 'core'));
    $prjInfo->printMessage();
    phpaga_footer();
    exit;
}

phpaga_header(array('menuitem' => 'core'));

$duration = phpaga_projects_getDuration($prjInfo['prj_id']);

/* user can file materials for other users if she has the right to
 * manage other people's projects or when this is her project */

if (!$can_manage) {
    $peInfo = PPerson::getInfo($_SESSION['auth_user']['pe_id']);
    $select_user[$peInfo['pe_id']] = sprintf('%s (%s)', $peInfo['pe_person'], $_SESSION['auth_user']['usr_login']);
} else {
    $users = PUser::findAsArray(array('persons{pe_id}=>project_members.prj_id=' => $prj_id), 
        array('persons{pe_id}.pe_lastname' => 'asc', 'persons{pe_id}.pe_firstname' => 'asc'));

    foreach ($users as $u)
        $select_user[$u['pe_id']] = $u['pe_name'].' ('.$u['usr_login'].')';
}

$products = phpaga_products_search($prodcount, $prodIds, array());

$tpl = new PSmarty;

$tpl->assign('prjInfo', $prjInfo);
$tpl->assign('matinfo', $matInfo);
$tpl->assign('HOURS_SO_FAR', $duration);
$tpl->assign('STATUSMSG', $status);
$tpl->assign('is_owner_or_manager', $can_manage);
$tpl->assign('select_person', $select_user);
$tpl->assign('select_curr', PCurrency::getList());
$tpl->assign("products", $products);
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

$tpl->display('edit_material.tpl.html');

phpaga_footer();

?>
