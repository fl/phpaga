<?php
/**
 * phpaga
 *
 * Export invoices
 *
 * Export invoices matching query options to csv file
 *
 * @author Mark Parssey <markpa@users.sourceforge.net>
 * @version $Id: bills_csv.php,v 1.0 2005/03/08 21:47:24
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$perms = array(PHPAGA_PERM_VIEW_INVOICES, PHPAGA_PERM_MANAGE_INVOICES);
PUser::protectPage($perms);

/* get search criteria */

/* company id */
if (isset ($REQUEST_DATA["cpn_id"]) && is_numeric ($REQUEST_DATA["cpn_id"]))
    $billInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];
else
    $billInfo["cpn_id"] = "";

/* billing method plugin id */
if (isset ($REQUEST_DATA["bmt_id"]) && is_numeric ($REQUEST_DATA["bmt_id"]))
    $billInfo["bmt_id"] = $REQUEST_DATA["bmt_id"];
else
    $billInfo["bmt_id"] = "";

/* currency id */
if (isset ($REQUEST_DATA["curr_id"]) && is_numeric ($REQUEST_DATA["curr_id"]))
    $billInfo["curr_id"] = $REQUEST_DATA["curr_id"];
else
    $billInfo["curr_id"] = "";

/* year */
if (isset ($REQUEST_DATA["bill_Year"]) && is_numeric ($REQUEST_DATA["bill_Year"]))
    $billInfo["bill_year"] = $REQUEST_DATA["bill_Year"];
else
    $billInfo["bill_year"] = "";

/* bill number */
if (isset ($REQUEST_DATA["bill_number"]) && strlen ($REQUEST_DATA["bill_number"]))
    $billInfo["bill_number"] = $REQUEST_DATA["bill_number"];
else
    $billInfo["bill_number"] = "";

/* has the invoice/bill been paid? */
if (isset($REQUEST_DATA["bill_paid"]) &&
    is_numeric($REQUEST_DATA["bill_paid"]) &&
    (($REQUEST_DATA["bill_paid"] ==  PHPAGA_BILL_PAID) ||
     ($REQUEST_DATA["bill_paid"] == PHPAGA_BILL_NOTPAID)))

    $billInfo["bill_paid"] = $REQUEST_DATA["bill_paid"];
else
    $billInfo["bill_paid"] = "";

/* has the invoice/bill been sent to the recipient? */
if (isset($REQUEST_DATA["bill_sent"]) &&
    is_numeric($REQUEST_DATA["bill_sent"]) &&
    (($REQUEST_DATA["bill_sent"] == PHPAGA_BILL_SENT) ||
     ($REQUEST_DATA["bill_sent"] == PHPAGA_BILL_NOTSENT)))

    $billInfo["bill_sent"] = $REQUEST_DATA["bill_sent"];
else
    $billInfo["bill_sent"] = "";

/* billing from date */
if (isset($REQUEST_DATA["bill_datefrom"]) && ($REQUEST_DATA["bill_datefrom"] != ""))
    $billInfo["bill_datefrom"] = rawurldecode($REQUEST_DATA["bill_datefrom"]);
else
    $billInfo["bill_datefrom"] = "";

/* billing to date */
if (isset($REQUEST_DATA["bill_dateto"]) && ($REQUEST_DATA["bill_dateto"] != ""))
    $billInfo["bill_dateto"] = rawurldecode($REQUEST_DATA["bill_dateto"]);
else
    $billInfo["bill_dateto"] = "";

$rows = phpaga_bills_search($count, $billInfo);

if (PhPagaError::isError($rows)) {
    phpaga_header(array("menuitem" => "finance"));
    $rows->printMessage();
    phpaga_footer();
} else {

    if ($count > 0)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=data.csv');

        print(sprintf("%s,%s,%s,%s\r\n",
                      _("Invoice no."),
                      _("Date"),
                      _("Company"),
                      _("Amount incl. VAT")));

        foreach($rows as $row) {
            print(sprintf("%s,%s,\"%s\",%s\r\n",
                          $row["bill_number"],
                          $row["bill_date"],
                          $row["cpn_name"],
                          $row["bill_endsum"]));
        }
    }
}

?>