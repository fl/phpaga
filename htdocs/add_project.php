<?php
/**
 * phpaga
 *
 * add project
 *
 * Add projects
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

PUser::protectPage(PHPAGA_PERM_MANAGE_PROJECTS);
$auth_user = PSession::getAuthUser();

$status = null;
$new_prj_id = null;
$new_cpn_id = null;

$nohide = false;

$prjInfo = array();
$prjInfo['opcatlist'] = array();

$cpnInfo = array(
    'cpn_name' => null,
    'cpn_address' => null,
    'cpn_address2' => null,
    'cpn_city' => null,
    'cpn_region' => null,
    'cpn_zip' => null,
    'ctr_id' => PHPAGA_DEFAULTCOUNTRY,
    'ccat_id' => null,
    'cpn_taxnr' => null,
    'cpn_phone' => null,
    'cpn_fax' => null,
    'cpn_url' => null,
    'cpn_email' => null,
    'cpn_defaultpayterm' => null,
    'pe_id' => null);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!isset($_POST['new_cpn']) || ($_POST['new_cpn'] != '1')) {
        $prjInfo['new_cpn'] = 0;
    } else {

        $prjInfo['new_cpn'] = 1;

        try {
            $company = new PCompany();
            $company->setCpnName(phpaga_fetch_POST('cpn_name'));
            $company->setCpnAddress(phpaga_fetch_POST('cpn_address'));
            $company->setCpnAddress2(phpaga_fetch_POST('cpn_address2'));
            $company->setCpnCity(phpaga_fetch_POST('cpn_city'));
            $company->setCpnRegion(phpaga_fetch_POST('cpn_region'));
            $company->setCpnZip(phpaga_fetch_POST('cpn_zip'));
            $company->setCtrId(phpaga_fetch_POST('ctr_id'));
            $company->setCcatId(phpaga_fetch_POST('ccat_id'));
            $company->setCpnTaxnr(phpaga_fetch_POST('cpn_taxnr'));
            $company->setCpnPhone(phpaga_fetch_POST('cpn_phone'));
            $company->setCpnFax(phpaga_fetch_POST('cpn_fax'));
            $company->setCpnUrl(phpaga_fetch_POST('cpn_url'));
            $company->setCpnEmail(phpaga_fetch_POST('cpn_email'));
            $company->setCpnDefaultpayterm(phpaga_fetch_POST('cpn_defaultpayterm'));
            $company->setPeId($auth_user['pe_id']);

            $company->store();

            $new_cpn_id = $company->getCpnId();
        } catch (Exception $e) {
            $status = phpaga_returnerror($e->getmessage());

            $cpnInfo['cpn_name'] = phpaga_fetch_POST('cpn_name');
            $cpnInfo['cpn_address'] = phpaga_fetch_POST('cpn_address');
            $cpnInfo['cpn_address2'] = phpaga_fetch_POST('cpn_address2');
            $cpnInfo['cpn_city'] = phpaga_fetch_POST('cpn_city');
            $cpnInfo['cpn_region'] = phpaga_fetch_POST('cpn_region');
            $cpnInfo['cpn_zip'] = phpaga_fetch_POST('cpn_zip');
            $cpnInfo['ctr_id'] = phpaga_fetch_POST('ctr_id');
            $cpnInfo['ccat_id'] = phpaga_fetch_POST('ccat_id');
            $cpnInfo['cpn_taxnr'] = phpaga_fetch_POST('cpn_taxnr');
            $cpnInfo['cpn_phone'] = phpaga_fetch_POST('cpn_phone');
            $cpnInfo['cpn_fax'] = phpaga_fetch_POST('cpn_fax');
            $cpnInfo['cpn_url'] = phpaga_fetch_POST('cpn_url');
            $cpnInfo['cpn_email'] = phpaga_fetch_POST('cpn_email');
            $cpnInfo['cpn_defaultpayterm'] = phpaga_fetch_POST('cpn_defaultpayterm');
            $cpnInfo['pe_id'] = $auth_user['pe_id'];
        }
    }

    $prjInfo['prjcat_id'] = $REQUEST_DATA['prjcat_id'];
    $prjInfo['prj_desc'] = $REQUEST_DATA['prj_desc'];
    $prjInfo['prj_title'] = $REQUEST_DATA['prj_title'];
    $prjInfo['prj_status'] = $REQUEST_DATA['prj_status'];
    $prjInfo['prj_priority'] = $REQUEST_DATA['prj_priority'];
    $prjInfo['prj_startdate'] = $REQUEST_DATA['prj_startdate'];
    $prjInfo['prj_estimatedend'] = $REQUEST_DATA['prj_estimatedend'];
    $prjInfo['prj_deadline'] = $REQUEST_DATA['prj_deadline'];

    if (strlen($new_cpn_id) && is_numeric($new_cpn_id)) {
        $prjInfo['cpn_id'] = $new_cpn_id;
        $prjInfo['new_cpn'] = 0;

        $cpnInfo = array('cpn_name' => null,
                         'cpn_address' => null,
                         'cpn_address2' => null,
                         'cpn_city' => null,
                         'cpn_region' => null,
                         'cpn_zip' => null,
                         'ctr_id' => PHPAGA_DEFAULTCOUNTRY,
                         'ccat_id' => null,
                         'cpn_taxnr' => null,
                         'cpn_phone' => null,
                         'cpn_fax' => null,
                         'cpn_url' => null,
                         'cpn_email' => null,
                         'cpn_defaultpayterm' => null);

    } elseif ($REQUEST_DATA['cpn_id'] != -10)
          $prjInfo['cpn_id'] = $REQUEST_DATA['cpn_id'];
    else
        $prjInfo['cpn_id'] = null;

    $prjInfo['quot_id'] = $REQUEST_DATA['quot_id'];
    $prjInfo['cpn_id_issuebill'] = $REQUEST_DATA['cpn_id_issuebill'];
    $prjInfo['pe_id'] = $_SESSION['auth_user']['pe_id'];
    $prjInfo['prj_id'] = 0;
    $prjInfo['prj_prj_id'] = $REQUEST_DATA['prj_prj_id'];
    $prjInfo['prj_planned_manhours'] = $REQUEST_DATA['prj_planned_manhours'];
    $prjInfo['prj_planned_cost_manhours'] = $REQUEST_DATA['prj_planned_cost_manhours'];
    $prjInfo['prj_planned_cost_material'] = $REQUEST_DATA['prj_planned_cost_material'];
    $prjInfo['prj_billabletype'] = $REQUEST_DATA['prj_billabletype'];
    $prjInfo['prj_manager_pe_id'] = $REQUEST_DATA['prj_manager_pe_id'];

    $oid = null;
    $orate = null;
    $otitle = null;
    $ocolor = null;

    $tmp_opcat = phpaga_operations_getOpcats(null, true);

    if (isset($REQUEST_DATA['opcat_rates']) && is_array($REQUEST_DATA['opcat_rates'])) {
        foreach ($REQUEST_DATA['opcat_rates'] as $oid => $orate) {

            if (isset($tmp_opcat[$oid])) {
                $otitle = $tmp_opcat[$oid]['opcat_title'];
                $ocolor = $tmp_opcat[$oid]['opcat_color'];
            }

            $prjInfo['opcatlist'][] = array('opcat_id' => $oid,
                                            'opcat_title' => $otitle,
                                            'opcat_color' => $ocolor,
                                            'opcat_hourlyrate' => $orate);
        }
    }

    if (!strlen($status)) {
        $result = phpaga_projects_validdata($prjInfo);

        if (PhPagaError::isError($result))
            $status = $result->getFormattedMessage();
        else {

            $new_prj_id = phpaga_projects_add($prjInfo);

            if (PhPagaError::isError($new_prj_id))
                $status = $new_prj_id->getFormattedMessage();
            else {
                header('Location: project.php?id='.$new_prj_id);
                exit;
            }
        }
    }

} else {

    $prjInfo['prj_title'] = null;

    $t_prj_id = phpaga_projects_getLastProject();

    if (!is_null($t_prj_id) && is_numeric($t_prj_id)) {
        $t_prjInfo = phpaga_projects_getInfo($t_prj_id);

        if (isset($t_prjInfo['prj_title']) && (is_numeric($t_prjInfo['prj_title'])))
            $prjInfo['prj_title'] = $t_prjInfo['prj_title'] + 1;
    }

    $prjInfo['prj_id'] = 0;
    $prjInfo['prjcat_id'] = null;
    $prjInfo['prj_desc'] = null;
    $prjInfo['prj_status'] = PHPAGA_PROJECTSTATUS_OPEN;
    $prjInfo['prj_priority'] = null;
    $prjInfo['prj_startdate'] = date('Y-m-d');
    $prjInfo['prj_estimatedend'] = null;
    $prjInfo['prj_deadline'] = null;
    $prjInfo['cpn_id'] = "";
    $prjInfo['cpn_id_issuebill'] = "";
    $prjInfo['quot_id'] = null;
    $prjInfo['prj_prj_id'] = null;
    $prjInfo['prj_manager_pe_id'] = null;
    $prjInfo['prj_planned_manhours'] = null;
    $prjInfo['prj_planned_cost_manhours'] = null;
    $prjInfo['prj_planned_cost_material'] = null;
    $prjInfo['prj_billabletype'] = PHPAGA_BILLABLETYPE_YES;
    $prjInfo['new_cpn'] = false;

    $prjInfo['opcatlist'] = phpaga_operations_getOpcats();

    if (isset($REQUEST_DATA['id']) && is_numeric($REQUEST_DATA['id'])
        && isset($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == ACTION_FROMQUOTATION))
    {
        /* create a bill from a quotation */

        $quotInfo['quot_id'] = $REQUEST_DATA['id'];
        $rows = phpaga_quotations_search($count, $quotInfo);

        if (!PhPagaError::isError($rows) && (count($rows) == 1)) {
            $quotInfo = $rows[0];

            $prjInfo['cpn_id'] = (int)$quotInfo['cpn_id'];
            $prjInfo['quot_id'] = (int)$quotInfo['quot_id'];
            $prjInfo['prj_planned_cost_manhours'] = $quotInfo['quot_startsum'];
        }
    }
}

$nohide = ($prjInfo['new_cpn'] == 1);

phpaga_header(array('menuitem' => 'core'));

$select_prjstat = phpaga_projects_getStatusList();
$select_priority = phpaga_projects_getPriorityList();

$select_cpn = PCompany::getSimpleArray(true, ''); 
phpaga_arrayAddOption($select_cpn, PHPAGA_OPTION_NEW, -10);

$select_prjcat = phpaga_projects_getCategoryList();
phpaga_arrayAddOption($select_prjcat, PHPAGA_OPTION_SELECT);

$select_parent_project = phpaga_projects_getProjectList();
phpaga_arrayAddOption($select_parent_project, PHPAGA_OPTION_NONE);

$select_persons = array();
$h_persons = PUser::findAsArray(array(), array('persons{pe_id}.pe_lastname' => 'asc', 
    'persons{pe_id}.pe_firstname' => 'asc'));
foreach ($h_persons as $person) {
    $select_persons[$person['pe_id']] = sprintf('%s (%s)', $person['pe_name'], $person['usr_login']);
}

phpaga_arrayAddOption($select_persons, PHPAGA_OPTION_SELECT);

$tpl = new PSmarty;

$tpl->assign('prjinfo', $prjInfo);
$tpl->assign('cpnInfo', $cpnInfo);
$tpl->assign('from_project', true);
$tpl->assign('select_cpn', $select_cpn);
$tpl->assign('select_cpn_issuebill', $select_cpn);
$tpl->assign('select_prjcat', $select_prjcat);
$tpl->assign('select_prjstat', $select_prjstat);
$tpl->assign('select_priority', $select_priority);
$tpl->assign('select_prj_manager', $select_persons);
$tpl->assign('select_parent_project', $select_parent_project);
$tpl->assign('select_prj_billabletype', $phpaga_projects_billabletype);
$tpl->assign('select_ctr', PCountry::getSimpleArray());
$tpl->assign('select_ccat', PCompanyCategory::getSimpleArray());
$tpl->assign('STATUSMSG', $status);
$tpl->assign('FORM_ACTION', basename ($_SERVER['PHP_SELF']));

$tpl->display('edit_project.tpl.html');

phpaga_footer();

?>