<?php
/**
 * phpaga
 *
 * User
 *
 * This is the user management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);
$error = array();

$action = phpaga_fetch_REQUEST('action');
if ($action==ACTION_ADD) {

    $user = new PUser();
    $user->setUsrLogin(phpaga_fetch_POST('usr_login'));
    //$error[] = PHPAGA_ERR_USR_NOPEASSOC;
    $user->setPeId(phpaga_fetch_POST('pe_id'));
    $user->setUsrPeId($_SESSION['auth_user']['pe_id']);
    $user->setUsrSkin(defined('PHPAGA_DEFAULT_SKIN') ? PHPAGA_DEFAULT_SKIN : 'phpaga');
    
    $perm = phpaga_fetch_POST('perm');
    if (!count($perm))
        $perm = null;
    else
        $perm = implode('|', $perm);
    $user->setUsrPerm($perm);

    $pwd_new = phpaga_fetch_POST('usr_pwd_new');
    $pwd_confirm = phpaga_fetch_POST('usr_pwd_confirm');

    if (!strlen($pwd_new))
        $error[] = PHPAGA_ERR_AUTH_PASSWDLENGTH;
    else if (!strlen($pwd_confirm) || ($pwd_new!=$pwd_confirm))
        $error[] = _("The password and its confirmation do not match.");
    else
        $user->setPassword($pwd_new);

    if (count($error) == 0) {

        try {
            $user->store();

            $usr_id = $user->getUsrId();
            $userconfig = phpaga_fetch_POST('userconfig');

            if (is_array($userconfig) && count($userconfig)) {
                foreach($userconfig as $key => $value) {
                    $result = PConfig::setUserItem($key, $value, $usr_id);
                    if (PhPagaError::isError($result))
                        $error[] = $result->printMesssage();
                }
            }

        } catch (Exception $e) {
            //if ($result->getCode() == PHPAGA_ERR_DB_DUPLICATE)
            //    $error[] = sprintf(_('Unable to insert user - the login %s is already in use.'), $user['usr_login']);
            $error[] = $e->getMessage();
        }

        if (count($error) == 0) {
            header('Location: users.php');
            exit;
        }
    }
}

$user = array('usr_login' => phpaga_fetch_POST('usr_login'),
    'usr_skin' => phpaga_fetch_POST('usr_skin', PHPAGA_DEFAULT_SKIN),
    'pe_id' => phpaga_fetch_POST('pe_id'),
    'pe_name' => phpaga_fetch_POST('pe_name'),
    'usr_pwd_new' => phpaga_fetch_POST('usr_pwd_new'),
    'usr_pwd_confirm' => phpaga_fetch_POST('usr_pwd_confirm'));

$persons_count = 0;
$persons = PPerson::getList($persons_count, null, null);
foreach ($persons as $person)
    $h_pe[$person['pe_id']] = $person['pe_person'];
$persons = $h_pe;

phpaga_header(array('menuitem' => 'administration'));

if (count($error) > 0)
    phpaga_error($error);

$users_tpl = new PSmarty;

$users_tpl->assign('SELF', basename($_SERVER['PHP_SELF']));
$users_tpl->assign('persons', $persons);
$users_tpl->assign('user', $user);
$users_tpl->assign('skins', phpaga_getSkinNames());
$users_tpl->assign('languages', PhPagaLanguage::getList());

$users_tpl->display('add_user.tpl.html');

phpaga_footer();

?>
