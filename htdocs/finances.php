<?php
/**
 * phpaga
 *
 * Finances info
 *
 * Show some financial information.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
include_once('./config.php');

PUser::protectPage(PHPAGA_PERM_VIEW_FINANCE);

$cpndetails = array();

$min_date = null;

$currencies = phpaga_bills_getCurrenciesUsed();
if (PhPagaError::isError($currencies))
    $currencies->printMessage();

$min_date = phpaga_bills_getInvoiceMinDate();
if (PhPagaError::isError($min_date)) {
    $min_date->printMessage();
    $minYear = phpaga_finances_getfiscalyear(date('Y-m-d'));
} else
    $minYear = phpaga_finances_getfiscalyear($min_date);

$maxYear = phpaga_finances_getfiscalyear(date('Y-m-d'));

if (isset ($REQUEST_DATA['fin_Year']) && is_numeric($REQUEST_DATA['fin_Year']))
    $year = $REQUEST_DATA['fin_Year'];
else
    $year = phpaga_finances_getfiscalyear(date('Y-m-d'));

$fy_dates = phpaga_finances_getfiscalyeardates($year);
$currency = phpaga_fetch_REQUEST('curr_id', PHPAGA_DEFAULTCURRENCY);
$currInfo = new PCurrency($currency);

if (!$currencies)
    $currencies = array($currency =>$currInfo->getCurrName());

$cpndetails = phpaga_finances_getSumsPerCustomerYearly($currency, $year);

if (PhPagaError::isError($cpndetails))
    $cpndetails->printMessage();

phpaga_header(array('menuitem' => 'finance'));

$finances_tpl = new PSmarty;

$config = PConfig::getArray();

$finances_tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));
$finances_tpl->assign('time', strtotime("$year-01-01"));
$finances_tpl->assign('maxYear', $maxYear);
$finances_tpl->assign('minYear', $minYear);
$finances_tpl->assign('curr_id', $currency);
$finances_tpl->assign('currencies', $currencies);
$finances_tpl->assign('YEAR', $year);
$finances_tpl->assign('fy_dates', $fy_dates);
$finances_tpl->assign('curr_name', $currInfo->getCurrName());
$finances_tpl->assign('CURRENCY', $currency);
$finances_tpl->assign('cpndetails', $cpndetails);
$finances_tpl->assign('config', $config);

$finances_tpl->display('finances.tpl.html');

phpaga_footer();

?>
