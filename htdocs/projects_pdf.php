<?php
/**
 * phpaga
 *
 * Print projects
 *
 * Print projects matching query options to pdf file
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS
 * IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

define('PHPAGA_COMPRESSOUT_DISABLED', true);
include_once('./config.php');
include_once(PHPAGA_LIBPATH.'pdf.php');

$fields = array('cpn_id',
    'cpn_id_issuebill',
    'prj_id',
    'prj_id_in',
    'pe_id',
    'prj_status',
    'prjcat_id',
    'prj_desc',
    'prj_startdatefrom',
    'prj_startdateto'
);

$download = phpaga_fetch_REQUEST('download', false);
$prjInfo = array();
foreach ($fields as $f)
    $prjInfo[$f] = phpaga_fetch_REQUEST($f);

$search_params_pdf = array();

if (is_numeric($prjInfo['cpn_id'])) {
    $cpnInfo = PCompany::getInfo($prjInfo['cpn_id']);
    if (!PhPagaError::isError($$cpnInfo))
        $search_params_pdf[] = array('param' => _('Company'),
                                     'value' => $cpnInfo['cpn_name']);
}

if (is_numeric($prjInfo['cpn_id_issuebill'])) {
    $cpnInfo = PCompany::getInfo($prjInfo['cpn_id_issuebill']);
    if (!PhPagaError::isError($$cpnInfo))
        $search_params_pdf[] = array('param' => _('Company issuing the bill'),
                                     'value' => $cpnInfo['cpn_name']);
}

if (is_numeric($prjInfo['prj_id'])) {
    $pInfo = phpaga_projects_getInfo($prjInfo['prj_id']);
    if (!PhPagaError::isError($pInfo))
        $search_params_pdf[] = array('param' => _('Project'),
                                     'value' => $pInfo['prj_title']);
}

if (strlen($prjInfo['prj_id_in'])) 
    $search_params_pdf[] = array('param' => _('Projects'), 'value' => _('(selection)'));

if (is_numeric($prjInfo['pe_id'])) {
    $peInfo = PPerson::getInfo ($prjInfo['pe_id']);
    if (!PhPagaError::isError($peInfo))
        $search_params_pdf[] = array('param' => _('Person'),
                                     'value' => $peInfo['pe_person']);
}

if (is_numeric($prjInfo['prj_status'])) {
    $prjstatList = phpaga_projects_getStatusList();
    if ((!PhPagaError::isError($prjstatList)) && isset($prjstatList[$prjInfo['prj_status']]))
        $search_params_pdf[] = array('param' => _('Project status'),
                                     'value' => $prjstatList[$prjInfo['prj_status']]);
}

if (is_numeric($prjInfo['prjcat_id'])) {
    $prjcatList = phpaga_projects_getCategoryList();
    if ((!PhPagaError::isError($prjcatList)) && isset($prjcatList[$prjInfo['prjcat_id']]))
        $search_params_pdf[] = array('param' => _('Project category'),
                                     'value' => $prjcatList[$prjInfo['prjcat_id']]);
}

if (strlen($prjInfo['prj_desc'])) {
    $search_params_pdf[] = array('param' => _('Task description'),
                                 'value' => $prjInfo['prj_desc']);
}

if (strlen($prjInfo['prj_startdatefrom'])) {
    $search_params_pdf[] = array('param' => _('Date from'),
                                 'value' => $prjInfo['prj_startdatefrom']);
}

if (strlen($prjInfo['prj_startdateto'])) {
    $search_params_pdf[] = array('param' => _('Date to'),
                                 'value' => $prjInfo['prj_startdateto']);
}

$rows = phpaga_projects_search($count, $duration, $prjIds, $prjInfo);

if (PhPagaError::isError($rows))
    $rows->printMessage();
else if ($count > 0) {
    $pdf = new PhPagaPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, strtoupper(PHPAGA_PDF_DOCFORMAT), true, 'UTF-8', false);
    $pdf->SetTitle('Tasks');

    $pdf->AddPage();
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    $pdf->AddTable(array(
        array('width' => 40, 'name' => 'prj_title', 'title' => _('Project')),
        array('width' => 30, 'name' => 'cpn_name', 'title' => _('Company')),
        array('width' => 60, 'name' => 'prj_desc', 'title' => _('Description')),
        array('width' => 20, 'name' => 'prjstat_title', 'title' => _('Status')),
        array('width' => 20, 'name' => 'prj_startdate_print', 'title' => _('Start')),
    ),
    $rows, 
    array('title' => _('Projects')));

    $dest = $download ? 'D' : 'I';
    $pdf->Output('projects.pdf', $dest);
}

?>
