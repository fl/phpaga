<?php
/**
 * phpaga
 *
 * Currencies
 *
 * This is the currencies management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once ("./config.php");
PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);
$gridconf = new PSimpleGridConf();
$gridconf->apiUrl = 'api.php/currency';
$gridconf->idField = 'curr_id';
$gridconf->title = _('Currencies');
$gridconf->sortName = 'curr_name';
$gridconf->colNames = array('Id', _('Name'), _('Decimals'));

$_id = new PSimpleGridColumn('curr_id', true);
$_name = new PSimpleGridColumn('curr_name');
$_name->editable = true;
$_name->width = 80;
$_name->enableSearch(array('sopt' => array('cn','bw','eq','ew','ne')));
$_name->setRequired();
$_name->setEditOptions(array('maxlength' => 15));
$_decimals = new PSimpleGridColumn('curr_decimals');
$_decimals->editable = true;
$_decimals->width = 20;
$_decimals->search = false;
$_decimals->align = 'right';
$_decimals->setRequired();
$_decimals->setEditOptions(array('size' => 1, 'maxlength' => 1, 'defaultValue' => 2));

$gridconf->colModel = array($_id, $_name, $_decimals);

phpaga_header(array('menuitem' => 'administration'));
$tpl = new PSmarty;
$tpl->assign('gridconf', $gridconf->build());
$tpl->display('simple_item_grid.tpl.html');
phpaga_footer();

?>
