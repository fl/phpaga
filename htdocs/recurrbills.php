<?php
/**
 * phpaga
 *
 * Bills
 *
 * This is the recurring invoices (bills) management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2007, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$perms = array(PHPAGA_PERM_VIEW_INVOICES, PHPAGA_PERM_MANAGE_INVOICES);
PUser::protectPage($perms);

$search_args = null;
$rows = null;
$count = null;
$browse = null;
$duelist = null;

$select_rbill_types = array();
$select_rbill_status = array(PHPAGA_RBILL_STATUS_ENABLED => _('enabled'),
                            PHPAGA_RBILL_STATUS_DISABLED => _('disabled'));
$offset = 0;
$r = null;
$info = null;

$billInfo = array('cpn_id' => null,
                  'rbill_type' => null,
                  'rbill_status' => -1,
                  'rbill_id' => null);


if (isset($REQUEST_DATA['action'])) {

    switch ($REQUEST_DATA['action']) {

        case ACTION_ENABLE:
            if (!isset($REQUEST_DATA['id']) || !is_numeric($REQUEST_DATA['id']))
                phpaga_error(_('Invalid recurring invoice!'));
            else
                $r = phpaga_bills_recurrSetStatus($REQUEST_DATA['id'], PHPAGA_RBILL_STATUS_ENABLED);
            break;

        case ACTION_DISABLE:
            if (!isset($REQUEST_DATA['id']) || !is_numeric($REQUEST_DATA['id']))
                phpaga_error(_('Invalid recurring invoice!'));
            else
                $r = phpaga_bills_recurrSetStatus($REQUEST_DATA['id'], PHPAGA_RBILL_STATUS_DISABLED);
            break;

        case ACTION_DELETE:
            if (!isset($REQUEST_DATA['id']) || !is_numeric($REQUEST_DATA['id']))
                phpaga_error(_('Invalid recurring invoice!'));
            else
                $r = phpaga_bills_recurrDelete($REQUEST_DATA['id']);
            break;

        default:
            break;
    }
}

phpaga_arrayAddOption($select_paid, PHPAGA_OPTION_ALL);

if (isset($REQUEST_DATA["offset"]) && is_numeric($REQUEST_DATA["offset"]))
    $offset = $REQUEST_DATA["offset"];

if (isset($REQUEST_DATA["id"]) && is_numeric($REQUEST_DATA["id"]) && (!isset($REQUEST_DATA['action']) || ($REQUEST_DATA['action'] != ACTION_DELETE)))
    $billInfo["rbill_id"] = $REQUEST_DATA["id"];

if (isset($REQUEST_DATA["cpn_id"]) && is_numeric($REQUEST_DATA["cpn_id"]))
    $billInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];

if (isset($REQUEST_DATA["rbill_type"]) && is_numeric($REQUEST_DATA["rbill_type"]))
    $billInfo["rbill_type"] = $REQUEST_DATA["rbill_type"];

if (isset($REQUEST_DATA["rbill_status"]) && is_numeric($REQUEST_DATA["rbill_status"]))
    $billInfo["rbill_status"] = (int)$REQUEST_DATA["rbill_status"];

foreach ($billInfo as $key =>$value) {
    if (isset($value) && strlen($value))
        $search_args .= "&amp;$key=".rawurlencode ($value);
}

$rows = phpaga_bills_recurrSearch($count, $billInfo, $offset, PHPAGA_RECORDS_PERPAGE);
$duelist = phpaga_bills_recurrGetDueList();

phpaga_header(array("menuitem" => "finance"));

if (PhPagaError::isError($r))
    $r->printMessage();

if (PhPagaError::isError($info)) {
    $info->printMessage();
    $info = null;
}

if (PhPagaError::isError($rows)) {
    $rows->printMessage();
    $rows = array();
}

if (isset($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == ACTION_DELETE) && $r === true)
    phpaga_message(_('Recurring invoice deleted'),
                   sprintf(_('The recurring invoice %s has been deleted successfully.'), $REQUEST_DATA['id']));

foreach ($phpaga_rbill_types as $k => $v) {
    if ($k != PHPAGA_RBILL_TYPE_NONE)
        $select_rbill_types[$k] = $v;
}

phpaga_arrayAddOption($select_rbill_types, PHPAGA_OPTION_ALL);
phpaga_arrayAddOption($select_rbill_status, PHPAGA_OPTION_ALL, -1);

$tpl = new PSmarty;

$tpl->assign('perm_invoices', PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES));
$tpl->assign('billInfo', $billInfo);
$tpl->assign('info', $info);
$tpl->assign('duelist', $duelist);
$tpl->assign('select_cpn', PCompany::getSimpleArray(true, ''));
$tpl->assign('select_rbill_types', $select_rbill_types);
$tpl->assign('select_rbill_status', $select_rbill_status);
$tpl->assign('search_args', $search_args);
$tpl->assign('rows', $rows);
$tpl->assign("BROWSE", phpaga_navigate(basename($_SERVER["PHP_SELF"]). "?$search_args", $count, $offset,
                                       PHPAGA_RECORDS_PERPAGE));

$tpl->display('recurrbills.tpl.html');

phpaga_footer();

?>
