<?php
/**
 * phpaga
 *
 * Billing method plugins
 *
 * This is the billing method plugins management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2004, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');
PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);

$err = array();
$bmt = array('bmt_filename' => phpaga_fetch_REQUEST('file', ''),
             'bmt_description' => phpaga_fetch_REQUEST('bmt_description', ''),
             'ctr_id' => phpaga_fetch_REQUEST('ctr_id', PHPAGA_DEFAULTCOUNTRY));

if (isset($REQUEST_DATA['action']) && ($REQUEST_DATA['action'] == ACTION_ADD)) {

    if (!strlen($bmt['bmt_filename']))
        $err[] = _('Invalid filename');

    if (!strlen($bmt['bmt_description']))
        $err[] = _('Invalid plugin description');

    if (!is_numeric($bmt['ctr_id']))
        $err[] = _('Invalid country');

    if (count($err) == 0) {

        $result = phpaga_bills_add_bmt_plugin($bmt);

        if (!PhPagaError::isError($result)) {
            header('Location: bmt_plugins.php');
            exit;
        } else
            $err[] = $result->getMessage();
    }
}

phpaga_header(array('menuitem' => 'administration'));

if (count($err) > 0) {
    $err = implode($err, ' | ');
    phpaga_error($err);
}

$tpl = new PSmarty;
$tpl->assign('bmt', $bmt);
$tpl->assign('countries', PCountry::getSimpleArray());
$tpl->display('add_bmt_plugin.tpl.html');

phpaga_footer();

?>
