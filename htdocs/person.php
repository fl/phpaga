<?php
/**
 * phpaga
 *
 * person
 *
 * View person information
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$error = array();
$sum_duration = 0;
$action = ACTION_NONE;
$mhr_year = date('Y');
$isowner = false;
$opcatresult = true;
$opcatresult_project = true;

if (isset($REQUEST_DATA['action']) && is_numeric($REQUEST_DATA['action']))
    $action = $REQUEST_DATA['action'];

if (isset($REQUEST_DATA['mhr_year']) && is_numeric($REQUEST_DATA['mhr_year']))
     $mhr_year = $REQUEST_DATA['mhr_year'];

if (isset($REQUEST_DATA['id']) && is_numeric($REQUEST_DATA['id']))
    $pe_id = $REQUEST_DATA['id'];
else {
    if (isset($_SESSION['auth_user']['pe_id']) && is_numeric($_SESSION['auth_user']['pe_id'])) 
        $pe_id = $_SESSION['auth_user']['pe_id'];
    else
        $pe_id = null;
}

try {
    $person = new PPerson($pe_id);
    $isowner = $person->hasOwner($_SESSION['auth_user']['pe_id']);
    
} catch (Exception $e) { }

phpaga_header(array('menuitem' => 'core'));

$peInfo = PPerson::getInfo($pe_id);

if (PhPagaError::isError($peInfo))
    $peInfo->printMessage();
elseif ($peInfo === false)
    phpaga_error(_('Invalid person ID.'));
else {

    if (count($error) > 0)
        phpaga_error($error);

    $peopcatStats = phpaga_operations_getOpcatStats(array('pe_id' => $pe_id), $sum_duration, 0);
    if (PhPagaError::isError($peopcatStats))
        $opcatresult = false;

    $peopcatStats_project = phpaga_operations_getOpcatStats(array('pe_id' => $pe_id), $sum_duration, PHPAGA_OPCGRP_PROJECT);
    if (PhPagaError::isError($peopcatStats_project))
        $opcatresult_project = false;

    $tpl = new PSmarty;
    $tpl->assign('person', $peInfo);
    $tpl->assign('MANHOURS_HEADER', sprintf(_('Manhours'), $mhr_year));
    $tpl->assign('TOTAL_DURATION', $sum_duration);
    $tpl->assign('curryear', date('Y'));
    $tpl->assign('currmonth', (int)date('m'));
    $tpl->assign('mhr_year', $mhr_year);
    $tpl->assign("opcatresult", $opcatresult);
    $tpl->assign("opcatresult_project", $opcatresult_project);
    $tpl->assign("opcats", $peopcatStats);
    $tpl->assign("opcats_project", $peopcatStats_project);
    $tpl->assign('perm_persons', PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPERSONS));
    $tpl->assign('perm_rate', ($isowner
                               || PUser::hasPerm(PHPAGA_PERM_MANAGE_OTHERPERSONS)
                               || PUser::hasPerm(PHPAGA_PERM_VIEW_FINANCE)));

    $tpl->assign('file_rel_type', PHPAGA_RELTYPE_PERSON);
    $tpl->assign('file_rel_id', $pe_id);
    $tpl->assign('returl', basename($_SERVER['PHP_SELF']).'?id='.$pe_id);
    $tpl->display('person.tpl.html');
}

phpaga_footer();

?>
