<?php
/**
 * phpaga
 *
 * add expense
 *
 * Add expenses to a project
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2003, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

$prj_id = phpaga_fetch_REQUEST('prj_id');
if (!is_numeric($prj_id)) {
    phpaga_header(array('menuitem' => 'core'));
    phpaga_error(_('Invalid project ID'));
    phpaga_footer();
    exit;
}

$ismember = phpaga_project_is_member($prj_id, $_SESSION['auth_user']['pe_id']);
$can_manage = phpaga_project_canmanage($prj_id);
if (!$ismember && !$can_manage) {
    phpaga_header(array('menuitem' => 'core'));
    phpaga_message (_('No access'),
                    _("You can't file expenses for this project because you are not a project member."));
    phpaga_footer();
    exit;
}

$status = '';
$prjInfo = array();
$users = array();
$select_user = array();
$expInfo = array(
    'exp_date' => date('Y-m-d'),
    'pe_id' => $_SESSION['auth_user']['pe_id'],
    'prj_id' => $prj_id,
    'exp_id' => null,
    'exp_desc' => null,
    'exp_sum' => null,
    'curr_id' => PHPAGA_DEFAULTCURRENCY,
    'exp_sum_foreign' => null,
    'curr_id_foreign' => null,
    'exp_exchange_rate' => null);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    try {
        $exp = new PExpense();
        $exp->setPrjId($prj_id);
        $exp->setPeId($can_manage ? phpaga_fetch_POST('pe_id') : $_SESSION['auth_user']['pe_id']);
        $exp->setExpDate(phpaga_fetch_POST('exp_date'));
        $exp->setExpDesc(phpaga_fetch_POST('exp_desc'));
        $exp->setExpSum(phpaga_fetch_POST('exp_sum'));
        $exp->setCurrId(phpaga_fetch_POST('curr_id'));
        $exp->setExpSumForeign(phpaga_fetch_POST('exp_sum_foreign'));
        $exp->setCurrIdForeign(phpaga_fetch_POST('curr_id_foreign'));
        $exp->setExpExchangeRate(phpaga_fetch_POST('exp_exchange_rate'));
        $exp->store();

        $status = phpaga_returnmessage(_('Information'), _('The expense has been inserted successfully.'));

    } catch (Exception $e) {
        $status = phpaga_returnerror($e->getMessage());

        $expInfo['prj_id'] = $prj_id;
        $expInfo['prj_id'] = $can_manage ? phpaga_fetch_POST('pe_id') : $_SESSION['auth_user']['pe_id'];
        $expInfo['exp_date'] = phpaga_fetch_POST('exp_date');
        $expInfo['exp_desc'] = phpaga_fetch_POST('exp_desc');
        $expInfo['exp_sum'] = phpaga_fetch_POST('exp_sum');
        $expInfo['curr_id'] = phpaga_fetch_POST('curr_id');
        $expInfo['exp_sum_foreign'] = phpaga_fetch_POST('exp_sum_foreign');
        $expInfo['curr_id_foreign'] = phpaga_fetch_POST('curr_id_foreign');
        $expInfo['exp_exchange_rate'] = phpaga_fetch_POST('exp_exchange_rate');
    }
}

$prjInfo = phpaga_projects_getInfo($prj_id);
if (PhPagaError::isError($prjInfo)) {
        phpaga_header(array('menuitem' => 'core'));
        $prjInfo->printMessage();
        phpaga_footer();
        exit;
}

phpaga_header(array('menuitem' => 'core'));

$duration = phpaga_projects_getDuration($prjInfo['prj_id']);

/* The user can file expenses for other users if she has the right to
 * manage other people's projects or when this is her project */

if (!$can_manage) {
    $peInfo = PPerson::getInfo($_SESSION['auth_user']['pe_id']);
    $select_user[$peInfo['pe_id']] = sprintf('%s (%s)',
        $peInfo['pe_person'],
        $_SESSION['auth_user']['usr_login']);
} else {
    $users = PUser::findAsArray(array('persons{pe_id}=>project_members.prj_id=' => $prj_id), 
        array('persons{pe_id}.pe_lastname' => 'asc', 'persons{pe_id}.pe_firstname' => 'asc'));
    foreach ($users as $u)
        $select_user[$u['pe_id']] = $u['pe_name'].' ('.$u['usr_login'].')';
}

$tpl = new PSmarty;

$tpl->assign('prjInfo', $prjInfo);
$tpl->assign('expinfo', $expInfo);
$tpl->assign('HOURS_SO_FAR', $duration);
$tpl->assign('STATUSMSG', $status);
$tpl->assign('is_owner_or_manager', $can_manage);
$tpl->assign('select_person', $select_user);
$tpl->assign('select_curr', PCurrency::getList());
$tpl->assign('select_curr_foreign', PCurrency::getList(array('' => '')));
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

$tpl->display('edit_expense.tpl.html');

phpaga_footer();

?>
