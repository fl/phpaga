<?php
/**
 * phpaga
 *
 * Operations
 *
 * This is the operation (task) management interface that allows to search for
 * tasks using different search options. It also gives the possibility to create
 * an invoice when listing unbilled tasks only, as well as selecting single
 * to be edited, viewed or removed.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

phpaga_header(array("menuitem" => "core"));

$opIds = array();
$opInfo = array("cpn_id" => "",
                "cpn_id_issuebill" => "",
                "pe_id" => "",
                "prj_id" => "",
                "prj_status" => "",
                "prjcat_id" => "",
                "opcat_id" => "",
                "op_desc" => "",
                "op_billed" => "",
                "prj_billabletype" => "",
                "op_startdatefrom" => "",
                "op_startdateto" => "",
                "jcat_id" => "");

$search_args = "";
$statusmsg = "";
$action = "";
$op_id = "";

$subtotal_duration = 0;
$offset = 0;
$order = PHPAGA_SORT_OPERATIONS;

if (isset($REQUEST_DATA["offset"]) && is_numeric($REQUEST_DATA["offset"]))
    $offset = $REQUEST_DATA["offset"];

if (isset($REQUEST_DATA["order"]) && is_numeric($REQUEST_DATA["order"])) {

    $order = $REQUEST_DATA["order"];

    $r = PConfig::setUserItem('SORT_OPERATIONS', $order);

    if (PhPagaError::isError($r))
        $r->printMessage();
}

if (isset($REQUEST_DATA["cpn_id"]) && is_numeric($REQUEST_DATA["cpn_id"]))
    $opInfo["cpn_id"] = $REQUEST_DATA["cpn_id"];

if (isset($REQUEST_DATA["cpn_id_issuebill"]) && is_numeric($REQUEST_DATA["cpn_id_issuebill"]))
    $opInfo["cpn_id_issuebill"] = $REQUEST_DATA["cpn_id_issuebill"];

if (isset($REQUEST_DATA["pe_id"]) && is_numeric($REQUEST_DATA["pe_id"]))
    $opInfo["pe_id"] = $REQUEST_DATA["pe_id"];

if (isset($REQUEST_DATA["prj_id"]) && is_numeric($REQUEST_DATA["prj_id"]))
    $opInfo["prj_id"] = $REQUEST_DATA["prj_id"];

if (isset($REQUEST_DATA["prj_status"]) && is_numeric($REQUEST_DATA["prj_status"]))
    $opInfo["prj_status"] = $REQUEST_DATA["prj_status"];

if (isset($REQUEST_DATA["prjcat_id"]) && is_numeric($REQUEST_DATA["prjcat_id"]))
    $opInfo["prjcat_id"] = $REQUEST_DATA["prjcat_id"];

if (isset($REQUEST_DATA["jcat_id"]) && is_numeric($REQUEST_DATA["jcat_id"]))
    $opInfo["jcat_id"] = $REQUEST_DATA["jcat_id"];

if (isset($REQUEST_DATA["opcat_id"]) && is_numeric($REQUEST_DATA["opcat_id"]))
    $opInfo["opcat_id"] = $REQUEST_DATA["opcat_id"];

if (isset($REQUEST_DATA["op_desc"]) && ($REQUEST_DATA["op_desc"] != ""))
    $opInfo["op_desc"] = rawurldecode($REQUEST_DATA["op_desc"]);

if (isset($REQUEST_DATA["op_billed"]) && is_numeric($REQUEST_DATA["op_billed"])
    && (($REQUEST_DATA["op_billed"] == PHPAGA_BILL_BILLED)
        || ($REQUEST_DATA["op_billed"] == PHPAGA_BILL_NOTBILLED)))

    $opInfo["op_billed"] = $REQUEST_DATA["op_billed"];

if (isset($REQUEST_DATA["prj_billabletype"]) && is_numeric($REQUEST_DATA["prj_billabletype"]))
    $opInfo["prj_billabletype"] = $REQUEST_DATA["prj_billabletype"];

if (isset($REQUEST_DATA["op_startdatefrom"]) && ($REQUEST_DATA["op_startdatefrom"] != ""))
    $opInfo["op_startdatefrom"] = rawurldecode($REQUEST_DATA["op_startdatefrom"]);

if (isset($REQUEST_DATA["op_startdateto"]) && ($REQUEST_DATA["op_startdateto"] != ""))
    $opInfo["op_startdateto"] = rawurldecode($REQUEST_DATA["op_startdateto"]);

if (isset($REQUEST_DATA["action"]) && is_numeric($REQUEST_DATA["action"])) {

    $action = $REQUEST_DATA["action"];

    if (isset($REQUEST_DATA["op_id"]) && is_numeric($REQUEST_DATA["op_id"])) {

        $op_id = $REQUEST_DATA["op_id"];

        if ($action == ACTION_OP_REMOVE) {

            $result = phpaga_operations_delete($op_id);

            if (PhPagaError::isError($result))
                $result->printMessage();
        }
    }
    else
        $op_id = '';
}

foreach ($opInfo as $key =>$value)
{
    if (isset($value) && ($value != ""))
    {
        $search_args .= "&amp;$key=".rawurlencode($value);
    }
}

$location = $_SERVER["PHP_SELF"]."?$search_args&amp;order=";

$select_project = phpaga_projects_getProjectList();
phpaga_arrayAddOption($select_project, PHPAGA_OPTION_ALL);

$select_prjstat = phpaga_projects_getStatusList();
phpaga_arrayAddOption($select_prjstat, PHPAGA_OPTION_ALL);

$select_opcat = phpaga_operations_getCategoryList();
phpaga_arrayAddOption($select_opcat, PHPAGA_OPTION_ALL);

$select_billed = array(PHPAGA_BILL_BILLED => _("billed"), PHPAGA_BILL_NOTBILLED => _("not billed"));
phpaga_arrayAddOption($select_billed, PHPAGA_OPTION_ALL);

$select_prjcat = phpaga_projects_getCategoryList();
phpaga_arrayAddOption($select_prjcat, PHPAGA_OPTION_ALL);

$select_jcat = phpaga_jobcat_getList();
phpaga_arrayAddOption($select_jcat, PHPAGA_OPTION_ALL);

$persons = PPerson::getList($persons_count, null, null);

foreach ($persons as $person) {
    $h_pe[$person['pe_id']] = $person['pe_person'];
}

$select_person = $h_pe;
phpaga_arrayAddOption($select_person, PHPAGA_OPTION_ALL);

phpaga_arrayAddOption($phpaga_projects_billabletype, PHPAGA_OPTION_ALL);

$tpl = new PSmarty;
$tpl->assign('operation', $opInfo);
$tpl->assign('select_opcat', $select_opcat);
$tpl->assign('select_billed', $select_billed);
$tpl->assign('select_project', $select_project);
$tpl->assign('select_prjstat', $select_prjstat);
$tpl->assign('select_prjcat', $select_prjcat);
$tpl->assign('select_prj_billabletype', $phpaga_projects_billabletype);
$tpl->assign('select_jcat', $select_jcat);
$tpl->assign('select_person', $select_person);
$tpl->assign('select_cpn', PCompany::getSimpleArray(true, ''));
$tpl->assign('ORDER', $order);
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

$oprows = phpaga_operations_search($count, $duration, $opIds, $opInfo, $order, $offset, PHPAGA_RECORDS_PERPAGE);

if (PhPagaError::isError($oprows))
    $statusmsg = $oprows->getFormattedMessage();
else if (!count($oprows))
    $statusmsg = phpaga_returnmessage(_("Information"),
                                      _("No tasks matching your query were found."));
else {

    while (list($key, $operation) = each($oprows))
        $subtotal_duration += $operation["op_duration"];

    switch ($order)
    {

        case ORDER_OP_PE_ASC:
            $sort["pe"] = ORDER_OP_PE_DESC;
            $sort["pr"] = ORDER_OP_PR_ASC;
            $sort["opcat"] = ORDER_OP_OPCAT_ASC;
            $sort["duration"] = ORDER_OP_DURATION_ASC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_ASC;
            break;

        case ORDER_OP_PR_ASC:
            $sort["pe"] = ORDER_OP_PE_ASC;
            $sort["pr"] = ORDER_OP_PR_DESC;
            $sort["opcat"] = ORDER_OP_OPCAT_ASC;
            $sort["duration"] = ORDER_OP_DURATION_ASC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_ASC;
            break;

        case ORDER_OP_OPCAT_ASC:
            $sort["pe"] = ORDER_OP_PE_ASC;
            $sort["pr"] = ORDER_OP_PR_ASC;
            $sort["opcat"] = ORDER_OP_OPCAT_DESC;
            $sort["duration"] = ORDER_OP_DURATION_ASC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_ASC;
            break;

        case ORDER_OP_DURATION_ASC:
            $sort["pe"] = ORDER_OP_PE_ASC;
            $sort["pr"] = ORDER_OP_PR_ASC;
            $sort["opcat"] = ORDER_OP_OPCAT_ASC;
            $sort["duration"] = ORDER_OP_DURATION_DESC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_ASC;
            break;

        case ORDER_OP_TIMESTAMP_START_ASC:
            $sort["pe"] = ORDER_OP_PE_ASC;
            $sort["pr"] = ORDER_OP_PR_ASC;
            $sort["opcat"] = ORDER_OP_OPCAT_ASC;
            $sort["duration"] = ORDER_OP_DURATION_ASC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_DESC;
            break;

        default:
            $sort["pe"] = ORDER_OP_PE_ASC;
            $sort["pr"] = ORDER_OP_PR_ASC;
            $sort["opcat"] = ORDER_OP_OPCAT_ASC;
            $sort["duration"] = ORDER_OP_DURATION_ASC;
            $sort["start"] = ORDER_OP_TIMESTAMP_START_ASC;
            break;
    }

    $tpl->assign("search_args", $search_args);

    /* show the "create invoice" link if we have unbilled operations only */

    if (PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES)
        && is_numeric($opInfo["op_billed"])
        && ($opInfo["op_billed"] == PHPAGA_BILL_NOTBILLED))

        $tpl->assign("show_create_bill", true);
    else
        $tpl->assign("show_create_bill", false);

    $tpl->assign("sort", $sort);
    $tpl->assign("operations", $oprows);
    $tpl->assign("SUBTOTALHOURS", $subtotal_duration);
    $tpl->assign("TOTALHOURS", $duration);

    $tpl->assign("BROWSE",
                 phpaga_navigate($_SERVER["PHP_SELF"].
                                 "?order=$order".$search_args,
                                 $count, $offset, PHPAGA_RECORDS_PERPAGE));

    //$tpl->assign("auth_user", $auth_user);
    $tpl->assign("refop", rawurlencode(str_replace("&", "|", $_SERVER["PHP_SELF"]."?order=$order". $search_args. "&offset=$offset")));
}

$tpl->assign('SELF', basename($_SERVER['PHP_SELF']));
$tpl->assign('STATUSMSG', $statusmsg);
$tpl->assign('perm_view_invoices', PUser::hasPerm(PHPAGA_PERM_VIEW_INVOICES));
$tpl->assign("DELETE_OP_URL_EXTRA", "&amp;order=$order".$search_args."&amp;offset=$offset");
$tpl->assign('location', $location);
$tpl->assign('porder', $order);

$tpl->display('operations.tpl.html');

phpaga_footer();

?>
