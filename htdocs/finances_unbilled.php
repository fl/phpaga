<?php
/**
 * phpaga
 *
 * Finances: Unbilled hours
 *
 * Show a list of unbilled hours, grouped by Customer and Project.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2005, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once ('./config.php');
PUser::protectPage(PHPAGA_PERM_VIEW_FINANCE);

if (isset($REQUEST_DATA['groupby'])) {

    if ($REQUEST_DATA['groupby'] == PHPAGA_GROUPBY_CUSTOMER)
        $groupby_sel = PHPAGA_GROUPBY_CUSTOMER;
    else
        $groupby_sel = PHPAGA_GROUPBY_PROJECT;

    $r = PConfig::setUserItem('FIN_UNBILLED_GRPBY', $groupby_sel);

    if (PhPagaError::isError($r))
        $r->printMessage();
} else
    $groupby_sel = PHPAGA_FIN_UNBILLED_GRPBY;

$percustomer = ($groupby_sel == PHPAGA_GROUPBY_CUSTOMER);

$summary = phpaga_bills_getSummaryNotBilledHours($percustomer);

phpaga_header(array('menuitem' => 'finance'));

if (PhPagaError::isError($summary))
    $summary->printMessage();
else {
    $tpl = new PSmarty;

    $config = PConfig::getArray();

    $tpl->assign('summary', $summary);
    $tpl->assign('percustomer', $percustomer);
    $tpl->assign('phpaga_groupby', $phpaga_groupby);
    $tpl->assign('groupby_sel', $groupby_sel);
    $tpl->assign('config', $config);
    $tpl->display('finances_unbilled.tpl.html');
}

phpaga_footer();

?>
