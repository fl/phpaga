<?php
  /**
   * phpaga
   *
   * add person
   *
   * Add a person
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

include_once('./config.php');

$status = null;

$peInfo = array(
    'pe_firstname' => null,
    'pe_lastname' => null,
    'pe_address' => null,
    'pe_address2' => null,
    'pe_city' => null,
    'pe_region' => null,
    'pe_zip' => null,
    'ctr_id' => PHPAGA_DEFAULTCOUNTRY,
    'pecat_id' => null,
    'pe_hourlyrate' => null,
    'pe_taxnr' => null,
    'pe_phonework' => null,
    'pe_phonemobile' => null,
    'pe_phonehome' => null,
    'pe_fax' => null,
    'pe_url' => null,
    'pe_email' => null,
    'jcat_id' => null,
    'pe_note' => null);

$auth_user = PSession::getAuthUser();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    try {
        $person = new PPerson();
        $person->setPeFirstname(phpaga_fetch_POST('pe_firstname'));
        $person->setPeLastname(phpaga_fetch_POST('pe_lastname'));
        $person->setPeAddress(phpaga_fetch_POST('pe_address'));
        $person->setPeAddress2(phpaga_fetch_POST('pe_address2'));
        $person->setPeCity(phpaga_fetch_POST('pe_city'));
        $person->setPeRegion(phpaga_fetch_POST('pe_region'));
        $person->setPeZip(phpaga_fetch_POST('pe_zip'));
        $person->setCtrId(phpaga_fetch_POST('ctr_id'));
        $person->setPecatId(phpaga_fetch_POST('pecat_id'));
        $person->setPeHourlyrate(phpaga_fetch_POST('pe_hourlyrate'));
        $person->setPeTaxnr(phpaga_fetch_POST('pe_taxnr'));
        $person->setPePhonework(phpaga_fetch_POST('pe_phonework'));
        $person->setPePhonemobile(phpaga_fetch_POST('pe_phonemobile'));
        $person->setPePhonehome(phpaga_fetch_POST('pe_phonehome'));
        $person->setPeFax(phpaga_fetch_POST('pe_fax'));
        $person->setPeUrl(phpaga_fetch_POST('pe_url'));
        $person->setPeEmail(phpaga_fetch_POST('pe_email'));
        $person->setCpnId(phpaga_fetch_POST('cpn_id'));
        $person->setJcatId(phpaga_fetch_POST('jcat_id'));
        $person->setPeNote(phpaga_fetch_POST('pe_note'));
        $person->setPePeId($auth_user['pe_id']);

        $person->store();

        header('Location: person.php?id='.$person->getPeId());
        exit;
    } catch (Exception $e) {
        $status = phpaga_returnerror($e->getmessage());

        $peInfo['pe_firstname'] = phpaga_fetch_POST('pe_firstname');
        $peInfo['pe_lastname'] = phpaga_fetch_POST('pe_lastname');
        $peInfo['pe_address'] = phpaga_fetch_POST('pe_address');
        $peInfo['pe_address2'] = phpaga_fetch_POST('pe_address2');
        $peInfo['pe_city'] = phpaga_fetch_POST('pe_city');
        $peInfo['pe_region'] = phpaga_fetch_POST('pe_region');
        $peInfo['pe_zip'] = phpaga_fetch_POST('pe_zip');
        $peInfo['ctr_id'] = phpaga_fetch_POST('ctr_id');
        $peInfo['pecat_id'] = phpaga_fetch_POST('pecat_id');
        $peInfo['pe_hourlyrate'] = phpaga_fetch_POST('pe_hourlyrate');
        $peInfo['pe_taxnr'] = phpaga_fetch_POST('pe_taxnr');
        $peInfo['pe_phonework'] = phpaga_fetch_POST('pe_phonework');
        $peInfo['pe_phonemobile'] = phpaga_fetch_POST('pe_phonemobile');
        $peInfo['pe_phonehome'] = phpaga_fetch_POST('pe_phonehome');
        $peInfo['pe_fax'] = phpaga_fetch_POST('pe_fax');
        $peInfo['pe_url'] = phpaga_fetch_POST('pe_url');
        $peInfo['pe_email'] = phpaga_fetch_POST('pe_email');
        $peInfo['cpn_id'] = phpaga_fetch_POST('cpn_id');
        $peInfo['jcat_id'] = phpaga_fetch_POST('jcat_id');
        $peInfo['pe_note'] = phpaga_fetch_POST('pe_note');
        $peInfo['pe_pe_id'] = $auth_user['pe_id'];
    }
} else {
    if (isset($_GET['cpn']))
        $peInfo['cpn_id'] = $_GET['cpn'];
}

phpaga_header(array('menuitem' => 'core'));

$tpl = new PSmarty;

$tpl->assign('peInfo', $peInfo);
$tpl->assign('select_ctr', PCountry::getSimpleArray());
$tpl->assign('select_cpn', PCompany::getSimpleArray(true, ''));
$tpl->assign('select_pecat', PPersonCategory::getSimpleArray(true, ''));
$tpl->assign('jobcategories', PJobCategory::getSimpleArray(true, ''));
$tpl->assign('STATUSMSG', $status);
$tpl->assign('FORM_ACTION', basename($_SERVER['PHP_SELF']));

$tpl->display('edit_person.tpl.html');

phpaga_footer();

?>