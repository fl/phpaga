<?php
/**
 * phpaga
 *
 * User
 *
 * This is the user management interface.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$error = null;
$permchecked = array();
$options_truefalse = array(true => _('Yes'), false => _('No'));
$action = phpaga_fetch_REQUEST('action');
$_id = phpaga_fetch_REQUEST('id');
$canManage = PUser::hasPerm(PHPAGA_PERM_MANAGE_SYSSETTINGS);

/* An admin can change other user's settings, simple users can only change their own settings. */
$usr_id = $canManage && is_numeric($_id) ? $_id : $_SESSION['auth_user']['usr_id'];

try {
    $user = new PUser($usr_id);
} catch (Exception $e) {
    phpaga_error($e->getMessage());
    die();
}

if ($action==ACTION_UPDATE) {

    if ($canManage) {
        $perm = phpaga_fetch_POST('perm');
        if (!count($perm))
            $perm = null;
        else
            $perm = implode('|', $perm);
        $user->setUsrPerm($perm);
    }

    $user->setUsrSkin(phpaga_fetch_POST('usr_skin'));

    /* Check if password change is requested and if new password and
     * confirmation match */

    $pwd_new = $REQUEST_DATA['usr_pwd_new'];
    if (strlen($pwd_new)) {

        $pwd_confirm = $REQUEST_DATA['usr_pwd_confirm'];
        if ($pwd_new!=$pwd_confirm)
            $error = _('The new password and its confirmation do not match.');
        else {

            /* If the current user has no admin rights then check the old passwd */
            if (!$canManage) {
                $pwd_old = $REQUEST_DATA['usr_pwd_old'];
                $h_userID = PUser::authenticate($user->getUsrLogin(), $pwd_old);
                if (PhPagaError::isError($h_userID))
                    $error = _('The old password is incorrect.');
                else
                    $user->setPassword($pwd_new);
            } else
                $user->setPassword($pwd_new);
        }
    }

    if (!strlen($error)) {
        $update_result = null;
        try {
            $user->store();
            $userconfig = phpaga_fetch_POST('userconfig');
            if (is_array($userconfig) && count($userconfig)) {
                foreach($userconfig as $key => $value) {
                    $result = PConfig::setUserItem($key, $value, $usr_id);
                    if (PhPagaError::isError($result)) {
                        $result->printMesssage();
                        $update_result = $result;
                    }
                }
            }

            /* Reload the auth_user information and set the session variables
             * accordingly if we're doing modifications on the current user
             */
            if ($_SESSION['auth_user']['usr_id'] == $usr_id) {
                $_SESSION['auth_user'] = $user->getAuthUserInfo();
            }

            if (!PhPagaError::isError($update_result)) {
                header('Location: '.$_SERVER['PHP_SELF'].'?id='.$usr_id.'&action='.ACTION_UPDATE_OK);
                exit;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
}

phpaga_header();

if (strlen($error))
    phpaga_error($error);

if ($action==ACTION_UPDATE_OK)
    phpaga_message(_('User data updated'), _('The user settings have been updated successfully.'));

try {
    $u = new PUser($usr_id);
    $user = $u->asArray();
} catch (Exception $e) {
    phpaga_error($e->getMessage());
    die();
}

foreach ($phpaga_perm as $h_perm => $permtxt)
    if (PUser::hasPerm($h_perm, $usr_id))
        $permchecked[$h_perm] = 'checked="checked\"';
    else
        $permchecked[$h_perm] = '';

$tpl = new PSmarty;

$tpl->assign('SELF', basename($_SERVER['PHP_SELF']));
$tpl->assign('user', $user);
$tpl->assign('skins', phpaga_getSkinNames());
$tpl->assign('languages', PhPagaLanguage::getList());
$tpl->assign('privmanage', $canManage);
$tpl->assign('perm', $phpaga_perm);
$tpl->assign('permchecked', $permchecked);
$tpl->assign('options_truefalse', $options_truefalse);

$tpl->display('user.tpl.html');

phpaga_footer();

?>
