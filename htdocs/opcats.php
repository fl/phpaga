<?php
  /**
   * phpaga
   *
   * Operation categories
   *
   * This is the operation categories management interface.
   *
   * @author Florian Lanthaler <florian@phpaga.net>
   * @version $Id$
   *
   * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
   *
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions are
   * met:
   *
   *    * Redistributions of source code must retain the above copyright
   *      notice, this list of conditions and the following disclaimer.
   *
   *    * Redistributions in binary form must reproduce the above copyright
   *      notice, this list of conditions and the following disclaimer in
   *      the documentation and/or other materials provided with the
   *      distribution.
   *
   *    * Neither the name of Florian Lanthaler nor the names of his
   *      contributors may be used to endorse or promote products derived
   *      from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
   * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
   * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

include_once("./config.php");
PUser::protectPage(PHPAGA_PERM_MANAGE_SYSSETTINGS);

$edit = false;
$rows = array();
$opcat_title = "";

$color = null;
$hourlyrate = null;

phpaga_header(array("menuitem" => "administration"));

if (isset($REQUEST_DATA["action"]) && strlen($REQUEST_DATA["action"]))
{

    switch ($REQUEST_DATA["action"])
    {

    case ACTION_ADD:

        /* add a new task category */

        if (isset($REQUEST_DATA["opcat_title"])
            && strlen($REQUEST_DATA["opcat_title"]))
        {
            if (isset($REQUEST_DATA["opcat_color"])
                && strlen($REQUEST_DATA["opcat_color"]))
                $color = $REQUEST_DATA["opcat_color"];
            else
                $color = null;

            if (isset($REQUEST_DATA["opcat_hourlyrate"])
                && is_numeric($REQUEST_DATA["opcat_hourlyrate"]))
                $hourlyrate = $REQUEST_DATA["opcat_hourlyrate"];
            else
                $hourlyrate = null;

            $result = phpaga_operations_addOpcat($REQUEST_DATA["opcat_title"], $color, $hourlyrate);

            if (PhPagaError::isError($result)) {
                $result->printMessage();
                $opcat_title = $REQUEST_DATA["opcat_title"];
            } else
                phpaga_message(_("Task category added"), _("The task category has been inserted successfully."));
        }
        break;

    case ACTION_UPDATE:

        if (!isset($REQUEST_DATA["id"]) || !is_numeric($REQUEST_DATA["id"]))
            phpaga_error(_("Illegal task category id."));
        else
        {
            $opcat_id = $REQUEST_DATA["id"];
            $edit = true;
        }
        break;

    case ACTION_UPDATE_OK:

        if (!isset($REQUEST_DATA["id"]) || !is_numeric($REQUEST_DATA["id"]))
            phpaga_error(_("Illegal task category id."));

        elseif (!isset($REQUEST_DATA["opcat_title"]) || !strlen($REQUEST_DATA["opcat_title"])) {
            phpaga_error(PHPAGA_ERR_OPCAT_INVALIDTITLE);
            $edit = true;
        }
        else
        {
            if (isset($REQUEST_DATA["opcat_color"])
                && strlen($REQUEST_DATA["opcat_color"]))
                $color = $REQUEST_DATA["opcat_color"];
            else
                $color = null;

            if (isset($REQUEST_DATA["opcat_hourlyrate"])
                && is_numeric($REQUEST_DATA["opcat_hourlyrate"]))
                $hourlyrate = $REQUEST_DATA["opcat_hourlyrate"];
            else
                $hourlyrate = null;


            $result = phpaga_operations_updateOpcat($REQUEST_DATA["id"], $REQUEST_DATA["opcat_title"], $color, $hourlyrate);
            if (PhPagaError::isError($result)) {
                phpaga_error($result);
                $edit = true;
            }
        }
        break;

    case ACTION_DELETE:

        /* delete a task category */

        if (isset($REQUEST_DATA["id"]) && strlen($REQUEST_DATA["id"]))
        {
            $result = phpaga_operations_deleteOpcat($REQUEST_DATA["id"]);

            if (PhPagaError::isError($result)) {

                /* if we could not delete the record because of
                 * dependencies then use a more descriptive
                 * error message
                 */

                if ($result->getCode() == PHPAGA_ERR_DEL_DEPENDENCIES)
                    phpaga_error(_("The task category is still used (by tasks or hourly rates information) and cannot be deleted."));
                else
                    $result->printMessage();
            }
            else
                phpaga_message(_("Task category deleted"), _("The task category has been successfully deleted."));
        }
        break;


    default:
        break;
    }
}

if ($edit) {
    $rows = phpaga_operations_getOpcats($opcat_id);
} else {
    $rows = phpaga_operations_getOpcats();
}

if (PhPagaError::isError($rows))
    $rows->printMessage();

$tpl = new PSmarty;

$tpl->assign("FORM_ACTION", basename($_SERVER["PHP_SELF"]));
$tpl->assign("opcat_title", $opcat_title);
$tpl->assign("edit", $edit);
$tpl->assign("rows", $rows);

$tpl->display("opcats.tpl.html");

phpaga_footer();

?>
