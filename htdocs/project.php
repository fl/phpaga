<?php
/**
 * phpaga
 *
 * project
 *
 * View project information
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$acerror = null;
$sum_duration = 0;
$opcatresult = true;
$opcatresult_person = true;
$ophide = true;
$prjmanage = false;
$prjmember = false;
$map = null;
$prj_id = null;

if (!isset($REQUEST_DATA["id"]) || !is_numeric($REQUEST_DATA["id"])) {
    phpaga_header(array("menuitem" => "core"));
    phpaga_message(_("Invalid project"),
                   _("Invalid project ID."));
    phpaga_footer();
    exit;
}

$prj_id = $REQUEST_DATA["id"];

if (!phpaga_project_canview($prj_id)) {
    phpaga_header(array("menuitem" => "core"));
    phpaga_message(_("No access"),
                   _("You do not have the required permissions to access this project's data."));
    phpaga_footer();
    exit;
}

$prjmanage = phpaga_project_canmanage($prj_id);
$prjmember = phpaga_project_is_member($prj_id, $_SESSION["auth_user"]["pe_id"]);

if (isset($REQUEST_DATA["ophide"]) && ($REQUEST_DATA["ophide"] == 1))
    $ophide = false;;

if (isset($REQUEST_DATA["action"]) && is_numeric($REQUEST_DATA["action"])) {

    if (!$prjmanage)
        phpaga_stoppage(_("No permission"),
                        _("You do not have the required permissions to perform this action."));

    $action = $REQUEST_DATA["action"];

    switch($action) {

    case ACTION_MATERIAL_REMOVE:

        if (isset($REQUEST_DATA["mat_id"]) && is_numeric($REQUEST_DATA["mat_id"])) {

            /* Only users with the permission to manage this project can remove a material entry */

            if ($prjmanage) {

                $result = phpaga_materials_delete($REQUEST_DATA["mat_id"]);

                if (PhPagaError::isError($result))
                    $result->printMessage();
            }
        }
        break;

    case ACTION_PRJ_REMOVE:

        if (!$prjmanage)
            phpaga_stoppage(_("No permission"),
                            _("You do not have the required permissions to perform this action."));

        if (strlen($prj_id)) {

            $acerror = phpaga_projects_delete($prj_id);

            if (!PhPagaError::isError($acerror)) {
                header("Location: projects.php");
                exit;
            } else if ($acerror->getCode() == PHPAGA_ERR_DEL_DEPENDENCIES) {

                $tpl = new PSmarty;
                $tpl->assign("prj_id", $prj_id);
                $tpl->display("delete_project_force.tpl.js");
                $acerror = null;
            }

        }
        break;

    case ACTION_PRJ_REMOVE_FORCE:

        if (!$prjmanage)
            phpaga_stoppage(_("No permission"),
                            _("You do not have the required permissions to perform this action."));

        if (strlen($prj_id)) {

            $acerror = phpaga_projects_delete($prj_id, true);

            if (!PhPagaError::isError($acerror)) {
                header("Location: projects.php");
            }
        }
        break;

    case ACTION_PRJ_MEMBER_REMOVE:

        if (!$prjmanage)
            phpaga_stoppage(_("No permission"),
                            _("You do not have the required permissions to perform this action."));

        if (isset($REQUEST_DATA["pe_id"]) && is_numeric($REQUEST_DATA["pe_id"])) {

            $pe_id = $REQUEST_DATA["pe_id"];

            $result = phpaga_projects_deletemember($prj_id, $pe_id);

            if (PhPagaError::isError($result))
                $result->printMessage();
        }
        break;

    default:
        break;

    }
}

$prjInfo = phpaga_projects_getInfo($prj_id);

if (PhPagaError::isError($prjInfo) || !count($prjInfo)) {
    phpaga_header(array("menuitem" => "core"));
    $prjInfo->printMessage();
    phpaga_footer();
    exit;
}

phpaga_header(array("menuitem" => "core"));

if (PhPagaError::isError($acerror))
    $acerror->printMessage();

$prjopcatStats = phpaga_operations_getOpcatStats(array("prj_id" => $prj_id), $sum_duration, 0);
if (PhPagaError::isError($prjopcatStats))
    $opcatresult = false;

$prjopcatStats_person = phpaga_operations_getOpcatStats(array("prj_id" => $prj_id), $sum_duration, PHPAGA_OPCGRP_PERSON);
if (PhPagaError::isError($prjopcatStats_person))
    $opcatresult_person = false;

$use_graphviz = (defined("PHPAGA_GRAPHVIZ_ENABLED") && (PHPAGA_GRAPHVIZ_ENABLED == true));

if ($use_graphviz)
    $map = phpaga_projects_getRelationGraph($prj_id, true);

$config = PConfig::getArray();

$tpl = new PSmarty;
$tpl->assign("config", $config);
$tpl->assign("project", $prjInfo);
$tpl->assign("use_graphviz", $use_graphviz);
$tpl->assign("relmap", $map);
$tpl->assign("refop", rawurlencode("id=$prj_id"));
$tpl->assign("SELF", basename($_SERVER["PHP_SELF"]));
$tpl->assign("opcatresult", $opcatresult);
$tpl->assign("opcatresult_person", $opcatresult_person);
$tpl->assign("opcats", $prjopcatStats);
$tpl->assign("opcats_person", $prjopcatStats_person);
$tpl->assign("auth_user", $_SESSION["auth_user"]);
$tpl->assign("ophide", $ophide);
$tpl->assign("prjmanage", $prjmanage);
$tpl->assign("prjmember", $prjmember);
$tpl->assign("file_rel_type", PHPAGA_RELTYPE_PROJECT);
$tpl->assign("file_rel_id", $prjInfo["prj_id"]);
$tpl->assign("DELETE_OP_URL_EXTRA", "&id=$prj_id");
$tpl->assign("returl", basename($_SERVER["PHP_SELF"])."?id=".$prjInfo["prj_id"]);
$tpl->assign("perm_invoice", PUser::hasPerm(PHPAGA_PERM_MANAGE_INVOICES));

$tpl->display("project.tpl.html");

phpaga_footer();

?>
