<?php
/**
 * phpaga
 *
 * add operation
 *
 * Add operations to an open project
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once("./config.php");

$status = "";
$users = array();
$select_user = array();
$opInfo = array("prj_id" => "");
$prj_id = "";
$ismember = false;
$prjInfo = array();
$op_id = null;

$can_manage = false;

if (isset($REQUEST_DATA["prj_id"]) && is_numeric($REQUEST_DATA["prj_id"]))
    $prj_id = $REQUEST_DATA["prj_id"];

$ismember = phpaga_project_is_member($prj_id, $_SESSION["auth_user"]["pe_id"]);

$can_manage = phpaga_project_canmanage($prj_id);

/* user can add tasks if she is a project member, if she has the right
 * to manage this project */

if (!$ismember && !$can_manage)
{
    phpaga_header(array("menuitem" => "core"));
    phpaga_message (_("No access"),
                    _("You can't file tasks for this project because you are not a project member."));
    phpaga_footer();
    exit;
}

/* if we're invoked by post, we have to save the modifications */
if ($_SERVER["REQUEST_METHOD"] == "POST")
{

    $opInfo["prj_id"] = $prj_id;

    /* only the project owner and overall project managers can set an
     * operation's user */

    if (!$can_manage)
        $opInfo["pe_id"] = $_SESSION["auth_user"]["pe_id"];
    else
        $opInfo["pe_id"] = $_POST["pe_id"];

    if (isset($REQUEST_DATA["op_billabletype"]) && ($REQUEST_DATA["op_billabletype"] == 0)) {

        $opInfo["op_billabletype"] = 0;
        $opInfo["billable_checked"] = true;
    } else {
        $opInfo["op_billabletype"] = 10;
        $opInfo["billable_checked"] = false;
    }

    $opInfo["opcat_id"] = $_POST["opcat_id"];
    $opInfo["op_desc"] = $_POST["op_desc"];
    $opInfo["op_duration"] = $_POST["op_duration"];

	/* if the duration is numeric, convert it from fractional hours into hours:minutes */

	if (is_numeric($opInfo["op_duration"]))
	{
	// Under 24 - assume the duration was given in hours
		if ($opInfo["op_duration"] < 24)
			$opInfo["op_duration"] = (int)$opInfo["op_duration"].':'.sprintf('%02d', 60 * ($opInfo["op_duration"] - (int)$opInfo["op_duration"]));
	// More than 60 minutes, different calculation
		elseif ($opInfo["op_duration"] >= 60)
			$opInfo["op_duration"] = (int)($opInfo["op_duration"] / 60).':'.sprintf('%02d', $opInfo["op_duration"] % 60);
	// duration given in minutes
		else
			$opInfo["op_duration"] = '0:'.(int)$opInfo["op_duration"];
	}

    /* if there is a valid start date and time then convert it to a timestamp */

    $opInfo["op_startdate"]  = $_POST["op_startdate"];
    $opInfo["op_starthours"] = $_POST["op_starthours"];

    $start_tmp = "";

    if ($opInfo["op_startdate"] != "")
    {
        $start_tmp = $opInfo["op_startdate"];

        if ($opInfo["op_starthours"] != "")
			$start_tmp .= " ".$opInfo["op_starthours"];

        $opInfo["op_start"] = $start_tmp;
    }
    else
        $opInfo["op_start"] = null;

    /* if there is a valid end date and time then convert it to a timestamp */

    $opInfo["op_enddate"]  = $_POST["op_enddate"];
    $opInfo["op_endhours"] = $_POST["op_endhours"];

    $end_tmp = "";

    if ($opInfo["op_enddate"] != "")
    {
        $end_tmp = $opInfo["op_enddate"];

        if ($opInfo["op_endhours"] != "")
            $end_tmp .= " ".$opInfo["op_endhours"];

        $opInfo["op_end"] = $end_tmp;
    }
    else
        $opInfo["op_end"] = null;

    $result = phpaga_operations_validdata($opInfo);

    if (PhPagaError::isError($result))
        $status = $result->getFormattedMessage();
    else {

        $op_id = phpaga_operations_add($opInfo);

        if (PhPagaError::isError($op_id))
            $status = $op_id->getFormattedMessage();
        else {

            $status = phpaga_returnmessage(_("Information"), _("The task has been inserted successfully."));

            $opInfo["opcat_id"] = "";
            $opInfo["op_desc"] = "";
            $opInfo["op_duration"] = "";
            $opInfo["op_startdate"] = date("Y-m-d");
            $opInfo["op_starthours"] = "";
            $opInfo["op_enddate"] = "";
            $opInfo["op_endhours"] = "";
        }
    }
}
else {

    $opInfo = array("prj_id" => $prj_id,
                    "pe_id" => $_SESSION["auth_user"]["pe_id"],
                    "opcat_id" => "",
                    "op_desc" => "",
                    "op_duration" => "",
                    "op_startdate" => date("Y-m-d"),
                    "op_starthours" => "",
                    "op_enddate" => "",
                    "op_endhours" => "");
}

$prjInfo = phpaga_projects_getInfo($prj_id);

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    if (isset($prjInfo["prj_billabletype"]) && ($prjInfo["prj_billabletype"] != 10))
        $opInfo["billable_checked"] = true;
    else
        $opInfo["billable_checked"] = false;
}

if (PhPagaError::isError($prjInfo))
    $prjInfo->printMessage();

phpaga_header(array("menuitem" => "core"));

$duration = phpaga_projects_getDuration($prjInfo["prj_id"]);
$select_opcat = phpaga_operations_getCategoryList();
phpaga_arrayAddOption($select_opcat, PHPAGA_OPTION_SELECT);

/* Only the project owner and overall project managers can set an
 * operation's user */
if (!$can_manage)
{
    $peInfo = PPerson::getInfo($_SESSION["auth_user"]["pe_id"]);

    $select_user[$peInfo["pe_id"]] = sprintf("%s (%s)",
                                             $peInfo["pe_person"],
                                             $_SESSION["auth_user"]["usr_login"]);
}
else
{
    $users = PUser::findAsArray(array('persons{pe_id}=>project_members.prj_id=' => $prj_id), 
        array('persons{pe_id}.pe_lastname' => 'asc', 'persons{pe_id}.pe_firstname' => 'asc'));

    foreach ($users as $u)
        $select_user[$u["pe_id"]] = $u["pe_name"]." (".$u["usr_login"].")";
}

$opInfo["op_desc"] = stripslashes($opInfo["op_desc"]);

$tpl = new PSmarty;

$tpl->assign("PRJ_OPERATION", _("Add task"));
$tpl->assign("FORM_ACTION", basename($_SERVER["PHP_SELF"]));

$tpl->assign("is_owner_or_manager", $can_manage);
$tpl->assign("prjInfo", $prjInfo);
$tpl->assign("opInfo", $opInfo);
$tpl->assign("HOURS_SO_FAR", $duration);
$tpl->assign("STATUSMSG", $status);
$tpl->assign("select_person", $select_user);
$tpl->assign("select_opcat", $select_opcat);
$tpl->display("edit_operation.tpl.html");

phpaga_footer();

?>
