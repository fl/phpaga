<?php
/**
 * phpaga
 *
 * Main
 *
 * This is the main file that displays a user's open projects and her last x tasks.
 *
 * @author Florian Lanthaler <florian@phpaga.net>
 * @version $Id$
 *
 * Copyright (c) 2002, Florian Lanthaler <florian@phpaga.net>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *
 *    * Neither the name of Florian Lanthaler nor the names of his
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

include_once('./config.php');

phpaga_header();

$tpl = new PSmarty;

$tpl->assign ('PE_ID', PSession::getPersonId());
$tpl->assign('curryear', date('Y'));
$tpl->assign('currmonth', (int)date('m'));
$tpl->assign('prjstat_sel', strlen(PHPAGA_MAIN_SHOW_PRJSTAT) ? PHPAGA_MAIN_SHOW_PRJSTAT : "''");
$tpl->assign('prjstats', PProjectStatus::getGridSelectionStr());
$tpl->assign('prjcats', PProjectCategory::getGridSelectionStr());
$tpl->assign("perm_projects", PUser::hasPerm(PHPAGA_PERM_MANAGE_PROJECTS));
//$tpl->assign('perm_view_invoices',phpaga_user_hasperm(PHPAGA_PERM_VIEW_INVOICES));
$tpl->display('main.tpl.html');

phpaga_footer();

?>
