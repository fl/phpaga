#!/usr/bin/zsh
#
# A dirty hack to quickly create phpaga packages
#
# - Export sources
# - Generate html and pdf documentation and clean up docs directory
# - Bundle 3rd-party libraries under ext/
# - Make tar.gz and .tar.bz2 packages
#
# @author Florian Lanthaler <florian@phpaga.net>
#
# This file is under no copyright. Use it as you see fit. I will pity you.


RELEASE=${1:-`cat ../VERSION`}
PTMPDIR=/tmp/phpaga-release/

if ! [ -d $PTMPDIR ]; then
  mkdir $PTMPDIR
fi

if ! [ -d "$PTMPDIR"/phpaga-libs ]; then
  cp -r ../../phpaga-libs $PTMPDIR
fi

if [ -d "$PTMPDIR"phpaga-$RELEASE ]; then
  echo "Remove package directory "$PTMPDIR"phpaga-$RELEASE first!"
  exit
fi

echo "Cloning repository..."
hg clone http://bitbucket.org/fl/phpaga "$PTMPDIR"phpaga-$RELEASE.clone
cd "$PTMPDIR"phpaga-$RELEASE.clone

echo "Creating hg archive..."
hg archive "$PTMPDIR"phpaga-$RELEASE

echo "Preparing files..."

rm "$PTMPDIR"phpaga-$RELEASE/VERSION
echo $RELEASE > "$PTMPDIR"phpaga-$RELEASE/VERSION

cd "$PTMPDIR"phpaga-$RELEASE

rm -f .hg_archival.txt
rm -f .hgtags
rm -f ./tools/make-package.sh

cd ./docs
sed -i "s/^release = .*$/release = '${RELEASE}'/" conf.py
mkdir pdf
make html
make latex
cd _build/latex
make all-pdf
mv phpaga.pdf ../../pdf/
cd ../../../

mv -f ./docs/_build/html ./docs/html
rm -rf ./docs/_static ./docs/_build ./docs/_templates ./docs/Makefile ./docs/conf.py

mkdir templates_c
chmod 0777 templates_c
chmod 0777 files

cd ext
unzip -q ../../phpaga-libs/tcpdf_5_9_038.zip
unzip -q ../../phpaga-libs/flourish_r1041.zip

tar xzf ../../phpaga-libs/Smarty-3.0.6.tar.gz
mv Smarty-3.0.6 smarty

cd ..
cp etc/config.local.php_sample etc/config.local.php

cd $PTMPDIR
echo "Creating packages..."
tar -cz phpaga-$RELEASE -f phpaga-"$RELEASE".tar.gz
tar -cj phpaga-$RELEASE -f phpaga-"$RELEASE".tar.bz2
zip -q -r phpaga-"$RELEASE".zip phpaga-$RELEASE

